# 1. cUrl的官方easy接口
********************


_常用：_



## 1.1 CURL *curl_easy_init(void);

***

翻：

    curl_easy_init()是用于分配、建立和初始化一个简单句柄并返回的外部接口。如果出现错误，将会返回NULL。

原：
    curl_easy_init() is the external interface to alloc, setup and init an easy handle that is returned. If anything goes wrong, NULL is returned.



## 1.2 CURLcode curl_easy_setopt(CURL *curl, CURLoption option, ...);

***

	This is a fucking API here.
	在这里，我们说明下这个函数的常用参数和使用。
	第一个参数不用多说，就是一个curl实例句柄。重点在option参数和可变参数列表
	option参数是一个枚举，对常用的可变参数进行说明，如下：

        详细请查看opt.md文件。

## 1.3 CURLcode curl_easy_perform(CURL *curl);

***

翻：
	
    curl_easy_perform()是用于执行预先设置的阻塞传输外部接口。
原：
	
    curl_easy_perform() is the external interface that performs a blocking
    transfer as previously setup.



## 1.4 void curl_easy_cleanup(CURL *curl);

***
翻：
	
    curl_easy_cleanup()是用于清除释放传入句柄的外部接口。
原：
	
    curl_easy_cleanup() is the external interface to cleaning/freeing the given
    easy handle.



_不常用：_



## 1.5 CURLcode curl_easy_getinfo(CURL *curl, CURLINFO info, ...);

***
翻：
    描述

    使用这个函数从curl会话中请求内部信息。第三个参数char型指针的指针，也就是二级指针，
    内容由内部分配和管理。只有函数返回CURLE_OK时，第三个参数的值才能有用。这个函数要在
    执行传输之后调用，在传输完成前的结果都将是未定义的（是指第三个参数对应的内存是不能正常
    访问的）。（CURLINFO是个枚举，请看info.md）

原：
    DESCRIPTION
    

    Request internal information from the curl session with this function.  The
    third argument MUST be a pointer to a long, a pointer to a charor a
    pointer to a double (as the documentation describes elsewhere).  The data
    pointed to will be filled in accordingly and can be relied upon only if the
    function returns CURLE_OK.  This function is intended to get used *AFTER* a
    performed transfer, all results from this function are undefined until the
    transfer is completed.



## 1.6 CURL *curl_easy_duphandle(CURL *curl);

***

翻：
    描述

    创建一个新的和传入的句柄具有相同选项设置的curl会话句柄。复制一个句柄只能克隆数据和
    选项，内部状态信息和持久连接不能被转移。在多线程应用中是有用的，当你执行curl_easy_duphandle()
    为每个线程创建句柄，可以避免一系列设置参数函数的调用（curl_easy_setopt()）。

原：
    DESCRIPTION

    Creates a new curl session handle with the same options set for the handle
    passed in. Duplicating a handle could only be a matter of cloning data and
    options, internal state info and things like persistent connections cannot
    be transferred. It is useful in multithreaded applications when you can run
    curl_easy_duphandle() for each new thread to avoid a series of identical
    curl_easy_setopt() invokes in every thread.



## 1.7 void curl_easy_reset(CURL *curl);
***

翻：
    描述

    将curl句柄重新初始化为默认的值。这将返回句柄的状态与刚创建时相同。
    保留:活动连接，会话ID缓存，DNS缓存和cookies。

原：
    DESCRIPTION
    
    Re-initializes a CURL handle to the default values. This puts back the
    handle to the same state as it was in when it was just created.
    
    It does keep: live connections, the Session ID cache, the DNS cache and the
    cookies.



## 1.8 CURLcode curl_easy_recv(CURL *curl, void *buffer, size_t buflen, size_t *n);

***
翻：
    描述

    从已连接的socket中接收数据。在设置了CURLOPT_CONNECT_ONLY选项，并调用了
    curl_easy_perform()函数之后使用。

原：
    DESCRIPTION

    Receives data from the connected socket. Use after successful
    curl_easy_perform() with CURLOPT_CONNECT_ONLY option.



## 1.9 CURLcode curl_easy_send(CURL *curl, const void *buffer, size_t buflen, size_t *n);

***
翻：
    描述

    发送数据基于已连接的socekt。在设置了CURLOPT_CONNECT_ONLY选项，并调用了
    curl_easy_perform()函数之后使用。

原：
    DESCRIPTION

    Sends data over the connected socket. Use after successful
    curl_easy_perform() with CURLOPT_CONNECT_ONLY option.



## 1.10 CURLcode curl_easy_upkeep(CURL *curl);

***
翻：
    描述

    对给定的会话句柄执行连接维护。

原：
    DESCRIPTION

    Performs connection upkeep for the given session handle.