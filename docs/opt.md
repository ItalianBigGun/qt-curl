CURLINFO_ACTIVESOCKET(3)                                                                 curl_easy_getinfo options                                                                 CURLINFO_ACTIVESOCKET(3)



NAME
~~~
       CURLINFO_ACTIVESOCKET - get the active socket

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_ACTIVESOCKET,
                                  curl_socket_t *socket);

~~~
DESCRIPTION
~~~
       Pass  a  pointer to a curl_socket_t to receive the most recently active socket used for the transfer connection by this curl session. If the socket is no longer valid, CURL_SOCKET_BAD is returned.
       When you are finished working with the socket, you must call curl_easy_cleanup(3) as usual on the easy handle and let libcurl close the socket and cleanup other resources associated with the  handle. This option returns the active socket only after the transfer is complete, and is typically used in combination with CURLOPT_CONNECT_ONLY(3), which skips the transfer phase.

       CURLINFO_ACTIVESOCKET(3) was added as a replacement for CURLINFO_LASTSOCKET(3) since that one isn't working on all platforms.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_socket_t sockfd;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Do not do the transfer - only connect to host */
         curl_easy_setopt(curl, CURLOPT_CONNECT_ONLY, 1L);
         res = curl_easy_perform(curl);

         /* Extract the socket from the curl handle */
         res = curl_easy_getinfo(curl, CURLINFO_ACTIVESOCKET, &sockfd);

         if(res != CURLE_OK) {
           printf("Error: %s\n", curl_easy_strerror(res));
           return 1;
         }
       }

~~~
AVAILABILITY
~~~
       Added in 7.45.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_LASTSOCKET(3),



libcurl 7.44.0                                                                                  12 Sep 2015                                                                        CURLINFO_ACTIVESOCKET(3)
CURLINFO_APPCONNECT_TIME(3)                                                              curl_easy_getinfo options                                                              CURLINFO_APPCONNECT_TIME(3)



NAME
~~~
       CURLINFO_APPCONNECT_TIME - get the time until the SSL/SSH handshake is completed

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_APPCONNECT_TIME, double *timep);

~~~
DESCRIPTION
~~~
       Pass  a  pointer to a double to receive the time, in seconds, it took from the start until the SSL/SSH connect/handshake to the remote host was completed.  This time is most often very near to the
       CURLINFO_PRETRANSFER_TIME(3) time, except for cases such as HTTP pipelining where the pretransfer time can be delayed due to waits in line for the pipeline and more.

       When a redirect is followed, the time from each request is added together.

       See also the TIMES overview in the curl_easy_getinfo(3) man page.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         double connect;
         curl_easy_setopt(curl, CURLOPT_URL, url);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_APPCONNECT_TIME, &connect);
           if(CURLE_OK == res) {
             printf("Time: %.1f", connect);
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_APPCONNECT_TIME_T(3)



libcurl 7.44.0                                                                                  28 Aug 2015                                                                     CURLINFO_APPCONNECT_TIME(3)
CURLINFO_APPCONNECT_TIME_T(3)                                                            curl_easy_getinfo options                                                            CURLINFO_APPCONNECT_TIME_T(3)



NAME
~~~
       CURLINFO_APPCONNECT_TIME_T - get the time until the SSL/SSH handshake is completed

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_APPCONNECT_TIME_T, curl_off_t *timep);

~~~
DESCRIPTION
~~~
       Pass a pointer to a curl_off_t to receive the time, in microseconds, it took from the start until the SSL/SSH connect/handshake to the remote host was completed.  This time is most often very near
       to the CURLINFO_PRETRANSFER_TIME_T(3) time, except for cases such as HTTP pipelining where the pretransfer time can be delayed due to waits in line for the pipeline and more.

       When a redirect is followed, the time from each request is added together.

       See also the TIMES overview in the curl_easy_getinfo(3) man page.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_off_t connect;
         curl_easy_setopt(curl, CURLOPT_URL, url);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_APPCONNECT_TIME_T, &connect);
           if(CURLE_OK == res) {
             printf("Time: %" CURL_FORMAT_CURL_OFF_T ".%06ld", connect / 1000000,
                    (long)(connect % 1000000));
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.61.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_APPCONNECT_TIME(3)



libcurl 7.61.0                                                                                  28 Apr 2018                                                                   CURLINFO_APPCONNECT_TIME_T(3)
CURLINFO_CERTINFO(3)                                                                     curl_easy_getinfo options                                                                     CURLINFO_CERTINFO(3)



NAME
~~~
       CURLINFO_CERTINFO - get the TLS certificate chain

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_CERTINFO,
                                  struct curl_certinfo **chainp);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to a 'struct curl_certinfo *' and you'll get it set to point to a struct that holds a number of linked lists with info about the certificate chain, assuming you had CURLOPT_CERTINFO(3) enabled when the request was made. The struct reports how many certs it found and then you can extract info for each of those certs by following the linked lists. The info  chain  is  provided in a series of data in the format "name:content" where the content is for the specific named data. See also the certinfo.c example.

~~~
PROTOCOLS
~~~
       All TLS-based

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://www.example.com/");

         /* connect to any HTTPS site, trusted or not */
         curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
         curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

         curl_easy_setopt(curl, CURLOPT_CERTINFO, 1L);

         res = curl_easy_perform(curl);

         if (!res) {
           struct curl_certinfo *ci;
           res = curl_easy_getinfo(curl, CURLINFO_CERTINFO, &ci);

           if (!res) {
             printf("%d certs!\n", ci->num_of_certs);

             for(i = 0; i < ci->num_of_certs; i++) {
               struct curl_slist *slist;

               for(slist = ci->certinfo[i]; slist; slist = slist->next)
                 printf("%s\n", slist->data);
             }
           }
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       This option is only working in libcurl built with OpenSSL, NSS, Schannel, GSKit or Secure Transport support. Schannel support added in 7.50.0. Secure Transport support added in 7.79.0.

       Added in 7.19.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                  12 Sep 2015                                                                            CURLINFO_CERTINFO(3)
CURLINFO_CONDITION_UNMET(3)                                                              curl_easy_getinfo options                                                              CURLINFO_CONDITION_UNMET(3)



NAME
~~~
       CURLINFO_CONDITION_UNMET - get info on unmet time conditional or 304 HTTP response.

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_CONDITION_UNMET, long *unmet);

~~~
DESCRIPTION
~~~
       Pass  a  pointer to a long to receive the number 1 if the condition provided in the previous request didn't match (see CURLOPT_TIMECONDITION(3)). Alas, if this returns a 1 you know that the reason
       you didn't get data in return is because it didn't fulfill the condition. The long this argument points to will get a zero stored if the condition instead was met. This can also return  1  if  the
       server responded with a 304 HTTP status code, for example after sending a custom "If-Match-*" header.

~~~
PROTOCOLS
~~~
       HTTP and some

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* January 1, 2020 is 1577833200 */
         curl_easy_setopt(curl, CURLOPT_TIMEVALUE, 1577833200L);

         /* If-Modified-Since the above time stamp */
         curl_easy_setopt(curl, CURLOPT_TIMECONDITION, CURL_TIMECOND_IFMODSINCE);

         /* Perform the request */
         res = curl_easy_perform(curl);

         if(!res) {
           /* check the time condition */
           long unmet;
           res = curl_easy_getinfo(curl, CURLINFO_CONDITION_UNMET, &unmet);
           if(!res) {
             printf("The time condition was %sfulfilled\n", unmet?"NOT":"");
           }
         }
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.4

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                   1 Sep 2015                                                                     CURLINFO_CONDITION_UNMET(3)
CURLINFO_CONNECT_TIME(3)                                                                 curl_easy_getinfo options                                                                 CURLINFO_CONNECT_TIME(3)



NAME
~~~
       CURLINFO_CONNECT_TIME - get the time until connect

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_CONNECT_TIME, double *timep);

~~~
DESCRIPTION
~~~
       Pass a pointer to a double to receive the total time in seconds from the start until the connection to the remote host (or proxy) was completed.

       When a redirect is followed, the time from each request is added together.

       See also the TIMES overview in the curl_easy_getinfo(3) man page.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         double connect;
         curl_easy_setopt(curl, CURLOPT_URL, url);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_CONNECT_TIME, &connect);
           if(CURLE_OK == res) {
             printf("Time: %.1f", connect);
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.4.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_CONNECT_TIME_T(3)



libcurl 7.44.0                                                                                  28 Aug 2015                                                                        CURLINFO_CONNECT_TIME(3)
CURLINFO_CONNECT_TIME_T(3)                                                               curl_easy_getinfo options                                                               CURLINFO_CONNECT_TIME_T(3)



NAME
~~~
       CURLINFO_CONNECT_TIME_T - get the time until connect

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_CONNECT_TIME_T, curl_off_t *timep);

~~~
DESCRIPTION
~~~
       Pass a pointer to a curl_off_t to receive the total time in microseconds from the start until the connection to the remote host (or proxy) was completed.

       When a redirect is followed, the time from each request is added together.

       See also the TIMES overview in the curl_easy_getinfo(3) man page.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_off_t connect;
         curl_easy_setopt(curl, CURLOPT_URL, url);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_CONNECT_TIME_T, &connect);
           if(CURLE_OK == res) {
             printf("Time: %" CURL_FORMAT_CURL_OFF_T ".%06ld", connect / 1000000,
                    (long)(connect % 1000000));
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.61.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_CONNECT_TIME(3)



libcurl 7.61.0                                                                                  28 Apr 2018                                                                      CURLINFO_CONNECT_TIME_T(3)
CURLINFO_CONTENT_LENGTH_DOWNLOAD(3)                                                      curl_easy_getinfo options                                                      CURLINFO_CONTENT_LENGTH_DOWNLOAD(3)



NAME
~~~
       CURLINFO_CONTENT_LENGTH_DOWNLOAD - get content-length of download

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_CONTENT_LENGTH_DOWNLOAD,
                                  double *content_length);

~~~
DESCRIPTION
~~~
       Pass a pointer to a double to receive the content-length of the download. This is the value read from the Content-Length: field. Since 7.19.4, this returns -1 if the size isn't known.

       CURLINFO_CONTENT_LENGTH_DOWNLOAD_T(3) is a newer replacement that returns a more sensible variable type.

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Perform the request */
         res = curl_easy_perform(curl);

         if(!res) {
           /* check the size */
           double cl;
           res = curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &cl);
           if(!res) {
             printf("Size: %.0f\n", cl);
           }
         }
       }

~~~
AVAILABILITY
~~~
       Added in 7.6.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_CONTENT_LENGTH_UPLOAD(3),



libcurl 7.44.0                                                                                   1 Sep 2015                                                             CURLINFO_CONTENT_LENGTH_DOWNLOAD(3)
CURLINFO_CONTENT_LENGTH_DOWNLOAD_T(3)                                                    curl_easy_getinfo options                                                    CURLINFO_CONTENT_LENGTH_DOWNLOAD_T(3)



NAME
~~~
       CURLINFO_CONTENT_LENGTH_DOWNLOAD_T - get content-length of download

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_CONTENT_LENGTH_DOWNLOAD_T,
                                  curl_off_t *content_length);

~~~
DESCRIPTION
~~~
       Pass a pointer to a curl_off_t to receive the content-length of the download. This is the value read from the Content-Length: field. Stores -1 if the size isn't known.

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Perform the request */
         res = curl_easy_perform(curl);

         if(!res) {
           /* check the size */
           curl_off_t cl;
           res = curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD_T, &cl);
           if(!res) {
             printf("Download size: %" CURL_FORMAT_CURL_OFF_T "\n", cl);
           }
         }
       }

~~~
AVAILABILITY
~~~
       Added in 7.55.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_CONTENT_LENGTH_UPLOAD_T(3),



libcurl 7.55.0                                                                                  25 May 2017                                                           CURLINFO_CONTENT_LENGTH_DOWNLOAD_T(3)
CURLINFO_CONTENT_LENGTH_UPLOAD(3)                                                        curl_easy_getinfo options                                                        CURLINFO_CONTENT_LENGTH_UPLOAD(3)



NAME
~~~
       CURLINFO_CONTENT_LENGTH_UPLOAD - get the specified size of the upload

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_CONTENT_LENGTH_UPLOAD,
                                  double *content_length);

~~~
DESCRIPTION
~~~
       Pass a pointer to a double to receive the specified size of the upload.  Since 7.19.4, this returns -1 if the size isn't known.

       CURLINFO_CONTENT_LENGTH_UPLOAD_T(3) is a newer replacement that returns a more sensible variable type.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Perform the upload */
         res = curl_easy_perform(curl);

         if(!res) {
           /* check the size */
           double cl;
           res = curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_UPLOAD, &cl);
           if(!res) {
             printf("Size: %.0f\n", cl);
           }
         }
       }

~~~
AVAILABILITY
~~~
       Added in 7.6.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_CONTENT_LENGTH_DOWNLOAD_T(3),



libcurl 7.44.0                                                                                   1 Sep 2015                                                               CURLINFO_CONTENT_LENGTH_UPLOAD(3)
CURLINFO_CONTENT_LENGTH_UPLOAD_T(3)                                                      curl_easy_getinfo options                                                      CURLINFO_CONTENT_LENGTH_UPLOAD_T(3)



NAME
~~~
       CURLINFO_CONTENT_LENGTH_UPLOAD_T - get the specified size of the upload

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_CONTENT_LENGTH_UPLOAD_T,
                                  curl_off_t *content_length);

~~~
DESCRIPTION
~~~
       Pass a pointer to a curl_off_t to receive the specified size of the upload. Stores -1 if the size isn't known.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Perform the upload */
         res = curl_easy_perform(curl);

         if(!res) {
           /* check the size */
           curl_off_t cl;
           res = curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_UPLOAD_T, &cl);
           if(!res) {
             printf("Upload size: %" CURL_FORMAT_CURL_OFF_T "\n", cl);
           }
         }
       }

~~~
AVAILABILITY
~~~
       Added in 7.55.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_CONTENT_LENGTH_DOWNLOAD_T(3),



libcurl 7.55.0                                                                                  25 May 2017                                                             CURLINFO_CONTENT_LENGTH_UPLOAD_T(3)
CURLINFO_CONTENT_TYPE(3)                                                                 curl_easy_getinfo options                                                                 CURLINFO_CONTENT_TYPE(3)



NAME
~~~
       CURLINFO_CONTENT_TYPE - get Content-Type

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_CONTENT_TYPE, char **ct);

~~~
DESCRIPTION
~~~
       Pass  a pointer to a char pointer to receive the content-type of the downloaded object. This is the value read from the Content-Type: field. If you get NULL, it means that the server didn't send a
       valid Content-Type header or that the protocol used doesn't support this.

       The ct pointer will be NULL or pointing to private memory you MUST NOT free it - it gets freed when you call curl_easy_cleanup(3) on the corresponding CURL handle.

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         res = curl_easy_perform(curl);

         if(!res) {
           /* extract the content-type */
           char *ct = NULL;
           res = curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &ct);
           if(!res && ct) {
             printf("Content-Type: %s\n", ct);
           }
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.9.4

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                   1 Sep 2015                                                                        CURLINFO_CONTENT_TYPE(3)
CURLINFO_COOKIELIST(3)                                                                   curl_easy_getinfo options                                                                   CURLINFO_COOKIELIST(3)



NAME
~~~
       CURLINFO_COOKIELIST - get all known cookies

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_COOKIELIST,
                                  struct curl_slist **cookies);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to a 'struct curl_slist *' to receive a linked-list of all cookies curl knows (expired ones, too). Don't forget to call curl_slist_free_all(3) on the list after it has been used.
       If there are no cookies (cookies for the handle have not been enabled or simply none have been received) 'struct curl_slist *' will be set to point to NULL.

       Since 7.43.0 cookies that were imported in the Set-Cookie format without a domain name are not exported by this option.

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* enable the cookie engine */
         curl_easy_setopt(curl, CURLOPT_COOKIEFILE, "");

         res = curl_easy_perform(curl);

         if(!res) {
           /* extract all known cookies */
           struct curl_slist *cookies = NULL;
           res = curl_easy_getinfo(curl, CURLINFO_COOKIELIST, &cookies);
           if(!res && cookies) {
             /* a linked list of cookies in cookie file format */
             struct curl_slist *each = cookies;
             while(each) {
               printf("%s\n", each->data);
               each = each->next;
             }
             /* we must free these cookies when we're done */
             curl_slist_free_all(cookies);
           }
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.14.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLOPT_COOKIELIST(3),



libcurl 7.44.0                                                                                   1 Sep 2015                                                                          CURLINFO_COOKIELIST(3)
CURLINFO_EFFECTIVE_METHOD(3)                                                             curl_easy_getinfo options                                                             CURLINFO_EFFECTIVE_METHOD(3)



NAME
~~~
       CURLINFO_EFFECTIVE_METHOD - get the last used HTTP method

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_EFFECTIVE_METHOD,
                                  char **methodp);

~~~
DESCRIPTION
~~~
       Pass in a pointer to a char pointer and get the last used effective HTTP method.

       In cases when you've asked libcurl to follow redirects, the method may very well not be the same method the first request would use.

       The methodp pointer will be NULL or pointing to private memory you MUST NOT free - it gets freed when you call curl_easy_cleanup(3) on the corresponding CURL handle.

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "data");
         curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           char *method = NULL;
           curl_easy_getinfo(curl, CURLINFO_EFFECTIVE_METHOD, &method);
           if(method)
             printf("Redirected to method: %s\n", method);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.72.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.72.0                                                                                  28 Aug 2015                                                                    CURLINFO_EFFECTIVE_METHOD(3)
CURLINFO_EFFECTIVE_URL(3)                                                                curl_easy_getinfo options                                                                CURLINFO_EFFECTIVE_URL(3)



NAME
~~~
       CURLINFO_EFFECTIVE_URL - get the last used URL

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_EFFECTIVE_URL, char **urlp);

~~~
DESCRIPTION
~~~
       Pass in a pointer to a char pointer and get the last used effective URL.

       In cases when you've asked libcurl to follow redirects, it may very well not be the same value you set with CURLOPT_URL(3).

       The urlp pointer will be NULL or pointing to private memory you MUST NOT free - it gets freed when you call curl_easy_cleanup(3) on the corresponding CURL handle.

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           char *url = NULL;
           curl_easy_getinfo(curl, CURLINFO_EFFECTIVE_URL, &url);
           if(url)
             printf("Redirect to: %s\n", url);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.4

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                  28 Aug 2015                                                                       CURLINFO_EFFECTIVE_URL(3)
CURLINFO_FILETIME(3)                                                                     curl_easy_getinfo options                                                                     CURLINFO_FILETIME(3)



NAME
~~~
       CURLINFO_FILETIME - get the remote time of the retrieved document

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_FILETIME, long *timep);

~~~
DESCRIPTION
~~~
       Pass a pointer to a long to receive the remote time of the retrieved document (in number of seconds since 1 jan 1970 in the GMT/UTC time zone). If you get -1, it can be because of many reasons (it
       might be unknown, the server might hide it or the server doesn't support the command that tells document time etc) and the time of the document is unknown.

       You must tell libcurl to collect this information before the transfer is made, by using the CURLOPT_FILETIME(3) option to curl_easy_setopt(3) or you will unconditionally get a -1 back.

       Consider using CURLINFO_FILETIME_T(3) to be able to extract dates beyond the year 2038 on systems using 32 bit longs.

~~~
PROTOCOLS
~~~
       HTTP(S), FTP(S), SFTP

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, url);
         /* Ask for filetime */
         curl_easy_setopt(curl, CURLOPT_FILETIME, 1L);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_FILETIME, &filetime);
           if((CURLE_OK == res) && (filetime >= 0)) {
             time_t file_time = (time_t)filetime;
             printf("filetime %s: %s", filename, ctime(&file_time));
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.5

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                  28 Aug 2015                                                                            CURLINFO_FILETIME(3)
CURLINFO_FILETIME(3)                                                                     curl_easy_getinfo options                                                                     CURLINFO_FILETIME(3)



NAME
~~~
       CURLINFO_FILETIME_T - get the remote time of the retrieved document

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_FILETIME_T, curl_off_t *timep);

~~~
DESCRIPTION
~~~
       Pass  a pointer to a curl_off_t to receive the remote time of the retrieved document (in number of seconds since 1 jan 1970 in the GMT/UTC time zone). If you get -1, it can be because of many reasons (it might be unknown, the server might hide it or the server doesn't support the command that tells document time etc) and the time of the document is unknown.

       You must ask libcurl to collect this information before the transfer is made, by using the CURLOPT_FILETIME(3) option to curl_easy_setopt(3) or you will unconditionally get a -1 back.

       This option is an alternative to CURLINFO_FILETIME(3) to allow systems with 32 bit long variables to extract dates outside of the 32bit timestamp range.

~~~
PROTOCOLS
~~~
       HTTP(S), FTP(S), SFTP

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, url);
         /* Ask for filetime */
         curl_easy_setopt(curl, CURLOPT_FILETIME, 1L);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           curl_off_t filetime;
           res = curl_easy_getinfo(curl, CURLINFO_FILETIME_T, &filetime);
           if((CURLE_OK == res) && (filetime >= 0)) {
             time_t file_time = (time_t)filetime;
             printf("filetime %s: %s", filename, ctime(&file_time));
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.59.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.59.0                                                                                  25 Jan 2018                                                                            CURLINFO_FILETIME(3)
CURLINFO_FTP_ENTRY_PATH(3)                                                               curl_easy_getinfo options                                                               CURLINFO_FTP_ENTRY_PATH(3)



NAME
~~~
       CURLINFO_FTP_ENTRY_PATH - get entry path in FTP server

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_FTP_ENTRY_PATH, char **path);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to a char pointer to receive a pointer to a string holding the path of the entry path. That is the initial path libcurl ended up in when logging on to the remote FTP server. This
       stores a NULL as pointer if something is wrong.

       The path pointer will be NULL or pointing to private memory you MUST NOT free - it gets freed when you call curl_easy_cleanup(3) on the corresponding CURL handle.

~~~
PROTOCOLS
~~~
       FTP(S) and SFTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com");

         res = curl_easy_perform(curl);

         if(!res) {
           /* extract the entry path */
           char *ep = NULL;
           res = curl_easy_getinfo(curl, CURLINFO_FTP_ENTRY_PATH, &ep);
           if(!res && ep) {
             printf("Entry path was: %s\n", ep);
           }
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.15.4. Works for SFTP since 7.21.4

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                  12 Sep 2015                                                                      CURLINFO_FTP_ENTRY_PATH(3)
CURLINFO_HEADER_SIZE(3)                                                                  curl_easy_getinfo options                                                                  CURLINFO_HEADER_SIZE(3)



NAME
~~~
       CURLINFO_HEADER_SIZE - get size of retrieved headers

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_HEADER_SIZE, long *sizep);

~~~
DESCRIPTION
~~~
       Pass a pointer to a long to receive the total size of all the headers received. Measured in number of bytes.

       The total includes the size of any received headers suppressed by CURLOPT_SUPPRESS_CONNECT_HEADERS(3).

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           long size;
           res = curl_easy_getinfo(curl, CURLINFO_HEADER_SIZE, &size);
           if(!res)
             printf("Header size: %ld bytes\n", size);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.4.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_REQUEST_SIZE(3), CURLINFO_SIZE_DOWNLOAD(3),



libcurl 7.44.0                                                                                   1 Sep 2015                                                                         CURLINFO_HEADER_SIZE(3)
CURLINFO_HTTPAUTH_AVAIL(3)                                                               curl_easy_getinfo options                                                               CURLINFO_HTTPAUTH_AVAIL(3)



NAME
~~~
       CURLINFO_HTTPAUTH_AVAIL - get available HTTP authentication methods

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_HTTPAUTH_AVAIL, long *authp);

~~~
DESCRIPTION
~~~
       Pass  a pointer to a long to receive a bitmask indicating the authentication method(s) available according to the previous response. The meaning of the bits is explained in the CURLOPT_HTTPAUTH(3)
       option for curl_easy_setopt(3).

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         res = curl_easy_perform(curl);

         if(!res) {
           /* extract the available authentication types */
           long auth;
           res = curl_easy_getinfo(curl, CURLINFO_HTTPAUTH_AVAIL, &auth);
           if(!res) {
             if(!auth)
               printf("No auth available, perhaps no 401?\n");
             else {
               printf("%s%s%s%s\n",
                      auth & CURLAUTH_BASIC ? "Basic ":"",
                      auth & CURLAUTH_DIGEST ? "Digest ":"",
                      auth & CURLAUTH_NEGOTIATE ? "Negotiate ":"",
                      auth % CURLAUTH_NTLM ? "NTLM ":"");
             }
           }
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added RFC2617 in 7.10.8 Added RFC7616 in 7.57.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                  12 Sep 2015                                                                      CURLINFO_HTTPAUTH_AVAIL(3)
CURLINFO_HTTP_CONNECTCODE(3)                                                             curl_easy_getinfo options                                                             CURLINFO_HTTP_CONNECTCODE(3)



NAME
~~~
       CURLINFO_HTTP_CONNECTCODE - get the CONNECT response code

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_HTTP_CONNECTCODE, long *p);

~~~
DESCRIPTION
~~~
       Pass a pointer to a long to receive the last received HTTP proxy response code to a CONNECT request. The returned value will be zero if no such response code was available.

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* typically CONNECT is used to do HTTPS over HTTP proxies */
         curl_easy_setopt(curl, CURLOPT_PROXY, "http://127.0.0.1");
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           long code;
           res = curl_easy_getinfo(curl, CURLINFO_HTTP_CONNECTCODE, &code);
           if(!res && code)
             printf("The CONNECT response code: %03ld\n", code);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.10.7

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLINFO_RESPONSE_CODE(3), curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                  28 Aug 2015                                                                    CURLINFO_HTTP_CONNECTCODE(3)
CURLINFO_HTTP_VERSION(3)                                                                 curl_easy_getinfo options                                                                 CURLINFO_HTTP_VERSION(3)



NAME
~~~
       CURLINFO_HTTP_VERSION - get the http version used in the connection

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_HTTP_VERSION, long *p);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to a long to receive the version used in the last http connection.  The returned value will be CURL_HTTP_VERSION_1_0, CURL_HTTP_VERSION_1_1, CURL_HTTP_VERSION_2_0, CURL_HTTP_VERSION_3 or 0 if the version can't be determined.

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           long http_version;
           curl_easy_getinfo(curl, CURLINFO_HTTP_VERSION, &http_version);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.50.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLINFO_RESPONSE_CODE(3), curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.50.0                                                                                  11 May 2016                                                                        CURLINFO_HTTP_VERSION(3)
CURLINFO_LASTSOCKET(3)                                                                   curl_easy_getinfo options                                                                   CURLINFO_LASTSOCKET(3)



NAME
~~~
       CURLINFO_LASTSOCKET - get the last socket used

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_LASTSOCKET, long *socket);

~~~
DESCRIPTION
~~~
       Deprecated since 7.45.0. Use CURLINFO_ACTIVESOCKET(3) instead.

       Pass  a  pointer  to  a  long  to  receive  the  last  socket  used  by  this curl session. If the socket is no longer valid, -1 is returned. When you finish working with the socket, you must call
       curl_easy_cleanup() as usual and let libcurl close the socket and cleanup other resources associated with the handle. This is typically used in combination with CURLOPT_CONNECT_ONLY(3).

       NOTE: this API is deprecated since it is not working on win64 where the SOCKET type is 64 bits large while its 'long' is 32 bits. Use the CURLINFO_ACTIVESOCKET(3) instead, if possible.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         long sockfd; /* doesn't work on win64! */
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Do not do the transfer - only connect to host */
         curl_easy_setopt(curl, CURLOPT_CONNECT_ONLY, 1L);
         res = curl_easy_perform(curl);

         /* Extract the socket from the curl handle */
         res = curl_easy_getinfo(curl, CURLINFO_LASTSOCKET, &sockfd);

         if(res != CURLE_OK) {
           printf("Error: %s\n", curl_easy_strerror(res));
           return 1;
         }
       }

~~~
AVAILABILITY
~~~
       Added in 7.15.2

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_ACTIVESOCKET(3),



libcurl 7.44.0                                                                                  12 Sep 2015                                                                          CURLINFO_LASTSOCKET(3)
CURLINFO_LOCAL_IP(3)                                                                     curl_easy_getinfo options                                                                     CURLINFO_LOCAL_IP(3)



NAME
~~~
       CURLINFO_LOCAL_IP - get local IP address of last connection

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_LOCAL_IP, char **ip);

~~~
DESCRIPTION
~~~
       Pass a pointer to a char pointer to receive the pointer to a null-terminated string holding the IP address of the local end of most recent connection done with this curl handle. This string may be
       IPv6 when that is enabled. Note that you get a pointer to a memory area that will be re-used at next request so you need to copy the string if you want to keep the information.

       The ip pointer will be NULL or pointing to private memory you MUST NOT free - it gets freed when you call curl_easy_cleanup(3) on the corresponding CURL handle.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       {
         char *ip;

         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Perform the request, res will get the return code */
         res = curl_easy_perform(curl);
         /* Check for errors */
         if((res == CURLE_OK) &&
            !curl_easy_getinfo(curl, CURLINFO_LOCAL_IP, &ip) && ip) {
           printf("Local IP: %s\n", ip);
         }

         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.21.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_PRIMARY_IP(3),



libcurl 7.44.0                                                                                  12 Sep 2015                                                                            CURLINFO_LOCAL_IP(3)
CURLINFO_LOCAL_PORT(3)                                                                   curl_easy_getinfo options                                                                   CURLINFO_LOCAL_PORT(3)



NAME
~~~
       CURLINFO_LOCAL_PORT - get the latest local port number

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_LOCAL_PORT, long *portp);

~~~
DESCRIPTION
~~~
       Pass a pointer to a long to receive the local port number of the most recent connection done with this curl handle.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       {
         CURL *curl;
         CURLcode res;

         curl = curl_easy_init();
         if(curl) {
           curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
           res = curl_easy_perform(curl);

           if(CURLE_OK == res) {
             long port;
             res = curl_easy_getinfo(curl, CURLINFO_LOCAL_PORT, &port);

             if(CURLE_OK == res) {
               printf("We used local port: %ld\n", port);
             }
           }
           curl_easy_cleanup(curl);
         }
         return 0;
       }

~~~
AVAILABILITY
~~~
       Added in 7.21.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_PRIMARY_PORT(3), CURLINFO_LOCAL_IP(3),



libcurl 7.44.0                                                                                  12 Sep 2015                                                                          CURLINFO_LOCAL_PORT(3)
CURLINFO_NAMELOOKUP_TIME(3)                                                              curl_easy_getinfo options                                                              CURLINFO_NAMELOOKUP_TIME(3)



NAME
~~~
       CURLINFO_NAMELOOKUP_TIME - get the name lookup time

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_NAMELOOKUP_TIME, double *timep);

~~~
DESCRIPTION
~~~
       Pass a pointer to a double to receive the total time in seconds from the start until the name resolving was completed.

       When a redirect is followed, the time from each request is added together.

       See also the TIMES overview in the curl_easy_getinfo(3) man page.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         double namelookup;
         curl_easy_setopt(curl, CURLOPT_URL, url);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_NAMELOOKUP_TIME, &namelookup);
           if(CURLE_OK == res) {
             printf("Time: %.1f", namelookup);
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.4.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_NAMELOOKUP_TIME_T(3)



libcurl 7.44.0                                                                                  28 Aug 2015                                                                     CURLINFO_NAMELOOKUP_TIME(3)
CURLINFO_NAMELOOKUP_TIME_T(3)                                                            curl_easy_getinfo options                                                            CURLINFO_NAMELOOKUP_TIME_T(3)



NAME
~~~
       CURLINFO_NAMELOOKUP_TIME_T - get the name lookup time in microseconds

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_NAMELOOKUP_TIME_T, curl_off_t *timep);

~~~
DESCRIPTION
~~~
       Pass a pointer to a curl_off_t to receive the total time in microseconds from the start until the name resolving was completed.

       When a redirect is followed, the time from each request is added together.

       See also the TIMES overview in the curl_easy_getinfo(3) man page.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_off_t namelookup;
         curl_easy_setopt(curl, CURLOPT_URL, url);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_NAMELOOKUP_TIME_T, &namelookup);
           if(CURLE_OK == res) {
             printf("Time: %" CURL_FORMAT_CURL_OFF_T ".%06ld", namelookup / 1000000,
                    (long)(namelookup % 1000000));
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.61.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_NAMELOOKUP_TIME(3)



libcurl 7.61.0                                                                                  28 Apr 2018                                                                   CURLINFO_NAMELOOKUP_TIME_T(3)
CURLINFO_NUM_CONNECTS(3)                                                                 curl_easy_getinfo options                                                                 CURLINFO_NUM_CONNECTS(3)



NAME
~~~
       CURLINFO_NUM_CONNECTS - get number of created connections

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_NUM_CONNECTS, long *nump);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to  a  long  to  receive how many new connections libcurl had to create to achieve the previous transfer (only the successful connects are counted).  Combined with CURLINFO_REDIRECT_COUNT(3) you are able to know how many times libcurl successfully reused existing connection(s) or not.  See the connection options of curl_easy_setopt(3) to see how  libcurl  tries  to  make
       persistent connections to save time.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           long connects;
           res = curl_easy_getinfo(curl, CURLINFO_NUM_CONNECTS, &connects);
           if(res)
             printf("It needed %d connects\n", connects);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.12.3

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                  12 Sep 2015                                                                        CURLINFO_NUM_CONNECTS(3)
CURLINFO_OS_ERRNO(3)                                                                     curl_easy_getinfo options                                                                     CURLINFO_OS_ERRNO(3)



NAME
~~~
       CURLINFO_OS_ERRNO - get errno number from last connect failure

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_OS_ERRNO, long *errnop);

~~~
DESCRIPTION
~~~
       Pass a pointer to a long to receive the errno variable from a connect failure.  Note that the value is only set on failure, it is not reset upon a successful operation. The number is OS and system
       specific.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         res = curl_easy_perform(curl);
         if(res != CURLE_OK) {
           long error;
           res = curl_easy_getinfo(curl, CURLINFO_OS_ERRNO, &error);
           if(res && error) {
             printf("Errno: %ld\n", error);
           }
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.12.2

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                   1 Sep 2015                                                                            CURLINFO_OS_ERRNO(3)
CURLINFO_PRETRANSFER_TIME(3)                                                             curl_easy_getinfo options                                                             CURLINFO_PRETRANSFER_TIME(3)



NAME
~~~
       CURLINFO_PRETRANSFER_TIME - get the time until the file transfer start

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_PRETRANSFER_TIME, double *timep);

~~~
DESCRIPTION
~~~
       Pass a pointer to a double to receive the time, in seconds, it took from the start until the file transfer is just about to begin.

       This  time-stamp  includes  all pre-transfer commands and negotiations that are specific to the particular protocol(s) involved. It includes the sending of the protocol- specific protocol instructions that triggers a transfer.

       When a redirect is followed, the time from each request is added together.

       See also the TIMES overview in the curl_easy_getinfo(3) man page.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         double pretransfer;
         curl_easy_setopt(curl, CURLOPT_URL, url);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_PRETRANSFER_TIME, &pretransfer);
           if(CURLE_OK == res) {
             printf("Time: %.1f", pretransfer);
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.4.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_PRETRANSFER_TIME_T(3)



libcurl 7.44.0                                                                                  28 Aug 2015                                                                    CURLINFO_PRETRANSFER_TIME(3)
CURLINFO_PRETRANSFER_TIME_T(3)                                                           curl_easy_getinfo options                                                           CURLINFO_PRETRANSFER_TIME_T(3)



NAME
~~~
       CURLINFO_PRETRANSFER_TIME_T - get the time until the file transfer start

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_PRETRANSFER_TIME_T, curl_off_t *timep);

~~~
DESCRIPTION
~~~
       Pass a pointer to a curl_off_t to receive the time, in microseconds, it took from the start until the file transfer is just about to begin. This includes all pre-transfer commands and negotiations
       that are specific to the particular protocol(s) involved. It does not involve the sending of the protocol- specific request that triggers a transfer.

       When a redirect is followed, the time from each request is added together.

       See also the TIMES overview in the curl_easy_getinfo(3) man page.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_off_t pretransfer;
         curl_easy_setopt(curl, CURLOPT_URL, url);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_PRETRANSFER_TIME_T, &pretransfer);
           if(CURLE_OK == res) {
             printf("Time: %" CURL_FORMAT_CURL_OFF_T ".%06ld", pretransfer / 1000000,
                    (long)(pretransfer % 1000000));
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.61.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_PRETRANSFER_TIME(3)



libcurl 7.61.0                                                                                  28 Apr 2018                                                                  CURLINFO_PRETRANSFER_TIME_T(3)
CURLINFO_PRIMARY_IP(3)                                                                   curl_easy_getinfo options                                                                   CURLINFO_PRIMARY_IP(3)



NAME
~~~
       CURLINFO_PRIMARY_IP - get IP address of last connection

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_PRIMARY_IP, char **ip);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to a char pointer to receive the pointer to a null-terminated string holding the IP address of the most recent connection done with this curl handle. This string may be IPv6 when
       that is enabled. Note that you get a pointer to a memory area that will be re-used at next request so you need to copy the string if you want to keep the information.

       The ip pointer will be NULL or pointing to private memory you MUST NOT free - it gets freed when you call curl_easy_cleanup(3) on the corresponding CURL handle.

~~~
PROTOCOLS
~~~
       All network based ones

~~~
EXAMPLE
~~~
       {
         char *ip;

         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Perform the request, res will get the return code */
         res = curl_easy_perform(curl);
         /* Check for errors */
         if((res == CURLE_OK) &&
            !curl_easy_getinfo(curl, CURLINFO_PRIMARY_IP, &ip) && ip) {
           printf("IP: %s\n", ip);
         }

         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_PRIMARY_PORT(3), CURLINFO_LOCAL_IP(3),



libcurl 7.44.0                                                                                  12 Sep 2015                                                                          CURLINFO_PRIMARY_IP(3)
CURLINFO_PRIMARY_PORT(3)                                                                 curl_easy_getinfo options                                                                 CURLINFO_PRIMARY_PORT(3)



NAME
~~~
       CURLINFO_PRIMARY_PORT - get the latest destination port number

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_PRIMARY_PORT, long *portp);

~~~
DESCRIPTION
~~~
       Pass a pointer to a long to receive the destination port of the most recent connection done with this curl handle.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           long port;
           res = curl_easy_getinfo(curl, CURLINFO_PRIMARY_PORT, &port);
           if(!res)
             printf("Connected to remote port: %ld\n", port);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.21.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                  12 Sep 2015                                                                        CURLINFO_PRIMARY_PORT(3)
CURLINFO_PRIVATE(3)                                                                      curl_easy_getinfo options                                                                      CURLINFO_PRIVATE(3)



NAME
~~~
       CURLINFO_PRIVATE - get the private pointer

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_PRIVATE, char **private);

~~~
DESCRIPTION
~~~
       Pass  a pointer to a char pointer to receive the pointer to the private data associated with the curl handle (set with the CURLOPT_PRIVATE(3)).  Please note that for internal reasons, the value is
       returned as a char pointer, although effectively being a 'void *'.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         void *pointer = 0x2345454;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

         /* set the private pointer */
         curl_easy_setopt(curl, CURLOPT_PRIVATE, pointer);
         ret = curl_easy_perform(curl);

         /* extract the private pointer again */
         ret = curl_easy_getinfo(curl, CURLINFO_PRIVATE, &pointer);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.10.3

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLOPT_PRIVATE(3),



libcurl 7.44.0                                                                                   1 Sep 2015                                                                             CURLINFO_PRIVATE(3)
CURLINFO_PROTOCOL(3)                                                                     curl_easy_getinfo options                                                                     CURLINFO_PROTOCOL(3)



NAME
~~~
       CURLINFO_PROTOCOL - get the protocol used in the connection

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_PROTOCOL, long *p);

~~~
DESCRIPTION
~~~
       Pass a pointer to a long to receive the version used in the last http connection.  The returned value will be exactly one of the CURLPROTO_* values:

       CURLPROTO_DICT, CURLPROTO_FILE, CURLPROTO_FTP, CURLPROTO_FTPS,
       CURLPROTO_GOPHER, CURLPROTO_HTTP, CURLPROTO_HTTPS, CURLPROTO_IMAP,
       CURLPROTO_IMAPS, CURLPROTO_LDAP, CURLPROTO_LDAPS, CURLPROTO_POP3,
       CURLPROTO_POP3S, CURLPROTO_RTMP, CURLPROTO_RTMPE, CURLPROTO_RTMPS,
       CURLPROTO_RTMPT, CURLPROTO_RTMPTE, CURLPROTO_RTMPTS, CURLPROTO_RTSP,
       CURLPROTO_SCP, CURLPROTO_SFTP, CURLPROTO_SMB, CURLPROTO_SMBS, CURLPROTO_SMTP,
       CURLPROTO_SMTPS, CURLPROTO_TELNET, CURLPROTO_TFTP, CURLPROTO_MQTT

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           long protocol;
           curl_easy_getinfo(curl, CURLINFO_PROTOCOL, &protocol);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLINFO_RESPONSE_CODE(3), curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.52.0                                                                                23 November 2016                                                                         CURLINFO_PROTOCOL(3)
CURLINFO_PROXYAUTH_AVAIL(3)                                                              curl_easy_getinfo options                                                              CURLINFO_PROXYAUTH_AVAIL(3)



NAME
~~~
       CURLINFO_PROXYAUTH_AVAIL - get available HTTP proxy authentication methods

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_PROXYAUTH_AVAIL, long *authp);

~~~
DESCRIPTION
~~~
       Pass a pointer to a long to receive a bitmask indicating the authentication method(s) available according to the previous response. The meaning of the bits is explained in the CURLOPT_PROXYAUTH(3)
       option for curl_easy_setopt(3).

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_PROXY, "http://127.0.0.1:80");

         res = curl_easy_perform(curl);

         if(!res) {
           /* extract the available proxy authentication types */
           long auth;
           res = curl_easy_getinfo(curl, CURLINFO_PROXYAUTH_AVAIL, &auth);
           if(!res) {
             if(!auth)
               printf("No proxy auth available, perhaps no 407?\n");
             else {
               printf("%s%s%s%s\n",
                      auth & CURLAUTH_BASIC ? "Basic ":"",
                      auth & CURLAUTH_DIGEST ? "Digest ":"",
                      auth & CURLAUTH_NEGOTIATE ? "Negotiate ":"",
                      auth % CURLAUTH_NTLM ? "NTLM ":"");
             }
           }
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added RFC2617 in 7.10.8 Added RFC7616 in 7.57.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                  12 Sep 2015                                                                     CURLINFO_PROXYAUTH_AVAIL(3)
CURLINFO_PROXY_ERROR(3)                                                                  curl_easy_getinfo options                                                                  CURLINFO_PROXY_ERROR(3)



NAME
~~~
       CURLINFO_PROXY_ERROR - get the detailed (SOCKS) proxy error

~~~
NAME
~~~
       #include <curl/curl.h>

       typedef enum {
         CURLPX_OK,
         CURLPX_BAD_ADDRESS_TYPE,
         CURLPX_BAD_VERSION,
         CURLPX_CLOSED,
         CURLPX_GSSAPI,
         CURLPX_GSSAPI_PERMSG,
         CURLPX_GSSAPI_PROTECTION,
         CURLPX_IDENTD,
         CURLPX_IDENTD_DIFFER,
         CURLPX_LONG_HOSTNAME,
         CURLPX_LONG_PASSWD,
         CURLPX_LONG_USER,
         CURLPX_NO_AUTH,
         CURLPX_RECV_ADDRESS,
         CURLPX_RECV_AUTH,
         CURLPX_RECV_CONNECT,
         CURLPX_RECV_REQACK,
         CURLPX_REPLY_ADDRESS_TYPE_NOT_SUPPORTED,
         CURLPX_REPLY_COMMAND_NOT_SUPPORTED,
         CURLPX_REPLY_CONNECTION_REFUSED,
         CURLPX_REPLY_GENERAL_SERVER_FAILURE,
         CURLPX_REPLY_HOST_UNREACHABLE,
         CURLPX_REPLY_NETWORK_UNREACHABLE,
         CURLPX_REPLY_NOT_ALLOWED,
         CURLPX_REPLY_TTL_EXPIRED,
         CURLPX_REPLY_UNASSIGNED,
         CURLPX_REQUEST_FAILED,
         CURLPX_RESOLVE_HOST,
         CURLPX_SEND_AUTH,
         CURLPX_SEND_CONNECT,
         CURLPX_SEND_REQUEST,
         CURLPX_UNKNOWN_FAIL,
         CURLPX_UNKNOWN_MODE,
         CURLPX_USER_REJECTED,
         CURLPX_LAST /* never use */
       } CURLproxycode;

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_PROXY_ERROR, long *detail);

~~~
DESCRIPTION
~~~
       Pass a pointer to a long to receive a detailed error code when the most recent transfer returned a CURLE_PROXY error.

       The return value will match the CURLproxycode set.

       The returned value will be zero (equal to CURLPX_OK) if no such response code was available.

~~~
PROTOCOLS
~~~
       All that can be done over SOCKS

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         curl_easy_setopt(curl, CURLOPT_PROXY, "socks5://127.0.0.1");
         res = curl_easy_perform(curl);
         if(res == CURLE_PROXY) {
           long proxycode;
           res = curl_easy_getinfo(curl, CURLINFO_PROXY_ERROR, &proxycode);
           if(!res && proxycode)
             printf("The detailed proxy error: %ld\n", proxycode);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.73.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLINFO_RESPONSE_CODE(3), libcurl-errors(3), curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.73.0                                                                                   3 Aug 2020                                                                         CURLINFO_PROXY_ERROR(3)
CURLINFO_PROXY_SSL_VERIFYRESULT(3)                                                       curl_easy_getinfo options                                                       CURLINFO_PROXY_SSL_VERIFYRESULT(3)



NAME
~~~
       CURLINFO_PROXY_SSL_VERIFYRESULT - get the result of the proxy certificate verification

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_PROXY_SSL_VERIFYRESULT, long *result);

~~~
DESCRIPTION
~~~
       Pass a pointer to a long to receive the result of the certificate verification that was requested (using the CURLOPT_PROXY_SSL_VERIFYPEER(3) option. This is only used for HTTPS proxies.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         long verifyresult;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://proxy:443");
         res = curl_easy_perform(curl);
         curl_easy_getinfo(curl, CURLINFO_PROXY_SSL_VERIFYRESULT, &verifyresult);
         printf("The peer verification said %s\n", verifyresult?
                "fine":"BAAAD");
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_SSL_VERIFYRESULT(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                              CURLINFO_PROXY_SSL_VERIFYRESULT(3)
CURLINFO_REDIRECT_COUNT(3)                                                               curl_easy_getinfo options                                                               CURLINFO_REDIRECT_COUNT(3)



NAME
~~~
       CURLINFO_REDIRECT_COUNT - get the number of redirects

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_REDIRECT_COUNT, long *countp);

~~~
DESCRIPTION
~~~
       Pass a pointer to a long to receive the total number of redirections that were actually followed.

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           long redirects;
           curl_easy_getinfo(curl, CURLINFO_REDIRECT_COUNT, &redirects);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.9.7

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                  28 Aug 2015                                                                      CURLINFO_REDIRECT_COUNT(3)
CURLINFO_REDIRECT_TIME(3)                                                                curl_easy_getinfo options                                                                CURLINFO_REDIRECT_TIME(3)



NAME
~~~
       CURLINFO_REDIRECT_TIME - get the time for all redirection steps

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_REDIRECT_TIME, double *timep);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to  a double to receive the total time, in seconds, it took for all redirection steps include name lookup, connect, pretransfer and transfer before final transaction was started.
       CURLINFO_REDIRECT_TIME contains the complete execution time for multiple redirections.

       See also the TIMES overview in the curl_easy_getinfo(3) man page.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         double redirect;
         curl_easy_setopt(curl, CURLOPT_URL, url);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_REDIRECT_TIME, &redirect);
           if(CURLE_OK == res) {
             printf("Time: %.1f", redirect);
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.9.7

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_REDIRECT_TIME_T(3)



libcurl 7.44.0                                                                                  28 Aug 2015                                                                       CURLINFO_REDIRECT_TIME(3)
CURLINFO_REDIRECT_TIME_T(3)                                                              curl_easy_getinfo options                                                              CURLINFO_REDIRECT_TIME_T(3)



NAME
~~~
       CURLINFO_REDIRECT_TIME_T - get the time for all redirection steps

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_REDIRECT_TIME_T, curl_off_t *timep);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to  a curl_off_t to receive the total time, in microseconds, it took for all redirection steps include name lookup, connect, pretransfer and transfer before final transaction was
       started. CURLINFO_REDIRECT_TIME_T contains the complete execution time for multiple redirections.

       See also the TIMES overview in the curl_easy_getinfo(3) man page.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_off_t redirect;
         curl_easy_setopt(curl, CURLOPT_URL, url);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_REDIRECT_TIME_T, &redirect);
           if(CURLE_OK == res) {
             printf("Time: %" CURL_FORMAT_CURL_OFF_T ".%06ld", redirect / 1000000,
                    (long)(redirect % 1000000));
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.61.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_REDIRECT_TIME(3)



libcurl 7.61.0                                                                                  28 Apr 2018                                                                     CURLINFO_REDIRECT_TIME_T(3)
CURLINFO_REDIRECT_URL(3)                                                                 curl_easy_getinfo options                                                                 CURLINFO_REDIRECT_URL(3)



NAME
~~~
       CURLINFO_REDIRECT_URL - get the URL a redirect would go to

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_REDIRECT_URL, char **urlp);

~~~
DESCRIPTION
~~~
       Pass a pointer to a char pointer to receive the URL a redirect would take you to if you would enable CURLOPT_FOLLOWLOCATION(3). This can come very handy if you think using the built-in libcurl redirect logic isn't good enough for you but you would still prefer to avoid implementing all the magic of figuring out the new URL.

       This URL is also set if the CURLOPT_MAXREDIRS(3) limit prevented a redirect to happen (since 7.54.1).

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           char *url = NULL;
           curl_easy_getinfo(curl, CURLINFO_REDIRECT_URL, &url);
           if(url)
             printf("Redirect to: %s\n", url);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.18.2

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                  28 Aug 2015                                                                        CURLINFO_REDIRECT_URL(3)
CURLINFO_REFERER(3)                                                                      curl_easy_getinfo options                                                                      CURLINFO_REFERER(3)



NAME
~~~
       CURLINFO_REFERER - get the referrer header

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_REFERER, char **hdrp);

~~~
DESCRIPTION
~~~
       Pass in a pointer to a char pointer and get the referrer header.

       The hdrp pointer will be NULL or pointing to private memory you MUST NOT free - it gets freed when you call curl_easy_cleanup(3) on the corresponding CURL handle.

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_REFERER, "https://example.org/referrer");
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           char *hdr = NULL;
           curl_easy_getinfo(curl, CURLINFO_REFERER, &hdr);
           if(hdr)
             printf("Referrer header: %s\n", hdr);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.76.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLOPT_REFERER(3),



libcurl 7.76.0                                                                                  11 Feb 2021                                                                             CURLINFO_REFERER(3)
CURLINFO_REQUEST_SIZE(3)                                                                 curl_easy_getinfo options                                                                 CURLINFO_REQUEST_SIZE(3)



NAME
~~~
       CURLINFO_REQUEST_SIZE - get size of sent request

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_REQUEST_SIZE, long *sizep);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to  a  long  to  receive the total size of the issued requests. This is so far only for HTTP requests. Note that this may be more than one request if CURLOPT_FOLLOWLOCATION(3) is
       enabled.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           long req;
           res = curl_easy_getinfo(curl, CURLINFO_REQUEST_SIZE, &req);
           if(!res)
             printf("Request size: %ld bytes\n", req);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.4.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_HEADER_SIZE(3), CURLINFO_SIZE_DOWNLOAD(3),



libcurl 7.44.0                                                                                   1 Sep 2015                                                                        CURLINFO_REQUEST_SIZE(3)
CURLINFO_RESPONSE_CODE(3)                                                                curl_easy_getinfo options                                                                CURLINFO_RESPONSE_CODE(3)



NAME
~~~
       CURLINFO_RESPONSE_CODE - get the last response code

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_RESPONSE_CODE, long *codep);

~~~
DESCRIPTION
~~~
       Pass  a  pointer to a long to receive the last received HTTP, FTP or SMTP response code. This option was previously known as CURLINFO_HTTP_CODE in libcurl 7.10.7 and earlier. The stored value will
       be zero if no server response code has been received. Note that a proxy's CONNECT response should be read with CURLINFO_HTTP_CONNECTCODE(3) and not this.

       Support for SMTP responses added in 7.25.0.

~~~
PROTOCOLS
~~~
       HTTP, FTP and SMTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           long response_code;
           curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.10.8. CURLINFO_HTTP_CODE was added in 7.4.1.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_HTTP_CONNECTCODE(3),



libcurl 7.44.0                                                                                  28 Aug 2015                                                                       CURLINFO_RESPONSE_CODE(3)
CURLINFO_RETRY_AFTER(3)                                                                  curl_easy_getinfo options                                                                  CURLINFO_RETRY_AFTER(3)



NAME
~~~
       CURLINFO_RETRY_AFTER - returns the Retry-After retry delay

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_RETRY_AFTER, curl_off_t *retry);

~~~
DESCRIPTION
~~~
       Pass  a  pointer to a curl_off_t variable to receive the number of seconds the HTTP server suggests the client should wait until the next request is issued. The information from the "Retry-After:"
       header.

       While the HTTP header might contain a fixed date string, the CURLINFO_RETRY_AFTER(3) will always return number of seconds to wait - or zero if there was no header or the header couldn't be parsed.

DEFAULT
       Returns zero delay if there was no header.

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           curl_off_t wait = 0;
           curl_easy_getinfo(curl, CURLINFO_RETRY_AFTER, &wait);
           if(wait)
             printf("Wait for %" CURL_FORMAT_CURL_OFF_T " seconds\n", wait);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in curl 7.66.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_STDERR(3), CURLOPT_HEADERFUNCTION(3),



libcurl 7.66.0                                                                                   6 Aug 2019                                                                         CURLINFO_RETRY_AFTER(3)
CURLINFO_RTSP_CLIENT_CSEQ(3)                                                             curl_easy_getinfo options                                                             CURLINFO_RTSP_CLIENT_CSEQ(3)



NAME
~~~
       CURLINFO_RTSP_CLIENT_CSEQ - get the next RTSP client CSeq

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_RTSP_CLIENT_CSEQ, long *cseq);

~~~
DESCRIPTION
~~~
       Pass a pointer to a long to receive the next CSeq that will be used by the application.

~~~
PROTOCOLS
~~~
       RTSP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "rtsp://rtsp.example.com");
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           long cseq;
           curl_easy_getinfo(curl, CURLINFO_RTSP_CLIENT_CSEQ, &cseq);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.20.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                   1 Sep 2015                                                                    CURLINFO_RTSP_CLIENT_CSEQ(3)
CURLINFO_RTSP_CSEQ_RECV(3)                                                               curl_easy_getinfo options                                                               CURLINFO_RTSP_CSEQ_RECV(3)



NAME
~~~
       CURLINFO_RTSP_CSEQ_RECV - get the recently received CSeq

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_RTSP_CSEQ_RECV, long *cseq);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to a long to receive the most recently received CSeq from the server. If your application encounters a CURLE_RTSP_CSEQ_ERROR then you may wish to troubleshoot and/or fix the CSeq
       mismatch by peeking at this value.

~~~
PROTOCOLS
~~~
       RTSP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "rtsp://rtsp.example.com");
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           long cseq;
           curl_easy_getinfo(curl, CURLINFO_RTSP_CSEQ_RECV, &cseq);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.20.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                  12 Sep 2015                                                                      CURLINFO_RTSP_CSEQ_RECV(3)
CURLINFO_RTSP_SERVER_CSEQ(3)                                                             curl_easy_getinfo options                                                             CURLINFO_RTSP_SERVER_CSEQ(3)



NAME
~~~
       CURLINFO_RTSP_SERVER_CSEQ - get the next RTSP server CSeq

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_RTSP_SERVER_CSEQ, long *cseq);

~~~
DESCRIPTION
~~~
       Pass a pointer to a long to receive the next CSeq that will be expected by the application.

       Llistening for server initiated requests is currently unimplemented!

       Applications wishing to resume an RTSP session on another connection should retrieve this info before closing the active connection.

~~~
PROTOCOLS
~~~
       RTSP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "rtsp://rtsp.example.com");
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           long cseq;
           curl_easy_getinfo(curl, CURLINFO_RTSP_SERVER_CSEQ, &cseq);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.20.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                   1 Sep 2015                                                                    CURLINFO_RTSP_SERVER_CSEQ(3)
CURLINFO_RTSP_SESSION_ID(3)                                                              curl_easy_getinfo options                                                              CURLINFO_RTSP_SESSION_ID(3)



NAME
~~~
       CURLINFO_RTSP_SESSION_ID - get RTSP session ID

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_RTSP_SESSION_ID, char **id);

~~~
DESCRIPTION
~~~
       Pass a pointer to a char pointer to receive a pointer to a string holding the most recent RTSP Session ID.

       Applications wishing to resume an RTSP session on another connection should retrieve this info before closing the active connection.

       The id pointer will be NULL or pointing to private memory you MUST NOT free - it gets freed when you call curl_easy_cleanup(3) on the corresponding CURL handle.

~~~
PROTOCOLS
~~~
       RTSP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "rtsp://rtsp.example.com");
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           char *id;
           curl_easy_getinfo(curl, CURLINFO_RTSP_SESSION_ID, &id);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.20.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                  12 Sep 2015                                                                     CURLINFO_RTSP_SESSION_ID(3)
CURLINFO_SCHEME(3)                                                                       curl_easy_getinfo options                                                                       CURLINFO_SCHEME(3)



NAME
~~~
       CURLINFO_SCHEME - get the URL scheme (sometimes called protocol) used in the connection

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_SCHEME, char **scheme);

~~~
DESCRIPTION
~~~
       Pass a pointer to a char pointer to receive the pointer to a null-terminated string holding the URL scheme used for the most recent connection done with this CURL handle.

       The scheme pointer will be NULL or pointing to private memory you MUST NOT free - it gets freed when you call curl_easy_cleanup(3) on the corresponding CURL handle.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         res = curl_easy_perform(curl);
         if(res == CURLE_OK) {
           char *scheme = NULL;
           curl_easy_getinfo(curl, CURLINFO_SCHEME, &scheme);
           if(scheme)
             printf("scheme: %s\n", scheme); /* scheme: HTTP */
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLINFO_RESPONSE_CODE(3), curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.52.0                                                                                23 November 2016                                                                           CURLINFO_SCHEME(3)
CURLINFO_SIZE_DOWNLOAD(3)                                                                curl_easy_getinfo options                                                                CURLINFO_SIZE_DOWNLOAD(3)



NAME
~~~
       CURLINFO_SIZE_DOWNLOAD - get the number of downloaded bytes

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_SIZE_DOWNLOAD, double *dlp);

~~~
DESCRIPTION
~~~
       Pass  a pointer to a double to receive the total amount of bytes that were downloaded.  The amount is only for the latest transfer and will be reset again for each new transfer. This counts actual
       payload data, what's also commonly called body. All meta and header data are excluded and will not be counted in this number.

       CURLINFO_SIZE_DOWNLOAD_T(3) is a newer replacement that returns a more sensible variable type.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Perform the request */
         res = curl_easy_perform(curl);

         if(!res) {
           /* check the size */
           double dl;
           res = curl_easy_getinfo(curl, CURLINFO_SIZE_DOWNLOAD, &dl);
           if(!res) {
             printf("Downloaded %.0f bytes\n", cl);
           }
         }
       }

~~~
AVAILABILITY
~~~
       Added in 7.4.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_SIZE_DOWNLOAD_T(3), CURLINFO_SIZE_UPLOAD_T(3),



libcurl 7.44.0                                                                                  28 Aug 2015                                                                       CURLINFO_SIZE_DOWNLOAD(3)
CURLINFO_SIZE_DOWNLOAD_T(3)                                                              curl_easy_getinfo options                                                              CURLINFO_SIZE_DOWNLOAD_T(3)



NAME
~~~
       CURLINFO_SIZE_DOWNLOAD_T - get the number of downloaded bytes

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_SIZE_DOWNLOAD_T, curl_off_t *dlp);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to  a curl_off_t to receive the total amount of bytes that were downloaded.  The amount is only for the latest transfer and will be reset again for each new transfer. This counts
       actual payload data, what's also commonly called body. All meta and header data are excluded and will not be counted in this number.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Perform the request */
         res = curl_easy_perform(curl);

         if(!res) {
           /* check the size */
           curl_off_t dl;
           res = curl_easy_getinfo(curl, CURLINFO_SIZE_DOWNLOAD_T, &dl);
           if(!res) {
             printf("Downloaded %" CURL_FORMAT_CURL_OFF_T " bytes\n", dl);
           }
         }
       }

~~~
AVAILABILITY
~~~
       Added in 7.55.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_SIZE_DOWNLOAD(3), CURLINFO_SIZE_UPLOAD_T(3),



libcurl 7.55.0                                                                                  25 May 2017                                                                     CURLINFO_SIZE_DOWNLOAD_T(3)
CURLINFO_SIZE_UPLOAD(3)                                                                  curl_easy_getinfo options                                                                  CURLINFO_SIZE_UPLOAD(3)



NAME
~~~
       CURLINFO_SIZE_UPLOAD - get the number of uploaded bytes

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_SIZE_UPLOAD, double *uploadp);

~~~
DESCRIPTION
~~~
       Pass a pointer to a double to receive the total amount of bytes that were uploaded.

       CURLINFO_SIZE_UPLOAD_T(3) is a newer replacement that returns a more sensible variable type.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Perform the request */
         res = curl_easy_perform(curl);

         if(!res) {
           double ul;
           res = curl_easy_getinfo(curl, CURLINFO_SIZE_UPLOAD, &ul);
           if(!res) {
             printf("Uploaded %.0f bytes\n", ul);
           }
         }
       }

~~~
AVAILABILITY
~~~
       Added in 7.4.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_SIZE_DOWNLOAD_T(3), CURLINFO_SIZE_UPLOAD_T(3),



libcurl 7.44.0                                                                                  28 Aug 2015                                                                         CURLINFO_SIZE_UPLOAD(3)
CURLINFO_SIZE_UPLOAD_T(3)                                                                curl_easy_getinfo options                                                                CURLINFO_SIZE_UPLOAD_T(3)



NAME
~~~
       CURLINFO_SIZE_UPLOAD_T - get the number of uploaded bytes

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_SIZE_UPLOAD_T, curl_off_t *uploadp);

~~~
DESCRIPTION
~~~
       Pass a pointer to a curl_off_t to receive the total amount of bytes that were uploaded.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Perform the request */
         res = curl_easy_perform(curl);

         if(!res) {
           curl_off_t ul;
           res = curl_easy_getinfo(curl, CURLINFO_SIZE_UPLOAD_T, &ul);
           if(!res) {
             printf("Uploaded %" CURL_FORMAT_CURL_OFF_T " bytes\n", ul);
           }
         }
       }

~~~
AVAILABILITY
~~~
       Added in 7.55.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_SIZE_DOWNLOAD_T(3), CURLINFO_SIZE_UPLOAD(3),



libcurl 7.55.0                                                                                  25 May 2017                                                                       CURLINFO_SIZE_UPLOAD_T(3)
CURLINFO_SPEED_DOWNLOAD(3)                                                               curl_easy_getinfo options                                                               CURLINFO_SPEED_DOWNLOAD(3)



NAME
~~~
       CURLINFO_SPEED_DOWNLOAD - get download speed

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_SPEED_DOWNLOAD, double *speed);

~~~
DESCRIPTION
~~~
       Pass a pointer to a double to receive the average download speed that curl measured for the complete download. Measured in bytes/second.

       CURLINFO_SPEED_DOWNLOAD_T(3) is a newer replacement that returns a more sensible variable type.

~~~
PROTOCOLS
~~~
~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Perform the request */
         res = curl_easy_perform(curl);

         if(!res) {
           double speed;
           res = curl_easy_getinfo(curl, CURLINFO_SPEED_DOWNLOAD, &speed);
           if(!res) {
             printf("Download speed %.0f bytes/sec\n", ul);
           }
         }
       }

~~~
AVAILABILITY
~~~
       Added in 7.4.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_SPEED_UPLOAD(3), CURLINFO_SIZE_UPLOAD_T(3),



libcurl 7.44.0                                                                                  28 Aug 2015                                                                      CURLINFO_SPEED_DOWNLOAD(3)
CURLINFO_SPEED_DOWNLOAD_T(3)                                                             curl_easy_getinfo options                                                             CURLINFO_SPEED_DOWNLOAD_T(3)



NAME
~~~
       CURLINFO_SPEED_DOWNLOAD_T - get download speed

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_SPEED_DOWNLOAD_T, curl_off_t *speed);

~~~
DESCRIPTION
~~~
       Pass a pointer to a curl_off_t to receive the average download speed that curl measured for the complete download. Measured in bytes/second.

~~~
PROTOCOLS
~~~
~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Perform the request */
         res = curl_easy_perform(curl);

         if(!res) {
           curl_off_t speed;
           res = curl_easy_getinfo(curl, CURLINFO_SPEED_DOWNLOAD_T, &speed);
           if(!res) {
             printf("Download speed %" CURL_FORMAT_CURL_OFF_T " bytes/sec\n", speed);
           }
         }
       }

~~~
AVAILABILITY
~~~
       Added in 7.55.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_SPEED_UPLOAD(3), CURLINFO_SIZE_UPLOAD_T(3),



libcurl 7.44.0                                                                                  28 Aug 2015                                                                    CURLINFO_SPEED_DOWNLOAD_T(3)
CURLINFO_SPEED_UPLOAD(3)                                                                 curl_easy_getinfo options                                                                 CURLINFO_SPEED_UPLOAD(3)



NAME
~~~
       CURLINFO_SPEED_UPLOAD - get upload speed

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_SPEED_UPLOAD, double *speed);

~~~
DESCRIPTION
~~~
       Pass a pointer to a double to receive the average upload speed that curl measured for the complete upload. Measured in bytes/second.

       CURLINFO_SPEED_UPLOAD_T(3) is a newer replacement that returns a more sensible variable type.

~~~
PROTOCOLS
~~~
~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Perform the request */
         res = curl_easy_perform(curl);

         if(!res) {
           double speed;
           res = curl_easy_getinfo(curl, CURLINFO_SPEED_UPLOAD, &speed);
           if(!res) {
             printf("Upload speed %.0f bytes/sec\n", ul);
           }
         }
       }

~~~
AVAILABILITY
~~~
       Added in

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_SPEED_DOWNLOAD(3),



libcurl 7.44.0                                                                                  28 Aug 2015                                                                        CURLINFO_SPEED_UPLOAD(3)
CURLINFO_SPEED_UPLOAD_T(3)                                                               curl_easy_getinfo options                                                               CURLINFO_SPEED_UPLOAD_T(3)



NAME
~~~
       CURLINFO_SPEED_UPLOAD_T - get upload speed

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_SPEED_UPLOAD_T, curl_off_t *speed);

~~~
DESCRIPTION
~~~
       Pass a pointer to a curl_off_t to receive the average upload speed that curl measured for the complete upload. Measured in bytes/second.

~~~
PROTOCOLS
~~~
~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Perform the request */
         res = curl_easy_perform(curl);

         if(!res) {
           curl_off_t speed;
           res = curl_easy_getinfo(curl, CURLINFO_SPEED_UPLOAD_T, &speed);
           if(!res) {
             printf("Upload speed %" CURL_FORMAT_CURL_OFF_T " bytes/sec\n", speed);
           }
         }
       }

~~~
AVAILABILITY
~~~
       Added in 7.55.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_SPEED_DOWNLOAD_T(3),



libcurl 7.55.0                                                                                  25 May 2017                                                                      CURLINFO_SPEED_UPLOAD_T(3)
CURLINFO_SSL_ENGINES(3)                                                                  curl_easy_getinfo options                                                                  CURLINFO_SSL_ENGINES(3)



NAME
~~~
       CURLINFO_SSL_ENGINES - get an slist of OpenSSL crypto-engines

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_SSL_ENGINES,
                                  struct curl_slist **engine_list);

~~~
DESCRIPTION
~~~
       Pass  the  address  of a 'struct curl_slist *' to receive a linked-list of OpenSSL crypto-engines supported. Note that engines are normally implemented in separate dynamic libraries. Hence not all
       the returned engines may be available at run-time. NOTE: you must call curl_slist_free_all(3) on the list pointer once you're done with it, as libcurl will not free the data for you.

~~~
PROTOCOLS
~~~
       All TLS based ones.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         struct curl_slist *engines;
         res = curl_easy_getinfo(curl, CURLINFO_SSL_ENGINES, &engines);
         if((res == CURLE_OK) && engines) {
           /* we have a list, free it when done using it */
           curl_slist_free_all(engines);
         }

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.12.3. Available in OpenSSL builds with "engine" support.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                   1 Sep 2015                                                                         CURLINFO_SSL_ENGINES(3)
CURLINFO_SSL_VERIFYRESULT(3)                                                             curl_easy_getinfo options                                                             CURLINFO_SSL_VERIFYRESULT(3)



NAME
~~~
       CURLINFO_SSL_VERIFYRESULT - get the result of the certificate verification

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_SSL_VERIFYRESULT, long *result);

~~~
DESCRIPTION
~~~
       Pass a pointer to a long to receive the result of the server SSL certificate verification that was requested (using the CURLOPT_SSL_VERIFYPEER(3) option).

       0 is a positive result. Non-zero is an error.

~~~
PROTOCOLS
~~~
       All using TLS

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         long verifyresult;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         res = curl_easy_perform(curl);
         curl_easy_getinfo(curl, CURLINFO_SSL_VERIFYRESULT, &verifyresult);
         printf("The peer verification said %s\n", verifyresult?
                "BAAAD":"fine");
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.5. Only set by the OpenSSL/libressl/boringssl, NSS and GnuTLS backends.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3),



libcurl 7.44.0                                                                                   1 Sep 2015                                                                    CURLINFO_SSL_VERIFYRESULT(3)
CURLINFO_STARTTRANSFER_TIME(3)                                                           curl_easy_getinfo options                                                           CURLINFO_STARTTRANSFER_TIME(3)



NAME
~~~
       CURLINFO_STARTTRANSFER_TIME - get the time until the first byte is received

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_STARTTRANSFER_TIME, double *timep);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to  a double to receive the time, in seconds, it took from the start until the first byte is received by libcurl. This includes CURLINFO_PRETRANSFER_TIME(3) and also the time the
       server needs to calculate the result.

       When a redirect is followed, the time from each request is added together.

       See also the TIMES overview in the curl_easy_getinfo(3) man page.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         double start;
         curl_easy_setopt(curl, CURLOPT_URL, url);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_STARTTRANSFER_TIME, &start);
           if(CURLE_OK == res) {
             printf("Time: %.1f", start);
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.9.2

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_STARTTRANSFER_TIME_T(3)



libcurl 7.44.0                                                                                  28 Aug 2015                                                                  CURLINFO_STARTTRANSFER_TIME(3)
CURLINFO_STARTTRANSFER_TIME_T(3)                                                         curl_easy_getinfo options                                                         CURLINFO_STARTTRANSFER_TIME_T(3)



NAME
~~~
       CURLINFO_STARTTRANSFER_TIME_T - get the time until the first byte is received

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_STARTTRANSFER_TIME_T, curl_off_t *timep);

~~~
DESCRIPTION
~~~
       Pass  a  pointer to a curl_off_t to receive the time, in microseconds, it took from the start until the first byte is received by libcurl. This includes CURLINFO_PRETRANSFER_TIME_T(3) and also the
       time the server needs to calculate the result.

       When a redirect is followed, the time from each request is added together.

       See also the TIMES overview in the curl_easy_getinfo(3) man page.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_off_t start;
         curl_easy_setopt(curl, CURLOPT_URL, url);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_STARTTRANSFER_TIME_T, &start);
           if(CURLE_OK == res) {
             printf("Time: %" CURL_FORMAT_CURL_OFF_T ".%06ld", start / 1000000,
                    (long)(start % 1000000));
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.61.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_STARTTRANSFER_TIME(3)



libcurl 7.61.0                                                                                  28 Apr 2018                                                                CURLINFO_STARTTRANSFER_TIME_T(3)
CURLINFO_TLS_SESSION(3)                                                                  curl_easy_getinfo options                                                                  CURLINFO_TLS_SESSION(3)



NAME
~~~
       CURLINFO_TLS_SESSION - get TLS session info

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_TLS_SESSION,
                                  struct curl_tlssessioninfo **session);

~~~
DESCRIPTION
~~~
       This  option  has been superseded by CURLINFO_TLS_SSL_PTR(3) which was added in 7.48.0. The only reason you would use this option instead is if you could be using a version of libcurl earlier than
       7.48.0.

       This option is exactly the same as CURLINFO_TLS_SSL_PTR(3) except in the case of OpenSSL. If the session backend is CURLSSLBACKEND_OPENSSL the session internals pointer  varies  depending  on  the
       option:

       CURLINFO_TLS_SESSION OpenSSL session internals is SSL_CTX *.

       CURLINFO_TLS_SSL_PTR OpenSSL session internals is SSL *.

       You  can  obtain  an SSL_CTX pointer from an SSL pointer using OpenSSL function SSL_get_SSL_CTX. Therefore unless you need compatibility with older versions of libcurl use CURLINFO_TLS_SSL_PTR(3).
       Refer to that document for more information.

~~~
PROTOCOLS
~~~
       All TLS-based

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         struct curl_tlssessioninfo *tls;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         res = curl_easy_perform(curl);
         curl_easy_getinfo(curl, CURLINFO_TLS_SESSION, &tls);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.34.0, and supported OpenSSL, GnuTLS, NSS and gskit only up until 7.48.0 was released.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_TLS_SSL_PTR(3),



libcurl 7.44.0                                                                                  12 Sep 2015                                                                         CURLINFO_TLS_SESSION(3)
CURLINFO_TLS_SSL_PTR(3)                                                                  curl_easy_getinfo options                                                                  CURLINFO_TLS_SSL_PTR(3)



NAME
~~~
       CURLINFO_TLS_SESSION, CURLINFO_TLS_SSL_PTR - get TLS session info

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_TLS_SSL_PTR,
                                  struct curl_tlssessioninfo **session);

       /* if you need compatibility with libcurl < 7.48.0 use
          CURLINFO_TLS_SESSION instead: */

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_TLS_SESSION,
                                  struct curl_tlssessioninfo **session);

~~~
DESCRIPTION
~~~
       Pass  a pointer to a 'struct curl_tlssessioninfo *'.  The pointer will be initialized to refer to a 'struct curl_tlssessioninfo *' that will contain an enum indicating the SSL library used for the
       handshake and a pointer to the respective internal TLS session structure of this underlying SSL library.

       This option may be useful for example to extract certificate information in a format convenient for further processing, such as manual validation. Refer to the LIMITATIONS section.

       struct curl_tlssessioninfo {
         curl_sslbackend backend;
         void *internals;
       };

       The backend struct member is one of the defines in the CURLSSLBACKEND_* series: CURLSSLBACKEND_NONE  (when  built  without  TLS  support),  CURLSSLBACKEND_WOLFSSL,  CURLSSLBACKEND_SECURETRANSPORT,
       CURLSSLBACKEND_GNUTLS,  CURLSSLBACKEND_GSKIT,  CURLSSLBACKEND_MBEDTLS,  CURLSSLBACKEND_NSS, CURLSSLBACKEND_OPENSSL, CURLSSLBACKEND_SCHANNEL or CURLSSLBACKEND_MESALINK. (Note that the OpenSSL forks
       are all reported as just OpenSSL here.)

       The internals struct member will point to a TLS library specific pointer for the active ("in use") SSL connection, with the following underlying types:

              GnuTLS gnutls_session_t

              gskit  gsk_handle

              NSS    PRFileDesc *

              OpenSSL
                     CURLINFO_TLS_SESSION: SSL_CTX *

                     CURLINFO_TLS_SSL_PTR: SSL *
       Since 7.48.0 the internals member can point to these other SSL backends as well:

              mbedTLS
                     mbedtls_ssl_context *

              Secure Channel
                     CtxtHandle *

              Secure Transport
                     SSLContext *

              wolfSSL
                     SSL *

              MesaLink
                     SSL *

       If the internals pointer is NULL then either the SSL backend is not supported, an SSL session has not yet been established or the connection is no  longer  associated  with  the  easy  handle  (eg
       curl_easy_perform has returned).

LIMITATIONS
       This option has some limitations that could make it unsafe when it comes to the manual verification of certificates.

       This  option only retrieves the first in-use SSL session pointer for your easy handle, however your easy handle may have more than one in-use SSL session if using FTP over SSL. That is because the
       FTP protocol has a control channel and a data channel and one or both may be over SSL. Currently there is no way to retrieve a second in-use SSL session associated with an easy handle.

       This option has not been thoroughly tested with plaintext protocols that can be upgraded/downgraded to/from SSL: FTP, SMTP, POP3, IMAP when used with CURLOPT_USE_SSL(3). Though you will be able to
       retrieve the SSL pointer, it's possible that before you can do that data (including auth) may have already been sent over a connection after it was upgraded.

       Renegotiation.  If  unsafe  renegotiation or renegotiation in a way that the certificate is allowed to change is allowed by your SSL library this may occur and the certificate may change, and data
       may continue to be sent or received after renegotiation but before you are able to get the (possibly) changed SSL pointer, with the (possibly) changed certificate information.

       If you are using OpenSSL or wolfSSL then CURLOPT_SSL_CTX_FUNCTION(3) can be used to set a certificate verification callback in the CTX. That is safer than using this option to poll for certificate
       changes and doesn't suffer from any of the problems above. There is currently no way in libcurl to set a verification callback for the other SSL backends.

       How are you using this option? Are you affected by any of these limitations?  Please let us know by making a comment at https://github.com/curl/curl/issues/685

~~~
PROTOCOLS
~~~
       All TLS-based

~~~
EXAMPLE
~~~
       #include <curl/curl.h>
       #include <openssl/ssl.h>

       CURL *curl;
       static size_t wf(void *ptr, size_t size, size_t nmemb, void *stream)
       {
         const struct curl_tlssessioninfo *info = NULL;
         CURLcode res = curl_easy_getinfo(curl, CURLINFO_TLS_SSL_PTR, &info);
         if(info && !res) {
           if(CURLSSLBACKEND_OPENSSL == info->backend) {
              printf("OpenSSL ver. %s\n", SSL_get_version((SSL*)info->internals));
           }
         }
         return size * nmemb;
       }

       int main(int argc, char** argv)
       {
         CURLcode res;
         curl = curl_easy_init();
         if(curl) {
           curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
           curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, wf);
           res = curl_easy_perform(curl);
           curl_easy_cleanup(curl);
         }
         return res;
       }

~~~
AVAILABILITY
~~~
       Added in 7.48.0.

       This option supersedes CURLINFO_TLS_SESSION(3) which was added in 7.34.0.  This option is exactly the same as that option except in the case of OpenSSL.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_TLS_SESSION(3),



libcurl 7.48.0                                                                                  23 Feb 2016                                                                         CURLINFO_TLS_SSL_PTR(3)
CURLINFO_TOTAL_TIME(3)                                                                   curl_easy_getinfo options                                                                   CURLINFO_TOTAL_TIME(3)



NAME
~~~
       CURLINFO_TOTAL_TIME - get total time of previous transfer

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_TOTAL_TIME, double *timep);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to  a double to receive the total time in seconds for the previous transfer, including name resolving, TCP connect etc. The double represents the time in seconds, including fractions.

       When a redirect is followed, the time from each request is added together.

       See also the TIMES overview in the curl_easy_getinfo(3) man page.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         double total;
         curl_easy_setopt(curl, CURLOPT_URL, url);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &total);
           if(CURLE_OK == res) {
             printf("Time: %.1f", total);
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.4.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_TOTAL_TIME_T(3)



libcurl 7.44.0                                                                                  28 Aug 2015                                                                          CURLINFO_TOTAL_TIME(3)
CURLINFO_TOTAL_TIME_T(3)                                                                 curl_easy_getinfo options                                                                 CURLINFO_TOTAL_TIME_T(3)



NAME
~~~
       CURLINFO_TOTAL_TIME_T - get total time of previous transfer in microseconds

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_getinfo(CURL *handle, CURLINFO_TOTAL_TIME_T, curl_off_t *timep);

~~~
DESCRIPTION
~~~
       Pass a pointer to a curl_off_t to receive the total time in microseconds for the previous transfer, including name resolving, TCP connect etc.  The curl_off_t represents the time in microseconds.

       When a redirect is followed, the time from each request is added together.

       See also the TIMES overview in the curl_easy_getinfo(3) man page.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_off_t total;
         curl_easy_setopt(curl, CURLOPT_URL, url);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME_T, &total);
           if(CURLE_OK == res) {
             printf("Time: %" CURL_FORMAT_CURL_OFF_T ".%06ld", total / 1000000,
                    (long)(total % 1000000));
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.61.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       curl_easy_getinfo(3), curl_easy_setopt(3), CURLINFO_TOTAL_TIME(3)



libcurl 7.61.0                                                                                  28 Apr 2018                                                                        CURLINFO_TOTAL_TIME_T(3)
CURLMOPT_CHUNK_LENGTH_PENALTY_SIZE(3)                                                    curl_multi_setopt options                                                    CURLMOPT_CHUNK_LENGTH_PENALTY_SIZE(3)



NAME
~~~
       CURLMOPT_CHUNK_LENGTH_PENALTY_SIZE - chunk length threshold for pipelining

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_CHUNK_LENGTH_PENALTY_SIZE, long size);

~~~
DESCRIPTION
~~~
       No function since pipelining was removed in 7.62.0.

       Pass  a  long  with  a  size  in  bytes.  If  a  pipelined  connection  is  currently  processing  a  chunked  (Transfer-encoding:  chunked)  request  with a current chunk length larger than CURLMOPT_CHUNK_LENGTH_PENALTY_SIZE(3), that pipeline will not be considered for additional requests, even if it is shorter than CURLMOPT_MAX_PIPELINE_LENGTH(3).

DEFAULT
       The default value is 0, which means that the penalization is inactive.

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURLM *m = curl_multi_init();
       long maxchunk = 10000;
       curl_multi_setopt(m, CURLMOPT_CHUNK_LENGTH_PENALTY_SIZE, maxchunk);

~~~
AVAILABILITY
~~~
       Added in 7.30.0

~~~
RETURN VALUE
~~~
       Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLMOPT_PIPELINING(3), CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE(3), CURLMOPT_MAX_PIPELINE_LENGTH(3),



libcurl 7.39.0                                                                                   4 Nov 2014                                                           CURLMOPT_CHUNK_LENGTH_PENALTY_SIZE(3)
CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE(3)                                                  curl_multi_setopt options                                                  CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE(3)



NAME
~~~
       CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE - size threshold for pipelining penalty

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE, long size);

~~~
DESCRIPTION
~~~
       No function since pipelining was removed in 7.62.0.

       Pass  a  long  with  a size in bytes. If a pipelined connection is currently processing a request with a Content-Length larger than this CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE(3), that pipeline will
       then not be considered for additional requests, even if it is shorter than CURLMOPT_MAX_PIPELINE_LENGTH(3).

DEFAULT
       The default value is 0, which means that the size penalization is inactive.

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURLM *m = curl_multi_init();
       long maxlength = 10000;
       curl_multi_setopt(m, CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE, maxlength);

~~~
AVAILABILITY
~~~
       Added in 7.30.0

~~~
RETURN VALUE
~~~
       Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLMOPT_PIPELINING(3), CURLMOPT_CHUNK_LENGTH_PENALTY_SIZE(3),



libcurl 7.39.0                                                                                   4 Nov 2014                                                         CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE(3)
CURLMOPT_MAX_CONCURRENT_STREAMS(3)                                                       curl_multi_setopt options                                                       CURLMOPT_MAX_CONCURRENT_STREAMS(3)



NAME
~~~
       CURLMOPT_MAX_CONCURRENT_STREAMS - max concurrent streams for http2

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_MAX_CONCURRENT_STREAMS,
                                   long max);

~~~
DESCRIPTION
~~~
       Pass a long indicating the max. The set number will be used as the maximum number of concurrent streams for a connections that libcurl should support on connections done using HTTP/2.

       Valid values range from 1 to 2147483647 (2^31 - 1) and defaults to 100.  The value passed here would be honoured based on other system resources properties.

DEFAULT
       100

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
         CURLM *m = curl_multi_init();
         /* max concurrent streams 200 */
         curl_multi_setopt(m, CURLMOPT_MAX_CONCURRENT_STREAMS, 200L);

~~~
AVAILABILITY
~~~
       Added in 7.67.0

~~~
RETURN VALUE
~~~
       Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_MAXCONNECTS(3), CURLMOPT_MAXCONNECTS(3),



libcurl 7.67.0                                                                                  06 Nov 2019                                                              CURLMOPT_MAX_CONCURRENT_STREAMS(3)
CURLMOPT_MAXCONNECTS(3)                                                                  curl_multi_setopt options                                                                  CURLMOPT_MAXCONNECTS(3)



NAME
~~~
       CURLMOPT_MAXCONNECTS - size of connection cache

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_MAXCONNECTS, long max);

~~~
DESCRIPTION
~~~
       Pass  a  long indicating the max. The set number will be used as the maximum amount of simultaneously open connections that libcurl may keep in its connection cache after completed use. By default
       libcurl will enlarge the size for each added easy handle to make it fit 4 times the number of added easy handles.

       By setting this option, you can prevent the cache size from growing beyond the limit set by you.

       When the cache is full, curl closes the oldest one in the cache to prevent the number of open connections from increasing.

       This option is for the multi handle's use only, when using the easy interface you should instead use the CURLOPT_MAXCONNECTS(3) option.

       See CURLMOPT_MAX_TOTAL_CONNECTIONS(3) for limiting the number of active connections.


DEFAULT
       See ~~~
DESCRIPTION
~~~

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURLM *m = curl_multi_init();
       /* only keep 10 connections in the cache */
       curl_multi_setopt(m, CURLMOPT_MAXCONNECTS, 10L);

~~~
AVAILABILITY
~~~
       Added in 7.16.3

~~~
RETURN VALUE
~~~
       Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLMOPT_MAX_HOST_CONNECTIONS(3), CURLOPT_MAXCONNECTS(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                         CURLMOPT_MAXCONNECTS(3)
CURLMOPT_MAX_HOST_CONNECTIONS(3)                                                         curl_multi_setopt options                                                         CURLMOPT_MAX_HOST_CONNECTIONS(3)



NAME
~~~
       CURLMOPT_MAX_HOST_CONNECTIONS - max number of connections to a single host

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_MAX_HOST_CONNECTIONS, long max);

~~~
DESCRIPTION
~~~
       Pass  a  long  to indicate max. The set number will be used as the maximum amount of simultaneously open connections to a single host (a host being the same as a host name + port number pair). For
       each new session to a host, libcurl will open a new connection up to the limit set by CURLMOPT_MAX_HOST_CONNECTIONS(3). When the limit is reached, the sessions will be pending until  a  connection
       becomes available. If CURLMOPT_PIPELINING(3) is enabled, libcurl will try to pipeline if the host is capable of it.

       The default max value is 0, unlimited.  However, for backwards compatibility, setting it to 0 when CURLMOPT_PIPELINING(3) is 1 will not be treated as unlimited. Instead it will open only 1 connection and try to pipeline on it.

       This set limit is also used for proxy connections, and then the proxy is considered to be the host for which this limit counts.

       When more transfers are added to the multi handle than what can be performed due to the set limit, they will be queued up waiting for their chance. When  that  happens,  the  CURLOPT_TIMEOUT_MS(3)
       timeout will be counted inclusive of the waiting time, meaning that if you set a too narrow timeout in such a case the transfer might never even start before it times out.

       Even in the queued up situation, the CURLOPT_CONNECTTIMEOUT_MS(3) timeout is however treated as a per-connect timeout.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURLM *m = curl_multi_init();
       /* do no more than 2 connections per host */
       curl_multi_setopt(m, CURLMOPT_MAX_HOST_CONNECTIONS, 2L);

~~~
AVAILABILITY
~~~
       Added in 7.30.0

~~~
RETURN VALUE
~~~
       Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLMOPT_MAXCONNECTS(3), CURLMOPT_MAX_TOTAL_CONNECTIONS(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                CURLMOPT_MAX_HOST_CONNECTIONS(3)
CURLMOPT_MAX_PIPELINE_LENGTH(3)                                                          curl_multi_setopt options                                                          CURLMOPT_MAX_PIPELINE_LENGTH(3)



NAME
~~~
       CURLMOPT_MAX_PIPELINE_LENGTH - maximum number of requests in a pipeline

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_MAX_PIPELINE_LENGTH, long max);

~~~
DESCRIPTION
~~~
       No function since pipelining was removed in 7.62.0.

       Pass  a  long.  The  set max number will be used as the maximum amount of outstanding requests in an HTTP/1.1 pipelined connection. This option is only used for HTTP/1.1 pipelining, not for HTTP/2
       multiplexing.

       When this limit is reached, libcurl will use another connection to the same host (see CURLMOPT_MAX_HOST_CONNECTIONS(3)), or queue the request until one of the pipelines to the  host  is  ready  to
       accept a request.  Thus, the total number of requests in-flight is CURLMOPT_MAX_HOST_CONNECTIONS(3) * CURLMOPT_MAX_PIPELINE_LENGTH(3).

DEFAULT
       5

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURLM *m = curl_multi_init();
       /* set a more conservative pipe length */
       curl_multi_setopt(m, CURLMOPT_MAX_PIPELINE_LENGTH, 3L);

~~~
AVAILABILITY
~~~
       Added in 7.30.0

~~~
RETURN VALUE
~~~
       Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLMOPT_PIPELINING(3), CURLMOPT_MAX_HOST_CONNECTIONS(3),



libcurl 7.39.0                                                                                   4 Nov 2014                                                                 CURLMOPT_MAX_PIPELINE_LENGTH(3)
CURLMOPT_MAX_TOTAL_CONNECTIONS(3)                                                        curl_multi_setopt options                                                        CURLMOPT_MAX_TOTAL_CONNECTIONS(3)



NAME
~~~
       CURLMOPT_MAX_TOTAL_CONNECTIONS - max simultaneously open connections

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_MAX_TOTAL_CONNECTIONS, long amount);

~~~
DESCRIPTION
~~~
       Pass  a  long  for  the amount. The set number will be used as the maximum number of simultaneously open connections in total using this multi handle. For each new session, libcurl will open a new
       connection up to the limit set by CURLMOPT_MAX_TOTAL_CONNECTIONS(3). When the limit is reached, the sessions will be pending until there are available  connections.  If  CURLMOPT_PIPELINING(3)  is
       enabled, libcurl will try to use multiplexing if the host is capable of it.

       When  more  transfers  are  added to the multi handle than what can be performed due to the set limit, they will be queued up waiting for their chance. When that happens, the CURLOPT_TIMEOUT_MS(3)
       timeout will be counted inclusive of the waiting time, meaning that if you set a too narrow timeout in such a case the transfer might never even start before it times out.

       Even in the queued up situation, the CURLOPT_CONNECTTIMEOUT_MS(3) timeout is however treated as a per-connect timeout.

DEFAULT
       The default value is 0, which means that there is no limit. It is then simply controlled by the number of easy handles added.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURLM *m = curl_multi_init();
       /* never do more than 15 connections */
       curl_multi_setopt(m, CURLMOPT_MAX_TOTAL_CONNECTIONS, 15L);

~~~
AVAILABILITY
~~~
       Added in 7.30.0

~~~
RETURN VALUE
~~~
       Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLMOPT_MAXCONNECTS(3), CURLMOPT_MAX_HOST_CONNECTIONS(3),



libcurl 7.39.0                                                                                   4 Nov 2014                                                               CURLMOPT_MAX_TOTAL_CONNECTIONS(3)
CURLMOPT_PIPELINING(3)                                                                   curl_multi_setopt options                                                                   CURLMOPT_PIPELINING(3)



NAME
~~~
       CURLMOPT_PIPELINING - enable HTTP pipelining and multiplexing

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_PIPELINING, long bitmask);

~~~
DESCRIPTION
~~~
       Pass in the bitmask parameter to instruct libcurl to enable HTTP pipelining and/or HTTP/2 multiplexing for this multi handle.

       When enabled, libcurl will attempt to use those protocol features when doing parallel requests to the same hosts.

       For  pipelining,  this  means  that if you add a second request that can use an already existing connection, the second request will be "piped" on the same connection rather than being executed in
       parallel.

       For multiplexing, this means that follow-up requests can re-use an existing connection and send the new request multiplexed over that at the same time as other transfers  are  already  using  that
       single connection.

       There are several other related options that are interesting to tweak and adjust to alter how libcurl spreads out requests on different connections or not etc.

       Before 7.43.0, this option was set to 1 and 0 to enable and disable HTTP/1.1 pipelining.

       Starting in 7.43.0, bitmask's second bit also has a meaning, and you can ask for pipelining and multiplexing independently of each other by toggling the correct bits.

       CURLPIPE_NOTHING
       Default, which means doing no attempts at pipelining or multiplexing.

       CURLPIPE_HTTP1
        If this bit is set, libcurl will try to pipeline HTTP/1.1 requests on connections that are already established and in use to hosts.

        This bit is deprecated and has no effect since version 7.62.0.

       CURLPIPE_MULTIPLEX
         If this bit is set, libcurl will try to multiplex the new transfer over an existing connection if possible. This requires HTTP/2.

DEFAULT
       Since 7.62.0, CURLPIPE_MULTIPLEX is enabled by default.

       Before that, default was CURLPIPE_NOTHING.

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURLM *m = curl_multi_init();
       /* try HTTP/2 multiplexing */
       curl_multi_setopt(m, CURLMOPT_PIPELINING, CURLPIPE_MULTIPLEX);

~~~
AVAILABILITY
~~~
       Added in 7.16.0. Multiplex support bit added in 7.43.0. HTTP/1 Pipelining support was disabled in 7.62.0.

~~~
RETURN VALUE
~~~
       Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLMOPT_MAX_PIPELINE_LENGTH(3),  CURLMOPT_PIPELINING_SITE_BL(3), CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE(3), CURLMOPT_CHUNK_LENGTH_PENALTY_SIZE(3), CURLMOPT_MAX_HOST_CONNECTIONS(3), CURLMOPT_MAXCONNECTS(3), CURLMOPT_MAX_HOST_CONNECTIONS(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                          CURLMOPT_PIPELINING(3)
CURLMOPT_PIPELINING_SERVER_BL(3)                                                         curl_multi_setopt options                                                         CURLMOPT_PIPELINING_SERVER_BL(3)



NAME
~~~
       CURLMOPT_PIPELINING_SERVER_BL - pipelining server block list

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_PIPELINING_SERVER_BL, char **servers);

~~~
DESCRIPTION
~~~
       No function since pipelining was removed in 7.62.0.

       Pass  a servers array of char *, ending with a NULL entry. This is a list of server types prefixes (in the Server: HTTP header) that are blocked from pipelining, i.e server types that are known to
       not support HTTP pipelining. The array is copied by libcurl.

       Note that the comparison matches if the Server: header begins with the string in the block list, i.e "Server: Ninja 1.2.3" and "Server: Ninja 1.4.0" can both be blocked by having  "Ninja"  in  the
       list.

       Pass a NULL pointer to clear the block list.

DEFAULT
       The default value is NULL, which means that there is no block list.

~~~
PROTOCOLS
~~~
~~~
EXAMPLE
~~~
         char *server_block_list[] =
         {
           "Microsoft-IIS/6.0",
           "nginx/0.8.54",
           NULL
         };

         curl_multi_setopt(m, CURLMOPT_PIPELINING_SERVER_BL, server_block_list);

~~~
AVAILABILITY
~~~
       Added in 7.30.0

~~~
RETURN VALUE
~~~
       Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLMOPT_PIPELINING(3), CURLMOPT_PIPELINING_SITE_BL(3),



libcurl 7.39.0                                                                                   4 Nov 2014                                                                CURLMOPT_PIPELINING_SERVER_BL(3)
CURLMOPT_PIPELINING_SITE_BL(3)                                                           curl_multi_setopt options                                                           CURLMOPT_PIPELINING_SITE_BL(3)



NAME
~~~
       CURLMOPT_PIPELINING_SITE_BL - pipelining host block list

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_PIPELINING_SITE_BL, char **hosts);

~~~
DESCRIPTION
~~~
       No function since pipelining was removed in 7.62.0.

       Pass  a  hosts  array of char *, ending with a NULL entry. This is a list of sites that are blocked from pipelining, i.e sites that are known to not support HTTP pipelining. The array is copied by
       libcurl.

       Pass a NULL pointer to clear the block list.

DEFAULT
       The default value is NULL, which means that there is no block list.

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
         char *site_block_list[] =
         {
           "www.haxx.se",
           "www.example.com:1234",
           NULL
         };

         curl_multi_setopt(m, CURLMOPT_PIPELINING_SITE_BL, site_block_list);

~~~
AVAILABILITY
~~~
       Added in 7.30.0

~~~
RETURN VALUE
~~~
       Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLMOPT_PIPELINING(3), CURLMOPT_PIPELINING_SERVER_BL(3),



libcurl 7.39.0                                                                                   4 Nov 2014                                                                  CURLMOPT_PIPELINING_SITE_BL(3)
CURLMOPT_PUSHDATA(3)                                                                     curl_multi_setopt options                                                                     CURLMOPT_PUSHDATA(3)



NAME
~~~
       CURLMOPT_PUSHDATA - pointer to pass to push callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_PUSHDATA, void *pointer);

~~~
DESCRIPTION
~~~
       Set pointer to pass as the last argument to the CURLMOPT_PUSHFUNCTION(3) callback. The pointer will not be touched or used by libcurl itself, only passed on to the callback function.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       /* only allow pushes for file names starting with "push-" */
       int push_callback(CURL *parent,
                         CURL *easy,
                         size_t num_headers,
                         struct curl_pushheaders *headers,
                         void *userp)
       {
         char *headp;
         int *transfers = (int *)userp;
         FILE *out;
         headp = curl_pushheader_byname(headers, ":path");
         if(headp && !strncmp(headp, "/push-", 6)) {
           fprintf(stderr, "The PATH is %s\n", headp);

           /* save the push here */
           out = fopen("pushed-stream", "wb");

           /* write to this file */
           curl_easy_setopt(easy, CURLOPT_WRITEDATA, out);

           (*transfers)++; /* one more */

           return CURL_PUSH_OK;
         }
         return CURL_PUSH_DENY;
       }

       curl_multi_setopt(multi, CURLMOPT_PUSHFUNCTION, push_callback);
       curl_multi_setopt(multi, CURLMOPT_PUSHDATA, &counter);

~~~
AVAILABILITY
~~~
       Added in 7.44.0

~~~
RETURN VALUE
~~~
       Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLMOPT_PUSHFUNCTION(3), CURLMOPT_PIPELINING(3), CURLOPT_PIPEWAIT(3), RFC7540



libcurl 7.44.0                                                                                   1 Jun 2015                                                                            CURLMOPT_PUSHDATA(3)
CURLMOPT_PUSHFUNCTION(3)                                                                 curl_multi_setopt options                                                                 CURLMOPT_PUSHFUNCTION(3)



NAME
~~~
       CURLMOPT_PUSHFUNCTION - callback that approves or denies server pushes

~~~
NAME
~~~
       #include <curl/curl.h>

       char *curl_pushheader_bynum(struct curl_pushheaders *h, size_t num);
       char *curl_pushheader_byname(struct curl_pushheaders *h, const char *name);

       int curl_push_callback(CURL *parent,
                              CURL *easy,
                              size_t num_headers,
                              struct curl_pushheaders *headers,
                              void *userp);

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_PUSHFUNCTION,
                                   curl_push_callback func);

~~~
DESCRIPTION
~~~
       This callback gets called when a new HTTP/2 stream is being pushed by the server (using the PUSH_PROMISE frame). If no push callback is set, all offered pushes will be denied automatically.

CALLBACK ~~~
DESCRIPTION
~~~
       The callback gets its arguments like this:

       parent  is  the  handle  of  the stream on which this push arrives. The new handle has been duphandle()d from the parent, meaning that it has gotten all its options inherited. It is then up to the
       application to alter any options if desired.

       easy is a newly created handle that represents this upcoming transfer.

       num_headers is the number of name+value pairs that was received and can be accessed

       headers is a handle used to access push headers using the accessor functions described below. This only accesses and provides the PUSH_PROMISE headers, the normal response headers will be provided
       in the header callback as usual.

       userp is the pointer set with CURLMOPT_PUSHDATA(3)

       If the callback returns CURL_PUSH_OK, the 'easy' handle will be added to the multi handle, the callback must not do that by itself.

       The  callback  can access PUSH_PROMISE headers with two accessor functions. These functions can only be used from within this callback and they can only access the PUSH_PROMISE headers. The normal
       response headers will be passed to the header callback for pushed streams just as for normal streams.

       curl_pushheader_bynum
              Returns the header at index 'num' (or NULL). The returned pointer points to a "name:value" string that will be freed when this callback returns.

       curl_pushheader_byname
              Returns the value for the given header name (or NULL). This is a shortcut so that the application doesn't have to loop through all headers to find the one it  is  interested  in.  The  data
              pointed will be freed when this callback returns. If more than one header field use the same name, this returns only the first one.

CALLBACK ~~~
RETURN VALUE
~~~
       CURL_PUSH_OK (0)
              The application has accepted the stream and it can now start receiving data, the ownership of the CURL handle has been taken over by the application.

       CURL_PUSH_DENY (1)
              The callback denies the stream and no data for this will reach the application, the easy handle will be destroyed by libcurl.

       CURL_PUSH_ERROROUT (2)
              Returning this will reject the pushed stream and return an error back on the parent stream making it get closed with an error. (Added in curl 7.72.0)

       *      All other return codes are reserved for future use.

DEFAULT
       NULL, no callback

~~~
PROTOCOLS
~~~
       HTTP(S) (HTTP/2 only)

~~~
EXAMPLE
~~~
       /* only allow pushes for file names starting with "push-" */
       int push_callback(CURL *parent,
                         CURL *easy,
                         size_t num_headers,
                         struct curl_pushheaders *headers,
                         void *userp)
       {
         char *headp;
         int *transfers = (int *)userp;
         FILE *out;
         headp = curl_pushheader_byname(headers, ":path");
         if(headp && !strncmp(headp, "/push-", 6)) {
           fprintf(stderr, "The PATH is %s\n", headp);

           /* save the push here */
           out = fopen("pushed-stream", "wb");

           /* write to this file */
           curl_easy_setopt(easy, CURLOPT_WRITEDATA, out);

           (*transfers)++; /* one more */

           return CURL_PUSH_OK;
         }
         return CURL_PUSH_DENY;
       }

       curl_multi_setopt(multi, CURLMOPT_PUSHFUNCTION, push_callback);
       curl_multi_setopt(multi, CURLMOPT_PUSHDATA, &counter);

~~~
AVAILABILITY
~~~
       Added in 7.44.0

~~~
RETURN VALUE
~~~
       Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLMOPT_PUSHDATA(3), CURLMOPT_PIPELINING(3), CURLOPT_PIPEWAIT(3), RFC7540



libcurl 7.44.0                                                                                   1 Jun 2015                                                                        CURLMOPT_PUSHFUNCTION(3)
CURLMOPT_SOCKETDATA(3)                                                                   curl_multi_setopt options                                                                   CURLMOPT_SOCKETDATA(3)



NAME
~~~
       CURLMOPT_SOCKETDATA - custom pointer passed to the socket callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_SOCKETDATA, void *pointer);

~~~
DESCRIPTION
~~~
       A data pointer to pass to the socket callback set with the CURLMOPT_SOCKETFUNCTION(3) option.

       This pointer will not be touched by libcurl but will only be passed in to the socket callbacks's userp argument.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       static int sock_cb(CURL *e, curl_socket_t s, int what, void *cbp, void *sockp)
       {
         GlobalInfo *g = (GlobalInfo*) cbp;
         SockInfo *fdp = (SockInfo*) sockp;

         if(what == CURL_POLL_REMOVE) {
           remsock(fdp);
         }
         else {
           if(!fdp) {
             addsock(s, e, what, g);
           }
           else {
             setsock(fdp, s, e, what, g);
           }
         }
         return 0;
       }

       main()
       {
         GlobalInfo setup;
         /* ... use socket callback and custom pointer */
         curl_multi_setopt(multi, CURLMOPT_SOCKETFUNCTION, sock_cb);
         curl_multi_setopt(multi, CURLMOPT_SOCKETDATA, &setup);
       }

~~~
AVAILABILITY
~~~
       Added in 7.15.4

~~~
RETURN VALUE
~~~
       Returns CURLM_OK.

~~~
SEE ALSO
       CURLMOPT_SOCKETFUNCTION(3), curl_multi_socket_action(3), CURLMOPT_TIMERFUNCTION(3)



libcurl 7.39.0                                                                                   3 Nov 2014                                                                          CURLMOPT_SOCKETDATA(3)
CURLMOPT_SOCKETFUNCTION(3)                                                               curl_multi_setopt options                                                               CURLMOPT_SOCKETFUNCTION(3)



NAME
~~~
       CURLMOPT_SOCKETFUNCTION - callback informed about what to wait for

~~~
NAME
~~~
       #include <curl/curl.h>

       int socket_callback(CURL *easy,      /* easy handle */
                           curl_socket_t s, /* socket */
                           int what,        /* describes the socket */
                           void *userp,     /* private callback pointer */
                           void *socketp);  /* private socket pointer */

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_SOCKETFUNCTION, socket_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       When  the  curl_multi_socket_action(3) function is called, it informs the application about updates in the socket (file descriptor) status by doing none, one, or multiple calls to the socket_callback. The callback function gets status updates with changes since the previous time the callback was called. If the given callback pointer is set to NULL, no callback will be called.

CALLBACK ARGUMENTS
       easy identifies the specific transfer for which this update is related.

       s is the specific socket this function invocation concerns. If the what argument is not CURL_POLL_REMOVE then it holds information about what activity on this socket the application is supposed to
       monitor. Subsequent calls to this callback might update the what bits for a socket that is already monitored.

       userp is set with CURLMOPT_SOCKETDATA(3).

       socketp is set with curl_multi_assign(3) or will be NULL.

       The what parameter informs the callback on the status of the given socket. It can hold one of these values:

       CURL_POLL_IN
              Wait for incoming data. For the socket to become readable.

       CURL_POLL_OUT
              Wait for outgoing data. For the socket to become writable.

       CURL_POLL_INOUT
              Wait for incoming and outgoing data. For the socket to become readable or writable.

       CURL_POLL_REMOVE
              The specified socket/file descriptor is no longer used by libcurl.

DEFAULT
       NULL (no callback)

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       static int sock_cb(CURL *e, curl_socket_t s, int what, void *cbp, void *sockp)
       {
         GlobalInfo *g = (GlobalInfo*) cbp;
         SockInfo *fdp = (SockInfo*) sockp;

         if(what == CURL_POLL_REMOVE) {
           remsock(fdp);
         }
         else {
           if(!fdp) {
             addsock(s, e, what, g);
           }
           else {
             setsock(fdp, s, e, what, g);
           }
         }
         return 0;
       }

       main()
       {
         GlobalInfo setup;
         /* ... use socket callback and custom pointer */
         curl_multi_setopt(multi, CURLMOPT_SOCKETFUNCTION, sock_cb);
         curl_multi_setopt(multi, CURLMOPT_SOCKETDATA, &setup);
       }

~~~
AVAILABILITY
~~~
       Added in 7.15.4

~~~
RETURN VALUE
~~~
       Returns CURLM_OK.

~~~
SEE ALSO
       CURLMOPT_SOCKETDATA(3), curl_multi_socket_action(3), CURLMOPT_TIMERFUNCTION(3)



libcurl 7.39.0                                                                                   3 Nov 2016                                                                      CURLMOPT_SOCKETFUNCTION(3)
CURLMOPT_TIMERDATA(3)                                                                    curl_multi_setopt options                                                                    CURLMOPT_TIMERDATA(3)



NAME
~~~
       CURLMOPT_TIMERDATA - custom pointer to pass to timer callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_TIMERDATA, void *pointer);

~~~
DESCRIPTION
~~~
       A data pointer to pass to the timer callback set with the CURLMOPT_TIMERFUNCTION(3) option.

       This pointer will not be touched by libcurl but will only be passed in to the timer callbacks's userp argument.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       static gboolean timeout_cb(gpointer user_data)
       {
         int running;
         if(user_data) {
           g_free(user_data);
           curl_multi_setopt(curl_handle, CURLMOPT_TIMERDATA, NULL);
         }
         curl_multi_socket_action(multi, CURL_SOCKET_TIMEOUT, 0, &running);
         return G_SOURCE_REMOVE;
       }

       static int timerfunc(CURLM *multi, long timeout_ms, void *userp)
       {
         guint *id = userp;

         if(id)
           g_source_remove(*id);

         /* -1 means we should just delete our timer. */
         if(timeout_ms == -1) {
           g_free(id);
           id = NULL;
         }
         else {
           if(!id)
             id = g_new(guint, 1);
           *id = g_timeout_add(timeout_ms, timeout_cb, id);
         }
         curl_multi_setopt(multi, CURLMOPT_TIMERDATA, id);
         return 0;
       }

       curl_multi_setopt(multi, CURLMOPT_TIMERFUNCTION, timerfunc);

~~~
AVAILABILITY
~~~
       Added in 7.16.0

~~~
RETURN VALUE
~~~
       Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLMOPT_TIMERFUNCTION(3), CURLMOPT_SOCKETFUNCTION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                           CURLMOPT_TIMERDATA(3)
CURLMOPT_TIMERFUNCTION(3)                                                                curl_multi_setopt options                                                                CURLMOPT_TIMERFUNCTION(3)



NAME
~~~
       CURLMOPT_TIMERFUNCTION - callback to receive timeout values

~~~
NAME
~~~
       #include <curl/curl.h>

       int timer_callback(CURLM *multi,    /* multi handle */
                          long timeout_ms, /* timeout in number of ms */
                          void *userp);    /* private callback pointer */

       CURLMcode curl_multi_setopt(CURLM *handle, CURLMOPT_TIMERFUNCTION, timer_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       Certain features, such as timeouts and retries, require you to call libcurl even when there is no activity on the file descriptors.

       Your  callback function timer_callback should install a non-repeating timer with an interval of timeout_ms. When that timer fires, call either curl_multi_socket_action(3) or curl_multi_perform(3),
       depending on which interface you use.

       A timeout_ms value of -1 passed to this callback means you should delete the timer. All other values are valid expire times in number of milliseconds.

       The timer_callback will only be called when the timeout expire time is changed.

       The userp pointer is set with CURLMOPT_TIMERDATA(3).

       The timer callback should return 0 on success, and -1 on error. This callback can be used instead of, or in addition to, curl_multi_timeout(3).

       WARNING: even if it feels tempting, avoid calling libcurl directly from within the callback itself when the timeout_ms value is zero, since it risks triggering  an  unpleasant  recursive  behavior
       that immediately calls another call to the callback with a zero timeout...

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       static gboolean timeout_cb(gpointer user_data)
       {
         int running;
         if(user_data) {
           g_free(user_data);
           curl_multi_setopt(curl_handle, CURLMOPT_TIMERDATA, NULL);
         }
         curl_multi_socket_action(multi, CURL_SOCKET_TIMEOUT, 0, &running);
         return G_SOURCE_REMOVE;
       }

       static int timerfunc(CURLM *multi, long timeout_ms, void *userp)
       {
         guint *id = userp;

         if(id)
           g_source_remove(*id);

         /* -1 means we should just delete our timer. */
         if(timeout_ms == -1) {
           g_free(id);
           id = NULL;
         }
         else {
           if(!id)
             id = g_new(guint, 1);
           *id = g_timeout_add(timeout_ms, timeout_cb, id);
         }
         curl_multi_setopt(multi, CURLMOPT_TIMERDATA, id);
         return 0;
       }

       curl_multi_setopt(multi, CURLMOPT_TIMERFUNCTION, timerfunc);

~~~
AVAILABILITY
~~~
       Added in 7.16.0

~~~
RETURN VALUE
~~~
       Returns CURLM_OK if the option is supported, and CURLM_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLMOPT_TIMERDATA(3), CURLMOPT_SOCKETFUNCTION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                       CURLMOPT_TIMERFUNCTION(3)
CURLOPT_ABSTRACT_UNIX_SOCKET(3)                                                           curl_easy_setopt options                                                          CURLOPT_ABSTRACT_UNIX_SOCKET(3)



NAME
~~~
       CURLOPT_ABSTRACT_UNIX_SOCKET - abstract Unix domain socket

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_ABSTRACT_UNIX_SOCKET, char *path);

~~~
DESCRIPTION
~~~
       Enables  the  use of an abstract Unix domain socket instead of establishing a TCP connection to a host. The parameter should be a char * to a null-terminated string holding the path of the socket.
       The path will be set to path prefixed by a NULL byte (this is the convention for abstract sockets, however it should be stressed that the path passed to this function should not contain a  leading
       NULL).

       On non-supporting platforms, the abstract address will be interpreted as an empty string and fail gracefully, generating a run-time error.

       This  option  shares the same semantics as CURLOPT_UNIX_SOCKET_PATH(3) in which documentation more details can be found. Internally, these two options share the same storage and therefore only one
       of them can be set per handle.

DEFAULT
       Default is NULL.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
         curl_easy_setopt(curl_handle, CURLOPT_ABSTRACT_UNIX_SOCKET, "/tmp/foo.sock");
         curl_easy_setopt(curl_handle, CURLOPT_URL, "http://localhost/");


~~~
AVAILABILITY
~~~
       Since 7.53.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_UNIX_SOCKET_PATH(3), unix(7),



libcurl 7.53.0                                                                                  08 Jan 2017                                                                 CURLOPT_ABSTRACT_UNIX_SOCKET(3)
CURLOPT_ACCEPT_ENCODING(3)                                                                curl_easy_setopt options                                                               CURLOPT_ACCEPT_ENCODING(3)



NAME
~~~
       CURLOPT_ACCEPT_ENCODING - automatic decompression of HTTP downloads

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_ACCEPT_ENCODING, char *enc);

~~~
DESCRIPTION
~~~
       Pass a char * argument specifying what encoding you'd like.

       Sets the contents of the Accept-Encoding: header sent in an HTTP request, and enables decoding of a response when a Content-Encoding: header is received.

       libcurl potentially supports several different compressed encodings depending on what support that has been built-in.

       To  aid  applications  not having to bother about what specific algorithms this particular libcurl build supports, libcurl allows a zero-length string to be set ("") to ask for an Accept-Encoding:
       header to be used that contains all built-in supported encodings.

       Alternatively, you can specify exactly the encoding or list of encodings you want in the response. Four encodings are supported: identity, meaning non-compressed, deflate which requests the server
       to  compress  its response using the zlib algorithm, gzip which requests the gzip algorithm, (since curl 7.57.0) br which is brotli and (since curl 7.72.0) zstd which is zstd.  Provide them in the
       string as a comma-separated list of accepted encodings, like:

         "br, gzip, deflate".

       Set CURLOPT_ACCEPT_ENCODING(3) to NULL to explicitly disable it, which makes libcurl not send an Accept-Encoding: header and not decompress received contents automatically.

       You can also opt to just include the Accept-Encoding: header in your request with CURLOPT_HTTPHEADER(3) but then there will be no automatic decompressing when receiving data.

       This is a request, not an order; the server may or may not do it.  This option must be set (to any non-NULL value) or else any unsolicited encoding done by the server is ignored.

       Servers might respond with Content-Encoding even without getting a Accept-Encoding: in the request. Servers might respond with a different Content-Encoding than what was asked for in the request.

       The Content-Length: servers send for a compressed response is supposed to indicate the length of the compressed content so when auto decoding is enabled it may not match the sum of bytes  reported
       by the write callbacks (although, sending the length of the non-compressed content is a common server mistake).

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* enable all supported built-in compressions */
         curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING, "");

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       This option was called CURLOPT_ENCODING before 7.21.6

       The  specific  libcurl  you're  using  must  have been built with zlib to be able to decompress gzip and deflate responses, with the brotli library to decompress brotli responses and with the zstd
       library to decompress zstd responses.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_TRANSFER_ENCODING(3), CURLOPT_HTTPHEADER(3), CURLOPT_HTTP_CONTENT_DECODING(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                      CURLOPT_ACCEPT_ENCODING(3)
CURLOPT_ACCEPTTIMEOUT_MS(3)                                                               curl_easy_setopt options                                                              CURLOPT_ACCEPTTIMEOUT_MS(3)



NAME
~~~
       CURLOPT_ACCEPTTIMEOUT_MS - timeout waiting for FTP server to connect back

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_ACCEPTTIMEOUT_MS, long ms);

~~~
DESCRIPTION
~~~
       Pass a long telling libcurl the maximum number of milliseconds to wait for a server to connect back to libcurl when an active FTP connection is used.

DEFAULT
       60000 milliseconds

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/path/file");

         /* wait no more than 5 seconds for FTP server responses */
         curl_easy_setopt(curl, CURLOPT_ACCEPTTIMEOUT_MS, 5000L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.24.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_STDERR(3), CURLOPT_DEBUGFUNCTION(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                     CURLOPT_ACCEPTTIMEOUT_MS(3)
CURLOPT_ADDRESS_SCOPE(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_ADDRESS_SCOPE(3)



NAME
~~~
       CURLOPT_ADDRESS_SCOPE - scope id for IPv6 addresses

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_ADDRESS_SCOPE, long scope);

~~~
DESCRIPTION
~~~
       Pass a long specifying the scope id value to use when connecting to IPv6 addresses.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       All, when using IPv6

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         long my_scope_id;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         my_scope_id = if_nametoindex("eth0");
         curl_easy_setopt(curl, CURLOPT_ADDRESS_SCOPE, my_scope_id);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.  Returns CURLE_BAD_FUNCTION_ARGUMENT if set to a negative value.

~~~
SEE ALSO
       CURLOPT_STDERR(3), CURLOPT_DEBUGFUNCTION(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                        CURLOPT_ADDRESS_SCOPE(3)
CURLOPT_ALTSVC(3)                                                                         curl_easy_setopt options                                                                        CURLOPT_ALTSVC(3)



NAME
~~~
       CURLOPT_ALTSVC - alt-svc cache file name

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_ALTSVC, char *filename);

~~~
DESCRIPTION
~~~
       Pass  in  a  pointer  to  a  filename  to  instruct libcurl to use that file as the Alt-Svc cache to read existing cache contents from and possibly also write it back to a after a transfer, unless
       CURLALTSVC_READONLYFILE is set in CURLOPT_ALTSVC_CTRL(3).

       Specify a blank file name ("") to make libcurl not load from a file at all.

DEFAULT
       NULL. The alt-svc cache is not read nor written to file.

~~~
PROTOCOLS
~~~
       HTTPS

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_ALTSVC_CTRL, CURLALTSVC_H1);
         curl_easy_setopt(curl, CURLOPT_ALTSVC, "altsvc-cache.txt");
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.64.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_ALTSVC_CTRL(3), CURLOPT_CONNECT_TO(3), CURLOPT_RESOLVE(3), CURLOPT_COOKIEFILE(3),



libcurl 7.64.1                                                                                   5 Feb 2019                                                                               CURLOPT_ALTSVC(3)
CURLOPT_ALTSVC_CTRL(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_ALTSVC_CTRL(3)



NAME
~~~
       CURLOPT_ALTSVC_CTRL - control alt-svc behavior

~~~
NAME
~~~
       #include <curl/curl.h>

       #define CURLALTSVC_READONLYFILE (1<<2)
       #define CURLALTSVC_H1           (1<<3)
       #define CURLALTSVC_H2           (1<<4)
       #define CURLALTSVC_H3           (1<<5)

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_ALTSVC_CTRL, long bitmask);

~~~
DESCRIPTION
~~~
       Populate the long bitmask with the correct set of features to instruct libcurl how to handle Alt-Svc for the transfers using this handle.

       libcurl  will  only accept Alt-Svc headers over a secure transport, meaning HTTPS. It will also only complete a request to an alternative origin if that origin is properly hosted over HTTPS. These
       requirements are there to make sure both the source and the destination are legitimate.

       Setting any bit will enable the alt-svc engine.

       CURLALTSVC_READONLYFILE
              Do not write the alt-svc cache back to the file specified with CURLOPT_ALTSVC(3) even if it gets updated. By default a file specified with that option will be read and written to as  deemed
              necessary.

       CURLALTSVC_H1
              Accept alternative services offered over HTTP/1.1.

       CURLALTSVC_H2
              Accept alternative services offered over HTTP/2. This will only be used if libcurl was also built to actually support HTTP/2, otherwise this bit will be ignored.

       CURLALTSVC_H3
              Accept alternative services offered over HTTP/3. This will only be used if libcurl was also built to actually support HTTP/3, otherwise this bit will be ignored.

DEFAULT
       Alt-Svc  handling  is  disabled  by default. If CURLOPT_ALTSVC(3) is set, CURLOPT_ALTSVC_CTRL(3) has a default value corresponding to CURLALTSVC_H1 | CURLALTSVC_H2 | CURLALTSVC_H3 - the HTTP/2 and
       HTTP/3 bits are only set if libcurl was built with support for those versions.

~~~
PROTOCOLS
~~~
       HTTPS

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_ALTSVC_CTRL, CURLALTSVC_H1);
         curl_easy_setopt(curl, CURLOPT_ALTSVC, "altsvc-cache.txt");
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.64.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_ALTSVC(3), CURLOPT_CONNECT_TO(3), CURLOPT_RESOLVE(3),



libcurl 7.64.1                                                                                   5 Feb 2019                                                                          CURLOPT_ALTSVC_CTRL(3)
CURLOPT_APPEND(3)                                                                         curl_easy_setopt options                                                                        CURLOPT_APPEND(3)



NAME
~~~
       CURLOPT_APPEND - append to the remote file

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_APPEND, long append);

~~~
DESCRIPTION
~~~
       A long parameter set to 1 tells the library to append to the remote file instead of overwrite it. This is only useful when uploading to an FTP site.

DEFAULT
       0 (disabled)

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {

         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/dir/to/newfile");
         curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
         curl_easy_setopt(curl, CURLOPT_APPEND, 1L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       This option was known as CURLOPT_FTPAPPEND up to 7.16.4

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_DIRLISTONLY(3), CURLOPT_RESUME_FROM(3), CURLOPT_UPLOAD(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                               CURLOPT_APPEND(3)
CURLOPT_AUTOREFERER(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_AUTOREFERER(3)



NAME
~~~
       CURLOPT_AUTOREFERER - automatically update the referer header

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_AUTOREFERER, long autorefer);

~~~
DESCRIPTION
~~~
       Pass a parameter set to 1 to enable this. When enabled, libcurl will automatically set the Referer: header field in HTTP requests to the full URL where it follows a Location: redirect.

DEFAULT
       0, disabled

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

         /* follow redirects */
         curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

         /* set Referer: automatically when following redirects */
         curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 1L);

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Along with HTTP

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_REFERER(3), CURLOPT_FOLLOWLOCATION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                          CURLOPT_AUTOREFERER(3)
CURLOPT_AWS_SIGV4(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_AWS_SIGV4(3)



NAME
~~~
       CURLOPT_AWS_SIGV4 - V4 signature

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_AWS_SIGV4, char *param);

~~~
DESCRIPTION
~~~
       Provides AWS V4 signature authentication on HTTP(S) header.

       Pass a char * that is the collection of specific arguments are used for creating outgoing authentication headers.  The format of the param option is:

       provider1[:provider2[:region[:service]]]

       provider1, provider2
              The providers arguments are used for generating some authentication parameters such as "Algorithm", "date", "request type" and "signed headers".

       region The argument is a geographic area of a resources collection.  It is extracted from the host name specified in the URL if omitted.

       service
              The argument is a function provided by a cloud.  It is extracted from the host name specified in the URL if omitted.

       NOTE: This call set CURLOPT_HTTPAUTH(3) to CURLAUTH_AWS_SIGV4.  Calling CURLOPT_HTTPAUTH(3) with CURLAUTH_AWS_SIGV4 is the same as calling this with "aws:amz" in parameter.

       Example  with  "Test:Try",  when  curl  will  do  the  algorithm, it will generate "TEST-HMAC-SHA256" for "Algorithm", "x-try-date" and "X-Try-Date" for "date", "test4_request" for "request type",
       "SignedHeaders=content-type;host;x-try-date" for "signed headers"

       If you use just "test", instead of "test:try", test will be use for every strings generated

DEFAULT
       By default, the value of this parameter is NULL.  Calling CURLOPT_HTTPAUTH(3) with CURLAUTH_AWS_SIGV4 is the same as calling this with "aws:amz" in parameter.

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();

       struct curl_slist *list = NULL;

       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL,
                         "https://service.region.example.com/uri");
         curl_easy_setopt(c, CURLOPT_AWS_SIGV4, "provider1:provider2");

         /* service and region also could be set in CURLOPT_AWS_SIGV4 */
         /*
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/uri");
         curl_easy_setopt(c, CURLOPT_AWS_SIGV4,
                          "provider1:provider2:region:service");
         */

         curl_easy_setopt(c, CURLOPT_USERPWD, "MY_ACCESS_KEY:MY_SECRET_KEY");
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.75.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

NOTES
       This option overrides the other auth types you might have set in CURL_HTTPAUTH which should be highlighted as this makes this auth method special.  This method can't be combined  with  other  auth
       types.

~~~
SEE ALSO
       CURLOPT_HEADEROPT(3), CURLOPT_HTTPHEADER(3),



libcurl 7.75.0                                                                                  03 Jun 2020                                                                            CURLOPT_AWS_SIGV4(3)
CURLOPT_BUFFERSIZE(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_BUFFERSIZE(3)



NAME
~~~
       CURLOPT_BUFFERSIZE - receive buffer size

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_BUFFERSIZE, long size);

~~~
DESCRIPTION
~~~
       Pass  a  long  specifying your preferred size (in bytes) for the receive buffer in libcurl.  The main point of this would be that the write callback gets called more often and with smaller chunks.
       Secondly, for some protocols, there's a benefit of having a larger buffer for performance.

       This is just treated as a request, not an order. You cannot be guaranteed to actually get the given size.

       This buffer size is by default CURL_MAX_WRITE_SIZE (16kB). The maximum buffer size allowed to be set is CURL_MAX_READ_SIZE (512kB). The minimum buffer size allowed to be set is 1024.

       DO NOT set this option on a handle that is currently used for an active transfer as that may lead to unintended consequences.

DEFAULT
       CURL_MAX_WRITE_SIZE (16kB)

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "sftp://example.com/foo.bin");

         /* ask libcurl to allocate a larger receive buffer */
         curl_easy_setopt(curl, CURLOPT_BUFFERSIZE, 120000L);

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.10.  Growing the buffer was added in 7.53.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_MAX_RECV_SPEED_LARGE(3), CURLOPT_WRITEFUNCTION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                           CURLOPT_BUFFERSIZE(3)
CURLOPT_CAINFO(3)                                                                         curl_easy_setopt options                                                                        CURLOPT_CAINFO(3)



NAME
~~~
       CURLOPT_CAINFO - path to Certificate Authority (CA) bundle

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CAINFO, char *path);

~~~
DESCRIPTION
~~~
       Pass a char * to a null-terminated string naming a file holding one or more certificates to verify the peer with.

       If CURLOPT_SSL_VERIFYPEER(3) is zero and you avoid verifying the server's certificate, CURLOPT_CAINFO(3) need not even indicate an accessible file.

       This option is by default set to the system path where libcurl's cacert bundle is assumed to be stored, as established at build time.

       If  curl  is  built  against  the  NSS  SSL  library,  the  NSS  PEM  PKCS#11 module (libnsspem.so) needs to be available for this option to work properly.  Starting with curl-7.55.0, if both CURLOPT_CAINFO(3) and CURLOPT_CAPATH(3) are unset, NSS-linked libcurl tries to load libnssckbi.so, which contains a more comprehensive set of trust information than supported by nss-pem, because libnssckbi.so also includes information about distrusted certificates.

       (iOS and macOS) When curl uses Secure Transport this option is supported. If the option is not set, then curl will use the certificates in the system and user Keychain to verify the peer.

       (Schannel)  This option is supported for Schannel in Windows 7 or later but we recommend not using it until Windows 8 since it works better starting then.  If the option is not set, then curl will
       use the certificates in the Windows' store of root certificates (the default for Schannel).

       The application does not have to keep the string around after setting this option.

DEFAULT
       Built-in system specific. When curl is built with Secure Transport or Schannel, this option is not set by default.

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_CAINFO, "/etc/certs/cabundle.pem");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       For the SSL engines that don't support certificate files the CURLOPT_CAINFO option is ignored. Schannel support added in libcurl 7.60.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_CAINFO_BLOB(3), CURLOPT_CAPATH(3), CURLOPT_SSL_VERIFYPEER(3), CURLOPT_SSL_VERIFYHOST(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                               CURLOPT_CAINFO(3)
CURLOPT_CAINFO_BLOB(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_CAINFO_BLOB(3)



NAME
~~~
       CURLOPT_CAINFO_BLOB - Certificate Authority (CA) bundle in PEM format

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CAINFO_BLOB, struct curl_blob *stblob);

~~~
DESCRIPTION
~~~
       Pass  a  pointer to a curl_blob structure, which contains information (pointer and size) about a memory block with binary data of PEM encoded content holding one or more certificates to verify the
       HTTPS server with.

       If CURLOPT_SSL_VERIFYPEER(3) is zero and you avoid verifying the server's certificate, CURLOPT_CAINFO_BLOB(3) is not needed.

       This option overrides CURLOPT_CAINFO(3).

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       char *strpem; /* strpem must point to a PEM string */
       CURL *curl = curl_easy_init();
       if(curl) {
         struct curl_blob blob;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         blob.data = strpem;
         blob.len = strlen(strpem);
         blob.flags = CURL_BLOB_COPY;
         curl_easy_setopt(curl, CURLOPT_CAINFO_BLOB, &blob);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.77.0.

       This option is supported by the BearSSL (since 7.79.0), OpenSSL, Secure Transport and Schannel backends.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_CAINFO(3), CURLOPT_CAPATH(3), CURLOPT_SSL_VERIFYPEER(3), CURLOPT_SSL_VERIFYHOST(3),



libcurl 7.77.0                                                                                 31 March 2021                                                                         CURLOPT_CAINFO_BLOB(3)
CURLOPT_CAPATH(3)                                                                         curl_easy_setopt options                                                                        CURLOPT_CAPATH(3)



NAME
~~~
       CURLOPT_CAPATH - directory holding CA certificates

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CAPATH, char *capath);

~~~
DESCRIPTION
~~~
       Pass  a  char * to a null-terminated string naming a directory holding multiple CA certificates to verify the peer with. If libcurl is built against OpenSSL, the certificate directory must be prepared using the openssl c_rehash utility.  This makes sense only when used in combination with the CURLOPT_SSL_VERIFYPEER(3) option.

       The CURLOPT_CAPATH(3) function apparently does not work in Windows due to some limitation in openssl.

       The application does not have to keep the string around after setting this option.

DEFAULT
       A default path detected at build time.

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_CAPATH, "/etc/cert-dir");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       This option is supported by the OpenSSL, GnuTLS and mbedTLS (since 7.56.0) backends. The NSS backend provides the option only for backward compatibility.

~~~
RETURN VALUE
~~~
       CURLE_OK if supported; or an error such as:

       CURLE_NOT_BUILT_IN - Not supported by the SSL backend

       CURLE_UNKNOWN_OPTION

       CURLE_OUT_OF_MEMORY

~~~
SEE ALSO
       CURLOPT_CAINFO(3), CURLOPT_STDERR(3), CURLOPT_DEBUGFUNCTION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                               CURLOPT_CAPATH(3)
CURLOPT_CERTINFO(3)                                                                       curl_easy_setopt options                                                                      CURLOPT_CERTINFO(3)



NAME
~~~
       CURLOPT_CERTINFO - request SSL certificate information

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CERTINFO, long certinfo);

~~~
DESCRIPTION
~~~
       Pass  a long set to 1 to enable libcurl's certificate chain info gatherer. With this enabled, libcurl will extract lots of information and data about the certificates in the certificate chain used
       in the SSL connection. This data may then be retrieved after a transfer using curl_easy_getinfo(3) and its option CURLINFO_CERTINFO(3).

DEFAULT
       0

~~~
PROTOCOLS
~~~
       All TLS-based

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://www.example.com/");

         /* connect to any HTTPS site, trusted or not */
         curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
         curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

         curl_easy_setopt(curl, CURLOPT_CERTINFO, 1L);

         res = curl_easy_perform(curl);

         if (!res) {
           struct curl_certinfo *ci;
           res = curl_easy_getinfo(curl, CURLINFO_CERTINFO, &ci);

           if (!res) {
             printf("%d certs!\n", ci->num_of_certs);

             for(i = 0; i < ci->num_of_certs; i++) {
               struct curl_slist *slist;

               for(slist = ci->certinfo[i]; slist; slist = slist->next)
                 printf("%s\n", slist->data);
             }
           }
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       This option is supported by the OpenSSL, GnuTLS, Schannel, NSS, GSKit and Secure Transport backends. Schannel support added in 7.50.0. Secure Transport support added in 7.79.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_CAINFO(3), CURLOPT_SSL_VERIFYPEER(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                             CURLOPT_CERTINFO(3)
CURLOPT_CHUNK_BGN_FUNCTION(3)                                                             curl_easy_setopt options                                                            CURLOPT_CHUNK_BGN_FUNCTION(3)



NAME
~~~
       CURLOPT_CHUNK_BGN_FUNCTION - callback before a transfer with FTP wildcardmatch

~~~
NAME
~~~
       #include <curl/curl.h>

       struct curl_fileinfo {
         char *filename;
         curlfiletype filetype;
         time_t time;   /* always zero! */
         unsigned int perm;
         int uid;
         int gid;
         curl_off_t size;
         long int hardlinks;

         struct {
           /* If some of these fields is not NULL, it is a pointer to b_data. */
           char *time;
           char *perm;
           char *user;
           char *group;
           char *target; /* pointer to the target filename of a symlink */
         } strings;

         unsigned int flags;

         /* used internally */
         char *b_data;
         size_t b_size;
         size_t b_used;
       };

       long chunk_bgn_callback(const void *transfer_info, void *ptr,
                               int remains);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CHUNK_BGN_FUNCTION,
                                 chunk_bgn_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       This callback function gets called by libcurl before a part of the stream is going to be transferred (if the transfer supports chunks).

       The transfer_info pointer will point to a struct curl_fileinfo with details about the file that is about to get transferred.

       This callback makes sense only when using the CURLOPT_WILDCARDMATCH(3) option for now.

       The  target  of  transfer_info  parameter is a "feature depended" structure. For the FTP wildcard download, the target is curl_fileinfo structure (see curl/curl.h).  The parameter ptr is a pointer
       given by CURLOPT_CHUNK_DATA(3). The parameter remains contains number of chunks remaining per the transfer. If the feature is not available, the parameter has zero value.

       Return CURL_CHUNK_BGN_FUNC_OK if everything is fine, CURL_CHUNK_BGN_FUNC_SKIP if you want to skip the concrete chunk or CURL_CHUNK_BGN_FUNC_FAIL to tell libcurl to stop if some error occurred.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       static long file_is_coming(struct curl_fileinfo *finfo,
                                  struct callback_data *data,
                                  int remains)
       {
         printf("%3d %40s %10luB ", remains, finfo->filename,
                (unsigned long)finfo->size);

         switch(finfo->filetype) {
         case CURLFILETYPE_DIRECTORY:
           printf(" DIR\n");
           break;
         case CURLFILETYPE_FILE:
           printf("FILE ");
           break;
         default:
           printf("OTHER\n");
           break;
         }

         if(finfo->filetype == CURLFILETYPE_FILE) {
           /* do not transfer files >= 50B */
           if(finfo->size > 50) {
             printf("SKIPPED\n");
             return CURL_CHUNK_BGN_FUNC_SKIP;
           }

           data->output = fopen(finfo->filename, "wb");
           if(!data->output) {
             return CURL_CHUNK_BGN_FUNC_FAIL;
           }
         }

         return CURL_CHUNK_BGN_FUNC_OK;
       }

       int main()
       {
         /* data for callback */
         struct callback_data callback_info;

         /* callback is called before download of concrete file started */
         curl_easy_setopt(curl, CURLOPT_CHUNK_BGN_FUNCTION, file_is_coming);
         curl_easy_setopt(curl, CURLOPT_CHUNK_DATA, &callback_info);
       }

~~~
AVAILABILITY
~~~
       This was added in 7.21.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_CHUNK_END_FUNCTION(3), CURLOPT_WILDCARDMATCH(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                   CURLOPT_CHUNK_BGN_FUNCTION(3)
CURLOPT_CHUNK_DATA(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_CHUNK_DATA(3)



NAME
~~~
       CURLOPT_CHUNK_DATA - custom pointer to the FTP chunk callbacks

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CHUNK_DATA, void *pointer);

~~~
DESCRIPTION
~~~
       Pass a pointer that will be untouched by libcurl and passed as the ptr argument to the CURLOPT_CHUNK_BGN_FUNCTION(3) and CURLOPT_CHUNK_END_FUNCTION(3).

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       static long file_is_coming(struct curl_fileinfo *finfo,
                                  struct callback_data *data,
                                  int remains)
       {
         printf("%3d %40s %10luB ", remains, finfo->filename,
                (unsigned long)finfo->size);

         switch(finfo->filetype) {
         case CURLFILETYPE_DIRECTORY:
           printf(" DIR\n");
           break;
         case CURLFILETYPE_FILE:
           printf("FILE ");
           break;
         default:
           printf("OTHER\n");
           break;
         }

         if(finfo->filetype == CURLFILETYPE_FILE) {
           /* do not transfer files >= 50B */
           if(finfo->size > 50) {
             printf("SKIPPED\n");
             return CURL_CHUNK_BGN_FUNC_SKIP;
           }

           data->output = fopen(finfo->filename, "wb");
           if(!data->output) {
             return CURL_CHUNK_BGN_FUNC_FAIL;
           }
         }

         return CURL_CHUNK_BGN_FUNC_OK;
       }

       int main()
       {
         /* data for callback */
         struct callback_data callback_info;

         /* callback is called before download of concrete file started */
         curl_easy_setopt(curl, CURLOPT_CHUNK_BGN_FUNCTION, file_is_coming);
         curl_easy_setopt(curl, CURLOPT_CHUNK_DATA, &callback_info);
       }

~~~
AVAILABILITY
~~~
       Added in 7.21.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_CHUNK_BGN_FUNCTION(3), CURLOPT_WILDCARDMATCH(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                           CURLOPT_CHUNK_DATA(3)
CURLOPT_CHUNK_END_FUNCTION(3)                                                             curl_easy_setopt options                                                            CURLOPT_CHUNK_END_FUNCTION(3)



NAME
~~~
       CURLOPT_CHUNK_END_FUNCTION - callback after a transfer with FTP wildcardmatch

~~~
NAME
~~~
       #include <curl/curl.h>

       long chunk_end_callback(void *ptr);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CHUNK_END_FUNCTION,
                                 chunk_end_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       This function gets called by libcurl as soon as a part of the stream has been transferred (or skipped).

       Return CURL_CHUNK_END_FUNC_OK if everything is fine or CURL_CHUNK_END_FUNC_FAIL to tell the lib to stop if some error occurred.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       static long file_is_downloaded(struct callback_data *data)
       {
         if(data->output) {
           fclose(data->output);
           data->output = 0x0;
         }
         return CURL_CHUNK_END_FUNC_OK;
       }

       int main()
       {
         /* data for callback */
         struct callback_data callback_info;
         curl_easy_setopt(curl, CURLOPT_CHUNK_END_FUNCTION, file_is_downloaded);
         curl_easy_setopt(curl, CURLOPT_CHUNK_DATA, &callback_info);
       }

~~~
AVAILABILITY
~~~
       Added in 7.21.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_WILDCARDMATCH(3), CURLOPT_CHUNK_BGN_FUNCTION(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                   CURLOPT_CHUNK_END_FUNCTION(3)
CURLOPT_CLOSESOCKETDATA(3)                                                                curl_easy_setopt options                                                               CURLOPT_CLOSESOCKETDATA(3)



NAME
~~~
       CURLOPT_CLOSESOCKETDATA - pointer passed to the socket close callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CLOSESOCKETDATA, void *pointer);

~~~
DESCRIPTION
~~~
       Pass a pointer that will be untouched by libcurl and passed as the first argument in the closesocket callback set with CURLOPT_CLOSESOCKETFUNCTION(3).

DEFAULT
       The default value of this parameter is NULL.

~~~
PROTOCOLS
~~~
       All except file:

~~~
EXAMPLE
~~~
       static int closesocket(void *clientp, curl_socket_t item)
       {
         printf("libcurl wants to close %d now\n", (int)item);
         return 0;
       }

       /* call this function to close sockets */
       curl_easy_setopt(curl, CURLOPT_CLOSESOCKETFUNCTION, closesocket);
       curl_easy_setopt(curl, CURLOPT_CLOSESOCKETDATA, &sockfd);

~~~
AVAILABILITY
~~~
       Added in 7.21.7

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_CLOSESOCKETFUNCTION(3), CURLOPT_OPENSOCKETFUNCTION(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                      CURLOPT_CLOSESOCKETDATA(3)
CURLOPT_CLOSESOCKETFUNCTION(3)                                                            curl_easy_setopt options                                                           CURLOPT_CLOSESOCKETFUNCTION(3)



NAME
~~~
       CURLOPT_CLOSESOCKETFUNCTION - callback to socket close replacement

~~~
NAME
~~~
       #include <curl/curl.h>

       int closesocket_callback(void *clientp, curl_socket_t item);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CLOSESOCKETFUNCTION, closesocket_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       This  callback  function  gets called by libcurl instead of the close(3) or closesocket(3) call when sockets are closed (not for any other file descriptors). This is pretty much the reverse to the
       CURLOPT_OPENSOCKETFUNCTION(3) option. Return 0 to signal success and 1 if there was an error.

       The clientp pointer is set with CURLOPT_CLOSESOCKETDATA(3). item is the socket libcurl wants to be closed.

DEFAULT
       By default libcurl uses the standard socket close function.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       static int closesocket(void *clientp, curl_socket_t item)
       {
         printf("libcurl wants to close %d now\n", (int)item);
         return 0;
       }

       /* call this function to close sockets */
       curl_easy_setopt(curl, CURLOPT_CLOSESOCKETFUNCTION, closesocket);
       curl_easy_setopt(curl, CURLOPT_CLOSESOCKETDATA, &sockfd);

~~~
AVAILABILITY
~~~
       Added in 7.21.7

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_CLOSESOCKETDATA(3), CURLOPT_OPENSOCKETFUNCTION(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                  CURLOPT_CLOSESOCKETFUNCTION(3)
CURLOPT_CONNECT_ONLY(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_CONNECT_ONLY(3)



NAME
~~~
       CURLOPT_CONNECT_ONLY - stop when connected to target server

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CONNECT_ONLY, long only);

~~~
DESCRIPTION
~~~
       Pass a long. If the parameter equals 1, it tells the library to perform all the required proxy authentication and connection setup, but no data transfer, and then return.

       The  option can be used to simply test a connection to a server, but is more useful when used with the CURLINFO_ACTIVESOCKET(3) option to curl_easy_getinfo(3) as the library can set up the connection and then the application can obtain the most recently used socket for special data transfers.

       Transfers marked connect only will not reuse any existing connections and connections marked connect only will not be allowed to get reused.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       HTTP, SMTP, POP3 and IMAP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_CONNECT_ONLY, 1L);
         ret = curl_easy_perform(curl);
         if(ret == CURLE_OK) {
           /* only connected! */
         }
       }

~~~
AVAILABILITY
~~~
       Added in 7.15.2

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_VERBOSE(3), CURLOPT_HTTPPROXYTUNNEL(3), curl_easy_recv(3), curl_easy_send(3)



libcurl 7.37.0                                                                                  19 Jun 2014                                                                         CURLOPT_CONNECT_ONLY(3)
CURLOPT_CONNECTTIMEOUT(3)                                                                 curl_easy_setopt options                                                                CURLOPT_CONNECTTIMEOUT(3)



NAME
~~~
       CURLOPT_CONNECTTIMEOUT - timeout for the connect phase

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CONNECTTIMEOUT, long timeout);

~~~
DESCRIPTION
~~~
       Pass  a  long.  It  should  contain the maximum time in seconds that you allow the connection phase to the server to take.  This only limits the connection phase, it has no impact once it has connected. Set to zero to switch to the default built-in connection timeout - 300 seconds. See also the CURLOPT_TIMEOUT(3) option.

       In unix-like systems, this might cause signals to be used unless CURLOPT_NOSIGNAL(3) is set.

       If both CURLOPT_CONNECTTIMEOUT(3) and CURLOPT_CONNECTTIMEOUT_MS(3) are set, the value set last will be used.

DEFAULT
       300

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* complete connection within 10 seconds */
         curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 10L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK. Returns CURLE_BAD_FUNCTION_ARGUMENT if set to a negative value or a value that when converted to milliseconds is too large.

~~~
SEE ALSO
       CURLOPT_CONNECTTIMEOUT_MS(3), CURLOPT_TIMEOUT(3), CURLOPT_LOW_SPEED_LIMIT(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                       CURLOPT_CONNECTTIMEOUT(3)
CURLOPT_CONNECTTIMEOUT_MS(3)                                                              curl_easy_setopt options                                                             CURLOPT_CONNECTTIMEOUT_MS(3)



NAME
~~~
       CURLOPT_CONNECTTIMEOUT_MS - timeout for the connect phase

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CONNECTTIMEOUT_MS, long timeout);

~~~
DESCRIPTION
~~~
       Pass a long. It should contain the maximum time in milliseconds that you allow the connection phase to the server to take.  This only limits the connection phase, it has no impact once it has connected. Set to zero to switch to the default built-in connection timeout - 300 seconds. See also the CURLOPT_TIMEOUT_MS(3) option.

       In unix-like systems, this might cause signals to be used unless CURLOPT_NOSIGNAL(3) is set.

       If both CURLOPT_CONNECTTIMEOUT(3) and CURLOPT_CONNECTTIMEOUT_MS(3) are set, the value set last will be used.

DEFAULT
       300000

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* complete connection within 10000 milliseconds */
         curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT_MS, 10000L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_CONNECTTIMEOUT(3), CURLOPT_TIMEOUT(3), CURLOPT_LOW_SPEED_LIMIT(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                    CURLOPT_CONNECTTIMEOUT_MS(3)
CURLOPT_CONNECT_TO(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_CONNECT_TO(3)



NAME
~~~
       CURLOPT_CONNECT_TO - connect to a specific host and port instead of the URL's host and port

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CONNECT_TO,
                                 struct curl_slist *connect_to);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to  a  linked  list  of strings with "connect to" information to use for establishing network connections with this handle. The linked list should be a fully valid list of struct
       curl_slist structs properly filled in. Use curl_slist_append(3) to create the list and curl_slist_free_all(3) to clean up an entire list.

       Each single string should be written using the format HOST:PORT:CONNECT-TO-HOST:CONNECT-TO-PORT where HOST is the host of the request, PORT is the port of the request, CONNECT-TO-HOST is the  host
       name to connect to, and CONNECT-TO-PORT is the port to connect to.

       The first string that matches the request's host and port is used.

       Dotted numerical IP addresses are supported for HOST and CONNECT-TO-HOST.  A numerical IPv6 address must be written within [brackets].

       Any  of the four values may be empty. When the HOST or PORT is empty, the host or port will always match (the request's host or port is ignored).  When CONNECT-TO-HOST or CONNECT-TO-PORT is empty,
       the "connect to" feature will be disabled for the host or port, and the request's host or port will be used to establish the network connection.

       This option is suitable to direct the request at a specific server, e.g. at a specific cluster node in a cluster of servers.

       The "connect to" host and port are only used to establish the network connection. They do NOT affect the host and port that are used for TLS/SSL (e.g. SNI, certificate  verification)  or  for  the
       application protocols.

       In contrast to CURLOPT_RESOLVE(3), the option CURLOPT_CONNECT_TO(3) does not pre-populate the DNS cache and therefore it does not affect future transfers of other easy handles that have been added
       to the same multi handle.

       The "connect to" host and port are ignored if they are equal to the host and the port in the request URL, because connecting to the host and the port in the request URL is the default behavior.

       If an HTTP proxy is used for a request having a special "connect to" host or port, and the "connect to" host or port differs from the request's host and  port,  the  HTTP  proxy  is  automatically
       switched to tunnel mode for this specific request. This is necessary because it is not possible to connect to a specific host or port in normal (non-tunnel) mode.

       When  this  option  is  passed  to  curl_easy_setopt(3),  libcurl  will  not  copy  the  entire  list  so you must keep it around until you no longer use this handle for a transfer before you call
       curl_slist_free_all(3) on the list.


DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl;
       struct curl_slist *connect_to = NULL;
       connect_to = curl_slist_append(NULL, "example.com::server1.example.com:");

       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_CONNECT_TO, connect_to);
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         curl_easy_perform(curl);

         /* always cleanup */
         curl_easy_cleanup(curl);
       }

       curl_slist_free_all(connect_to);

~~~
AVAILABILITY
~~~
       Added in 7.49.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_URL(3), CURLOPT_RESOLVE(3), CURLOPT_FOLLOWLOCATION(3), CURLOPT_HTTPPROXYTUNNEL(3),



libcurl 7.49.0                                                                                 10 April 2016                                                                          CURLOPT_CONNECT_TO(3)
CURLOPT_CONV_FROM_NETWORK_FUNCTION(3)                                                     curl_easy_setopt options                                                    CURLOPT_CONV_FROM_NETWORK_FUNCTION(3)



NAME
~~~
       CURLOPT_CONV_FROM_NETWORK_FUNCTION - convert data from network to host encoding

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode conv_callback(char *ptr, size_t length);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CONV_FROM_NETWORK_FUNCTION,
                                 conv_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       Applies to non-ASCII platforms. curl_version_info(3) will return the CURL_VERSION_CONV feature bit set if this option is provided.

       The  data  to  be  converted is in a buffer pointed to by the ptr parameter.  The amount of data to convert is indicated by the length parameter.  The converted data overlays the input data in the
       buffer pointed to by the ptr parameter. CURLE_OK must be returned upon successful conversion.  A CURLcode return value defined by curl.h, such as CURLE_CONV_FAILED, should be returned if an  error
       was encountered.

       CURLOPT_CONV_FROM_NETWORK_FUNCTION converts to host encoding from the network encoding.  It is used when commands or ASCII data are received over the network.

       If  you  set  a callback pointer to NULL, or don't set it at all, the built-in libcurl iconv functions will be used.  If HAVE_ICONV was not defined when libcurl was built, and no callback has been
       established, conversion will return the CURLE_CONV_REQD error code.

       If HAVE_ICONV is defined, CURL_ICONV_CODESET_OF_HOST must also be defined.  For example:

        #define CURL_ICONV_CODESET_OF_HOST "IBM-1047"

       The iconv code in libcurl will default the network and UTF8 codeset names as follows:

        #define CURL_ICONV_CODESET_OF_NETWORK "ISO8859-1"

        #define CURL_ICONV_CODESET_FOR_UTF8   "UTF-8"

       You will need to override these definitions if they are different on your system.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       FTP, SMTP, IMAP, POP3

~~~
EXAMPLE
~~~
       static CURLcode my_conv_from_ascii_to_ebcdic(char *buffer, size_t length)
       {
         char *tempptrin, *tempptrout;
         size_t bytes = length;
         int rc;
         tempptrin = tempptrout = buffer;
         rc = platform_a2e(&tempptrin, &bytes, &tempptrout, &bytes);
         if(rc == PLATFORM_CONV_OK) {
           return CURLE_OK;
         }
         else {
           return CURLE_CONV_FAILED;
         }
       }

       /* use platform-specific functions for codeset conversions */
       curl_easy_setopt(curl, CURLOPT_CONV_FROM_NETWORK_FUNCTION,
                        my_conv_from_ascii_to_ebcdic);

~~~
AVAILABILITY
~~~
       Available only if CURL_DOES_CONVERSIONS was defined when libcurl was built.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_CONV_TO_NETWORK_FUNCTION(3), CURLOPT_CONV_FROM_UTF8_FUNCTION(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                           CURLOPT_CONV_FROM_NETWORK_FUNCTION(3)
CURLOPT_CONV_FROM_UTF8_FUNCTION(3)                                                        curl_easy_setopt options                                                       CURLOPT_CONV_FROM_UTF8_FUNCTION(3)



NAME
~~~
       CURLOPT_CONV_FROM_UTF8_FUNCTION - convert data from UTF8 to host encoding

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode conv_callback(char *ptr, size_t length);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CONV_FROM_UTF8_FUNCTION,
                                 conv_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       Applies to non-ASCII platforms. curl_version_info(3) will return the CURL_VERSION_CONV feature bit set if this option is provided.

       The  data  to  be  converted is in a buffer pointed to by the ptr parameter.  The amount of data to convert is indicated by the length parameter.  The converted data overlays the input data in the
       buffer pointed to by the ptr parameter. CURLE_OK must be returned upon successful conversion.  A CURLcode return value defined by curl.h, such as CURLE_CONV_FAILED, should be returned if an  error
       was encountered.

       CURLOPT_CONV_FROM_UTF8_FUNCTION converts to host encoding from UTF8 encoding. It is required only for SSL processing.

       If  you  set  a callback pointer to NULL, or don't set it at all, the built-in libcurl iconv functions will be used.  If HAVE_ICONV was not defined when libcurl was built, and no callback has been
       established, conversion will return the CURLE_CONV_REQD error code.

       If HAVE_ICONV is defined, CURL_ICONV_CODESET_OF_HOST must also be defined.  For example:

        #define CURL_ICONV_CODESET_OF_HOST "IBM-1047"

       The iconv code in libcurl will default the network and UTF8 codeset names as follows:

        #define CURL_ICONV_CODESET_OF_NETWORK "ISO8859-1"

        #define CURL_ICONV_CODESET_FOR_UTF8   "UTF-8"

       You will need to override these definitions if they are different on your system.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       TLS-based protocols.

~~~
EXAMPLE
~~~
       static CURLcode my_conv_from_utf8_to_ebcdic(char *buffer, size_t length)
       {
         char *tempptrin, *tempptrout;
         size_t bytes = length;
         int rc;
         tempptrin = tempptrout = buffer;
         rc = platform_u2e(&tempptrin, &bytes, &tempptrout, &bytes);
         if(rc == PLATFORM_CONV_OK) {
           return CURLE_OK;
         }
         else {
           return CURLE_CONV_FAILED;
         }
       }

       curl_easy_setopt(curl, CURLOPT_CONV_FROM_UTF8_FUNCTION,
                        my_conv_from_utf8_to_ebcdic);

~~~
AVAILABILITY
~~~
       Available only if CURL_DOES_CONVERSIONS was defined when libcurl was built.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_CONV_TO_NETWORK_FUNCTION(3), CURLOPT_CONV_FROM_NETWORK_FUNCTION(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                              CURLOPT_CONV_FROM_UTF8_FUNCTION(3)
CURLOPT_CONV_TO_NETWORK_FUNCTION(3)                                                       curl_easy_setopt options                                                      CURLOPT_CONV_TO_NETWORK_FUNCTION(3)



NAME
~~~
       CURLOPT_CONV_TO_NETWORK_FUNCTION - convert data to network from host encoding

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode conv_callback(char *ptr, size_t length);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CONV_TO_NETWORK_FUNCTION,
                                 conv_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       Applies to non-ASCII platforms. curl_version_info(3) will return the CURL_VERSION_CONV feature bit set if this option is provided.

       The  data  to  be  converted is in a buffer pointed to by the ptr parameter.  The amount of data to convert is indicated by the length parameter.  The converted data overlays the input data in the
       buffer pointed to by the ptr parameter. CURLE_OK must be returned upon successful conversion.  A CURLcode return value defined by curl.h, such as CURLE_CONV_FAILED, should be returned if an  error
       was encountered.

       CURLOPT_CONV_TO_NETWORK_FUNCTION converts from host encoding to the network encoding.  It is used when commands or ASCII data are sent over the network.

       If  you  set  a callback pointer to NULL, or don't set it at all, the built-in libcurl iconv functions will be used.  If HAVE_ICONV was not defined when libcurl was built, and no callback has been
       established, conversion will return the CURLE_CONV_REQD error code.

       If HAVE_ICONV is defined, CURL_ICONV_CODESET_OF_HOST must also be defined.  For example:

        #define CURL_ICONV_CODESET_OF_HOST "IBM-1047"

       The iconv code in libcurl will default the network and UTF8 codeset names as follows:

        #define CURL_ICONV_CODESET_OF_NETWORK "ISO8859-1"

        #define CURL_ICONV_CODESET_FOR_UTF8   "UTF-8"

       You will need to override these definitions if they are different on your system.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       FTP, SMTP, IMAP, POP3

~~~
EXAMPLE
~~~
       static CURLcode my_conv_from_ebcdic_to_ascii(char *buffer, size_t length)
       {
         char *tempptrin, *tempptrout;
         size_t bytes = length;
         int rc;
         tempptrin = tempptrout = buffer;
         rc = platform_e2a(&tempptrin, &bytes, &tempptrout, &bytes);
         if(rc == PLATFORM_CONV_OK) {
           return CURLE_OK;
         }
         else {
           return CURLE_CONV_FAILED;
         }
       }

       curl_easy_setopt(curl, CURLOPT_CONV_TO_NETWORK_FUNCTION,
                        my_conv_from_ebcdic_to_ascii);

~~~
AVAILABILITY
~~~
       Available only if CURL_DOES_CONVERSIONS was defined when libcurl was built.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_CONV_FROM_NETWORK_FUNCTION(3), CURLOPT_CONV_FROM_UTF8_FUNCTION(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                             CURLOPT_CONV_TO_NETWORK_FUNCTION(3)
CURLOPT_COOKIE(3)                                                                         curl_easy_setopt options                                                                        CURLOPT_COOKIE(3)



NAME
~~~
       CURLOPT_COOKIE - HTTP Cookie header

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_COOKIE, char *cookie);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to a null-terminated string as parameter. It will be used to set a cookie in the HTTP request. The format of the string should be NAME
~~~=CONTENTS, where NAME
~~~ is the cookie name and
       CONTENTS is what the cookie should contain.

       If you need to set multiple cookies, set them all using a single option concatenated like this: "name1=content1; name2=content2;" etc.

       This option sets the cookie header explicitly in the outgoing request(s). If multiple requests are done due to authentication, followed redirections or similar,  they  will  all  get  this  cookie
       passed on.

       The  cookies set by this option are separate from the internal cookie storage held by the cookie engine and will not be modified by it. If you enable the cookie engine and either you've imported a
       cookie of the same name (e.g. 'foo') or the server has set one, it will have no effect on the cookies you set here.  A request to the server will send both the 'foo' held by the cookie engine  and
       the 'foo' held by this option. To set a cookie that is instead held by the cookie engine and can be modified by the server use CURLOPT_COOKIELIST(3).

       Using this option multiple times will only make the latest string override the previous ones.

       This option will not enable the cookie engine. Use CURLOPT_COOKIEFILE(3) or CURLOPT_COOKIEJAR(3) to enable parsing and sending cookies automatically.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL, no cookies

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         curl_easy_setopt(curl, CURLOPT_COOKIE, "tool=curl; fun=yes;");

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       If HTTP is enabled

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is enabled, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_COOKIEFILE(3), CURLOPT_COOKIEJAR(3), CURLOPT_COOKIELIST(3), CURLOPT_HTTPHEADER(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                               CURLOPT_COOKIE(3)
CURLOPT_COOKIEFILE(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_COOKIEFILE(3)



NAME
~~~
       CURLOPT_COOKIEFILE - file name to read cookies from

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_COOKIEFILE, char *filename);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to  a  null-terminated string as parameter. It should point to the file name of your file holding cookie data to read. The cookie data can be in either the old Netscape / Mozilla
       cookie data format or just regular HTTP headers (Set-Cookie style) dumped to a file.

       It also enables the cookie engine, making libcurl parse and send cookies on subsequent requests with this handle.

       Given an empty or non-existing file or by passing the empty string ("") to this option, you can enable the cookie engine without reading any initial cookies. If you tell libcurl the file  name  is
       "-" (just a single minus sign), libcurl will instead read from stdin.

       This option only reads cookies. To make libcurl write cookies to file, see CURLOPT_COOKIEJAR(3).

       If  you  use  the Set-Cookie file format and don't specify a domain then the cookie is not sent since the domain will never match. To address this, set a domain in Set-Cookie line (doing that will
       include sub-domains) or preferably: use the Netscape format.

       If you use this option multiple times, you just add more files to read.  Subsequent files will add more cookies.

       The application does not have to keep the string around after setting this option.

       Setting this option to NULL will (since 7.77.0) explicitly disable the cookie engine and clear the list of files to read cookies from.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

         /* get cookies from an existing file */
         curl_easy_setopt(curl, CURLOPT_COOKIEFILE, "/tmp/cookies.txt");

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

Cookie file format
       The cookie file format and general cookie concepts in curl are described in the HTTP-COOKIES.md file, also hosted online here: https://curl.se/docs/http-cookies.html

~~~
AVAILABILITY
~~~
       As long as HTTP is supported

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_COOKIE(3), CURLOPT_COOKIEJAR(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                           CURLOPT_COOKIEFILE(3)
CURLOPT_COOKIEJAR(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_COOKIEJAR(3)



NAME
~~~
       CURLOPT_COOKIEJAR - file name to store cookies to

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_COOKIEJAR, char *filename);

~~~
DESCRIPTION
~~~
       Pass  a  filename  as char *, null-terminated. This will make libcurl write all internally known cookies to the specified file when curl_easy_cleanup(3) is called. If no cookies are known, no file
       will be created. Specify "-" as filename to instead have the cookies written to stdout. Using this option also enables cookies for this session, so if you for example follow  a  location  it  will
       make matching cookies get sent accordingly.

       Note that libcurl doesn't read any cookies from the cookie jar. If you want to read cookies from a file, use CURLOPT_COOKIEFILE(3).

       If  the  cookie  jar file can't be created or written to (when the curl_easy_cleanup(3) is called), libcurl will not and cannot report an error for this. Using CURLOPT_VERBOSE(3) or CURLOPT_DEBUGFUNCTION(3) will get a warning to display, but that is the only visible feedback you get about this possibly lethal situation.

       Since 7.43.0 cookies that were imported in the Set-Cookie format without a domain name are not exported by this option.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

         /* export cookies to this file when closing the handle */
         curl_easy_setopt(curl, CURLOPT_COOKIEJAR, "/tmp/cookies.txt");

         ret = curl_easy_perform(curl);

         /* close the handle, write the cookies! */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Along with HTTP

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_COOKIEFILE(3), CURLOPT_COOKIE(3), CURLOPT_COOKIELIST(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                            CURLOPT_COOKIEJAR(3)
CURLOPT_COOKIELIST(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_COOKIELIST(3)



NAME
~~~
       CURLOPT_COOKIELIST - add to or manipulate cookies held in memory

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_COOKIELIST,
                                 char *cookie);

~~~
DESCRIPTION
~~~
       Pass a char * to a cookie string.

       Such  a  cookie  can be either a single line in Netscape / Mozilla format or just regular HTTP-style header (Set-Cookie: ...) format. This will also enable the cookie engine. This adds that single
       cookie to the internal cookie store.

       Exercise caution if you are using this option and multiple transfers may occur.  If you use the Set-Cookie format and don't specify a domain then the cookie is sent  for  any  domain  (even  after
       redirects are followed) and cannot be modified by a server-set cookie. If a server sets a cookie of the same name (or maybe you've imported one) then both will be sent on a future transfer to that
       server, likely not what you intended. To address these issues set a domain in Set-Cookie (doing that will include sub-domains) or use the Netscape format as shown in ~~~
EXAMPLE
~~~.

       Additionally, there are commands available that perform actions if you pass in these exact strings:

       ALL    erases all cookies held in memory


       SESS   erases all session cookies held in memory


       FLUSH  writes all known cookies to the file specified by CURLOPT_COOKIEJAR(3)


       RELOAD loads all cookies from the files specified by CURLOPT_COOKIEFILE(3)


DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       /* This example shows an inline import of a cookie in Netscape format.
       You can set the cookie as HttpOnly to prevent XSS attacks by prepending
       #HttpOnly_ to the hostname. That may be useful if the cookie will later
       be imported by a browser.
       */

       #define SEP  "\t"  /* Tab separates the fields */

       char *my_cookie =
         "example.com"    /* Hostname */
         SEP "FALSE"      /* Include subdomains */
         SEP "/"          /* Path */
         SEP "FALSE"      /* Secure */
         SEP "0"          /* Expiry in epoch time format. 0 == Session */
         SEP "foo"        /* Name */
         SEP "bar";       /* Value */

       /* my_cookie is imported immediately via CURLOPT_COOKIELIST.
       */
       curl_easy_setopt(curl, CURLOPT_COOKIELIST, my_cookie);

       /* The list of cookies in cookies.txt will not be imported until right
       before a transfer is performed. Cookies in the list that have the same
       hostname, path and name as in my_cookie are skipped. That is because
       libcurl has already imported my_cookie and it's considered a "live"
       cookie. A live cookie won't be replaced by one read from a file.
       */
       curl_easy_setopt(curl, CURLOPT_COOKIEFILE, "cookies.txt");  /* import */

       /* Cookies are exported after curl_easy_cleanup is called. The server
       may have added, deleted or modified cookies by then. The cookies that
       were skipped on import are not exported.
       */
       curl_easy_setopt(curl, CURLOPT_COOKIEJAR, "cookies.txt");  /* export */

       curl_easy_perform(curl);  /* cookies imported from cookies.txt */

       curl_easy_cleanup(curl);  /* cookies exported to cookies.txt */

Cookie file format
       The cookie file format and general cookie concepts in curl are described in the HTTP-COOKIES.md file, also hosted online here: https://curl.se/docs/http-cookies.html

~~~
AVAILABILITY
~~~
       ALL was added in 7.14.1

       SESS was added in 7.15.4

       FLUSH was added in 7.17.1

       RELOAD was added in 7.39.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_COOKIEFILE(3), CURLOPT_COOKIEJAR(3), CURLOPT_COOKIE(3), CURLINFO_COOKIELIST(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                           CURLOPT_COOKIELIST(3)
CURLOPT_COOKIESESSION(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_COOKIESESSION(3)



NAME
~~~
       CURLOPT_COOKIESESSION - start a new cookie session

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_COOKIESESSION, long init);

~~~
DESCRIPTION
~~~
       Pass  a  long  set  to  1  to mark this as a new cookie "session". It will force libcurl to ignore all cookies it is about to load that are "session cookies" from the previous session. By default,
       libcurl always stores and loads all cookies, independent if they are session cookies or not. Session cookies are cookies without expiry date and they are meant to be alive and  existing  for  this
       "session" only.

       A "session" is usually defined in browser land for as long as you have your browser up, more or less.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

         /* new "session", don't load session cookies */
         curl_easy_setopt(curl, CURLOPT_COOKIESESSION, 1L);

         /* get the (non session) cookies from this file */
         curl_easy_setopt(curl, CURLOPT_COOKIEFILE, "/tmp/cookies.txt");

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Along with HTTP

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_COOKIEFILE(3), CURLOPT_COOKIE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                        CURLOPT_COOKIESESSION(3)
CURLOPT_COPYPOSTFIELDS(3)                                                                 curl_easy_setopt options                                                                CURLOPT_COPYPOSTFIELDS(3)



NAME
~~~
       CURLOPT_COPYPOSTFIELDS - have libcurl copy data to POST

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_COPYPOSTFIELDS, char *data);

~~~
DESCRIPTION
~~~
       Pass  a char * as parameter, which should be the full data to post in a HTTP POST operation. It behaves as the CURLOPT_POSTFIELDS(3) option, but the original data is instead copied by the library,
       allowing the application to overwrite the original data after setting this option.

       Because data are copied, care must be taken when using this option in conjunction with CURLOPT_POSTFIELDSIZE(3) or CURLOPT_POSTFIELDSIZE_LARGE(3): If the size  has  not  been  set  prior  to  CURLOPT_COPYPOSTFIELDS(3),  the  data is assumed to be a null-terminated string; else the stored size informs the library about the byte count to copy. In any case, the size must not be changed after
       CURLOPT_COPYPOSTFIELDS(3), unless another CURLOPT_POSTFIELDS(3) or CURLOPT_COPYPOSTFIELDS(3) option is issued.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         char local_buffer[1024]="data to send";
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* size of the data to copy from the buffer and send in the request */
         curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, 12L);

         /* send data from the local stack */
         curl_easy_setopt(curl, CURLOPT_COPYPOSTFIELDS, local_buffer);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.17.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_POSTFIELDS(3), CURLOPT_POSTFIELDSIZE(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                       CURLOPT_COPYPOSTFIELDS(3)
CURLOPT_CRLF(3)                                                                           curl_easy_setopt options                                                                          CURLOPT_CRLF(3)



NAME
~~~
       CURLOPT_CRLF - CRLF conversion

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CRLF, long conv);

~~~
DESCRIPTION
~~~
       Pass a long. If the value is set to 1 (one), libcurl converts Unix newlines to CRLF newlines on transfers. Disable this option again by setting the value to 0 (zero).

       This is a legacy option of questionable use.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/");
         curl_easy_setopt(curl, CURLOPT_CRLF, 1L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       SMTP since 7.40.0, other protocols since they were introduced

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_CONV_FROM_NETWORK_FUNCTION(3), CURLOPT_CONV_TO_NETWORK_FUNCTION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                                 CURLOPT_CRLF(3)
CURLOPT_CRLFILE(3)                                                                        curl_easy_setopt options                                                                       CURLOPT_CRLFILE(3)



NAME
~~~
       CURLOPT_CRLFILE - Certificate Revocation List file

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CRLFILE, char *file);

~~~
DESCRIPTION
~~~
       Pass a char * to a null-terminated string naming a file with the concatenation of CRL (in PEM format) to use in the certificate validation that occurs during the SSL exchange.

       When curl is built to use NSS or GnuTLS, there is no way to influence the use of CRL passed to help in the verification process.

       When libcurl is built with OpenSSL support, X509_V_FLAG_CRL_CHECK and X509_V_FLAG_CRL_CHECK_ALL are both set, requiring CRL check against all the elements of the certificate chain if a CRL file is
       passed. Also note that CURLOPT_CRLFILE(3) will imply CURLSSLOPT_NO_PARTIALCHAIN (see CURLOPT_SSL_OPTIONS(3)) since curl 7.71.0 due to an OpenSSL bug.

       This option makes sense only when used in combination with the CURLOPT_SSL_VERIFYPEER(3) option.

       A specific error code (CURLE_SSL_CRL_BADFILE) is defined with the option. It is returned when the SSL exchange fails because the CRL file cannot be loaded.  A failure in  certificate  verification
       due to a revocation information found in the CRL does not trigger this specific error.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS-based protocols

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_CRLFILE, "/etc/certs/crl.pem");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSL_VERIFYPEER(3), CURLOPT_SSL_VERIFYHOST(3), CURLOPT_PROXY_CRLFILE(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                              CURLOPT_CRLFILE(3)
CURLOPT_CURLU(3)                                                                          curl_easy_setopt options                                                                         CURLOPT_CURLU(3)



NAME
~~~
       CURLOPT_CURLU - URL in CURLU * format

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CURLU, void *pointer);

~~~
DESCRIPTION
~~~
       Pass in a pointer to the URL to work with. The parameter should be a CURLU *. Setting CURLOPT_CURLU(3) will explicitly override CURLOPT_URL(3).

       CURLOPT_URL(3) or CURLOPT_CURLU(3) must be set before a transfer is started.

       libcurl  will  use  this  handle and its contents read-only and will not change its contents. An application can very well update the contents of the URL handle after a transfer is done and if the
       same handle is then used in a subsequent request the updated contents will then be used.

DEFAULT
       The default value of this parameter is NULL.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *handle = curl_easy_init();
       CURLU *urlp = curl_url();
       int res = 0;
       if(curl) {

         res = curl_url_set(urlp, CURLUPART_URL, "https://example.com", 0);

         curl_easy_setopt(handle, CURLOPT_CURLU, urlp);

         ret = curl_easy_perform(handle);

         curl_url_cleanup(urlp);
         curl_easy_cleanup(handle);
       }

~~~
AVAILABILITY
~~~
       Added in 7.63.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_URL(3), curl_url(3), curl_url_get(3), curl_url_set(3), curl_url_dup(3), curl_url_cleanup(3), curl_url_strerror(3)



libcurl 7.63.0                                                                                  28 Oct 2018                                                                                CURLOPT_CURLU(3)
CURLOPT_CUSTOMREQUEST(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_CUSTOMREQUEST(3)



NAME
~~~
       CURLOPT_CUSTOMREQUEST - custom string for request

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_CUSTOMREQUEST, char *request);

~~~
DESCRIPTION
~~~
       Pass a pointer to a null-terminated string as parameter.

       When you change the request method by setting CURLOPT_CUSTOMREQUEST(3) to something, you don't actually change how libcurl behaves or acts in regards to the particular request method, it will only
       change the actual string sent in the request.

       Restore to the internal default by setting this to NULL.

       This option can be used to specify the request:

       HTTP   Instead of GET or HEAD when performing HTTP based requests. This is particularly useful, for example, for performing an HTTP DELETE request.

              For example:

              When you tell libcurl to do a HEAD request, but then specify a GET though a custom request libcurl will still act as if it sent a HEAD. To switch to a proper HEAD use CURLOPT_NOBODY(3),  to
              switch to a proper POST use CURLOPT_POST(3) or CURLOPT_POSTFIELDS(3) and to switch to a proper GET use CURLOPT_HTTPGET(3).

              Many  people  have  wrongly  used  this option to replace the entire request with their own, including multiple headers and POST contents. While that might work in many cases, it will cause
              libcurl to send invalid requests and it could possibly confuse the remote server badly. Use CURLOPT_POST(3) and CURLOPT_POSTFIELDS(3) to set POST data. Use CURLOPT_HTTPHEADER(3) to  replace
              or extend the set of headers sent by libcurl. Use CURLOPT_HTTP_VERSION(3) to change HTTP version.


       FTP    Instead of LIST and NLST when performing FTP directory listings.

       IMAP   Instead of LIST when issuing IMAP based requests.

       POP3   Instead of LIST and RETR when issuing POP3 based requests.

              For example:

              When  you  tell  libcurl  to use a custom request it will behave like a LIST or RETR command was sent where it expects data to be returned by the server. As such CURLOPT_NOBODY(3) should be
              used when specifying commands such as DELE and NOOP for example.

       SMTP   Instead of a HELP or VRFY when issuing SMTP based requests.

              For example:

              Normally a multiline response is returned which can be used, in conjunction with CURLOPT_MAIL_RCPT(3), to specify an EXPN request. If the CURLOPT_NOBODY(3)  option  is  specified  then  the
              request can be used to issue NOOP and RSET commands.

              The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP, FTP, IMAP, POP3 and SMTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

         /* DELETE the given path */
         curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       IMAP is supported since 7.30.0, POP3 since 7.26.0 and SMTP since 7.34.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_HTTPHEADER(3), CURLOPT_NOBODY(3), CURLOPT_REQUEST_TARGET(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                        CURLOPT_CUSTOMREQUEST(3)
CURLOPT_DEBUGDATA(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_DEBUGDATA(3)



NAME
~~~
       CURLOPT_DEBUGDATA - custom pointer for debug callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DEBUGDATA, void *pointer);

~~~
DESCRIPTION
~~~
       Pass a pointer to whatever you want passed in to your CURLOPT_DEBUGFUNCTION(3) in the last void * argument. This pointer is not used by libcurl, it is only passed to the callback.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       int main(void)
       {
         CURL *curl;
         CURLcode res;
         struct data my_tracedata;

         curl = curl_easy_init();
         if(curl) {
           curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, my_trace);

           curl_easy_setopt(curl, CURLOPT_DEBUGDATA, &my_tracedata);

           /* the DEBUGFUNCTION has no effect until we enable VERBOSE */
           curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

           curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
           res = curl_easy_perform(curl);

           /* always cleanup */
           curl_easy_cleanup(curl);
         }
         return 0;
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_STDERR(3), CURLOPT_DEBUGFUNCTION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                            CURLOPT_DEBUGDATA(3)
CURLOPT_DEBUGFUNCTION(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_DEBUGFUNCTION(3)



NAME
~~~
       CURLOPT_DEBUGFUNCTION - debug callback

~~~
NAME
~~~
       #include <curl/curl.h>

       typedef enum {
         CURLINFO_TEXT = 0,
         CURLINFO_HEADER_IN,    /* 1 */
         CURLINFO_HEADER_OUT,   /* 2 */
         CURLINFO_DATA_IN,      /* 3 */
         CURLINFO_DATA_OUT,     /* 4 */
         CURLINFO_SSL_DATA_IN,  /* 5 */
         CURLINFO_SSL_DATA_OUT, /* 6 */
         CURLINFO_END
       } curl_infotype;

       int debug_callback(CURL *handle,
                          curl_infotype type,
                          char *data,
                          size_t size,
                          void *userptr);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DEBUGFUNCTION,
                                 debug_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       CURLOPT_DEBUGFUNCTION(3)  replaces the standard debug function used when CURLOPT_VERBOSE(3) is in effect. This callback receives debug information, as specified in the type argument. This function
       must return 0. The data pointed to by the char * passed to this function WILL NOT be null-terminated, but will be exactly of the size as told by the size argument.

       The userptr argument is the pointer set with CURLOPT_DEBUGDATA(3).

       Available curl_infotype values:

       CURLINFO_TEXT
              The data is informational text.

       CURLINFO_HEADER_IN
              The data is header (or header-like) data received from the peer.

       CURLINFO_HEADER_OUT
              The data is header (or header-like) data sent to the peer.

       CURLINFO_DATA_IN
              The data is protocol data received from the peer.

       CURLINFO_DATA_OUT
              The data is protocol data sent to the peer.

       CURLINFO_SSL_DATA_OUT
              The data is SSL/TLS (binary) data sent to the peer.

       CURLINFO_SSL_DATA_IN
              The data is SSL/TLS (binary) data received from the peer.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       static
       void dump(const char *text,
                 FILE *stream, unsigned char *ptr, size_t size)
       {
         size_t i;
         size_t c;
         unsigned int width=0x10;

         fprintf(stream, "%s, %10.10ld bytes (0x%8.8lx)\n",
                 text, (long)size, (long)size);

         for(i=0; i<size; i+= width) {
           fprintf(stream, "%4.4lx: ", (long)i);

           /* show hex to the left */
           for(c = 0; c < width; c++) {
             if(i+c < size)
               fprintf(stream, "%02x ", ptr[i+c]);
             else
               fputs("   ", stream);
           }

           /* show data on the right */
           for(c = 0; (c < width) && (i+c < size); c++) {
             char x = (ptr[i+c] >= 0x20 && ptr[i+c] < 0x80) ? ptr[i+c] : '.';
             fputc(x, stream);
           }

           fputc('\n', stream); /* newline */
         }
       }

       static
       int my_trace(CURL *handle, curl_infotype type,
                    char *data, size_t size,
                    void *userp)
       {
         const char *text;
         (void)handle; /* prevent compiler warning */
         (void)userp;

         switch (type) {
         case CURLINFO_TEXT:
           fprintf(stderr, "== Info: %s", data);
         default: /* in case a new one is introduced to shock us */
           return 0;

         case CURLINFO_HEADER_OUT:
           text = "=> Send header";
           break;
         case CURLINFO_DATA_OUT:
           text = "=> Send data";
           break;
         case CURLINFO_SSL_DATA_OUT:
           text = "=> Send SSL data";
           break;
         case CURLINFO_HEADER_IN:
           text = "<= Recv header";
           break;
         case CURLINFO_DATA_IN:
           text = "<= Recv data";
           break;
         case CURLINFO_SSL_DATA_IN:
           text = "<= Recv SSL data";
           break;
         }

         dump(text, stderr, (unsigned char *)data, size);
         return 0;
       }

       int main(void)
       {
         CURL *curl;
         CURLcode res;

         curl = curl_easy_init();
         if(curl) {
           curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, my_trace);

           /* the DEBUGFUNCTION has no effect until we enable VERBOSE */
           curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

           /* example.com is redirected, so we tell libcurl to follow redirection */
           curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

           curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
           res = curl_easy_perform(curl);
           /* Check for errors */
           if(res != CURLE_OK)
             fprintf(stderr, "curl_easy_perform() failed: %s\n",
                     curl_easy_strerror(res));

           /* always cleanup */
           curl_easy_cleanup(curl);
         }
         return 0;
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_VERBOSE(3), CURLOPT_DEBUGDATA(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                        CURLOPT_DEBUGFUNCTION(3)
CURLOPT_DEFAULT_PROTOCOL(3)                                                               curl_easy_setopt options                                                              CURLOPT_DEFAULT_PROTOCOL(3)



NAME
~~~
       CURLOPT_DEFAULT_PROTOCOL - default protocol to use if the URL is missing a scheme name

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DEFAULT_PROTOCOL, char *protocol);

~~~
DESCRIPTION
~~~
       This option tells libcurl to use protocol if the URL is missing a scheme name.

       Use one of these protocol (scheme) names:

       dict, file, ftp, ftps, gopher, http, https, imap, imaps, ldap, ldaps, pop3, pop3s, rtsp, scp, sftp, smb, smbs, smtp, smtps, telnet, tftp

       An  unknown  or unsupported protocol causes error CURLE_UNSUPPORTED_PROTOCOL when libcurl parses a schemeless URL. Parsing happens when curl_easy_perform(3) or curl_multi_perform(3) is called. The
       protocols supported by libcurl will vary depending on how it was built. Use curl_version_info(3) if you need a list of protocol names supported by the build of libcurl that you are using.

       This option does not change the default proxy protocol (http).

       Without this option libcurl would make a guess based on the host, see CURLOPT_URL(3) for details.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL (make a guess based on the host)

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         /* set a URL without a scheme */
         curl_easy_setopt(curl, CURLOPT_URL, "example.com");

         /* set the default protocol (scheme) for schemeless URLs */
         curl_easy_setopt(curl, CURLOPT_DEFAULT_PROTOCOL, "https");

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.45.0

~~~
RETURN VALUE
~~~
       CURLE_OK if the option is supported.

       CURLE_OUT_OF_MEMORY if there was insufficient heap space.

       CURLE_UNKNOWN_OPTION if the option is not supported.

~~~
SEE ALSO
       CURLOPT_URL(3),



libcurl 7.45.0                                                                                  18 Aug 2015                                                                     CURLOPT_DEFAULT_PROTOCOL(3)
CURLOPT_DIRLISTONLY(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_DIRLISTONLY(3)



NAME
~~~
       CURLOPT_DIRLISTONLY - ask for names only in a directory listing

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DIRLISTONLY, long listonly);

~~~
DESCRIPTION
~~~
       For  FTP  and  SFTP  based  URLs  a parameter set to 1 tells the library to list the names of files in a directory, rather than performing a full directory listing that would normally include file
       sizes, dates etc.

       For POP3 a parameter of 1 tells the library to list the email message or messages on the POP3 server. This can be used to change the default behavior of libcurl, when combined with a URL that contains a message ID, to perform a "scan listing" which can then be used to determine the size of an email.

       Note:  For  FTP this causes a NLST command to be sent to the FTP server.  Beware that some FTP servers list only files in their response to NLST; they might not include subdirectories and symbolic
       links.

       Setting this option to 1 also implies a directory listing even if the URL doesn't end with a slash, which otherwise is necessary.

       Do NOT use this option if you also use CURLOPT_WILDCARDMATCH(3) as it will effectively break that feature then.

DEFAULT
       0, disabled

~~~
PROTOCOLS
~~~
       FTP, SFTP and POP3

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/dir/");

         /* list only */
         curl_easy_setopt(curl, CURLOPT_DIRLISTONLY, 1L);

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       This option was known as CURLOPT_FTPLISTONLY up to 7.16.4. POP3 is supported since 7.21.5.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_CUSTOMREQUEST(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                          CURLOPT_DIRLISTONLY(3)
CURLOPT_DISALLOW_USERNAME_IN_URL(3)                                                       curl_easy_setopt options                                                      CURLOPT_DISALLOW_USERNAME_IN_URL(3)



NAME
~~~
       CURLOPT_DISALLOW_USERNAME_IN_URL - disallow specifying username in the url

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DISALLOW_USERNAME_IN_URL, long disallow);

~~~
DESCRIPTION
~~~
       A long parameter set to 1 tells the library to not allow URLs that include a username.

DEFAULT
       0 (disabled) - user names are allowed by default.

~~~
PROTOCOLS
~~~
       Several

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {

         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_DISALLOW_USERNAME_IN_URL, 1L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in libcurl 7.61.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

       curl_easy_perform() will return CURLE_LOGIN_DENIED if this option is enabled and a URL containing a username is specified.

~~~
SEE ALSO
       libcurl-security(3), ,CURLOPT_PROTOCOLS(3)



libcurl 7.61.0                                                                                  30 May 2018                                                             CURLOPT_DISALLOW_USERNAME_IN_URL(3)
CURLOPT_DNS_CACHE_TIMEOUT(3)                                                              curl_easy_setopt options                                                             CURLOPT_DNS_CACHE_TIMEOUT(3)



NAME
~~~
       CURLOPT_DNS_CACHE_TIMEOUT - life-time for DNS cache entries

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DNS_CACHE_TIMEOUT, long age);

~~~
DESCRIPTION
~~~
       Pass  a long, this sets the timeout in seconds. Name resolves will be kept in memory and used for this number of seconds. Set to zero to completely disable caching, or set to -1 to make the cached
       entries remain forever. By default, libcurl caches this info for 60 seconds.

       The name resolve functions of various libc implementations don't re-read name server information unless explicitly told so (for example, by calling res_init(3)). This may  cause  libcurl  to  keep
       using the older server even if DHCP has updated the server info, and this may look like a DNS cache issue to the casual libcurl-app user.

       Note  that DNS entries have a "TTL" property but libcurl doesn't use that. This DNS cache timeout is entirely speculative that a name will resolve to the same address for a certain small amount of
       time into the future.

DEFAULT
       60

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

         /* only reuse addresses for a very short time */
         curl_easy_setopt(curl, CURLOPT_DNS_CACHE_TIMEOUT, 2L);

         ret = curl_easy_perform(curl);

         /* in this second request, the cache will not be used if more than
            two seconds have passed since the previous name resolve */
         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_DNS_USE_GLOBAL_CACHE(3), CURLOPT_DNS_SERVERS(3), CURLOPT_RESOLVE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                    CURLOPT_DNS_CACHE_TIMEOUT(3)
CURLOPT_DNS_INTERFACE(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_DNS_INTERFACE(3)



NAME
~~~
       CURLOPT_DNS_INTERFACE - interface to speak DNS over

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DNS_INTERFACE, char *ifname);

~~~
DESCRIPTION
~~~
       Pass  a char * as parameter. Set the name of the network interface that the DNS resolver should bind to. This must be an interface name (not an address). Set this option to NULL to use the default
       setting (don't bind to a specific interface).

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
         curl_easy_setopt(curl, CURLOPT_DNS_INTERFACE, "eth0");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.33.0. This option also requires that libcurl was built with a resolver backend that supports this operation. The c-ares backend is the only such one.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_NOT_BUILT_IN if support was disabled at compile-time.

~~~
SEE ALSO
       CURLOPT_DNS_SERVERS(3), CURLOPT_DNS_LOCAL_IP4(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                        CURLOPT_DNS_INTERFACE(3)
CURLOPT_DNS_LOCAL_IP4(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_DNS_LOCAL_IP4(3)



NAME
~~~
       CURLOPT_DNS_LOCAL_IP4 - IPv4 address to bind DNS resolves to

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DNS_LOCAL_IP4, char *address);

~~~
DESCRIPTION
~~~
       Set  the  local  IPv4  address  that the resolver should bind to. The argument should be of type char * and contain a single numerical IPv4 address as a string.  Set this option to NULL to use the
       default setting (don't bind to a specific IP address).

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
         curl_easy_setopt(curl, CURLOPT_DNS_LOCAL_IP4, "192.168.0.14");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       This option requires that libcurl was built with a resolver backend that supports this operation. The c-ares backend is the only such one.

       Added in 7.33.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, CURLE_NOT_BUILT_IN if support was disabled at compile-time, or CURLE_BAD_FUNCTION_ARGUMENT when given a bad address.

~~~
SEE ALSO
       CURLOPT_DNS_INTERFACE(3), CURLOPT_DNS_LOCAL_IP6(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                        CURLOPT_DNS_LOCAL_IP4(3)
CURLOPT_DNS_LOCAL_IP6(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_DNS_LOCAL_IP6(3)



NAME
~~~
       CURLOPT_DNS_LOCAL_IP6 - IPv6 address to bind DNS resolves to

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DNS_LOCAL_IP6, char *address);

~~~
DESCRIPTION
~~~
       Set  the  local IPv6 address that the resolver should bind to. The argument should be of type char * and contain a single IPv6 address as a string.  Set this option to NULL to use the default setting (don't bind to a specific IP address).

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
         curl_easy_setopt(curl, CURLOPT_DNS_LOCAL_IP6, "fe80::a9ff:fe46:b619");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       This option requires that libcurl was built with a resolver backend that supports this operation. The c-ares backend is the only such one.

       Added in 7.33.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, CURLE_NOT_BUILT_IN if support was disabled at compile-time, or CURLE_BAD_FUNCTION_ARGUMENT when given a bad address.

~~~
SEE ALSO
       CURLOPT_DNS_INTERFACE(3), CURLOPT_DNS_LOCAL_IP4(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                        CURLOPT_DNS_LOCAL_IP6(3)
CURLOPT_DNS_SERVERS(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_DNS_SERVERS(3)



NAME
~~~
       CURLOPT_DNS_SERVERS - DNS servers to use

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DNS_SERVERS, char *servers);

~~~
DESCRIPTION
~~~
       Pass a char * that is the list of DNS servers to be used instead of the system default. The format of the dns servers option is:

       host[:port][,host[:port]]...

       For example:

       192.168.1.100,192.168.1.101,3.4.5.6

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL - use system default

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
         curl_easy_setopt(curl, CURLOPT_DNS_SERVERS, "192.168.1.100:53,192.168.1.101");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       This option requires that libcurl was built with a resolver backend that supports this operation. The c-ares backend is the only such one.

       Added in 7.24.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, CURLE_NOT_BUILT_IN if support was disabled at compile-time, CURLE_BAD_FUNCTION_ARGUMENT when given an invalid server list,
       or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_DNS_LOCAL_IP4(3), CURLOPT_DNS_CACHE_TIMEOUT(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                          CURLOPT_DNS_SERVERS(3)
CURLOPT_DNS_SHUFFLE_ADDRESSES(3)                                                          curl_easy_setopt options                                                         CURLOPT_DNS_SHUFFLE_ADDRESSES(3)



NAME
~~~
       CURLOPT_DNS_SHUFFLE_ADDRESSES - shuffle IP addresses for hostname

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DNS_SHUFFLE_ADDRESSES, long onoff);

~~~
DESCRIPTION
~~~
       When a name is resolved and more than one IP address is returned, shuffle the order of all returned addresses so that they will be used in a random order.  This is similar to the ordering behavior
       of gethostbyname which is no longer used on most platforms.

       Addresses will not be reshuffled if a name resolution is completed using the DNS cache. CURLOPT_DNS_CACHE_TIMEOUT(3) can be used together with this option to reduce DNS cache  timeout  or  disable
       caching entirely if frequent reshuffling is needed.

       Since  the addresses returned will be reordered randomly, their order will not be in accordance with RFC 3484 or any other deterministic order that may be generated by the system's name resolution
       implementation. This may have performance impacts and may cause IPv4 to be used before IPv6 or vice versa.

DEFAULT
       0 (disabled)

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_DNS_SHUFFLE_ADDRESSES, 1L);

         curl_easy_perform(curl);

         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.60.0

~~~
RETURN VALUE
~~~
       CURLE_OK or an error such as CURLE_UNKNOWN_OPTION.

~~~
SEE ALSO
       CURLOPT_DNS_CACHE_TIMEOUT(3), CURLOPT_IPRESOLVE(3),



libcurl 7.60.0                                                                                  3 March 2018                                                               CURLOPT_DNS_SHUFFLE_ADDRESSES(3)
CURLOPT_DNS_USE_GLOBAL_CACHE(3)                                                           curl_easy_setopt options                                                          CURLOPT_DNS_USE_GLOBAL_CACHE(3)



NAME
~~~
       CURLOPT_DNS_USE_GLOBAL_CACHE - global DNS cache

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DNS_USE_GLOBAL_CACHE,
                                 long enable);

~~~
DESCRIPTION
~~~
       Has no function since 7.62.0. Do not use!

       Pass  a  long.  If  the enable value is 1, it tells curl to use a global DNS cache that will survive between easy handle creations and deletions. This is not thread-safe and this will use a global
       variable.

       WARNING: this option is considered obsolete. Stop using it. Switch over to using the share interface instead! See CURLOPT_SHARE(3) and curl_share_init(3).

DEFAULT
       0

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* switch off the use of a global, thread unsafe, cache */
         curl_easy_setopt(curl, CURLOPT_DNS_USE_GLOBAL_CACHE, 0L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Deprecated since 7.62.0. Has no function.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SHARE(3), CURLOPT_DNS_CACHE_TIMEOUT(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                 CURLOPT_DNS_USE_GLOBAL_CACHE(3)
CURLOPT_DOH_SSL_VERIFYHOST(3)                                                             curl_easy_setopt options                                                            CURLOPT_DOH_SSL_VERIFYHOST(3)



NAME
~~~
       CURLOPT_DOH_SSL_VERIFYHOST - verify the host name in the DoH SSL certificate

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DOH_SSL_VERIFYHOST, long verify);

~~~
DESCRIPTION
~~~
       Pass a long set to 2L as asking curl to verify the DoH (DNS-over-HTTPS) server's certificate name fields against the host name.

       This option is the DoH equivalent of CURLOPT_SSL_VERIFYHOST(3) and only affects requests to the DoH server.

       When  CURLOPT_DOH_SSL_VERIFYHOST(3)  is 2, the SSL certificate provided by the DoH server must indicate that the server name is the same as the server name to which you meant to connect to, or the
       connection fails.

       Curl considers the DoH server the intended one when the Common Name field or a Subject Alternate Name field in the certificate matches the host name in the DoH URL to which you told Curl  to  connect.

       When the verify value is set to 1L it is treated the same as 2L. However for consistency with the other VERIFYHOST options we suggest use 2 and not 1.

       When the verify value is set to 0L, the connection succeeds regardless of the names used in the certificate. Use that ability with caution!

       See  also  CURLOPT_DOH_SSL_VERIFYPEER(3)  to  verify  the  digital  signature  of  the  DoH  server  certificate.   If  libcurl is built against NSS and CURLOPT_DOH_SSL_VERIFYPEER(3) is zero, CURLOPT_DOH_SSL_VERIFYHOST(3) is also set to zero and cannot be overridden.

DEFAULT
       2

~~~
PROTOCOLS
~~~
       DoH

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         curl_easy_setopt(curl, CURLOPT_DOH_URL, "https://cloudflare-dns.com/dns-query");

         /* Disable host name verification of the DoH server */
         curl_easy_setopt(curl, CURLOPT_DOH_SSL_VERIFYHOST, 0L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.76.0

       If built TLS enabled.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_DOH_SSL_VERIFYPEER(3), CURLOPT_SSL_VERIFYHOST(3), CURLOPT_SSL_VERIFYPEER(3), CURLOPT_PROXY_SSL_VERIFYPEER(3), CURLOPT_PROXY_SSL_VERIFYHOST(3),



libcurl 7.76.0                                                                                  11 Feb 2021                                                                   CURLOPT_DOH_SSL_VERIFYHOST(3)
CURLOPT_DOH_SSL_VERIFYPEER(3)                                                             curl_easy_setopt options                                                            CURLOPT_DOH_SSL_VERIFYPEER(3)



NAME
~~~
       CURLOPT_DOH_SSL_VERIFYPEER - verify the DoH SSL certificate

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DOH_SSL_VERIFYPEER, long verify);

~~~
DESCRIPTION
~~~
       Pass a long as parameter set to 1L to enable or 0L to disable.

       This option tells curl to verify the authenticity of the DoH (DNS-over-HTTPS) server's certificate. A value of 1 means curl verifies; 0 (zero) means it doesn't.

       This option is the DoH equivalent of CURLOPT_SSL_VERIFYPEER(3) and only affects requests to the DoH server.

       When negotiating a TLS or SSL connection, the server sends a certificate indicating its identity. Curl verifies whether the certificate is authentic, i.e. that you can trust that the server is who
       the certificate says it is.  This trust is based on a chain of digital signatures, rooted in certification authority (CA) certificates you supply.  curl uses a default bundle  of  CA  certificates
       (the path for that is determined at build time) and you can specify alternate certificates with the CURLOPT_CAINFO(3) option or the CURLOPT_CAPATH(3) option.

       When CURLOPT_DOH_SSL_VERIFYPEER(3) is enabled, and the verification fails to prove that the certificate is authentic, the connection fails.  When the option is zero, the peer certificate verification succeeds regardless.

       Authenticating the certificate is not enough to be sure about the server. You typically also want to ensure that the server is the server you mean to be talking  to.   Use  CURLOPT_DOH_SSL_VERIFYHOST(3) for that. The check that the host name in the certificate is valid for the host name you're connecting to is done independently of the CURLOPT_DOH_SSL_VERIFYPEER(3) option.

       WARNING: disabling verification of the certificate allows bad guys to man-in-the-middle the communication without you knowing it. Disabling verification makes the communication insecure. Just having encryption on a transfer is not enough as you cannot be sure that you are communicating with the correct end-point.

DEFAULT
       1

~~~
PROTOCOLS
~~~
       DoH

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         curl_easy_setopt(curl, CURLOPT_DOH_URL, "https://cloudflare-dns.com/dns-query");

         /* Disable certificate verification of the DoH server */
         curl_easy_setopt(curl, CURLOPT_DOH_SSL_VERIFYPEER, 0L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.76.0

       If built TLS enabled.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_DOH_SSL_VERIFYHOST(3), CURLOPT_SSL_VERIFYHOST(3), CURLOPT_SSL_VERIFYPEER(3), CURLOPT_PROXY_SSL_VERIFYPEER(3), CURLOPT_PROXY_SSL_VERIFYHOST(3), CURLOPT_CAINFO(3), CURLOPT_CAPATH(3),



libcurl 7.76.0                                                                                  11 Feb 2021                                                                   CURLOPT_DOH_SSL_VERIFYPEER(3)
CURLOPT_DOH_SSL_VERIFYSTATUS(3)                                                           curl_easy_setopt options                                                          CURLOPT_DOH_SSL_VERIFYSTATUS(3)



NAME
~~~
       CURLOPT_DOH_SSL_VERIFYSTATUS - verify the DoH SSL certificate's status

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DOH_SSL_VERIFYSTATUS, long verify);

~~~
DESCRIPTION
~~~
       Pass a long as parameter set to 1 to enable or 0 to disable.

       This option determines whether libcurl verifies the status of the DoH (DNS-over-HTTPS) server cert using the "Certificate Status Request" TLS extension (aka. OCSP stapling).

       This option is the DoH equivalent of CURLOPT_SSL_VERIFYSTATUS(3) and only affects requests to the DoH server.

       Note that if this option is enabled but the server does not support the TLS extension, the verification will fail.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       DoH

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         curl_easy_setopt(curl, CURLOPT_DOH_URL, "https://cloudflare-dns.com/dns-query");

         /* Ask for OCSP stapling when verifying the DoH server */
         curl_easy_setopt(curl, CURLOPT_DOH_SSL_VERIFYSTATUS, 1L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.76.0. This option is currently only supported by the OpenSSL, GnuTLS and NSS TLS backends.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if OCSP stapling is supported by the SSL backend, otherwise returns CURLE_NOT_BUILT_IN.

~~~
SEE ALSO
       CURLOPT_DOH_SSL_VERIFYHOST(3), CURLOPT_DOH_SSL_VERIFYPEER(3), CURLOPT_SSL_VERIFYSTATUS(3),



libcurl 7.76.0                                                                                  11 Feb 2021                                                                 CURLOPT_DOH_SSL_VERIFYSTATUS(3)
CURLOPT_DOH_URL(3)                                                                        curl_easy_setopt options                                                                       CURLOPT_DOH_URL(3)



NAME
~~~
       CURLOPT_DOH_URL - provide the DNS-over-HTTPS URL

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_DOH_URL, char *URL);

~~~
DESCRIPTION
~~~
       Pass  in  a  pointer  to  a  URL  for  the  DoH  server  to  use for name resolving. The parameter should be a char * to a null-terminated string which must be URL-encoded in the following format:
       "https://host:port/path". It MUST specify a HTTPS URL.

       libcurl doesn't validate the syntax or use this variable until the transfer is issued. Even if you set a crazy value here, curl_easy_setopt(3) will still return CURLE_OK.

       curl sends POST requests to the given DNS-over-HTTPS URL.

       To find the DoH server itself, which might be specified using a name, libcurl will use the default name lookup function. You can bootstrap that by providing the address for  the  DoH  server  with
       CURLOPT_RESOLVE(3).

       Disable DoH use again by setting this option to NULL.

INHERIT OPTIONS
       DoH lookups use SSL and some SSL settings from your transfer are inherited, like CURLOPT_SSL_CTX_FUNCTION(3).

       The hostname and peer certificate verification settings are not inherited but can be controlled separately via CURLOPT_DOH_SSL_VERIFYHOST(3) and CURLOPT_DOH_SSL_VERIFYPEER(3).

       A set CURLOPT_OPENSOCKETFUNCTION(3) callback is not inherited.

DEFAULT
       NULL - there is no default DoH URL. If this option isn't set, libcurl will use the default name resolver.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_DOH_URL, "https://dns.example.com");
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.62.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK on success or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

       Note that curl_easy_setopt(3) won't actually parse the given string so given a bad DoH URL, curl will not detect a problem until it tries to resolve a name with it.

~~~
SEE ALSO
       CURLOPT_VERBOSE(3), CURLOPT_RESOLVE(3),



libcurl 7.62.0                                                                                  18 Jun 2018                                                                              CURLOPT_DOH_URL(3)
CURLOPT_EGDSOCKET(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_EGDSOCKET(3)



NAME
~~~
       CURLOPT_EGDSOCKET - EGD socket path

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_EGDSOCKET, char *path);

~~~
DESCRIPTION
~~~
       Pass a char * to the null-terminated path name to the Entropy Gathering Daemon socket. It will be used to seed the random engine for TLS.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_EGDSOCKET, "/var/egd.socket");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       If built with TLS enabled. Only the OpenSSL backend will use this.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_RANDOM_FILE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                            CURLOPT_EGDSOCKET(3)
CURLOPT_ERRORBUFFER(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_ERRORBUFFER(3)



NAME
~~~
       CURLOPT_ERRORBUFFER - error buffer for error messages

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_ERRORBUFFER, char *buf);

~~~
DESCRIPTION
~~~
       Pass  a  char  *  to a buffer that libcurl may store human readable error messages on failures or problems. This may be more helpful than just the return code from curl_easy_perform(3) and related
       functions. The buffer must be at least CURL_ERROR_SIZE bytes big.

       You must keep the associated buffer available until libcurl no longer needs it. Failing  to  do  so  will  cause  very  odd  behavior  or  even  crashes.  libcurl  will  need  it  until  you  call
       curl_easy_cleanup(3) or you set the same option again to use a different pointer.

       Do  not rely on the contents of the buffer unless an error code was returned.  Since 7.60.0 libcurl will initialize the contents of the error buffer to an empty string before performing the transfer. For earlier versions if an error code was returned but there was no error detail then the buffer is untouched.

       Consider CURLOPT_VERBOSE(3) and CURLOPT_DEBUGFUNCTION(3) to better debug and trace why errors happen.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         CURLcode res;
         char errbuf[CURL_ERROR_SIZE];

         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* provide a buffer to store errors in */
         curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errbuf);

         /* set the error buffer as empty before performing a request */
         errbuf[0] = 0;

         /* perform the request */
         res = curl_easy_perform(curl);

         /* if the request did not complete correctly, show the error
         information. if no detailed error information was written to errbuf
         show the more generic information from curl_easy_strerror instead.
         */
         if(res != CURLE_OK) {
           size_t len = strlen(errbuf);
           fprintf(stderr, "\nlibcurl: (%d) ", res);
           if(len)
             fprintf(stderr, "%s%s", errbuf,
                     ((errbuf[len - 1] != '\n') ? "\n" : ""));
           else
             fprintf(stderr, "%s\n", curl_easy_strerror(res));
         }
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_DEBUGFUNCTION(3), CURLOPT_VERBOSE(3), curl_easy_strerror(3), curl_multi_strerror(3), curl_share_strerror(3), curl_url_strerror(3)



libcurl 7.37.0                                                                                  17 Jun 2014                                                                          CURLOPT_ERRORBUFFER(3)
CURLOPT_EXPECT_100_TIMEOUT_MS(3)                                                          curl_easy_setopt options                                                         CURLOPT_EXPECT_100_TIMEOUT_MS(3)



NAME
~~~
       CURLOPT_EXPECT_100_TIMEOUT_MS - timeout for Expect: 100-continue response

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_EXPECT_100_TIMEOUT_MS,
                                 long milliseconds);

~~~
DESCRIPTION
~~~
       Pass a long to tell libcurl the number of milliseconds to wait for a server response with the HTTP status 100 (Continue), 417 (Expectation Failed) or similar after sending an HTTP request containing an Expect: 100-continue header. If this times out before a response is received, the request body is sent anyway.

DEFAULT
       1000 milliseconds

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* wait 3 seconds for 100-continue */
         curl_easy_setopt(curl, CURLOPT_EXPECT_100_TIMEOUT_MS, 3000L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.36.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_POST(3), CURLOPT_HTTPPOST(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                CURLOPT_EXPECT_100_TIMEOUT_MS(3)
CURLOPT_FAILONERROR(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_FAILONERROR(3)



NAME
~~~
       CURLOPT_FAILONERROR - request failure on HTTP response >= 400

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FAILONERROR, long fail);

~~~
DESCRIPTION
~~~
       A  long  parameter  set  to 1 tells the library to fail the request if the HTTP code returned is equal to or larger than 400. The default action would be to return the page normally, ignoring that
       code.

       This method is not fail-safe and there are occasions where non-successful response codes will slip through, especially when authentication is involved (response codes 401 and 407).

       You might get some amounts of headers transferred before this situation is detected, like when a "100-continue" is received as a response to a POST/PUT and a 401 or  407  is  received  immediately
       afterwards.

       When this option is used and an error is detected, it will cause the connection to get closed and CURLE_HTTP_RETURNED_ERROR is returned.

DEFAULT
       0, do not fail on error

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L);
         ret = curl_easy_perform(curl);
         if(ret == CURLE_HTTP_RETURNED_ERROR) {
           /* an HTTP response error problem */
         }
       }

~~~
AVAILABILITY
~~~
       Along with HTTP.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is enabled, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_HTTP200ALIASES(3), CURLOPT_KEEP_SENDING_ON_ERROR(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                          CURLOPT_FAILONERROR(3)
CURLOPT_FILETIME(3)                                                                       curl_easy_setopt options                                                                      CURLOPT_FILETIME(3)



NAME
~~~
       CURLOPT_FILETIME - get the modification time of the remote resource

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FILETIME, long gettime);

~~~
DESCRIPTION
~~~
       Pass a long. If it is 1, libcurl will attempt to get the modification time of the remote document in this operation. This requires that the remote server sends the time or replies to a time querying command. The curl_easy_getinfo(3) function with the CURLINFO_FILETIME(3) argument can be used after a transfer to extract the received time (if any).

DEFAULT
       0

~~~
PROTOCOLS
~~~
       HTTP, FTP, SFTP, FILE

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, url);
         /* Ask for filetime */
         curl_easy_setopt(curl, CURLOPT_FILETIME, 1L);
         res = curl_easy_perform(curl);
         if(CURLE_OK == res) {
           res = curl_easy_getinfo(curl, CURLINFO_FILETIME, &filetime);
           if((CURLE_OK == res) && (filetime >= 0)) {
             time_t file_time = (time_t)filetime;
             printf("filetime %s: %s", filename, ctime(&file_time));
           }
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Always, for SFTP since 7.49.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       curl_easy_getinfo(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                             CURLOPT_FILETIME(3)
CURLOPT_FNMATCH_DATA(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_FNMATCH_DATA(3)



NAME
~~~
       CURLOPT_FNMATCH_DATA - custom pointer to fnmatch callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FNMATCH_DATA,
                                 void *pointer);

~~~
DESCRIPTION
~~~
       Pass a pointer that will be untouched by libcurl and passed as the ptr argument to the CURLOPT_FNMATCH_FUNCTION(3).

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       static int my_fnmatch(void *clientp,
                             const char *pattern, const char *string)
       {
         struct local_stuff *data = (struct local_stuff *)clientp;
         if(string_match(pattern, string))
           return CURL_FNMATCHFUNC_MATCH;
         else
           return CURL_FNMATCHFUNC_NOMATCH;
       }

       {
         struct local_stuff local_data;
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://ftp.example.com/file*");
         curl_easy_setopt(curl, CURLOPT_WILDCARDMATCH, 1L);
         curl_easy_setopt(curl, CURLOPT_FNMATCH_FUNCTION, my_fnmatch);
         curl_easy_setopt(curl, CURLOPT_FNMATCH_DATA, &local_data);
       }

~~~
AVAILABILITY
~~~
       Added in 7.21.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_FNMATCH_FUNCTION(3), CURLOPT_WILDCARDMATCH(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                         CURLOPT_FNMATCH_DATA(3)
CURLOPT_FNMATCH_FUNCTION(3)                                                               curl_easy_setopt options                                                              CURLOPT_FNMATCH_FUNCTION(3)



NAME
~~~
       CURLOPT_FNMATCH_FUNCTION - wildcard match callback

~~~
NAME
~~~
       #include <curl/curl.h>

       int fnmatch_callback(void *ptr,
                            const char *pattern,
                            const char *string);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FNMATCH_FUNCTION,
                                 fnmatch_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       This callback is used for wildcard matching.

       Return CURL_FNMATCHFUNC_MATCH if pattern matches the string, CURL_FNMATCHFUNC_NOMATCH if not or CURL_FNMATCHFUNC_FAIL if an error occurred.

DEFAULT
       NULL == an internal function for wildcard matching.

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       static int my_fnmatch(void *clientp,
                             const char *pattern, const char *string)
       {
         struct local_stuff *data = (struct local_stuff *)clientp;
         if(string_match(pattern, string))
           return CURL_FNMATCHFUNC_MATCH;
         else
           return CURL_FNMATCHFUNC_NOMATCH;
       }

       {
         struct local_stuff local_data;
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://ftp.example.com/file*");
         curl_easy_setopt(curl, CURLOPT_WILDCARDMATCH, 1L);
         curl_easy_setopt(curl, CURLOPT_FNMATCH_FUNCTION, my_fnmatch);
         curl_easy_setopt(curl, CURLOPT_FNMATCH_DATA, &local_data);
       }

~~~
AVAILABILITY
~~~
       Added in 7.21.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_FNMATCH_DATA(3), CURLOPT_DEBUGFUNCTION(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                     CURLOPT_FNMATCH_FUNCTION(3)
CURLOPT_FOLLOWLOCATION(3)                                                                 curl_easy_setopt options                                                                CURLOPT_FOLLOWLOCATION(3)



NAME
~~~
       CURLOPT_FOLLOWLOCATION - follow HTTP 3xx redirects

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FOLLOWLOCATION, long enable);

~~~
DESCRIPTION
~~~
       A  long  parameter  set  to  1 tells the library to follow any Location: header that the server sends as part of an HTTP header in a 3xx response. The Location: header can specify a relative or an
       absolute URL to follow.

       libcurl will issue another request for the new URL and follow new Location: headers all the way until no more such headers are returned.  CURLOPT_MAXREDIRS(3) can be used to limit  the  number  of
       redirects libcurl will follow.

       libcurl  limits  what  protocols  it  automatically  follows to. The accepted protocols are set with CURLOPT_REDIR_PROTOCOLS(3). By default libcurl will allow HTTP, HTTPS, FTP and FTPS on redirect
       (7.65.2). Older versions of libcurl allowed all protocols on redirect except those disabled for security reasons: Since 7.19.4 FILE and SCP are disabled, and since 7.40.0 SMB  and  SMBS  are  also
       disabled.

       When  following  a  Location:, the 3xx response code that redirected it also dictates which request method it will use in the subsequent request: For 301, 302 and 303 responses libcurl will switch
       method from POST to GET unless CURLOPT_POSTREDIR(3) instructs libcurl otherwise. All other 3xx codes will make libcurl send the same method again.

       For users who think the existing location following is too naive, too simple or just lacks features, it is very easy  to  instead  implement  your  own  redirect  follow  logic  with  the  use  of
       curl_easy_getinfo(3)'s CURLINFO_REDIRECT_URL(3) option instead of using CURLOPT_FOLLOWLOCATION(3).

DEFAULT
       0, disabled

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* example.com is redirected, so we tell libcurl to follow redirection */
         curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Along with HTTP

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_REDIR_PROTOCOLS(3), CURLOPT_PROTOCOLS(3), CURLOPT_POSTREDIR(3), CURLINFO_REDIRECT_URL(3), CURLINFO_REDIRECT_COUNT(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                       CURLOPT_FOLLOWLOCATION(3)
CURLOPT_FORBID_REUSE(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_FORBID_REUSE(3)



NAME
~~~
       CURLOPT_FORBID_REUSE - make connection get closed at once after use

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FORBID_REUSE, long close);

~~~
DESCRIPTION
~~~
       Pass  a  long.  Set close to 1 to make libcurl explicitly close the connection when done with the transfer. Normally, libcurl keeps all connections alive when done with one transfer in case a succeeding one follows that can re-use them.  This option should be used with caution and only if you understand what it does as it can seriously impact performance.

       Set to 0 to have libcurl keep the connection open for possible later re-use (default behavior).

DEFAULT
       0

~~~
PROTOCOLS
~~~
       Most

~~~
EXAMPLE
~~~
       {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_FORBID_REUSE, 1L);
         curl_easy_perform(curl);

         /* this second transfer may not reuse the same connection */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_FRESH_CONNECT(3), CURLOPT_MAXCONNECTS(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                         CURLOPT_FORBID_REUSE(3)
CURLOPT_FRESH_CONNECT(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_FRESH_CONNECT(3)



NAME
~~~
       CURLOPT_FRESH_CONNECT - force a new connection to be used

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FRESH_CONNECT, long fresh);

~~~
DESCRIPTION
~~~
       Pass  a  long. Set to 1 to make the next transfer use a new (fresh) connection by force instead of trying to re-use an existing one.  This option should be used with caution and only if you understand what it does as it may seriously impact performance.

       Related functionality is CURLOPT_FORBID_REUSE(3) which makes sure the connection is closed after use so that it won't be re-used.

       Set fresh to 0 to have libcurl attempt re-using an existing connection (default behavior).

DEFAULT
       0

~~~
PROTOCOLS
~~~
       Most

~~~
EXAMPLE
~~~
       {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_FRESH_CONNECT, 1L);
         /* this transfer must use a new connection, not reuse an existing */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_FORBID_REUSE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                        CURLOPT_FRESH_CONNECT(3)
CURLOPT_FTP_ACCOUNT(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_FTP_ACCOUNT(3)



NAME
~~~
       CURLOPT_FTP_ACCOUNT - account info for FTP

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FTP_ACCOUNT, char *account);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to  a null-terminated string (or NULL to disable). When an FTP server asks for "account data" after user name and password has been provided, this data is sent off using the ACCT
       command.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/foo.bin");

         curl_easy_setopt(curl, CURLOPT_FTP_ACCOUNT, "human-resources");

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.13.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_USERNAME(3), CURLOPT_PASSWORD(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                          CURLOPT_FTP_ACCOUNT(3)
CURLOPT_FTP_ALTERNATIVE_TO_USER(3)                                                        curl_easy_setopt options                                                       CURLOPT_FTP_ALTERNATIVE_TO_USER(3)



NAME
~~~
       CURLOPT_FTP_ALTERNATIVE_TO_USER - command to use instead of USER with FTP

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FTP_ALTERNATIVE_TO_USER,
                                 char *cmd);

~~~
DESCRIPTION
~~~
       Pass  a char * as parameter, pointing to a string which will be used to authenticate if the usual FTP "USER user" and "PASS password" negotiation fails. This is currently only known to be required
       when connecting to Tumbleweed's Secure Transport FTPS server using client certificates for authentication.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/foo.bin");

         curl_easy_setopt(curl, CURLOPT_FTP_ALTERNATIVE_TO_USER, "two users");

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.15.5

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_FTP_SKIP_PASV_IP(3), CURLOPT_FTP_RESPONSE_TIMEOUT(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                              CURLOPT_FTP_ALTERNATIVE_TO_USER(3)
CURLOPT_FTP_CREATE_MISSING_DIRS(3)                                                        curl_easy_setopt options                                                       CURLOPT_FTP_CREATE_MISSING_DIRS(3)



NAME
~~~
       CURLOPT_FTP_CREATE_MISSING_DIRS - create missing dirs for FTP and SFTP

~~~
NAME
~~~
       #include <curl/curl.h>

       typedef enum {
         CURLFTP_CREATE_DIR_NONE,
         CURLFTP_CREATE_DIR,
         CURLFTP_CREATE_DIR_RETRY
       } curl_ftpcreatedir;

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FTP_CREATE_MISSING_DIRS,
                                 long create);

~~~
DESCRIPTION
~~~
       Pass a long telling libcurl to create the dir. If the value is CURLFTP_CREATE_DIR (1), libcurl will attempt to create any remote directory that it fails to "move" into.

       For FTP requests, that means a CWD command fails. CWD being the command that changes working directory.

       For  SFTP  requests,  libcurl  will attempt to create the remote directory if it can't obtain a handle to the target-location. The creation will fail if a file of the same name as the directory to
       create already exists or lack of permissions prevents creation.

       Setting create to CURLFTP_CREATE_DIR_RETRY (2), tells libcurl to retry the CWD command again if the subsequent MKD command fails. This is especially useful if you're doing many  simultaneous  connections  against  the  same  server and they all have this option enabled, as then CWD may first fail but then another connection does MKD before this connection and thus MKD fails but trying CWD
       works!

DEFAULT
       CURLFTP_CREATE_DIR_NONE (0)

~~~
PROTOCOLS
~~~
       FTP and SFTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/non-existing/new.txt");
         curl_easy_setopt(curl, CURLOPT_FTP_CREATE_MISSING_DIRS,
                                CURLFTP_CREATE_DIR_RETRY);

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.10.7. SFTP support added in 7.16.3. The retry option was added in 7.19.4.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if the create value is not.

~~~
SEE ALSO
       CURLOPT_FTP_FILEMETHOD(3), CURLOPT_FTP_USE_EPSV(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                              CURLOPT_FTP_CREATE_MISSING_DIRS(3)
CURLOPT_FTP_FILEMETHOD(3)                                                                 curl_easy_setopt options                                                                CURLOPT_FTP_FILEMETHOD(3)



NAME
~~~
       CURLOPT_FTP_FILEMETHOD - select directory traversing method for FTP

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FTP_FILEMETHOD,
                                 long method);

~~~
DESCRIPTION
~~~
       Pass a long telling libcurl which method to use to reach a file on a FTP(S) server.

       This option exists because some server implementations aren't compliant to what the standards say should work.

       The argument should be one of the following alternatives:

       CURLFTPMETHOD_MULTICWD
              libcurl  does  a single CWD operation for each path part in the given URL. For deep hierarchies this means many commands. This is how RFC1738 says it should be done. This is the default but
              the slowest behavior.

       CURLFTPMETHOD_NOCWD
              libcurl does no CWD at all. libcurl will do SIZE, RETR, STOR etc and give a full path to the server for all these commands. This is the fastest behavior.

       CURLFTPMETHOD_SINGLECWD
              libcurl does one CWD with the full target directory and then operates on the file "normally" (like in the multicwd case). This is somewhat more standards compliant than 'nocwd' but  without
              the full penalty of 'multicwd'.

DEFAULT
       CURLFTPMETHOD_MULTICWD

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/1/2/3/4/new.txt");
         curl_easy_setopt(curl, CURLOPT_FTP_FILEMETHOD,
                                CURLFTPMETHOD_SINGLECWD);

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.15.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_DIRLISTONLY(3), CURLOPT_FTP_SKIP_PASV_IP(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                       CURLOPT_FTP_FILEMETHOD(3)
CURLOPT_FTPPORT(3)                                                                        curl_easy_setopt options                                                                       CURLOPT_FTPPORT(3)



NAME
~~~
       CURLOPT_FTPPORT - make FTP transfer active

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FTPPORT, char *spec);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to a null-terminated string as parameter. It specifies that the FTP transfer will be made actively and the given string will be used to get the IP address to use for the FTP PORT
       instruction.

       The PORT instruction tells the remote server to connect to our specified IP address. The string may be a plain IP address, a host name, a network interface name (under Unix) or just a  '-'  symbol
       to let the library use your system's default IP address. Default FTP operations are passive, and thus won't use PORT.

       The  address  can be followed by a ':' to specify a port, optionally followed by a '-' to specify a port range.  If the port specified is 0, the operating system will pick a free port.  If a range
       is provided and all ports in the range are not available, libcurl will report CURLE_FTP_PORT_FAILED for the handle.  Invalid port/range settings are ignored.  IPv6 addresses followed by a port  or
       portrange have to be in brackets.  IPv6 addresses without port/range specifier can be in brackets.

       Examples with specified ports:

         eth0:0
         192.168.1.2:32000-33000
         curl.se:32123
         [::1]:1234-4567

       You disable PORT again and go back to using the passive version by setting this option to NULL.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/old-server/file.txt");
         curl_easy_setopt(curl, CURLOPT_FTPPORT, "-");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Port range support was added in 7.19.5

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_FTP_USE_EPRT(3), CURLOPT_FTP_USE_EPSV(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                              CURLOPT_FTPPORT(3)
CURLOPT_FTP_RESPONSE_TIMEOUT(3)                                                           curl_easy_setopt options                                                          CURLOPT_FTP_RESPONSE_TIMEOUT(3)



NAME
~~~
       CURLOPT_FTP_RESPONSE_TIMEOUT - time allowed to wait for FTP response

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FTP_RESPONSE_TIMEOUT, long timeout);

~~~
DESCRIPTION
~~~
       Pass  a  long.  Causes libcurl to set a timeout period (in seconds) on the amount of time that the server is allowed to take in order to send a response message for a command before the session is
       considered dead.  While libcurl is waiting for a response, this value overrides CURLOPT_TIMEOUT(3).  It  is  recommended  that  if  used  in  conjunction  with  CURLOPT_TIMEOUT(3),  you  set  CURLOPT_FTP_RESPONSE_TIMEOUT(3) to a value smaller than CURLOPT_TIMEOUT(3).

DEFAULT
       None

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/slow.txt");
         /* wait no more than 23 seconds */
         curl_easy_setopt(curl, CURLOPT_FTP_RESPONSE_TIMEOUT, 23L);
         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.10.8

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if FTP is supported, and CURLE_UNKNOWN_OPTION if not. Returns CURLE_BAD_FUNCTION_ARGUMENT if set to a negative value or a value that when converted to milliseconds is too large.

~~~
SEE ALSO
       CURLOPT_TIMEOUT(3), CURLOPT_CONNECTTIMEOUT(3), CURLOPT_LOW_SPEED_LIMIT(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                 CURLOPT_FTP_RESPONSE_TIMEOUT(3)
CURLOPT_FTP_SKIP_PASV_IP(3)                                                               curl_easy_setopt options                                                              CURLOPT_FTP_SKIP_PASV_IP(3)



NAME
~~~
       CURLOPT_FTP_SKIP_PASV_IP - ignore the IP address in the PASV response

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FTP_SKIP_PASV_IP, long skip);

~~~
DESCRIPTION
~~~
       Pass  a  long.  If  skip  is  set  to 1, it instructs libcurl to not use the IP address the server suggests in its 227-response to libcurl's PASV command when libcurl connects the data connection.
       Instead libcurl will re-use the same IP address it already uses for the control connection. But it will use the port number from the 227-response.

       This option thus allows libcurl to work around broken server installations that due to NATs, firewalls or incompetence report the wrong IP address back. Setting the option also  reduces  the  risk
       for various sorts of client abuse by malicious servers.

       This option has no effect if PORT, EPRT or EPSV is used instead of PASV.

DEFAULT
       1 since 7.74.0, was 0 before then.

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/file.txt");

         /* please ignore the IP in the PASV response */
         curl_easy_setopt(curl, CURLOPT_FTP_SKIP_PASV_IP, 1L);
         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.14.2

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_FTPPORT(3), CURLOPT_FTP_USE_EPRT(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                     CURLOPT_FTP_SKIP_PASV_IP(3)
CURLOPT_FTPSSLAUTH(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_FTPSSLAUTH(3)



NAME
~~~
       CURLOPT_FTPSSLAUTH - order in which to attempt TLS vs SSL

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FTPSSLAUTH, long order);

~~~
DESCRIPTION
~~~
       Pass a long using one of the values from below, to alter how libcurl issues "AUTH TLS" or "AUTH SSL" when FTP over SSL is activated. This is only interesting if CURLOPT_USE_SSL(3) is also set.

       Possible order values:

       CURLFTPAUTH_DEFAULT
              Allow libcurl to decide.

       CURLFTPAUTH_SSL
              Try "AUTH SSL" first, and only if that fails try "AUTH TLS".

       CURLFTPAUTH_TLS
              Try "AUTH TLS" first, and only if that fails try "AUTH SSL".

DEFAULT
       CURLFTPAUTH_DEFAULT

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/file.txt");
         curl_easy_setopt(curl, CURLOPT_USE_SSL, CURLUSESSL_TRY);
         /* funny server, ask for SSL before TLS */
         curl_easy_setopt(curl, CURLOPT_FTPSSLAUTH, CURLFTPAUTH_SSL);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.12.2

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_USE_SSL(3), CURLOPT_FTP_SSL_CCC(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                           CURLOPT_FTPSSLAUTH(3)
CURLOPT_FTP_SSL_CCC(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_FTP_SSL_CCC(3)



NAME
~~~
       CURLOPT_FTP_SSL_CCC - switch off SSL again with FTP after auth

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FTP_SSL_CCC,
                                 long how);

~~~
DESCRIPTION
~~~
       If enabled, this option makes libcurl use CCC (Clear Command Channel). It shuts down the SSL/TLS layer after authenticating. The rest of the control channel communication will be unencrypted. This
       allows NAT routers to follow the FTP transaction. Pass a long using one of the values below

       CURLFTPSSL_CCC_NONE
              Don't attempt to use CCC.

       CURLFTPSSL_CCC_PASSIVE
              Do not initiate the shutdown, but wait for the server to do it. Do not send a reply.

       CURLFTPSSL_CCC_ACTIVE
              Initiate the shutdown and wait for a reply.

DEFAULT
       CURLFTPSSL_CCC_NONE

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/file.txt");
         curl_easy_setopt(curl, CURLOPT_USE_SSL, CURLUSESSL_CONTROL);
         /* go back to clear-text FTP after authenticating */
         curl_easy_setopt(curl, CURLOPT_FTP_SSL_CCC, CURLFTPSSL_CCC_ACTIVE);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.16.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_USE_SSL(3), CURLOPT_FTPSSLAUTH(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                          CURLOPT_FTP_SSL_CCC(3)
CURLOPT_FTP_USE_EPRT(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_FTP_USE_EPRT(3)



NAME
~~~
       CURLOPT_FTP_USE_EPRT - use EPRT for FTP

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FTP_USE_EPRT, long enabled);

~~~
DESCRIPTION
~~~
       Pass  a  long. If the value is 1, it tells curl to use the EPRT command when doing active FTP downloads (which is enabled by CURLOPT_FTPPORT(3)). Using EPRT means that it will first attempt to use
       EPRT before using PORT, but if you pass zero to this option, it will not try using EPRT, only plain PORT.

       If the server is an IPv6 host, this option will have no effect as EPRT is necessary then.

DEFAULT
~~~
PROTOCOLS
~~~
~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/file.txt");

         /* contact us back, aka "active" FTP */
         curl_easy_setopt(curl, CURLOPT_FTPPORT, "-");

         /* FTP the way the neanderthals did it */
         curl_easy_setopt(curl, CURLOPT_FTP_USE_EPRT, 0L);

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.10.5

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_FTP_USE_EPSV(3), CURLOPT_FTPPORT(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                         CURLOPT_FTP_USE_EPRT(3)
CURLOPT_FTP_USE_EPSV(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_FTP_USE_EPSV(3)



NAME
~~~
       CURLOPT_FTP_USE_EPSV - use EPSV for FTP

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FTP_USE_EPSV, long epsv);

~~~
DESCRIPTION
~~~
       Pass  epsv  as a long. If the value is 1, it tells curl to use the EPSV command when doing passive FTP downloads (which it does by default). Using EPSV means that it will first attempt to use EPSV
       before using PASV, but if you pass zero to this option, it will not try using EPSV, only plain PASV.

       If the server is an IPv6 host, this option will have no effect as of 7.12.3.

DEFAULT
       1

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/old-server/file.txt");

         /* let's shut off this modern feature */
         curl_easy_setopt(curl, CURLOPT_FTP_USE_EPSV, 0L);

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Along with FTP

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if FTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_FTP_USE_EPRT(3), CURLOPT_FTPPORT(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                         CURLOPT_FTP_USE_EPSV(3)
CURLOPT_FTP_USE_PRET(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_FTP_USE_PRET(3)



NAME
~~~
       CURLOPT_FTP_USE_PRET - use PRET for FTP

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_FTP_USE_PRET, long enable);

~~~
DESCRIPTION
~~~
       Pass a long. If the value is 1, it tells curl to send a PRET command before PASV (and EPSV). Certain FTP servers, mainly drftpd, require this non-standard command for directory listings as well as
       up and downloads in PASV mode. Has no effect when using the active FTP transfers mode.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/old-server/file.txt");

         /* a drftpd server, do it! */
         curl_easy_setopt(curl, CURLOPT_FTP_USE_PRET, 1L);

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.20.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_FTP_USE_EPRT(3), CURLOPT_FTP_USE_EPSV(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                         CURLOPT_FTP_USE_PRET(3)
CURLOPT_GSSAPI_DELEGATION(3)                                                              curl_easy_setopt options                                                             CURLOPT_GSSAPI_DELEGATION(3)



NAME
~~~
       CURLOPT_GSSAPI_DELEGATION - allowed GSS-API delegation

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_GSSAPI_DELEGATION, long level);

~~~
DESCRIPTION
~~~
       Set  the  long parameter level to CURLGSSAPI_DELEGATION_FLAG to allow unconditional GSSAPI credential delegation. The delegation is disabled by default since 7.21.7.  Set the parameter to CURLGSSAPI_DELEGATION_POLICY_FLAG to delegate only if the OK-AS-DELEGATE flag is set in the service ticket in case this  feature  is  supported  by  the  GSS-API  implementation  and  the  definition  of
       GSS_C_DELEG_POLICY_FLAG was available at compile-time.

DEFAULT
       CURLGSSAPI_DELEGATION_NONE

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* delegate if okayed by policy */
         curl_easy_setopt(curl, CURLOPT_GSSAPI_DELEGATION,
                                CURLGSSAPI_DELEGATION_POLICY_FLAG);
         ret = curl_easy_perform(curl);
       }


~~~
AVAILABILITY
~~~
       Added in 7.22.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_HTTPAUTH(3), CURLOPT_PROXYAUTH(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                    CURLOPT_GSSAPI_DELEGATION(3)
CURLOPT_HAPPY_EYEBALLS_TIMEOUT_MS(3)                                                      curl_easy_setopt options                                                     CURLOPT_HAPPY_EYEBALLS_TIMEOUT_MS(3)



NAME
~~~
       CURLOPT_HAPPY_EYEBALLS_TIMEOUT_MS - head start for ipv6 for happy eyeballs

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HAPPY_EYEBALLS_TIMEOUT_MS, long timeout);

~~~
DESCRIPTION
~~~
       Happy  eyeballs  is  an  algorithm that attempts to connect to both IPv4 and IPv6 addresses for dual-stack hosts, preferring IPv6 first for timeout milliseconds. If the IPv6 address cannot be connected to within that time then a connection attempt is made to the IPv4 address in parallel. The first connection to be established is the one that is used.

       The range of suggested useful values for timeout is limited. Happy Eyeballs RFC 6555 says "It is RECOMMENDED that connection attempts be paced 150-250 ms apart to  balance  human  factors  against
       network load." libcurl currently defaults to 200 ms. Firefox and Chrome currently default to 300 ms.

DEFAULT
       CURL_HET_DEFAULT (currently defined as 200L)

~~~
PROTOCOLS
~~~
       All except FILE

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_HAPPY_EYEBALLS_TIMEOUT_MS, 300L);

         curl_easy_perform(curl);

         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.59.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_CONNECTTIMEOUT_MS(3), CURLOPT_TIMEOUT(3), CURLOPT_LOW_SPEED_LIMIT(3),



libcurl 7.59.0                                                                                   1 Feb 2018                                                            CURLOPT_HAPPY_EYEBALLS_TIMEOUT_MS(3)
CURLOPT_HAPROXYPROTOCOL(3)                                                                curl_easy_setopt options                                                               CURLOPT_HAPROXYPROTOCOL(3)



NAME
~~~
       CURLOPT_HAPROXYPROTOCOL - send HAProxy PROXY protocol v1 header

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HAPROXYPROTOCOL,
                                 long haproxy_protocol);

~~~
DESCRIPTION
~~~
       A long parameter set to 1 tells the library to send an HAProxy PROXY protocol v1 header at beginning of the connection. The default action is not to send this header.

       This option is primarily useful when sending test requests to a service that expects this header.

       Most applications do not need this option.

DEFAULT
       0, do not send any HAProxy PROXY protocol header

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_HAPROXYPROTOCOL, 1L);
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Along with HTTP. Added in 7.60.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is enabled, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_PROXY(3),



libcurl 7.60.0                                                                                   5 Feb 2018                                                                      CURLOPT_HAPROXYPROTOCOL(3)
CURLOPT_HEADER(3)                                                                         curl_easy_setopt options                                                                        CURLOPT_HEADER(3)



NAME
~~~
       CURLOPT_HEADER - pass headers to the data stream

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HEADER, long onoff);

~~~
DESCRIPTION
~~~
       Pass the long value onoff set to 1 to ask libcurl to include the headers in the write callback (CURLOPT_WRITEFUNCTION(3)). This option is relevant for protocols that actually have headers or other
       meta-data (like HTTP and FTP).

       When asking to get the headers passed to the same callback as the body, it is not possible to accurately separate them again without detailed knowledge about the protocol in use.

       Further: the CURLOPT_WRITEFUNCTION(3) callback is limited to only ever get a maximum of CURL_MAX_WRITE_SIZE bytes passed to it (16KB), while a header can  be  longer  and  the  CURLOPT_HEADERFUNCTION(3) supports getting called with headers up to CURL_MAX_HTTP_HEADER bytes big (100KB).

       It is often better to use CURLOPT_HEADERFUNCTION(3) to get the header data separately.

       While named confusingly similar, CURLOPT_HTTPHEADER(3) is used to set custom HTTP headers!

DEFAULT
       0

~~~
PROTOCOLS
~~~
       Most

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         curl_easy_setopt(curl, CURLOPT_HEADER, 1L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Provided in all libcurl versions.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK.

~~~
SEE ALSO
       CURLOPT_HEADERFUNCTION(3), CURLOPT_HTTPHEADER(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                               CURLOPT_HEADER(3)
CURLOPT_HEADERDATA(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_HEADERDATA(3)



NAME
~~~
       CURLOPT_HEADERDATA - pointer to pass to header callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HEADERDATA, void *pointer);

~~~
DESCRIPTION
~~~
       Pass a pointer to be used to write the header part of the received data to.

       If CURLOPT_WRITEFUNCTION(3) or CURLOPT_HEADERFUNCTION(3) is used, pointer will be passed in to the respective callback.

       If neither of those options are set, pointer must be a valid FILE * and it will be used by a plain fwrite() to write headers to.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       struct my_info {
         int shoesize;
         char *secret;
       };

       static size_t header_callback(char *buffer, size_t size,
                                     size_t nitems, void *userdata)
       {
         struct my_info *i = (struct my_info *)userdata;

         /* now this callback can access the my_info struct */

         return nitems * size;
       }

       CURL *curl = curl_easy_init();
       if(curl) {
         struct my_info my = { 10, "the cookies are in the cupboard" };
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, header_callback);

         /* pass in custom data to the callback */
         curl_easy_setopt(curl, CURLOPT_HEADERDATA, &my);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_HEADERFUNCTION(3), CURLOPT_WRITEFUNCTION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                           CURLOPT_HEADERDATA(3)
CURLOPT_HEADERFUNCTION(3)                                                                 curl_easy_setopt options                                                                CURLOPT_HEADERFUNCTION(3)



NAME
~~~
       CURLOPT_HEADERFUNCTION - callback that receives header data

~~~
NAME
~~~
       #include <curl/curl.h>

       size_t header_callback(char *buffer,
                              size_t size,
                              size_t nitems,
                              void *userdata);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HEADERFUNCTION, header_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       This  function  gets called by libcurl as soon as it has received header data. The header callback will be called once for each header and only complete header lines are passed on to the callback.
       Parsing headers is very easy using this. buffer points to the delivered data, and the size of that data is nitems; size is always 1. Do not assume that the header line is null-terminated!

       The pointer named userdata is the one you set with the CURLOPT_HEADERDATA(3) option.

       This callback function must return the number of bytes actually taken care of.  If that amount differs from the amount passed in to your function, it'll signal an error to the library.  This  will
       cause the transfer to get aborted and the libcurl function in progress will return CURLE_WRITE_ERROR.

       A complete HTTP header that is passed to this function can be up to CURL_MAX_HTTP_HEADER (100K) bytes and includes the final line terminator.

       If  this  option is not set, or if it is set to NULL, but CURLOPT_HEADERDATA(3) is set to anything but NULL, the function used to accept response data will be used instead. That is, it will be the
       function specified with CURLOPT_WRITEFUNCTION(3), or if it is not specified or NULL - the default, stream-writing function.

       It's important to note that the callback will be invoked for the headers of all responses received after initiating a request and not just the final response. This  includes  all  responses  which
       occur  during  authentication  negotiation. If you need to operate on only the headers from the final response, you will need to collect headers in the callback yourself and use HTTP status lines,
       for example, to delimit response boundaries.

       For an HTTP transfer, the status line and the blank line preceding the response body are both included as headers and passed to this function.

       When a server sends a chunked encoded transfer, it may contain a trailer. That trailer is identical to an HTTP header and if such a trailer is received it is passed to the application  using  this
       callback as well. There are several ways to detect it being a trailer and not an ordinary header: 1) it comes after the response-body. 2) it comes after the final header line (CR LF) 3) a Trailer:
       header among the regular response-headers mention what header(s) to expect in the trailer.

       For non-HTTP protocols like FTP, POP3, IMAP and SMTP this function will get called with the server responses to the commands that libcurl sends.

LIMITATIONS
       libcurl does not unfold HTTP "folded headers" (deprecated since RFC 7230). A folded header is a header that continues on a subsequent line and starts with a whitespace. Such folds will  be  passed
       to the header callback as a separate one, although strictly it is just a continuation of the previous line.

DEFAULT
       Nothing.

~~~
PROTOCOLS
~~~
       Used for all protocols with headers or meta-data concept: HTTP, FTP, POP3, IMAP, SMTP and more.

~~~
EXAMPLE
~~~
       static size_t header_callback(char *buffer, size_t size,
                                     size_t nitems, void *userdata)
       {
         /* received header is nitems * size long in 'buffer' NOT ZERO TERMINATED */
         /* 'userdata' is set with CURLOPT_HEADERDATA */
         return nitems * size;
       }

       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, header_callback);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_HEADERDATA(3), CURLOPT_WRITEFUNCTION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                       CURLOPT_HEADERFUNCTION(3)
CURLOPT_HEADEROPT(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_HEADEROPT(3)



NAME
~~~
       CURLOPT_HEADEROPT - send HTTP headers to both proxy and host or separately

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HEADEROPT, long bitmask);

~~~
DESCRIPTION
~~~
       Pass a long that is a bitmask of options of how to deal with headers. The two mutually exclusive options are:

       CURLHEADER_UNIFIED - the headers specified in CURLOPT_HTTPHEADER(3) will be used in requests both to servers and proxies. With this option enabled, CURLOPT_PROXYHEADER(3) will not have any effect.

       CURLHEADER_SEPARATE  -  makes CURLOPT_HTTPHEADER(3) headers only get sent to a server and not to a proxy. Proxy headers must be set with CURLOPT_PROXYHEADER(3) to get used. Note that if a non-CONNECT request is sent to a proxy, libcurl will send both server headers and proxy headers. When doing CONNECT, libcurl will send CURLOPT_PROXYHEADER(3) headers only  to  the  proxy  and  then  CURLOPT_HTTPHEADER(3) headers only to the server.

DEFAULT
       CURLHEADER_SEPARATE (changed in 7.42.1, used CURLHEADER_UNIFIED before then)

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         struct curl_slist *list;
         list = curl_slist_append(NULL, "Shoesize: 10");
         list = curl_slist_append(list, "Accept:");
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "http://localhost:8080");
         curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);

         /* HTTPS over a proxy makes a separate CONNECT to the proxy, so tell
            libcurl to not send the custom headers to the proxy. Keep them
            separate! */
         curl_easy_setopt(curl, CURLOPT_HEADEROPT, CURLHEADER_SEPARATE);
         ret = curl_easy_perform(curl);
         curl_slist_free_all(list);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.37.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_HTTPHEADER(3), CURLOPT_PROXYHEADER(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                            CURLOPT_HEADEROPT(3)
CURLOPT_HSTS(3)                                                                           curl_easy_setopt options                                                                          CURLOPT_HSTS(3)



NAME
~~~
       CURLOPT_HSTS - HSTS cache file name

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HSTS, char *filename);

~~~
DESCRIPTION
~~~
       Make the filename point to a file name to load an existing HSTS cache from, and to store the cache in when the easy handle is closed. Setting a file name with this option will also enable HSTS for
       this handle (the equivalent of setting CURLHSTS_ENABLE with CURLOPT_HSTS_CTRL(3)).

       If the given file does not exist or contains no HSTS entries at startup, the HSTS cache will simply start empty. Setting the file name to NULL or "" will only enable HSTS without reading  from  or
       writing to any file.

       If this option is set multiple times, libcurl will load cache entries from each given file but will only store the last used name for later writing.

FILE FORMAT
       The HSTS cache is saved to and loaded from a text file with one entry per physical line. Each line in the file has the following format:

       [host] [stamp]

       [host] is the domain name for the entry and the name is dot-prefixed if it is a includeSubDomain entry (if the entry is valid for all subdmains to the name as well or only for the exact name).

       [stamp] is the time (in UTC) when the entry expires and it uses the format "YYYYMMDD HH:MM:SS".

       Lines starting with "#" are treated as comments and are ignored. There is currently no length or size limit.

DEFAULT
       NULL, no file name

~~~
PROTOCOLS
~~~
       HTTPS and HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_HSTS, "/home/user/.hsts-cache");
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.74.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_HSTS_CTRL(3), CURLOPT_ALTSVC(3), CURLOPT_RESOLVE(3),



libcurl 7.74.0                                                                                   5 Feb 2019                                                                                 CURLOPT_HSTS(3)
CURLOPT_HSTS_CTRL(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_HSTS_CTRL(3)



NAME
~~~
       CURLOPT_HSTS_CTRL - control HSTS behavior

~~~
NAME
~~~
       #include <curl/curl.h>

       #define CURLHSTS_ENABLE       (1<<0)
       #define CURLHSTS_READONLYFILE (1<<1)

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HSTS_CTRL, long bitmask);

~~~
DESCRIPTION
~~~
       HSTS  (HTTP Strict Transport Security) means that an HTTPS server can instruct the client to not contact it again over clear-text HTTP for a certain period into the future. libcurl will then automatically redirect HTTP attempts to such hosts to instead use HTTPS. This is done by libcurl retaining this knowledge in an in-memory cache.

       Populate the long bitmask with the correct set of features to instruct libcurl how to handle HSTS for the transfers using this handle.

BITS
       CURLHSTS_ENABLE
              Enable the in-memory HSTS cache for this handle.

       CURLHSTS_READONLYFILE
              Make the HSTS file (if specified) read-only - makes libcurl not save the cache to the file when closing the handle.

DEFAULT
       0. HSTS is disabled by default.

~~~
PROTOCOLS
~~~
       HTTPS and HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_HSTS_CTRL, CURLHSTS_ENABLE);
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.74.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_HSTS(3), CURLOPT_CONNECT_TO(3), CURLOPT_RESOLVE(3), CURLOPT_ALTSVC(3),



libcurl 7.74.0                                                                                   4 Sep 2020                                                                            CURLOPT_HSTS_CTRL(3)
CURLOPT_HSTSREADDATA(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_HSTSREADDATA(3)



NAME
~~~
       CURLOPT_HSTSREADDATA - pointer passed to the HSTS read callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HSTSREADDATA, void *pointer);

~~~
DESCRIPTION
~~~
       Data pointer to pass to the HSTS read function. If you use the CURLOPT_HSTSREADFUNCTION(3) option, this is the pointer you'll get as input in the 3rd argument to the callback.

       This option doesn't enable HSTS, you need to use CURLOPT_HSTS_CTRL(3) to do that.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       This feature is only used for HTTP(S) transfer.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       struct MyData this;
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "http://example.com");

         /* pass pointer that gets passed in to the
            CURLOPT_HSTSREADFUNCTION callback */
         curl_easy_setopt(curl, CURLOPT_HSTSREADDATA, &this);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.74.0

~~~
RETURN VALUE
~~~
       This will return CURLE_OK.

~~~
SEE ALSO
       CURLOPT_HSTSREADFUNCTION(3), CURLOPT_HSTSWRITEDATA(3), CURLOPT_HSTSWRITEFUNCTION(3),



libcurl 7.74.0                                                                                  14 Sep 2020                                                                         CURLOPT_HSTSREADDATA(3)
CURLOPT_HSTSREADFUNCTION(3)                                                               curl_easy_setopt options                                                              CURLOPT_HSTSREADFUNCTION(3)



NAME
~~~
       CURLOPT_HSTSREADFUNCTION - read callback for HSTS hosts

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLSTScode hstsread(CURL *easy, struct curl_hstsentry *sts, void *userp);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HSTSREADFUNCTION, hstsread);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, as the prototype shows above.

       This callback function gets called by libcurl repeatedly when it populates the in-memory HSTS cache.

       Set the userp argument with the CURLOPT_HSTSREADDATA(3) option or it will be NULL.

       When this callback is invoked, the sts pointer points to a populated struct: Copy the host name to 'name' (no longer than 'namelen' bytes). Make it null-terminated. Set 'includeSubDomains' to TRUE
       or FALSE. Set 'expire' to a date stamp or a zero length string for *forever* (wrong date stamp format might cause the name to not get accepted)

       The callback should return CURLSTS_OK if it returns a name and is prepared to be called again (for another host) or CURLSTS_DONE if it has no entry to return. It can also  return  CURLSTS_FAIL  to
       signal error. Returning CURLSTS_FAIL will stop the transfer from being performed and make CURLE_ABORTED_BY_CALLBACK get returned.

       This option doesn't enable HSTS, you need to use CURLOPT_HSTS_CTRL(3) to do that.

DEFAULT
       NULL - no callback.

~~~
PROTOCOLS
~~~
       This feature is only used for HTTP(S) transfer.

~~~
EXAMPLE
~~~
       {
         /* set HSTS read callback */
         curl_easy_setopt(curl, CURLOPT_HSTSREADFUNCTION, hstsread);

         /* pass in suitable argument to the callback */
         curl_easy_setopt(curl, CURLOPT_HSTSREADDATA, &hstspreload[0]);

         result = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.74.0

~~~
RETURN VALUE
~~~
       This will return CURLE_OK.

~~~
SEE ALSO
       CURLOPT_HSTSREADDATA(3), CURLOPT_HSTSWRITEFUNCTION(3), CURLOPT_HSTS(3), CURLOPT_HSTS_CTRL(3),




libcurl 7.74.0                                                                                  14 Sep 2020                                                                     CURLOPT_HSTSREADFUNCTION(3)
CURLOPT_HSTSWRITEDATA(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_HSTSWRITEDATA(3)



NAME
~~~
       CURLOPT_HSTSWRITEDATA - pointer passed to the HSTS write callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HSTSWRITEDATA, void *pointer);

~~~
DESCRIPTION
~~~
       Data pointer to pass to the HSTS write function. If you use the CURLOPT_HSTSWRITEFUNCTION(3) option, this is the pointer you'll get as input in the 4th argument to the callback.

       This option doesn't enable HSTS, you need to use CURLOPT_HSTS_CTRL(3) to do that.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       This feature is only used for HTTP(S) transfer.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       struct MyData this;
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "http://example.com");

         /* pass pointer that gets passed in to the
            CURLOPT_HSTSWRITEFUNCTION callback */
         curl_easy_setopt(curl, CURLOPT_HSTSWRITEDATA, &this);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.74.0

~~~
RETURN VALUE
~~~
       This will return CURLE_OK.

~~~
SEE ALSO
       CURLOPT_HSTSWRITEFUNCTION(3), CURLOPT_HSTSREADDATA(3), CURLOPT_HSTSREADFUNCTION(3),



libcurl 7.74.0                                                                                  14 Sep 2020                                                                        CURLOPT_HSTSWRITEDATA(3)
CURLOPT_HSTSWRITEFUNCTION(3)                                                              curl_easy_setopt options                                                             CURLOPT_HSTSWRITEFUNCTION(3)



NAME
~~~
       CURLOPT_HSTSWRITEFUNCTION - write callback for HSTS hosts

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLSTScode hstswrite(CURL *easy, struct curl_hstsentry *sts,
                             struct curl_index *count, void *userp);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HSTSWRITEFUNCTION, hstswrite);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, as the prototype shows above.

       This callback function gets called by libcurl repeatedly to allow the application to store the in-memory HSTS cache when libcurl is about to discard it.

       Set the userp argument with the CURLOPT_HSTSWRITEDATA(3) option or it will be NULL.

       When  the callback is invoked, the sts pointer points to a populated struct: Read the host name to 'name' (it is 'namelen' bytes long and null terminated. The 'includeSubDomains' field is non-zero
       if the entry matches subdomains. The 'expire' string is a date stamp null-terminated string using the syntax YYYYMMDD HH:MM:SS.

       The callback should return CURLSTS_OK if it succeeded and is prepared to be called again (for another host) or CURLSTS_DONE if there's nothing more to do. It can also return CURLSTS_FAIL to signal
       error.

DEFAULT
       NULL - no callback.

~~~
PROTOCOLS
~~~
       This feature is only used for HTTP(S) transfer.

~~~
EXAMPLE
~~~
       {
         /* set HSTS read callback */
         curl_easy_setopt(curl, CURLOPT_HSTSWRITEFUNCTION, hstswrite);

         /* pass in suitable argument to the callback */
         curl_easy_setopt(curl, CURLOPT_HSTSWRITEDATA, &hstspreload[0]);

         result = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.74.0

~~~
RETURN VALUE
~~~
       This will return CURLE_OK.

~~~
SEE ALSO
       CURLOPT_HSTSWRITEDATA(3), CURLOPT_HSTSWRITEFUNCTION(3), CURLOPT_HSTS(3), CURLOPT_HSTS_CTRL(3),



libcurl 7.74.0                                                                                  14 Sep 2020                                                                    CURLOPT_HSTSWRITEFUNCTION(3)
CURLOPT_HTTP09_ALLOWED(3)                                                                 curl_easy_setopt options                                                                CURLOPT_HTTP09_ALLOWED(3)



NAME
~~~
       CURLOPT_HTTP09 - allow HTTP/0.9 response

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HTTP09_ALLOWED, long allowed);

~~~
DESCRIPTION
~~~
       Pass the long argument allowed set to 1L to allow HTTP/0.9 responses.

       A HTTP/0.9 response is a server response entirely without headers and only a body. You can connect to lots of random TCP services and still get a response that curl might consider to be HTTP/0.9!

DEFAULT
       curl allowed HTTP/0.9 responses by default before 7.66.0

       Since 7.66.0, libcurl requires this option set to 1L to allow HTTP/0.9 responses.

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_HTTP09_ALLOWED, 1L);
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Option added in 7.64.0, present along with HTTP.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SSLVERSION(3), CURLOPT_HTTP_VERSION(3),



libcurl 7.64.0                                                                                  17 Dec 2018                                                                       CURLOPT_HTTP09_ALLOWED(3)
CURLOPT_HTTP200ALIASES(3)                                                                 curl_easy_setopt options                                                                CURLOPT_HTTP200ALIASES(3)



NAME
~~~
       CURLOPT_HTTP200ALIASES - alternative matches for HTTP 200 OK

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HTTP200ALIASES,
                                 struct curl_slist *aliases);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to a linked list of aliases to be treated as valid HTTP 200 responses.  Some servers respond with a custom header response line.  For example, SHOUTcast servers respond with "ICY
       200 OK". Also some very old Icecast 1.3.x servers will respond like that for certain user agent headers or in absence of such. By including this string in your list of aliases, the  response  will
       be treated as a valid HTTP header line such as "HTTP/1.0 200 OK".

       The  linked list should be a fully valid list of struct curl_slist structs, and be properly filled in.  Use curl_slist_append(3) to create the list and curl_slist_free_all(3) to clean up an entire
       list.

       The alias itself is not parsed for any version strings. The protocol is assumed to match HTTP 1.0 when an alias match.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         struct curl_slist *list;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         list = curl_slist_append(NULL, "ICY 200 OK");
         list = curl_slist_append(list, "WEIRDO 99 FINE");

         curl_easy_setopt(curl, CURLOPT_HTTP200ALIASES, list);
         curl_easy_perform(curl);
         curl_slist_free_all(list); /* free the list again */
       }

~~~
AVAILABILITY
~~~
       Added in 7.10.3

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_HTTP_VERSION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                       CURLOPT_HTTP200ALIASES(3)
CURLOPT_HTTPAUTH(3)                                                                       curl_easy_setopt options                                                                      CURLOPT_HTTPAUTH(3)



NAME
~~~
       CURLOPT_HTTPAUTH - HTTP server authentication methods to try

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HTTPAUTH, long bitmask);

~~~
DESCRIPTION
~~~
       Pass a long as parameter, which is set to a bitmask, to tell libcurl which authentication method(s) you want it to use speaking to the remote server.

       The  available bits are listed below. If more than one bit is set, libcurl will first query the site to see which authentication methods it supports and then pick the best one you allow it to use.
       For some methods, this will induce an extra network round-trip. Set the actual name and password with the CURLOPT_USERPWD(3) option or with  the  CURLOPT_USERNAME(3)  and  the  CURLOPT_PASSWORD(3)
       options.

       For authentication with a proxy, see CURLOPT_PROXYAUTH(3).


       CURLAUTH_BASIC
              HTTP Basic authentication. This is the default choice, and the only method that is in wide-spread use and supported virtually everywhere. This sends the user name and password over the net       work in plain text, easily captured by others.

       CURLAUTH_DIGEST
              HTTP Digest authentication.  Digest authentication is defined in RFC2617 and is a more secure way to do authentication over public networks than the regular old-fashioned Basic method.

       CURLAUTH_DIGEST_IE
              HTTP Digest authentication with an IE flavor.  Digest authentication is defined in RFC2617 and is a more secure way to do authentication over public networks than the regular  old-fashioned
              Basic method. The IE flavor is simply that libcurl will use a special "quirk" that IE is known to have used before version 7 and that some servers require the client to use.

       CURLAUTH_BEARER
              HTTP Bearer token authentication, used primarily in OAuth 2.0 protocol.

              You can set the Bearer token to use with CURLOPT_XOAUTH2_BEARER(3).

       CURLAUTH_NEGOTIATE
              HTTP Negotiate (SPNEGO) authentication. Negotiate authentication is defined in RFC 4559 and is the most secure way to perform authentication over HTTP.

              You need to build libcurl with a suitable GSS-API library or SSPI on Windows for this to work.

       CURLAUTH_NTLM
              HTTP  NTLM  authentication. A proprietary protocol invented and used by Microsoft. It uses a challenge-response and hash concept similar to Digest, to prevent the password from being eaves       dropped.

              You need to build libcurl with either OpenSSL, GnuTLS or NSS support for this option to work, or build libcurl on Windows with SSPI support.

       CURLAUTH_NTLM_WB
              NTLM delegating to winbind helper. Authentication is performed by a separate binary application that is executed when needed. The name of the application is specified at compile time but is
              typically /usr/bin/ntlm_auth

              Note  that  libcurl will fork when necessary to run the winbind application and kill it when complete, calling waitpid() to await its exit when done. On POSIX operating systems, killing the
              process will cause a SIGCHLD signal to be raised (regardless of whether CURLOPT_NOSIGNAL(3) is set), which must be handled intelligently by the application. In particular,  the  application
              must not unconditionally call wait() in its SIGCHLD signal handler to avoid being subject to a race condition.  This behavior is subject to change in future versions of libcurl.

       CURLAUTH_ANY
              This is a convenience macro that sets all bits and thus makes libcurl pick any it finds suitable. libcurl will automatically select the one it finds most secure.

       CURLAUTH_ANYSAFE
              This is a convenience macro that sets all bits except Basic and thus makes libcurl pick any it finds suitable. libcurl will automatically select the one it finds most secure.

       CURLAUTH_ONLY
              This is a meta symbol. OR this value together with a single specific auth value to force libcurl to probe for un-restricted auth and if not, only that single auth algorithm is acceptable.

       CURLAUTH_AWS_SIGV4
              provides AWS V4 signature authentication on HTTPS header see CURLOPT_AWS_SIGV4(3).

DEFAULT
       CURLAUTH_BASIC

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* allow whatever auth the server speaks */
         curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
         curl_easy_setopt(curl, CURLOPT_USERPWD, "james:bond");
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Option Added in 7.10.6.

       CURLAUTH_DIGEST_IE was added in 7.19.3

       CURLAUTH_ONLY was added in 7.21.3

       CURLAUTH_NTLM_WB was added in 7.22.0

       CURLAUTH_BEARER was added in 7.61.0

       CURLAUTH_AWS_SIGV4 was added in 7.74.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_NOT_BUILT_IN if the bitmask specified no supported authentication methods.

~~~
SEE ALSO
       CURLOPT_PROXYAUTH(3), CURLOPT_USERPWD(3),



libcurl 7.38.0                                                                                   2 Aug 2014                                                                             CURLOPT_HTTPAUTH(3)
CURLOPT_HTTP_CONTENT_DECODING(3)                                                          curl_easy_setopt options                                                         CURLOPT_HTTP_CONTENT_DECODING(3)



NAME
~~~
       CURLOPT_HTTP_CONTENT_DECODING - HTTP content decoding control

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HTTP_CONTENT_DECODING,
                                 long enabled);

~~~
DESCRIPTION
~~~
       Pass  a  long to tell libcurl how to act on content decoding. If set to zero, content decoding will be disabled. If set to 1 it is enabled. Libcurl has no default content decoding but requires you
       to use CURLOPT_ACCEPT_ENCODING(3) for that.

DEFAULT
       1

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_HTTP_CONTENT_DECODING, 0L);
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.16.2

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_STDERR(3), CURLOPT_DEBUGFUNCTION(3), CURLOPT_ACCEPT_ENCODING(3)



libcurl 7.37.0                                                                                  19 Jun 2014                                                                CURLOPT_HTTP_CONTENT_DECODING(3)
CURLOPT_HTTPGET(3)                                                                        curl_easy_setopt options                                                                       CURLOPT_HTTPGET(3)



NAME
~~~
       CURLOPT_HTTPGET - ask for an HTTP GET request

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HTTPGET, long useget);

~~~
DESCRIPTION
~~~
       Pass a long. If useget is 1, this forces the HTTP request to get back to using GET. Usable if a POST, HEAD, PUT, etc has been used previously using the same curl handle.

       When setting CURLOPT_HTTPGET(3) to 1, it will automatically set CURLOPT_NOBODY(3) to 0 and CURLOPT_UPLOAD(3) to 0.

       Setting  this  option  to  zero has no effect. Applications need to explicitly select which HTTP request method to use, they cannot deselect a method. To reset a handle to default method, consider
       curl_easy_reset(3).

DEFAULT
       0

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* use a GET to fetch this */
         curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Along with HTTP

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_NOBODY(3), CURLOPT_UPLOAD(3), CURLOPT_POST(3), curl_easy_reset(3)



libcurl 7.37.0                                                                                  17 Jun 2014                                                                              CURLOPT_HTTPGET(3)
CURLOPT_HTTPHEADER(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_HTTPHEADER(3)



NAME
~~~
       CURLOPT_HTTPHEADER - custom HTTP headers

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HTTPHEADER, struct curl_slist *headers);

~~~
DESCRIPTION
~~~
       Pass a pointer to a linked list of HTTP headers to pass to the server and/or proxy in your HTTP request. The same list can be used for both host and proxy requests!

       The  linked list should be a fully valid list of struct curl_slist structs properly filled in. Use curl_slist_append(3) to create the list and curl_slist_free_all(3) to clean up an entire list. If
       you add a header that is otherwise generated and used by libcurl internally, your added one will be used instead. If you add a header with no content as in 'Accept:' (no data on the right side  of
       the  colon),  the internally used header will get disabled. With this option you can add new headers, replace internal headers and remove internal headers. To add a header with no content (nothing
       to the right side of the colon), use the form 'MyHeader;' (note the ending semicolon).

       The headers included in the linked list must not be CRLF-terminated, because libcurl adds CRLF after each header item. Failure to comply with this will result in strange bugs  because  the  server
       will most likely ignore part of the headers you specified.

       The  first  line in a request (containing the method, usually a GET or POST) is not a header and cannot be replaced using this option. Only the lines following the request-line are headers. Adding
       this method line in this list of headers will only cause your request to send an invalid header. Use CURLOPT_CUSTOMREQUEST(3) to change the method.

       When this option is passed to curl_easy_setopt(3), libcurl will not copy the entire list so you must keep it  around  until  you  no  longer  use  this  handle  for  a  transfer  before  you  call
       curl_slist_free_all(3) on the list.

       Pass a NULL to this option to reset back to no custom headers.

       The most commonly replaced headers have "shortcuts" in the options CURLOPT_COOKIE(3), CURLOPT_USERAGENT(3) and CURLOPT_REFERER(3). We recommend using those.

       There's an alternative option that sets or replaces headers only for requests that are sent with CONNECT to a proxy: CURLOPT_PROXYHEADER(3). Use CURLOPT_HEADEROPT(3) to control the behavior.

SECURITY CONCERNS
       By  default,  this  option makes libcurl send the given headers in all HTTP requests done by this handle. You should therefore use this option with caution if you for example connect to the remote
       site using a proxy and a CONNECT request, you should to consider if that proxy is supposed to also get the headers. They may be private or otherwise sensitive to leak.

       Use CURLOPT_HEADEROPT(3) to make the headers only get sent to where you intend them to get sent.

       Custom headers are sent in all requests done by the easy handles, which implies that if you tell libcurl to follow redirects (CURLOPT_FOLLOWLOCATION(3)), the same set of  custom  headers  will  be
       sent in the subsequent request. Redirects can of course go to other hosts and thus those servers will get all the contents of your custom headers too.

       Starting  in  7.58.0,  libcurl  will  specifically  prevent  "Authorization:"  headers  from being sent to other hosts than the first used one, unless specifically permitted with the CURLOPT_UNRESTRICTED_AUTH(3) option.

       Starting in 7.64.0, libcurl will specifically prevent "Cookie:" headers from being sent to other hosts than the first used one, unless specifically permitted with the  CURLOPT_UNRESTRICTED_AUTH(3)
       option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();

       struct curl_slist *list = NULL;

       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         list = curl_slist_append(list, "Shoesize: 10");
         list = curl_slist_append(list, "Accept:");

         curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);

         curl_easy_perform(curl);

         curl_slist_free_all(list); /* free the list again */
       }


~~~
AVAILABILITY
~~~
       As long as HTTP is enabled

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_CUSTOMREQUEST(3), CURLOPT_HEADEROPT(3), CURLOPT_PROXYHEADER(3), CURLOPT_HEADER(3)



libcurl 7.37.0                                                                                  17 Jun 2014                                                                           CURLOPT_HTTPHEADER(3)
CURLOPT_HTTPPOST(3)                                                                       curl_easy_setopt options                                                                      CURLOPT_HTTPPOST(3)



NAME
~~~
       CURLOPT_HTTPPOST - multipart formpost content

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HTTPPOST,
                                 struct curl_httppost *formpost);

~~~
DESCRIPTION
~~~
       Tells  libcurl  you  want  a multipart/formdata HTTP POST to be made and you instruct what data to pass on to the server in the formpost argument.  Pass a pointer to a linked list of curl_httppost
       structs as parameter.  The easiest way to create such a list, is to use curl_formadd(3) as documented. The data in this list must remain intact as long as the curl transfer is alive and  is  using
       it.

       Using POST with HTTP 1.1 implies the use of a "Expect: 100-continue" header.  You can disable this header with CURLOPT_HTTPHEADER(3).

       When setting CURLOPT_HTTPPOST(3), it will automatically set CURLOPT_NOBODY(3) to 0.

       This option is deprecated! Do not use it. Use CURLOPT_MIMEPOST(3) instead after having prepared mime data.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       /* Fill in the file upload field. This makes libcurl load data from
          the given file name when curl_easy_perform() is called. */
       curl_formadd(&formpost,
                    &lastptr,
                    CURLFORM_COPYNAME, "sendfile",
                    CURLFORM_FILE, "postit2.c",
                    CURLFORM_END);

       /* Fill in the filename field */
       curl_formadd(&formpost,
                    &lastptr,
                    CURLFORM_COPYNAME, "filename",
                    CURLFORM_COPYCONTENTS, "postit2.c",
                    CURLFORM_END);

       /* Fill in the submit field too, even if this is rarely needed */
       curl_formadd(&formpost,
                    &lastptr,
                    CURLFORM_COPYNAME, "submit",
                    CURLFORM_COPYCONTENTS, "send",
                    CURLFORM_END);

~~~
AVAILABILITY
~~~
       As long as HTTP is enabled. Deprecated in 7.56.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is enabled, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_POSTFIELDS(3), CURLOPT_POST(3), CURLOPT_MIMEPOST(3), curl_formadd(3), curl_formfree(3), curl_mime_init(3)



libcurl 7.37.0                                                                                  17 Jun 2014                                                                             CURLOPT_HTTPPOST(3)
CURLOPT_HTTPPROXYTUNNEL(3)                                                                curl_easy_setopt options                                                               CURLOPT_HTTPPROXYTUNNEL(3)



NAME
~~~
       CURLOPT_HTTPPROXYTUNNEL - tunnel through HTTP proxy

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HTTPPROXYTUNNEL, long tunnel);

~~~
DESCRIPTION
~~~
       Set the tunnel parameter to 1L to make libcurl tunnel all operations through the HTTP proxy (set with CURLOPT_PROXY(3)). There is a big difference between using a proxy and to tunnel through it.

       Tunneling  means  that  an HTTP CONNECT request is sent to the proxy, asking it to connect to a remote host on a specific port number and then the traffic is just passed through the proxy. Proxies
       tend to white-list specific port numbers it allows CONNECT requests to and often only port 80 and 443 are allowed.

       To suppress proxy CONNECT response headers from user callbacks use CURLOPT_SUPPRESS_CONNECT_HEADERS(3).

       HTTP proxies can generally only speak HTTP (for obvious reasons), which makes libcurl convert non-HTTP requests to HTTP when using an HTTP proxy without this tunnel option set. For example, asking
       for  an  FTP  URL and specifying an HTTP proxy will make libcurl send an FTP URL in an HTTP GET request to the proxy. By instead tunneling through the proxy, you avoid that conversion (that rarely
       works through the proxy anyway).

DEFAULT
       0

~~~
PROTOCOLS
~~~
       All network protocols

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/file.txt");
         curl_easy_setopt(curl, CURLOPT_PROXY, "http://127.0.0.1:80");
         curl_easy_setopt(curl, CURLOPT_HTTPPROXYTUNNEL, 1L);
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_PROXY(3), CURLOPT_PROXYTYPE(3), CURLOPT_PROXYPORT(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                      CURLOPT_HTTPPROXYTUNNEL(3)
CURLOPT_HTTP_TRANSFER_DECODING(3)                                                         curl_easy_setopt options                                                        CURLOPT_HTTP_TRANSFER_DECODING(3)



NAME
~~~
       CURLOPT_HTTP_TRANSFER_DECODING - HTTP transfer decoding control

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HTTP_TRANSFER_DECODING,
                                long enabled);

~~~
DESCRIPTION
~~~
       Pass  a  long  to  tell  libcurl how to act on transfer decoding. If set to zero, transfer decoding will be disabled, if set to 1 it is enabled (default). libcurl does chunked transfer decoding by
       default unless this option is set to zero.

DEFAULT
       1

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_HTTP_TRANSFER_DECODING, 0L);
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.16.2

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_HTTP_CONTENT_DECODING(3), CURLOPT_ACCEPT_ENCODING(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                               CURLOPT_HTTP_TRANSFER_DECODING(3)
CURLOPT_HTTP_VERSION(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_HTTP_VERSION(3)



NAME
~~~
       CURLOPT_HTTP_VERSION - HTTP protocol version to use

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_HTTP_VERSION, long version);

~~~
DESCRIPTION
~~~
       Pass version a long, set to one of the values described below. They ask libcurl to use the specific HTTP versions.

       Note that the HTTP version is just a request. libcurl will still prioritize to re-use an existing connection so it might then re-use a connection using a HTTP version you haven't asked for.


       CURL_HTTP_VERSION_NONE
              We don't care about what version the library uses. libcurl will use whatever it thinks fit.

       CURL_HTTP_VERSION_1_0
              Enforce HTTP 1.0 requests.

       CURL_HTTP_VERSION_1_1
              Enforce HTTP 1.1 requests.

       CURL_HTTP_VERSION_2_0
              Attempt HTTP 2 requests. libcurl will fall back to HTTP 1.1 if HTTP 2 can't be negotiated with the server. (Added in 7.33.0)

              The alias CURL_HTTP_VERSION_2 was added in 7.43.0 to better reflect the actual protocol name.

       CURL_HTTP_VERSION_2TLS
              Attempt  HTTP  2  over TLS (HTTPS) only. libcurl will fall back to HTTP 1.1 if HTTP 2 can't be negotiated with the HTTPS server. For clear text HTTP servers, libcurl will use 1.1. (Added in
              7.47.0)

       CURL_HTTP_VERSION_2_PRIOR_KNOWLEDGE
              Issue non-TLS HTTP requests using HTTP/2 without HTTP/1.1 Upgrade. It requires prior knowledge that the server supports HTTP/2 straight away. HTTPS requests will still do HTTP/2  the  stan       dard way with negotiated protocol version in the TLS handshake. (Added in 7.49.0)

       CURL_HTTP_VERSION_3
              (Added  in  7.66.0) Setting this value will make libcurl attempt to use HTTP/3 directly to server given in the URL. Note that this cannot gracefully downgrade to earlier HTTP version if the
              server doesn't support HTTP/3.

              For more reliably upgrading to HTTP/3, set the preferred version to something lower and let the server announce its HTTP/3 support via Alt-Svc:. See CURLOPT_ALTSVC(3).

DEFAULT
       Since curl 7.62.0: CURL_HTTP_VERSION_2TLS

       Before that: CURL_HTTP_VERSION_1_1

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2TLS);
         ret = curl_easy_perform(curl);
         if(ret == CURLE_HTTP_RETURNED_ERROR) {
           /* an HTTP response error problem */
         }
       }

~~~
AVAILABILITY
~~~
       Along with HTTP

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SSLVERSION(3), CURLOPT_HTTP200ALIASES(3), CURLOPT_HTTP09_ALLOWED(3), CURLOPT_ALTSVC(3)



libcurl 7.37.0                                                                                  17 Jun 2014                                                                         CURLOPT_HTTP_VERSION(3)
CURLOPT_IGNORE_CONTENT_LENGTH(3)                                                          curl_easy_setopt options                                                         CURLOPT_IGNORE_CONTENT_LENGTH(3)



NAME
~~~
       CURLOPT_IGNORE_CONTENT_LENGTH - ignore content length

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_IGNORE_CONTENT_LENGTH,
                                 long ignore);

~~~
DESCRIPTION
~~~
       If ignore is set to 1L, ignore the Content-Length header in the HTTP response and ignore asking for or relying on it for FTP transfers.

       This  is  useful  for  HTTP with Apache 1.x (and similar servers) which will report incorrect content length for files over 2 gigabytes. If this option is used, curl will not be able to accurately
       report progress, and will simply stop the download when the server ends the connection.

       It is also useful with FTP when for example the file is growing while the transfer is in progress which otherwise will unconditionally cause libcurl to report error.

       Only use this option if strictly necessary.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* we know the server is silly, ignore content-length */
         curl_easy_setopt(curl, CURLOPT_IGNORE_CONTENT_LENGTH, 1L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.14.1. Support for FTP added in 7.46.0. This option is not working for HTTP when libcurl is built to use the hyper backend.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_HTTP_VERSION(3), CURLOPT_MAXFILESIZE_LARGE(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                CURLOPT_IGNORE_CONTENT_LENGTH(3)
CURLOPT_INFILESIZE(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_INFILESIZE(3)



NAME
~~~
       CURLOPT_INFILESIZE - size of the input file to send off

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_INFILESIZE, long filesize);

~~~
DESCRIPTION
~~~
       When  uploading  a  file  to  a  remote  site,  filesize  should  be used to tell libcurl what the expected size of the input file is. This value must be passed as a long. See also CURLOPT_INFILESIZE_LARGE(3) for sending files larger than 2GB.

       For uploading using SCP, this option or CURLOPT_INFILESIZE_LARGE(3) is mandatory.

       To unset this value again, set it to -1.

       When sending emails using SMTP, this command can be used to specify the optional SIZE parameter for the MAIL FROM command.

       This option does not limit how much data libcurl will actually send, as that is controlled entirely by what the read callback returns, but telling one value and sending a different amount may lead
       to errors.

DEFAULT
       Unset

~~~
PROTOCOLS
~~~
       Many

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         long uploadsize = FILE_SIZE;

         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/destination.tar.gz");

         curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);

         curl_easy_setopt(curl, CURLOPT_INFILESIZE, uploadsize);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       SMTP support added in 7.23.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_INFILESIZE_LARGE(3), CURLOPT_UPLOAD(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                           CURLOPT_INFILESIZE(3)
CURLOPT_INFILESIZE_LARGE(3)                                                               curl_easy_setopt options                                                              CURLOPT_INFILESIZE_LARGE(3)



NAME
~~~
       CURLOPT_INFILESIZE_LARGE - size of the input file to send off

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_INFILESIZE_LARGE,
                                 curl_off_t filesize);

~~~
DESCRIPTION
~~~
       When uploading a file to a remote site, filesize should be used to tell libcurl what the expected size of the input file is. This value must be passed as a curl_off_t.

       For uploading using SCP, this option or CURLOPT_INFILESIZE(3) is mandatory.

       To unset this value again, set it to -1.

       When sending emails using SMTP, this command can be used to specify the optional SIZE parameter for the MAIL FROM command.

       This option does not limit how much data libcurl will actually send, as that is controlled entirely by what the read callback returns, but telling one value and sending a different amount may lead
       to errors.

DEFAULT
       Unset

~~~
PROTOCOLS
~~~
       Many

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_off_t uploadsize = FILE_SIZE;

         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/destination.tar.gz");

         curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);

         curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, uploadsize);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       SMTP support added in 7.23.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_INFILESIZE(3), CURLOPT_UPLOAD(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                     CURLOPT_INFILESIZE_LARGE(3)
CURLOPT_INTERFACE(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_INTERFACE(3)



NAME
~~~
       CURLOPT_INTERFACE - source interface for outgoing traffic

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_INTERFACE, char *interface);

~~~
DESCRIPTION
~~~
       Pass a char * as parameter. This sets the interface name to use as outgoing network interface. The name can be an interface name, an IP address, or a host name.

       If  the parameter starts with "if!" then it is treated as only as interface name and no attempt will ever be named to do treat it as an IP address or to do name resolution on it.  If the parameter
       starts with "host!" it is treated as either an IP address or a hostname.  Hostnames are resolved synchronously.  Using the if! format is highly recommended when using the multi interfaces to avoid
       allowing  the  code to block.  If "if!" is specified but the parameter does not match an existing interface, CURLE_INTERFACE_FAILED is returned from the libcurl function used to perform the transfer.

       libcurl does not support using network interface names for this option on Windows.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL, use whatever the TCP stack finds suitable

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

         curl_easy_setopt(curl, CURLOPT_INTERFACE, "eth0");

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       The "if!" and "host!" syntax was added in 7.24.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK on success or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SOCKOPTFUNCTION(3), CURLOPT_TCP_NODELAY(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                            CURLOPT_INTERFACE(3)
CURLOPT_INTERLEAVEDATA(3)                                                                 curl_easy_setopt options                                                                CURLOPT_INTERLEAVEDATA(3)



NAME
~~~
       CURLOPT_INTERLEAVEDATA - custom pointer passed to RTSP interleave callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_INTERLEAVEDATA, void *pointer);

~~~
DESCRIPTION
~~~
       This  is  the  userdata pointer that will be passed to CURLOPT_INTERLEAVEFUNCTION(3) when interleaved RTP data is received. If the interleave function callback is not set, this pointer is not used
       anywhere.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       RTSP

~~~
EXAMPLE
~~~
       static size_t rtp_write(void *ptr, size_t size, size_t nmemb, void *user)
       {
         struct local *l = (struct local *)user;
         /* take care of the packet in 'ptr', then return... */
         return size * nmemb;
       }
       {
         struct local rtp_data;
         curl_easy_setopt(curl, CURLOPT_INTERLEAVEFUNCTION, rtp_write);
         curl_easy_setopt(curl, CURLOPT_INTERLEAVEDATA, &rtp_data);
       }

~~~
AVAILABILITY
~~~
       Added in 7.20.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_INTERLEAVEFUNCTION(3), CURLOPT_RTSP_REQUEST(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                       CURLOPT_INTERLEAVEDATA(3)
CURLOPT_INTERLEAVEFUNCTION(3)                                                             curl_easy_setopt options                                                            CURLOPT_INTERLEAVEFUNCTION(3)



NAME
~~~
       CURLOPT_INTERLEAVEFUNCTION - callback for RTSP interleaved data

~~~
NAME
~~~
       #include <curl/curl.h>

       size_t interleave_callback(void *ptr, size_t size, size_t nmemb,
                                  void *userdata);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_INTERLEAVEFUNCTION,
                                 interleave_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       This  callback  function  gets called by libcurl as soon as it has received interleaved RTP data. This function gets called for each $ block and therefore contains exactly one upper-layer protocol
       unit (e.g.  one RTP packet). Curl writes the interleaved header as well as the included data for each call. The first byte is always an ASCII dollar sign. The dollar sign is followed by a one byte
       channel  identifier  and  then a 2 byte integer length in network byte order. See RFC2326 Section 10.12 for more information on how RTP interleaving behaves. If unset or set to NULL, curl will use
       the default write function.

       Interleaved RTP poses some challenges for the client application. Since the stream data is sharing the RTSP control connection, it is critical to service the RTP in a timely fashion.  If  the  RTP
       data  is  not handled quickly, subsequent response processing may become unreasonably delayed and the connection may close. The application may use CURL_RTSPREQ_RECEIVE to service RTP data when no
       requests are desired. If the application makes a request, (e.g.  CURL_RTSPREQ_PAUSE) then the response handler will process any pending RTP data before marking the request as finished.

       The CURLOPT_INTERLEAVEDATA(3) is passed in the userdata argument in the callback.

DEFAULT
       NULL, the interleave data is then passed to the regular write function: CURLOPT_WRITEFUNCTION(3).

~~~
PROTOCOLS
~~~
       RTSP

~~~
EXAMPLE
~~~
       static size_t rtp_write(void *ptr, size_t size, size_t nmemb, void *user)
       {
         struct local *l = (struct local *)user;
         /* take care of the packet in 'ptr', then return... */
         return size * nmemb;
       }
       {
         struct local rtp_data;
         curl_easy_setopt(curl, CURLOPT_INTERLEAVEFUNCTION, rtp_write);
         curl_easy_setopt(curl, CURLOPT_INTERLEAVEDATA, &rtp_data);
       }

~~~
AVAILABILITY
~~~
       Added in 7.20.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_INTERLEAVEDATA(3), CURLOPT_RTSP_REQUEST(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                   CURLOPT_INTERLEAVEFUNCTION(3)
CURLOPT_IOCTLDATA(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_IOCTLDATA(3)



NAME
~~~
       CURLOPT_IOCTLDATA - custom pointer passed to I/O callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_IOCTLDATA, void *pointer);

~~~
DESCRIPTION
~~~
       Pass the pointer that will be untouched by libcurl and passed as the 3rd argument in the ioctl callback set with CURLOPT_IOCTLFUNCTION(3).

DEFAULT
       By default, the value of this parameter is NULL.

~~~
PROTOCOLS
~~~
       Used with HTTP

~~~
EXAMPLE
~~~
       static curlioerr ioctl_callback(CURL *handle, int cmd, void *clientp)
       {
         struct data *io = (struct data *)clientp;
         if(cmd == CURLIOCMD_RESTARTREAD) {
           lseek(fd, 0, SEEK_SET);
           current_offset = 0;
           return CURLIOE_OK;
         }
         return CURLIOE_UNKNOWNCMD;
       }
       {
         struct data ioctl_data;
         curl_easy_setopt(curl, CURLOPT_IOCTLFUNCTION, ioctl_callback);
         curl_easy_setopt(curl, CURLOPT_IOCTLDATA, &ioctl_data);
       }

~~~
AVAILABILITY
~~~
       Added in 7.12.3

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_IOCTLFUNCTION(3), CURLOPT_SEEKFUNCTION(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                            CURLOPT_IOCTLDATA(3)
CURLOPT_IOCTLFUNCTION(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_IOCTLFUNCTION(3)



NAME
~~~
       CURLOPT_IOCTLFUNCTION - callback for I/O operations

~~~
NAME
~~~
       #include <curl/curl.h>

       typedef enum {
         CURLIOE_OK,            /* I/O operation successful */
         CURLIOE_UNKNOWNCMD,    /* command was unknown to callback */
         CURLIOE_FAILRESTART,   /* failed to restart the read */
         CURLIOE_LAST           /* never use */
       } curlioerr;

       typedef enum  {
         CURLIOCMD_NOP,         /* no operation */
         CURLIOCMD_RESTARTREAD, /* restart the read stream from start */
         CURLIOCMD_LAST         /* never use */
       } curliocmd;

       curlioerr ioctl_callback(CURL *handle, int cmd, void *clientp);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_IOCTLFUNCTION, ioctl_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       This callback function gets called by libcurl when something special I/O-related needs to be done that the library can't do by itself. For now, rewinding the read data stream is the only action it
       can request. The rewinding of the read data stream may be necessary when doing an HTTP PUT or POST with a multi-pass authentication method.

       The callback MUST return CURLIOE_UNKNOWNCMD if the input cmd is not CURLIOCMD_RESTARTREAD.

       The clientp argument to the callback is set with the CURLOPT_IOCTLDATA(3) option.

       This option is deprecated! Do not use it. Use CURLOPT_SEEKFUNCTION(3) instead to provide seeking! If CURLOPT_SEEKFUNCTION(3) is set, this parameter will be ignored when seeking.

DEFAULT
       By default, this parameter is set to NULL. Not used.

~~~
PROTOCOLS
~~~
       Used with HTTP

~~~
EXAMPLE
~~~
       static curlioerr ioctl_callback(CURL *handle, int cmd, void *clientp)
       {
         struct data *io = (struct data *)clientp;
         if(cmd == CURLIOCMD_RESTARTREAD) {
           lseek(fd, 0, SEEK_SET);
           current_offset = 0;
           return CURLIOE_OK;
         }
         return CURLIOE_UNKNOWNCMD;
       }
       {
         struct data ioctl_data;
         curl_easy_setopt(curl, CURLOPT_IOCTLFUNCTION, ioctl_callback);
         curl_easy_setopt(curl, CURLOPT_IOCTLDATA, &ioctl_data);
       }

~~~
AVAILABILITY
~~~
       Added in 7.12.3

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_IOCTLDATA(3), CURLOPT_SEEKFUNCTION(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                        CURLOPT_IOCTLFUNCTION(3)
CURLOPT_IPRESOLVE(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_IPRESOLVE(3)



NAME
~~~
       CURLOPT_IPRESOLVE - IP protocol version to use

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_IPRESOLVE, long resolve);

~~~
DESCRIPTION
~~~
       Allows  an  application  to  select what kind of IP addresses to use when establishing a connection or choosing one from the connection pool. This is interesting when using host names that resolve
       addresses using more than one version of IP. The allowed values are:

       CURL_IPRESOLVE_WHATEVER
              Default, can use addresses of all IP versions that your system allows.

       CURL_IPRESOLVE_V4
              Uses only IPv4 addresses.

       CURL_IPRESOLVE_V6
              Uses only IPv6 addresses.

DEFAULT
       CURL_IPRESOLVE_WHATEVER

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

         /* of all addresses example.com resolves to, only IPv6 ones are used */
         curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V6);

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }


~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_HTTP_VERSION(3), CURLOPT_SSLVERSION(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                            CURLOPT_IPRESOLVE(3)
CURLOPT_ISSUERCERT(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_ISSUERCERT(3)



NAME
~~~
       CURLOPT_ISSUERCERT - issuer SSL certificate filename

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_ISSUERCERT, char *file);

~~~
DESCRIPTION
~~~
       Pass  a  char  * to a null-terminated string naming a file holding a CA certificate in PEM format. If the option is set, an additional check against the peer certificate is performed to verify the
       issuer is indeed the one associated with the certificate provided by the option. This additional check is useful in multi-level PKI where one needs to enforce that the peer certificate is  from  a
       specific branch of the tree.

       This option makes sense only when used in combination with the CURLOPT_SSL_VERIFYPEER(3) option. Otherwise, the result of the check is not considered as failure.

       A  specific  error  code (CURLE_SSL_ISSUER_ERROR) is defined with the option, which is returned if the setup of the SSL/TLS session has failed due to a mismatch with the issuer of peer certificate
       (CURLOPT_SSL_VERIFYPEER(3) has to be set too for the check to fail). (Added in 7.19.0)

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS-based protocols

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_ISSUERCERT, "/etc/certs/cacert.pem");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       If built TLS enabled

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_CRLFILE(3), CURLOPT_SSL_VERIFYPEER(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                           CURLOPT_ISSUERCERT(3)
CURLOPT_ISSUERCERT_BLOB(3)                                                                curl_easy_setopt options                                                               CURLOPT_ISSUERCERT_BLOB(3)



NAME
~~~
       CURLOPT_ISSUERCERT_BLOB - issuer SSL certificate from memory blob

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_ISSUERCERT_BLOB, struct curl_blob *stblob);

~~~
DESCRIPTION
~~~
       Pass  a pointer to a curl_blob structure, which contains information (pointer and size) about a memory block with binary data of a CA certificate in PEM format. If the option is set, an additional
       check against the peer certificate is performed to verify the issuer is indeed the one associated with the certificate provided by the option. This additional check is useful  in  multi-level  PKI
       where one needs to enforce that the peer certificate is from a specific branch of the tree.

       This option should be used in combination with the CURLOPT_SSL_VERIFYPEER(3) option. Otherwise, the result of the check is not considered as failure.

       A  specific  error  code (CURLE_SSL_ISSUER_ERROR) is defined with the option, which is returned if the setup of the SSL/TLS session has failed due to a mismatch with the issuer of peer certificate
       (CURLOPT_SSL_VERIFYPEER(3) has to be set too for the check to fail).

       If the blob is initialized with the flags member of struct curl_blob set to CURL_BLOB_COPY, the application does not have to keep the buffer around after setting this.

       This option is an alternative to CURLOPT_ISSUERCERT(3) which instead expects a file name as input.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS-based protocols

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         struct curl_blob blob;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         blob.data = certificateData;
         blob.len = filesize;
         blob.flags = CURL_BLOB_COPY;
         curl_easy_setopt(curl, CURLOPT_ISSUERCERT_BLOB, &blob);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in libcurl 7.71.0. This option is supported by the OpenSSL backends.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_ISSUERCERT(3), CURLOPT_CRLFILE(3), CURLOPT_SSL_VERIFYPEER(3),



libcurl 7.71.0                                                                                  24 Jun 2020                                                                      CURLOPT_ISSUERCERT_BLOB(3)
CURLOPT_KEEP_SENDING_ON_ERROR(3)                                                          curl_easy_setopt options                                                         CURLOPT_KEEP_SENDING_ON_ERROR(3)



NAME
~~~
       CURLOPT_KEEP_SENDING_ON_ERROR - keep sending on early HTTP response >= 300

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_KEEP_SENDING_ON_ERROR,
                                 long keep_sending);

~~~
DESCRIPTION
~~~
       A  long  parameter  set  to 1 tells the library to keep sending the request body if the HTTP code returned is equal to or larger than 300. The default action would be to stop sending and close the
       stream or connection.

       This option is suitable for manual NTLM authentication, i.e. if an application does not use CURLOPT_HTTPAUTH(3), but instead sets "Authorization: NTLM ..."  headers  manually  using  CURLOPT_HTTPHEADER(3).

       Most applications do not need this option.

DEFAULT
       0, stop sending on error

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "sending data");
         curl_easy_setopt(curl, CURLOPT_KEEP_SENDING_ON_ERROR, 1L);
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Along with HTTP. Added in 7.51.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is enabled, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_FAILONERROR(3), CURLOPT_HTTPHEADER(3),



libcurl 7.51.0                                                                                  22 Sep 2016                                                                CURLOPT_KEEP_SENDING_ON_ERROR(3)
CURLOPT_KEYPASSWD(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_KEYPASSWD(3)



NAME
~~~
       CURLOPT_KEYPASSWD - passphrase to private key

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_KEYPASSWD, char *pwd);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to a null-terminated string as parameter. It will be used as the password required to use the CURLOPT_SSLKEY(3) or CURLOPT_SSH_PRIVATE_KEYFILE(3) private key.  You never needed a
       pass phrase to load a certificate but you need one to load your private key.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
         curl_easy_setopt(curl, CURLOPT_SSLCERT, "client.pem");
         curl_easy_setopt(curl, CURLOPT_SSLKEY, "key.pem");
         curl_easy_setopt(curl, CURLOPT_KEYPASSWD, "superman");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       This option was known as CURLOPT_SSLKEYPASSWD up to 7.16.4 and CURLOPT_SSLCERTPASSWD up to 7.9.2.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS enabled, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSLKEY(3), CURLOPT_SSH_PRIVATE_KEYFILE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                            CURLOPT_KEYPASSWD(3)
CURLOPT_KRBLEVEL(3)                                                                       curl_easy_setopt options                                                                      CURLOPT_KRBLEVEL(3)



NAME
~~~
       CURLOPT_KRBLEVEL - FTP kerberos security level

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_KRBLEVEL, char *level);

~~~
DESCRIPTION
~~~
       Pass a char * as parameter. Set the kerberos security level for FTP; this also enables kerberos awareness.  This is a string that should match one of the following: 'clear', 'safe', 'confidential'
       or 'private'.  If the string is set but doesn't match one of these, 'private' will be used. Set the string to NULL to disable kerberos support for FTP.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/foo.bin");
         curl_easy_setopt(curl, CURLOPT_KRBLEVEL, "private");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       This option was known as CURLOPT_KRB4LEVEL up to 7.16.3

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_KRBLEVEL(3), CURLOPT_USE_SSL(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                             CURLOPT_KRBLEVEL(3)
CURLOPT_LOCALPORT(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_LOCALPORT(3)



NAME
~~~
       CURLOPT_LOCALPORT - local port number to use for socket

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_LOCALPORT, long port);

~~~
DESCRIPTION
~~~
       Pass  a  long.  This  sets  the  local port number of the socket used for the connection. This can be used in combination with CURLOPT_INTERFACE(3) and you are recommended to use CURLOPT_LOCALPORTRANGE(3) as well when this option is set. Valid port numbers are 1 - 65535.

DEFAULT
       0, disabled - use whatever the system thinks is fine

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
         curl_easy_setopt(curl, CURLOPT_LOCALPORT, 49152L);
         /* and try 20 more ports following that */
         curl_easy_setopt(curl, CURLOPT_LOCALPORTRANGE, 20L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.15.2

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_LOCALPORTRANGE(3), CURLOPT_INTERFACE(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                            CURLOPT_LOCALPORT(3)
CURLOPT_LOCALPORTRANGE(3)                                                                 curl_easy_setopt options                                                                CURLOPT_LOCALPORTRANGE(3)



NAME
~~~
       CURLOPT_LOCALPORTRANGE - number of additional local ports to try

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_LOCALPORTRANGE,
                                 long range);

~~~
DESCRIPTION
~~~
       Pass  a  long. The range argument is the number of attempts libcurl will make to find a working local port number. It starts with the given CURLOPT_LOCALPORT(3) and adds one to the number for each
       retry. Setting this option to 1 or below will make libcurl do only one try for the exact port number. Port numbers by nature are scarce resources that will be busy at times so setting  this  value
       to something too low might cause unnecessary connection setup failures.

DEFAULT
       1

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
         curl_easy_setopt(curl, CURLOPT_LOCALPORT, 49152L);
         /* and try 20 more ports following that */
         curl_easy_setopt(curl, CURLOPT_LOCALPORTRANGE, 20L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.15.2

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_LOCALPORT(3), CURLOPT_INTERFACE(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                       CURLOPT_LOCALPORTRANGE(3)
CURLOPT_LOGIN_OPTIONS(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_LOGIN_OPTIONS(3)



NAME
~~~
       CURLOPT_LOGIN_OPTIONS - login options

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_LOGIN_OPTIONS, char *options);

~~~
DESCRIPTION
~~~
       Pass a char * as parameter, which should be pointing to the null-terminated options string to use for the transfer.

       For more information about the login options please see RFC2384, RFC5092 and IETF draft draft-earhart-url-smtp-00.txt

       CURLOPT_LOGIN_OPTIONS(3)  can be used to set protocol specific login options, such as the preferred authentication mechanism via "AUTH=NTLM" or "AUTH=*", and should be used in conjunction with the
       CURLOPT_USERNAME(3) option.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       Only IMAP, POP3 and SMTP support login options.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "smtp://example.com/");
         curl_easy_setopt(curl, CURLOPT_LOGIN_OPTIONS, "AUTH=*");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.34.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_USERNAME(3), CURLOPT_PASSWORD(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                        CURLOPT_LOGIN_OPTIONS(3)
CURLOPT_LOW_SPEED_LIMIT(3)                                                                curl_easy_setopt options                                                               CURLOPT_LOW_SPEED_LIMIT(3)



NAME
~~~
       CURLOPT_LOW_SPEED_LIMIT - low speed limit in bytes per second

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_LOW_SPEED_LIMIT, long speedlimit);

~~~
DESCRIPTION
~~~
       Pass a long as parameter. It contains the average transfer speed in bytes per second that the transfer should be below during CURLOPT_LOW_SPEED_TIME(3) seconds for libcurl to consider it to be too
       slow and abort.

DEFAULT
       0, disabled

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, url);
         /* abort if slower than 30 bytes/sec during 60 seconds */
         curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 60L);
         curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 30L);
         res = curl_easy_perform(curl);
         if(CURLE_OPERATION_TIMEDOUT == res) {
           printf("Timeout!\n");
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_LOW_SPEED_TIME(3), CURLOPT_TIMEOUT(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                      CURLOPT_LOW_SPEED_LIMIT(3)
CURLOPT_LOW_SPEED_TIME(3)                                                                 curl_easy_setopt options                                                                CURLOPT_LOW_SPEED_TIME(3)



NAME
~~~
       CURLOPT_LOW_SPEED_TIME - low speed limit time period

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_LOW_SPEED_TIME, long speedtime);

~~~
DESCRIPTION
~~~
       Pass a long as parameter. It contains the time in number seconds that the transfer speed should be below the CURLOPT_LOW_SPEED_LIMIT(3) for the library to consider it too slow and abort.

DEFAULT
       0, disabled

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, url);
         /* abort if slower than 30 bytes/sec during 60 seconds */
         curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, 60L);
         curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 30L);
         res = curl_easy_perform(curl);
         if(CURLE_OPERATION_TIMEDOUT == res) {
           printf("Timeout!\n");
         }
         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_LOW_SPEED_LIMIT(3), CURLOPT_TIMEOUT(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                       CURLOPT_LOW_SPEED_TIME(3)
CURLOPT_MAIL_AUTH(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_MAIL_AUTH(3)



NAME
~~~
       CURLOPT_MAIL_AUTH - SMTP authentication address

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_MAIL_AUTH, char *auth);

~~~
DESCRIPTION
~~~
       Pass a pointer to a null-terminated string as parameter. This will be used to specify the authentication address (identity) of a submitted message that is being relayed to another server.

       This  optional  parameter  allows  co-operating  agents  in a trusted environment to communicate the authentication of individual messages and should only be used by the application program, using
       libcurl, if the application is itself a mail server acting in such an environment. If the application is operating as such and the AUTH address is not known or is invalid,  then  an  empty  string
       should be used for this parameter.

       Unlike CURLOPT_MAIL_FROM(3) and CURLOPT_MAIL_RCPT(3), the address should not be specified within a pair of angled brackets (<>). However, if an empty string is used then a pair of brackets will be
       sent by libcurl as required by RFC2554.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       SMTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "smtp://example.com/");
         curl_easy_setopt(curl, CURLOPT_MAIL_AUTH, "<secret@cave>");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.25.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_MAIL_FROM(3), CURLOPT_MAIL_RCPT(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                            CURLOPT_MAIL_AUTH(3)
CURLOPT_MAIL_FROM(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_MAIL_FROM(3)



NAME
~~~
       CURLOPT_MAIL_FROM - SMTP sender address

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_MAIL_FROM, char *from);

~~~
DESCRIPTION
~~~
       Pass a pointer to a null-terminated string as parameter. This should be used to specify the sender's email address when sending SMTP mail with libcurl.

       An originator email address should be specified with angled brackets (<>) around it, which if not specified will be added automatically.

       If this parameter is not specified then an empty address will be sent to the mail server which may cause the email to be rejected.

       The application does not have to keep the string around after setting this option.

DEFAULT
       blank

~~~
PROTOCOLS
~~~
       SMTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "smtp://example.com/");
         curl_easy_setopt(curl, CURLOPT_MAIL_FROM, "president@example.com");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.20.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_MAIL_RCPT(3), CURLOPT_MAIL_AUTH(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                            CURLOPT_MAIL_FROM(3)
CURLOPT_MAIL_RCPT(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_MAIL_RCPT(3)



NAME
~~~
       CURLOPT_MAIL_RCPT - list of SMTP mail recipients

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_MAIL_RCPT,
                                 struct curl_slist *rcpts);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to  a linked list of recipients to pass to the server in your SMTP mail request. The linked list should be a fully valid list of struct curl_slist structs properly filled in. Use
       curl_slist_append(3) to create the list and curl_slist_free_all(3) to clean up an entire list.

       When performing a mail transfer, each recipient should be specified within a pair of angled brackets (<>), however, should you not use an angled bracket as the first character libcurl will  assume
       you provided a single email address and enclose that address within brackets for you.

       When performing an address verification (VRFY command), each recipient should be specified as the user name or user name and domain (as per Section 3.5 of RFC5321).

       When performing a mailing list expand (EXPN command), each recipient should be specified using the mailing list name, such as "Friends" or "London-Office".

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       SMTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         struct curl_slist *list;
         list = curl_slist_append(NULL, "root@localhost");
         list = curl_slist_append(list, "person@example.com");
         curl_easy_setopt(curl, CURLOPT_URL, "smtp://example.com/");
         curl_easy_setopt(curl, CURLOPT_MAIL_RCPT, list);
         ret = curl_easy_perform(curl);
         curl_slist_free_all(list);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.20.0. The VRFY and EXPN logic was added in 7.34.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_MAIL_FROM(3), CURLOPT_MAIL_AUTH(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                            CURLOPT_MAIL_RCPT(3)
CURLOPT_MAIL_RCPT_ALLLOWFAILS(3)                                                          curl_easy_setopt options                                                         CURLOPT_MAIL_RCPT_ALLLOWFAILS(3)



NAME
~~~
       CURLOPT_MAIL_RCPT_ALLLOWFAILS - allow RCPT TO command to fail for some recipients

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_MAIL_RCPT_ALLLOWFAILS,
                                 long allow);

~~~
DESCRIPTION
~~~
       If allow is set to 1L, allow RCPT TO command to fail for some recipients.

       When sending data to multiple recipients, by default curl will abort SMTP conversation if at least one of the recipients causes RCPT TO command to return an error.

       The default behavior can be changed by setting ignore to 1L which will make curl ignore errors and proceed with the remaining valid recipients.

       If all recipients trigger RCPT TO failures and this flag is specified, curl will still abort the SMTP conversation and return the error received from to the last RCPT TO command.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       SMTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         struct curl_slist *list;

         /* Adding one valid and one invalid email address */
         list = curl_slist_append(NULL, "person@example.com");
         list = curl_slist_append(list, "invalidemailaddress");

         curl_easy_setopt(curl, CURLOPT_URL, "smtp://example.com/");
         curl_easy_setopt(curl, CURLOPT_MAIL_RCPT_ALLLOWFAILS, 1L);

         ret = curl_easy_perform(curl);
         curl_slist_free_all(list);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.69.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_MAIL_FROM(3), CURLOPT_MAIL_RCPT(3),



libcurl 7.69.0                                                                                  16 Jan 2020                                                                CURLOPT_MAIL_RCPT_ALLLOWFAILS(3)
CURLOPT_MAXAGE_CONN(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_MAXAGE_CONN(3)



NAME
~~~
       CURLOPT_MAXAGE_CONN - max idle time allowed for reusing a connection

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_MAXAGE_CONN, long maxage);

~~~
DESCRIPTION
~~~
       Pass a long as parameter containing maxage - the maximum time in seconds that you allow an existing connection to have to be considered for reuse for this request.

       The  "connection  cache" that holds previously used connections. When a new request is to be done, it will consider any connection that matches for reuse. The CURLOPT_MAXAGE_CONN(3) limit prevents
       libcurl from trying very old connections for reuse, since old connections have a high risk of not working and thus trying them is a performance loss and sometimes service loss due to the difficulties to figure out the situation. If a connection is found in the cache that is older than this set maxage, it will instead be closed.

DEFAULT
       Default maxage is 118 seconds.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* only allow 30 seconds idle time */
         curl_easy_setopt(curl, CURLOPT_MAXAGE_CONN, 30L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in libcurl 7.65.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK.

~~~
SEE ALSO
       CURLOPT_TIMEOUT(3), CURLOPT_FORBID_REUSE(3), CURLOPT_FRESH_CONNECT(3),



libcurl 7.65.0                                                                                  18 Apr 2019                                                                          CURLOPT_MAXAGE_CONN(3)
CURLOPT_MAXCONNECTS(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_MAXCONNECTS(3)



NAME
~~~
       CURLOPT_MAXCONNECTS - maximum connection cache size

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_MAXCONNECTS, long amount);

~~~
DESCRIPTION
~~~
       Pass  a  long.  The  set amount will be the maximum number of simultaneously open persistent connections that libcurl may cache in the pool associated with this handle. The default is 5, and there
       isn't much point in changing this value unless you are perfectly aware of how this works and changes libcurl's behavior. This concerns connections using any of the protocols that  support  persistent connections.

       When reaching the maximum limit, curl closes the oldest one in the cache to prevent increasing the number of open connections.

       If you already have performed transfers with this curl handle, setting a smaller CURLOPT_MAXCONNECTS(3) than before may cause open connections to get closed unnecessarily.

       If you add this easy handle to a multi handle, this setting is not acknowledged, and you must instead use curl_multi_setopt(3) and the CURLMOPT_MAXCONNECTS(3) option.

DEFAULT
       5

~~~
PROTOCOLS
~~~
       Most

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* limit the connection cache for this handle to no more than 3 */
         curl_easy_setopt(curl, CURLOPT_MAXCONNECTS, 3L);
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLMOPT_MAXCONNECTS(3), CURLOPT_MAXREDIRS(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                          CURLOPT_MAXCONNECTS(3)
CURLOPT_MAXFILESIZE(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_MAXFILESIZE(3)



NAME
~~~
       CURLOPT_MAXFILESIZE - maximum file size allowed to download

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_MAXFILESIZE, long size);

~~~
DESCRIPTION
~~~
       Pass  a  long  as  parameter.  This  allows you to specify the maximum size (in bytes) of a file to download. If the file requested is found larger than this value, the transfer will not start and
       CURLE_FILESIZE_EXCEEDED will be returned.

       The file size is not always known prior to download, and for such files this option has no effect even if the file transfer ends up being larger than this given limit.

       If you want a limit above 2GB, use CURLOPT_MAXFILESIZE_LARGE(3).

DEFAULT
       None

~~~
PROTOCOLS
~~~
       FTP, HTTP and MQTT

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* refuse to download if larger than 1000 bytes! */
         curl_easy_setopt(curl, CURLOPT_MAXFILESIZE, 1000L);
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_MAXFILESIZE_LARGE(3), CURLOPT_MAX_RECV_SPEED_LARGE(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                          CURLOPT_MAXFILESIZE(3)
CURLOPT_MAXFILESIZE_LARGE(3)                                                              curl_easy_setopt options                                                             CURLOPT_MAXFILESIZE_LARGE(3)



NAME
~~~
       CURLOPT_MAXFILESIZE_LARGE - maximum file size allowed to download

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_MAXFILESIZE_LARGE,
                                 curl_off_t size);

~~~
DESCRIPTION
~~~
       Pass a curl_off_t as parameter. This allows you to specify the maximum size (in bytes) of a file to download. If the file requested is found larger than this value, the transfer will not start and
       CURLE_FILESIZE_EXCEEDED will be returned.

       The file size is not always known prior to download, and for such files this option has no effect even if the file transfer ends up being larger than this given limit.

DEFAULT
       None

~~~
PROTOCOLS
~~~
       FTP, HTTP and MQTT

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_off_t ridiculous = 1 << 48;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* refuse to download if larger than ridiculous */
         curl_easy_setopt(curl, CURLOPT_MAXFILESIZE_LARGE, ridiculous);
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.11.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_MAXFILESIZE(3), CURLOPT_MAX_RECV_SPEED_LARGE(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                    CURLOPT_MAXFILESIZE_LARGE(3)
CURLOPT_MAX_RECV_SPEED_LARGE(3)                                                           curl_easy_setopt options                                                          CURLOPT_MAX_RECV_SPEED_LARGE(3)



NAME
~~~
       CURLOPT_MAX_RECV_SPEED_LARGE - rate limit data download speed

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_MAX_RECV_SPEED_LARGE,
                                 curl_off_t maxspeed);

~~~
DESCRIPTION
~~~
       Pass  a curl_off_t as parameter.  If a download exceeds this maxspeed (counted in bytes per second) the transfer will pause to keep the speed less than or equal to the parameter value. Defaults to
       unlimited speed.

       This is not an exact science. libcurl attempts to keep the average speed below the given threshold over a period time.

       If you set maxspeed to a value lower than CURLOPT_BUFFERSIZE(3), libcurl might download faster than the set limit initially.

       This option doesn't affect transfer speeds done with FILE:// URLs.

DEFAULT
       0, disabled

~~~
PROTOCOLS
~~~
       All but file://

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* cap the download speed to 31415 bytes/sec */
         curl_easy_setopt(curl, CURLOPT_MAX_RECV_SPEED_LARGE, (curl_off_t)31415);
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.15.5

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_MAX_SEND_SPEED_LARGE(3), CURLOPT_LOW_SPEED_LIMIT(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                 CURLOPT_MAX_RECV_SPEED_LARGE(3)
CURLOPT_MAXREDIRS(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_MAXREDIRS(3)



NAME
~~~
       CURLOPT_MAXREDIRS - maximum number of redirects allowed

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_MAXREDIRS, long amount);

~~~
DESCRIPTION
~~~
       Pass  a  long. The set number will be the redirection limit amount. If that many redirections have been followed, the next redirect will cause an error (CURLE_TOO_MANY_REDIRECTS). This option only
       makes sense if the CURLOPT_FOLLOWLOCATION(3) is used at the same time.

       Setting the limit to 0 will make libcurl refuse any redirect.

       Set it to -1 for an infinite number of redirects.

DEFAULT
       -1, unlimited

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");

         /* enable redirect following */
         curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

         /* allow three redirects */
         curl_easy_setopt(curl, CURLOPT_MAXREDIRS, 3L);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Along with HTTP

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_FOLLOWLOCATION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                            CURLOPT_MAXREDIRS(3)
CURLOPT_MAX_SEND_SPEED_LARGE(3)                                                           curl_easy_setopt options                                                          CURLOPT_MAX_SEND_SPEED_LARGE(3)



NAME
~~~
       CURLOPT_MAX_SEND_SPEED_LARGE - rate limit data upload speed

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_MAX_SEND_SPEED_LARGE,
                                 curl_off_t maxspeed);

~~~
DESCRIPTION
~~~
       Pass  a  curl_off_t  as  parameter  with  the maxspeed.  If an upload exceeds this speed (counted in bytes per second) the transfer will pause to keep the speed less than or equal to the parameter
       value.  Defaults to unlimited speed.

       This is not an exact science. libcurl attempts to keep the average speed below the given threshold over a period time.

       If you set maxspeed to a value lower than CURLOPT_UPLOAD_BUFFERSIZE(3), libcurl might "shoot over" the limit on its first send and still send off a full buffer.

       This option doesn't affect transfer speeds done with FILE:// URLs.

DEFAULT
       0, disabled

~~~
PROTOCOLS
~~~
       All except file://

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* cap the upload speed to 1000 bytes/sec */
         curl_easy_setopt(curl, CURLOPT_MAX_SEND_SPEED_LARGE, (curl_off_t)1000);
         /* (set some upload options as well!) */
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.15.5

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_MAX_RECV_SPEED_LARGE(3), CURLOPT_LOW_SPEED_LIMIT(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                 CURLOPT_MAX_SEND_SPEED_LARGE(3)
CURLOPT_MIMEPOST(3)                                                                       curl_easy_setopt options                                                                      CURLOPT_MIMEPOST(3)



NAME
~~~
       CURLOPT_MIMEPOST - send data from mime structure

~~~
NAME
~~~
       #include <curl/curl.h>

       curl_mime *mime;

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_MIMEPOST, mime);

~~~
DESCRIPTION
~~~
       Pass a mime handle previously obtained from curl_mime_init(3).

       This setting is supported by the HTTP protocol to post forms and by the SMTP and IMAP protocols to provide the e-mail data to send/upload.

       This option is the preferred way of posting an HTTP form, replacing and extending the deprecated CURLOPT_HTTPPOST(3) option.

~~~
PROTOCOLS
~~~
       HTTP, SMTP, IMAP.

~~~
EXAMPLE
~~~
       Using this option implies the use of several mime structure building functions: see https://curl.se/libcurl/c/smtp-mime.html for a complete example.

~~~
AVAILABILITY
~~~
       Since 7.56.0.

~~~
RETURN VALUE
~~~
       This will return CURLE_OK.

~~~
SEE ALSO
       curl_mime_init(3)



libcurl 7.56.0                                                                                  22 Aug 2017                                                                             CURLOPT_MIMEPOST(3)
CURLOPT_NETRC(3)                                                                          curl_easy_setopt options                                                                         CURLOPT_NETRC(3)



NAME
~~~
       CURLOPT_NETRC - request that .netrc is used

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_NETRC, long level);

~~~
DESCRIPTION
~~~
       This  parameter  controls  the  preference  level  of  libcurl  between  using  user names and passwords from your ~/.netrc file, relative to user names and passwords in the URL supplied with CURLOPT_URL(3). On Windows, libcurl will use the file as %HOME%/_netrc, but you can also tell libcurl a different file name to use with CURLOPT_NETRC_FILE(3).

       libcurl uses a user name (and supplied or prompted password) supplied with CURLOPT_USERPWD(3) or CURLOPT_USERNAME(3) in preference to any of the options controlled by this parameter.

       Only machine name, user name and password are taken into account (init macros and similar things aren't supported).

       libcurl does not verify that the file has the correct properties set (as the standard Unix ftp client does). It should only be readable by user.

       level should be set to one of the values described below.


       CURL_NETRC_OPTIONAL
              The use of the ~/.netrc file is optional, and information in the URL is to be preferred.  The file will be scanned for the host and user name (to find the password only)  or  for  the  host
              only, to find the first user name and password after that machine, which ever information is not specified.

              Undefined values of the option will have this effect.

       CURL_NETRC_IGNORED
              The library will ignore the ~/.netrc file.

              This is the default.

       CURL_NETRC_REQUIRED
              The use of the ~/.netrc file is required, and information in the URL is to be ignored.  The file will be scanned for the host and user name (to find the password only) or for the host only,
              to find the first user name and password after that machine, which ever information is not specified.

DEFAULT
       CURL_NETRC_IGNORED

~~~
PROTOCOLS
~~~
       Most

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/");
         curl_easy_setopt(curl, CURLOPT_NETRC, CURL_NETRC_OPTIONAL);
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_USERPWD(3), CURLOPT_USERNAME(3), CURLOPT_NETRC_FILE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                                CURLOPT_NETRC(3)
CURLOPT_NETRC_FILE(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_NETRC_FILE(3)



NAME
~~~
       CURLOPT_NETRC_FILE - file name to read .netrc info from

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_NETRC_FILE, char *file);

~~~
DESCRIPTION
~~~
       Pass  a  char * as parameter, pointing to a null-terminated string containing the full path name to the file you want libcurl to use as .netrc file. If this option is omitted, and CURLOPT_NETRC(3)
       is set, libcurl will attempt to find a .netrc file in the current user's home directory.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/");
         curl_easy_setopt(curl, CURLOPT_NETRC, CURL_NETRC_OPTIONAL);
         curl_easy_setopt(curl, CURLOPT_NETRC_FILE, "/tmp/magic-netrc");
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.10.9

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_NETRC(3), CURLOPT_USERNAME(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                           CURLOPT_NETRC_FILE(3)
CURLOPT_NEW_DIRECTORY_PERMS(3)                                                            curl_easy_setopt options                                                           CURLOPT_NEW_DIRECTORY_PERMS(3)



NAME
~~~
       CURLOPT_NEW_DIRECTORY_PERMS - permissions for remotely created directories

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_NEW_DIRECTORY_PERMS,
                                 long mode);

~~~
DESCRIPTION
~~~
       Pass  a long as a parameter, containing the value of the permissions that will be assigned to newly created directories on the remote server.  The default value is 0755, but any valid value can be
       used.  The only protocols that can use this are sftp://, scp://, and file://.

DEFAULT
       0755

~~~
PROTOCOLS
~~~
       SFTP, SCP and FILE

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "sftp://upload.example.com/newdir/file.zip");
         curl_easy_setopt(curl, CURLOPT_FTP_CREATE_MISSING_DIRS, 1L);
         curl_easy_setopt(curl, CURLOPT_NEW_DIRECTORY_PERMS, 0644L);
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.16.4

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_NEW_FILE_PERMS(3), CURLOPT_UPLOAD(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                  CURLOPT_NEW_DIRECTORY_PERMS(3)
CURLOPT_NEW_FILE_PERMS(3)                                                                 curl_easy_setopt options                                                                CURLOPT_NEW_FILE_PERMS(3)



NAME
~~~
       CURLOPT_NEW_FILE_PERMS - permissions for remotely created files

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_NEW_FILE_PERMS,
                                 long mode);

~~~
DESCRIPTION
~~~
       Pass  a long as a parameter, containing the value of the permissions that will be assigned to newly created files on the remote server.  The default value is 0644, but any valid value can be used.
       The only protocols that can use this are sftp://, scp://, and file://.

DEFAULT
       0644

~~~
PROTOCOLS
~~~
       SFTP, SCP and FILE

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "sftp://upload.example.com/file.txt");
         curl_easy_setopt(curl, CURLOPT_NEW_FILE_PERMS, 0664L);
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.16.4

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_NEW_DIRECTORY_PERMS(3), CURLOPT_UPLOAD(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                       CURLOPT_NEW_FILE_PERMS(3)
CURLOPT_NOBODY(3)                                                                         curl_easy_setopt options                                                                        CURLOPT_NOBODY(3)



NAME
~~~
       CURLOPT_NOBODY - do the download request without getting the body

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_NOBODY, long opt);

~~~
DESCRIPTION
~~~
       A  long parameter set to 1 tells libcurl to not include the body-part in the output when doing what would otherwise be a download. For HTTP(S), this makes libcurl do a HEAD request. For most other
       protocols it means just not asking to transfer the body data.

       For HTTP operations when CURLOPT_NOBODY(3) has been set, unsetting the option (with 0) will make it a GET again - only if the method is still set to be HEAD. The proper way to get back  to  a  GET
       request is to set CURLOPT_HTTPGET(3) and for other methods, use the POST or UPLOAD options.

       Enabling CURLOPT_NOBODY(3) means asking for a download without a body.

       If you do a transfer with HTTP that involves a method other than HEAD, you will get a body (unless the resource and server sends a zero byte body for the specific URL you request).

DEFAULT
       0, the body is transferred

~~~
PROTOCOLS
~~~
       Most

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* get us the resource without a body - use HEAD! */
         curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_HTTPGET(3), CURLOPT_POSTFIELDS(3), CURLOPT_UPLOAD(3), CURLOPT_REQUEST_TARGET(3), CURLOPT_MIMEPOST(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                               CURLOPT_NOBODY(3)
CURLOPT_NOPROGRESS(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_NOPROGRESS(3)



NAME
~~~
       CURLOPT_NOPROGRESS - switch off the progress meter

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_NOPROGRESS, long onoff);

~~~
DESCRIPTION
~~~
       If  onoff  is to 1, it tells the library to shut off the progress meter completely for requests done with this handle. It will also prevent the CURLOPT_XFERINFOFUNCTION(3) or CURLOPT_PROGRESSFUNCTION(3) from getting called.

DEFAULT
       1, meaning it normally runs without a progress meter.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* enable progress meter */
         curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK.

~~~
SEE ALSO
       CURLOPT_XFERINFOFUNCTION(3), CURLOPT_PROGRESSFUNCTION(3), CURLOPT_VERBOSE(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                           CURLOPT_NOPROGRESS(3)
CURLOPT_NOPROXY(3)                                                                        curl_easy_setopt options                                                                       CURLOPT_NOPROXY(3)



NAME
~~~
       CURLOPT_NOPROXY - disable proxy use for specific hosts

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_NOPROXY, char *noproxy);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to  a null-terminated string. The string consists of a comma separated list of host names that do not require a proxy to get reached, even if one is specified.  The only wildcard
       available is a single * character, which matches all hosts, and effectively disables the proxy. Each name in this list is matched as either a domain which contains the hostname,  or  the  hostname
       itself. For example, example.com would match example.com, example.com:80, and www.example.com, but not www.notanexample.com or example.com.othertld.

       If  the  name in the noproxy list has a leading period, it is a domain match against the provided host name. This way ".example.com" will switch off proxy use for both "www.example.com" as well as
       for "foo.example.com".

       Setting the noproxy string to "" (an empty string) will explicitly enable the proxy for all host names, even if there is an environment variable set for it.

       Enter IPv6 numerical addresses in the list of host names without enclosing brackets:

        "example.com,::1,localhost"

       IPv6 numerical addresses are compared as strings, so they will only match if the representations are the same: "::1" is the same as "::0:1" but they don't match.

       The application does not have to keep the string around after setting this option.

Environment variables
       If there's an environment variable called no_proxy (or NO_PROXY), it will be used if the CURLOPT_NOPROXY(3) option is not set. It works exactly the same way.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       Most

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         /* accept various URLs */
         curl_easy_setopt(curl, CURLOPT_URL, input);
         /* use this proxy */
         curl_easy_setopt(curl, CURLOPT_PROXY, "http://proxy:80");
         /* ... but make sure this host name is not proxied */
         curl_easy_setopt(curl, CURLOPT_NOPROXY, "www.example.com");
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.4

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY(3), CURLOPT_PROXYAUTH(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                              CURLOPT_NOPROXY(3)
CURLOPT_NOSIGNAL(3)                                                                       curl_easy_setopt options                                                                      CURLOPT_NOSIGNAL(3)



NAME
~~~
       CURLOPT_NOSIGNAL - skip all signal handling

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_NOSIGNAL, long onoff);

~~~
DESCRIPTION
~~~
       If  onoff  is  1,  libcurl  will  not use any functions that install signal handlers or any functions that cause signals to be sent to the process. This option is here to allow multi-threaded unix
       applications to still set/use all timeout options etc, without risking getting signals.

       If this option is set and libcurl has been built with the standard name resolver, timeouts will not occur while the name resolve takes place.  Consider building libcurl with the c-ares or threaded
       resolver backends to enable asynchronous DNS lookups, to enable timeouts for name resolves without the use of signals.

       Setting  CURLOPT_NOSIGNAL(3)  to  1  makes  libcurl NOT ask the system to ignore SIGPIPE signals, which otherwise are sent by the system when trying to send data to a socket which is closed in the
       other end. libcurl makes an effort to never cause such SIGPIPEs to trigger, but some operating systems have no way to avoid them and even on those that have there are some corner cases  when  they
       may still happen, contrary to our desire. In addition, using CURLAUTH_NTLM_WB authentication could cause a SIGCHLD signal to be raised.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");

         curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1L);

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.10

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_TIMEOUT(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                             CURLOPT_NOSIGNAL(3)
CURLOPT_OPENSOCKETDATA(3)                                                                 curl_easy_setopt options                                                                CURLOPT_OPENSOCKETDATA(3)



NAME
~~~
       CURLOPT_OPENSOCKETDATA - custom pointer passed to open socket callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_OPENSOCKETDATA, void *pointer);

~~~
DESCRIPTION
~~~
       Pass a pointer that will be untouched by libcurl and passed as the first argument in the opensocket callback set with CURLOPT_OPENSOCKETFUNCTION(3).

DEFAULT
       The default value of this parameter is NULL.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       /* make libcurl use the already established socket 'sockfd' */

       static curl_socket_t opensocket(void *clientp,
                                       curlsocktype purpose,
                                       struct curl_sockaddr *address)
       {
         curl_socket_t sockfd;
         sockfd = *(curl_socket_t *)clientp;
         /* the actual externally set socket is passed in via the OPENSOCKETDATA
            option */
         return sockfd;
       }

       static int sockopt_callback(void *clientp, curl_socket_t curlfd,
                                   curlsocktype purpose)
       {
         /* This return code was added in libcurl 7.21.5 */
         return CURL_SOCKOPT_ALREADY_CONNECTED;
       }

       curl = curl_easy_init();
       if(curl) {
         /* libcurl will internally think that you connect to the host
          * and port that you specify in the URL option. */
         curl_easy_setopt(curl, CURLOPT_URL, "http://99.99.99.99:9999");
         /* call this function to get a socket */
         curl_easy_setopt(curl, CURLOPT_OPENSOCKETFUNCTION, opensocket);
         curl_easy_setopt(curl, CURLOPT_OPENSOCKETDATA, &sockfd);

         /* call this function to set options for the socket */
         curl_easy_setopt(curl, CURLOPT_SOCKOPTFUNCTION, sockopt_callback);

         res = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.17.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_OPENSOCKETFUNCTION(3), CURLOPT_SOCKOPTFUNCTION(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                       CURLOPT_OPENSOCKETDATA(3)
CURLOPT_OPENSOCKETFUNCTION(3)                                                             curl_easy_setopt options                                                            CURLOPT_OPENSOCKETFUNCTION(3)



NAME
~~~
       CURLOPT_OPENSOCKETFUNCTION - callback for opening socket

~~~
NAME
~~~
       #include <curl/curl.h>

       typedef enum  {
         CURLSOCKTYPE_IPCXN,  /* socket created for a specific IP connection */
       } curlsocktype;

       struct curl_sockaddr {
         int family;
         int socktype;
         int protocol;
         unsigned int addrlen;
         struct sockaddr addr;
       };

       curl_socket_t opensocket_callback(void *clientp,
                                         curlsocktype purpose,
                                         struct curl_sockaddr *address);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_OPENSOCKETFUNCTION, opensocket_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       This  callback  function gets called by libcurl instead of the socket(2) call. The callback's purpose argument identifies the exact purpose for this particular socket. CURLSOCKTYPE_IPCXN is for IP
       based connections and is the only purpose currently used in libcurl. Future versions of libcurl may support more purposes.

       The clientp pointer contains whatever user-defined value set using the CURLOPT_OPENSOCKETDATA(3) function.

       The callback gets the resolved peer address as the address argument and is allowed to modify the address or refuse to connect completely. The callback function  should  return  the  newly  created
       socket or CURL_SOCKET_BAD in case no connection could be established or another error was detected. Any additional setsockopt(2) calls can of course be done on the socket at the user's discretion.
       A CURL_SOCKET_BAD return value from the callback function will signal an unrecoverable error to libcurl and it will return CURLE_COULDNT_CONNECT from the function  that  triggered  this  callback.
       This return code can be used for IP address block listing.

       If you want to pass in a socket with an already established connection, pass the socket back with this callback and then use CURLOPT_SOCKOPTFUNCTION(3) to signal that it already is connected.

DEFAULT
       The default behavior is the equivalent of this:
          return socket(addr->family, addr->socktype, addr->protocol);

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       /* make libcurl use the already established socket 'sockfd' */

       static curl_socket_t opensocket(void *clientp,
                                       curlsocktype purpose,
                                       struct curl_sockaddr *address)
       {
         curl_socket_t sockfd;
         sockfd = *(curl_socket_t *)clientp;
         /* the actual externally set socket is passed in via the OPENSOCKETDATA
            option */
         return sockfd;
       }

       static int sockopt_callback(void *clientp, curl_socket_t curlfd,
                                   curlsocktype purpose)
       {
         /* This return code was added in libcurl 7.21.5 */
         return CURL_SOCKOPT_ALREADY_CONNECTED;
       }

       curl = curl_easy_init();
       if(curl) {
         /* libcurl will internally think that you connect to the host
          * and port that you specify in the URL option. */
         curl_easy_setopt(curl, CURLOPT_URL, "http://99.99.99.99:9999");
         /* call this function to get a socket */
         curl_easy_setopt(curl, CURLOPT_OPENSOCKETFUNCTION, opensocket);
         curl_easy_setopt(curl, CURLOPT_OPENSOCKETDATA, &sockfd);

         /* call this function to set options for the socket */
         curl_easy_setopt(curl, CURLOPT_SOCKOPTFUNCTION, sockopt_callback);

         res = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.17.1.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_OPENSOCKETDATA(3), CURLOPT_SOCKOPTFUNCTION(3), CURLOPT_CLOSESOCKETFUNCTION(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                   CURLOPT_OPENSOCKETFUNCTION(3)
CURLOPT_PASSWORD(3)                                                                       curl_easy_setopt options                                                                      CURLOPT_PASSWORD(3)



NAME
~~~
       CURLOPT_PASSWORD - password to use in authentication

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PASSWORD, char *pwd);

~~~
DESCRIPTION
~~~
       Pass a char * as parameter, which should be pointing to the null-terminated password to use for the transfer.

       The CURLOPT_PASSWORD(3) option should be used in conjunction with the CURLOPT_USERNAME(3) option.

       The application does not have to keep the string around after setting this option.

DEFAULT
       blank

~~~
PROTOCOLS
~~~
       Most

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

         curl_easy_setopt(curl, CURLOPT_PASSWORD, "qwerty");

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_USERPWD(3), CURLOPT_USERNAME(3), CURLOPT_HTTPAUTH(3), CURLOPT_PROXYAUTH(3)



libcurl 7.37.0                                                                                  19 Jun 2014                                                                             CURLOPT_PASSWORD(3)
CURLOPT_PATH_AS_IS(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_PATH_AS_IS(3)



NAME
~~~
       CURLOPT_PATH_AS_IS - do not handle dot dot sequences

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PATH_AS_IS, long leaveit);

~~~
DESCRIPTION
~~~
       Set the long leaveit to 1, to explicitly tell libcurl to not alter the given path before passing it on to the server.

       This instructs libcurl to NOT squash sequences of "/../" or "/./" that may exist in the URL's path part and that is supposed to be removed according to RFC 3986 section 5.2.4.

       Some server implementations are known to (erroneously) require the dot dot sequences to remain in the path and some clients want to pass these on in order to try out server implementations.

       By default libcurl will merge such sequences before using the path.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/../../etc/password");

         curl_easy_setopt(curl, CURLOPT_PATH_AS_IS, 1L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.42.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_STDERR(3), CURLOPT_DEBUGFUNCTION(3), CURLOPT_URL(3),



libcurl 7.42.0                                                                                  17 Jun 2014                                                                           CURLOPT_PATH_AS_IS(3)
CURLOPT_PINNEDPUBLICKEY(3)                                                                curl_easy_setopt options                                                               CURLOPT_PINNEDPUBLICKEY(3)



NAME
~~~
       CURLOPT_PINNEDPUBLICKEY - pinned public key

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PINNEDPUBLICKEY, char *pinnedpubkey);

~~~
DESCRIPTION
~~~
       Pass  a  pointer to a null-terminated string as parameter. The string can be the file name of your pinned public key. The file format expected is "PEM" or "DER".  The string can also be any number
       of base64 encoded sha256 hashes preceded by "sha256//" and separated by ";"

       When negotiating a TLS or SSL connection, the server sends a certificate indicating its identity. A public key is extracted from this certificate and if it does not exactly match  the  public  key
       provided to this option, curl will abort the connection before sending or receiving any data.

       On mismatch, CURLE_SSL_PINNEDPUBKEYNOTMATCH is returned.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_PINNEDPUBLICKEY, "/etc/publickey.der");
         /* OR
         curl_easy_setopt(curl, CURLOPT_PINNEDPUBLICKEY, "sha256//YhKJKSzoTt2b5FP18fvpHo7fJYqQCjAa3HWY3tvRMwE=;sha256//t62CeU2tQiqkexU74Gxa2eg7fRbEgoChTociMee9wno=");
         */

         /* Perform the request */
         curl_easy_perform(curl);
       }

PUBLIC KEY EXTRACTION
       If you do not have the server's public key file you can extract it from the server's certificate.
       # retrieve the server's certificate if you don't already have it
       #
       # be sure to examine the certificate to see if it is what you expected
       #
       # Windows-specific:
       # - Use NUL instead of /dev/null.
       # - OpenSSL may wait for input instead of disconnecting. Hit enter.
       # - If you don't have sed, then just copy the certificate into a file:
       #   Lines from -----BEGIN CERTIFICATE----- to -----END CERTIFICATE-----.
       #
       openssl s_client -servername www.example.com -connect www.example.com:443 < /dev/null | sed -n "/-----BEGIN/,/-----END/p" > www.example.com.pem

       # extract public key in pem format from certificate
       openssl x509 -in www.example.com.pem -pubkey -noout > www.example.com.pubkey.pem

       # convert public key from pem to der
       openssl asn1parse -noout -inform pem -in www.example.com.pubkey.pem -out www.example.com.pubkey.der

       # sha256 hash and base64 encode der to string for use
       openssl dgst -sha256 -binary www.example.com.pubkey.der | openssl base64
       The public key in PEM format contains a header, base64 data and a footer:
       -----BEGIN PUBLIC KEY-----
       [BASE 64 DATA]
       -----END PUBLIC KEY-----

~~~
AVAILABILITY
~~~
       PEM/DER support:

         7.39.0: OpenSSL, GnuTLS

         7.39.0-7.48.0,7.58.1+: GSKit

         7.43.0: NSS and wolfSSL

         7.47.0: mbedtls

         7.54.1: SecureTransport on macOS 10.7+/iOS 10+

         7.58.1: SChannel

       sha256 support:

         7.44.0: OpenSSL, GnuTLS, NSS and wolfSSL

         7.47.0: mbedtls

         7.54.1: SecureTransport on macOS 10.7+/iOS 10+

         7.58.1: SChannel Windows XP SP3+

       Other SSL backends not supported.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS enabled, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSL_VERIFYPEER(3), CURLOPT_SSL_VERIFYHOST(3), CURLOPT_CAINFO(3), CURLOPT_CAPATH(3),



libcurl 7.38.0                                                                                  27 Aug 2014                                                                      CURLOPT_PINNEDPUBLICKEY(3)
CURLOPT_PIPEWAIT(3)                                                                       curl_easy_setopt options                                                                      CURLOPT_PIPEWAIT(3)



NAME
~~~
       CURLOPT_PIPEWAIT - wait for pipelining/multiplexing

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PIPEWAIT, long wait);

~~~
DESCRIPTION
~~~
       Set wait to 1L to tell libcurl to prefer to wait for a connection to confirm or deny that it can do pipelining or multiplexing before continuing.

       When about to perform a new transfer that allows pipelining or multiplexing, libcurl will check for existing connections to re-use and pipeline on. If no such connection exists it will immediately
       continue and create a fresh new connection to use.

       By setting this option to 1 - and having CURLMOPT_PIPELINING(3) enabled for the multi handle this transfer is associated with - libcurl will instead wait for the connection to reveal if it is possible to pipeline/multiplex on before it continues. This enables libcurl to much better keep the number of connections to a minimum when using pipelining or multiplexing protocols.

       The  effect  thus  becomes that with this option set, libcurl prefers to wait and re-use an existing connection for pipelining rather than the opposite: prefer to open a new connection rather than
       waiting.

       The waiting time is as long as it takes for the connection to get up and for libcurl to get the necessary response back that informs it about its protocol and support level.

DEFAULT
       0 (off)

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PIPEWAIT, 1L);

         /* now add this easy handle to the multi handle */
       }

~~~
AVAILABILITY
~~~
       Added in 7.43.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_FORBID_REUSE(3), CURLOPT_FRESH_CONNECT(3), CURLMOPT_PIPELINING(3), CURLMOPT_MAX_HOST_CONNECTIONS(3),



libcurl 7.43.0                                                                                  12 May 2015                                                                             CURLOPT_PIPEWAIT(3)
CURLOPT_PORT(3)                                                                           curl_easy_setopt options                                                                          CURLOPT_PORT(3)



NAME
~~~
       CURLOPT_PORT - remote port number to connect to

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PORT, long number);

~~~
DESCRIPTION
~~~
       This option sets number to be the remote port number to connect to, instead of the one specified in the URL or the default port for the used protocol.

       Usually, you just let the URL decide which port to use but this allows the application to override that.

       While this option accepts a 'long', a port number is usually a 16 bit number and therefore using a port number over 65535 will cause a run-time error.

DEFAULT
       By default this is 0 which makes it not used.

~~~
PROTOCOLS
~~~
       Used for all protocols that speak to a port number.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
         curl_easy_setopt(curl, CURLOPT_PORT, 8080L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_STDERR(3), CURLOPT_DEBUGFUNCTION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                                 CURLOPT_PORT(3)
CURLOPT_POST(3)                                                                           curl_easy_setopt options                                                                          CURLOPT_POST(3)



NAME
~~~
       CURLOPT_POST - request an HTTP POST

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_POST, long post);

~~~
DESCRIPTION
~~~
       A parameter set to 1 tells libcurl to do a regular HTTP post. This will also make the library use a "Content-Type: application/x-www-form-urlencoded" header. (This is by far the most commonly used
       POST method).

       Use one of CURLOPT_POSTFIELDS(3) or CURLOPT_COPYPOSTFIELDS(3) options to specify what data to post and CURLOPT_POSTFIELDSIZE(3) or CURLOPT_POSTFIELDSIZE_LARGE(3) to set the data size.

       Optionally, you can provide data to POST using the CURLOPT_READFUNCTION(3) and CURLOPT_READDATA(3) options but then you must make sure to not set CURLOPT_POSTFIELDS(3) to anything but  NULL.  When
       providing  data  with  a  callback,  you  must  transmit it using chunked transfer-encoding or you must set the size of the data with the CURLOPT_POSTFIELDSIZE(3) or CURLOPT_POSTFIELDSIZE_LARGE(3)
       options. To enable chunked encoding, you simply pass in the appropriate Transfer-Encoding header, see the post-callback.c example.

       You can override the default POST Content-Type: header by setting your own with CURLOPT_HTTPHEADER(3).

       Using POST with HTTP 1.1 implies the use of a "Expect: 100-continue" header.  You can disable this header with CURLOPT_HTTPHEADER(3) as usual.

       If you use POST to an HTTP 1.1 server, you can send data without knowing the size before starting the POST if you use chunked encoding. You enable this by adding a header like  "Transfer-Encoding:
       chunked"  with  CURLOPT_HTTPHEADER(3). With HTTP 1.0 or without chunked transfer, you must specify the size in the request. (Since 7.66.0, libcurl will automatically use chunked encoding for POSTs
       if the size is unknown.)

       When setting CURLOPT_POST(3) to 1, libcurl will automatically set CURLOPT_NOBODY(3) and CURLOPT_HTTPGET(3) to 0.

       If you issue a POST request and then want to make a HEAD or GET using the same re-used handle, you must explicitly set the new request type using CURLOPT_NOBODY(3) or CURLOPT_HTTPGET(3)  or  similar.

DEFAULT
       0, disabled

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
         curl_easy_setopt(curl, CURLOPT_POST, 1L);

         /* set up the read callback with CURLOPT_READFUNCTION */

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Along with HTTP

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_POSTFIELDS(3), CURLOPT_HTTPPOST(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                                 CURLOPT_POST(3)
CURLOPT_POSTFIELDS(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_POSTFIELDS(3)



NAME
~~~
       CURLOPT_POSTFIELDS - data to POST to server

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_POSTFIELDS, char *postdata);

~~~
DESCRIPTION
~~~
       Pass  a  char * as parameter, pointing to the full data to send in an HTTP POST operation. You must make sure that the data is formatted the way you want the server to receive it. libcurl will not
       convert or encode it for you in any way. For example, the web server may assume that this data is url-encoded.

       The data pointed to is NOT copied by the library: as a consequence, it must be preserved by the calling application until the associated transfer  finishes.   This  behavior  can  be  changed  (so
       libcurl does copy the data) by setting the CURLOPT_COPYPOSTFIELDS(3) option.

       This POST is a normal application/x-www-form-urlencoded kind (and libcurl will set that Content-Type by default when this option is used), which is commonly used by HTML forms. Change Content-Type
       with CURLOPT_HTTPHEADER(3).

       You can use curl_easy_escape(3) to url-encode your data, if necessary. It returns a pointer to an encoded string that can be passed as postdata.

       Using CURLOPT_POSTFIELDS(3) implies setting CURLOPT_POST(3) to 1.

       If CURLOPT_POSTFIELDS(3) is explicitly set to NULL then libcurl will get the POST data from the read callback. If you want to send a zero-byte POST set CURLOPT_POSTFIELDS(3) to an empty string, or
       set CURLOPT_POST(3) to 1 and CURLOPT_POSTFIELDSIZE(3) to 0.

       libcurl  will  use  assume this option points to a nul-terminated string unless you also set CURLOPT_POSTFIELDSIZE(3) to specify the length of the provided data, which then is strictly required if
       you want to send off nul bytes included in the data.

       Using POST with HTTP 1.1 implies the use of a "Expect: 100-continue" header, and libcurl will add that header automatically if the POST is either known to be larger than 1MB  or  if  the  expected
       size is unknown. You can disable this header with CURLOPT_HTTPHEADER(3) as usual.

       To make multipart/formdata posts (aka RFC2388-posts), check out the CURLOPT_HTTPPOST(3) option combined with curl_formadd(3).

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         const char *data = "data to send";

         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* size of the POST data */
         curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, 12L);

         /* pass in a pointer to the data - libcurl will not copy */
         curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_POSTFIELDSIZE(3), CURLOPT_READFUNCTION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                           CURLOPT_POSTFIELDS(3)
CURLOPT_POSTFIELDSIZE(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_POSTFIELDSIZE(3)



NAME
~~~
       CURLOPT_POSTFIELDSIZE - size of POST data pointed to

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_POSTFIELDSIZE, long size);

~~~
DESCRIPTION
~~~
       If  you want to post data to the server without having libcurl do a strlen() to measure the data size, this option must be used. When this option is used you can post fully binary data, which otherwise is likely to fail. If this size is set to -1, the library will use strlen() to get the size.

       If you post more than 2GB, use CURLOPT_POSTFIELDSIZE_LARGE(3).

DEFAULT
       -1

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         const char *data = "data to send";

         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* size of the POST data */
         curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long) strlen(data));

         curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Along with HTTP

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_POSTFIELDS(3), CURLOPT_POSTFIELDSIZE_LARGE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                        CURLOPT_POSTFIELDSIZE(3)
CURLOPT_POSTFIELDSIZE_LARGE(3)                                                            curl_easy_setopt options                                                           CURLOPT_POSTFIELDSIZE_LARGE(3)



NAME
~~~
       CURLOPT_POSTFIELDSIZE_LARGE - size of POST data pointed to

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_POSTFIELDSIZE_LARGE,
                                 curl_off_t size);

~~~
DESCRIPTION
~~~
       If  you want to post data to the server without having libcurl do a strlen() to measure the data size, this option must be used. When this option is used you can post fully binary data, which otherwise is likely to fail. If this size is set to -1, the library will use strlen() to get the size.

DEFAULT
       -1

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         const char *data = large_chunk;
         curl_off_t length_of_data; /* set somehow */

         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* size of the POST data */
         curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE_LARGE, length_of_data);

         curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Along with HTTP

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_POSTFIELDS(3), CURLOPT_COPYPOSTFIELDS(3), CURLOPT_POSTFIELDSIZE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                  CURLOPT_POSTFIELDSIZE_LARGE(3)
CURLOPT_POSTQUOTE(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_POSTQUOTE(3)



NAME
~~~
       CURLOPT_POSTQUOTE - (S)FTP commands to run after the transfer

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_POSTQUOTE, struct curl_slist *cmds);

~~~
DESCRIPTION
~~~
       Pass a pointer to a linked list of FTP or SFTP commands to pass to the server after your FTP transfer request. The commands will only be run if no error occurred. The linked list should be a fully
       valid list of struct curl_slist structs properly filled in as described for CURLOPT_QUOTE(3).

       Disable this operation again by setting a NULL to this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       SFTP and FTP

~~~
EXAMPLE
~~~
       struct curl_slist *cmdlist = NULL;
       cmdlist = curl_slist_append(cmdlist, "RNFR source-name");
       cmdlist = curl_slist_append(cmdlist, "RNTO new-name");

       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/foo.bin");

         /* pass in the FTP commands to run after the transfer */
         curl_easy_setopt(curl, CURLOPT_POSTQUOTE, cmdlist);

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       If support for the protocols are built-in.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_QUOTE(3), CURLOPT_PREQUOTE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                            CURLOPT_POSTQUOTE(3)
CURLOPT_POSTREDIR(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_POSTREDIR(3)



NAME
~~~
       CURLOPT_POSTREDIR - how to act on an HTTP POST redirect

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_POSTREDIR,
                                 long bitmask);

~~~
DESCRIPTION
~~~
       Pass  a  bitmask to control how libcurl acts on redirects after POSTs that get a 301, 302 or 303 response back.  A parameter with bit 0 set (value CURL_REDIR_POST_301) tells the library to respect
       RFC 7231 (section 6.4.2 to 6.4.4) and not convert POST requests into GET requests when following a 301 redirection.  Setting bit 1 (value CURL_REDIR_POST_302) makes libcurl  maintain  the  request
       method  after  a 302 redirect whilst setting bit 2 (value CURL_REDIR_POST_303) makes libcurl maintain the request method after a 303 redirect. The value CURL_REDIR_POST_ALL is a convenience define
       that sets all three bits.

       The non-RFC behavior is ubiquitous in web browsers, so the library does the conversion by default to maintain consistency. However, a server may require a POST to remain a POST after such a  redirection. This option is meaningful only when setting CURLOPT_FOLLOWLOCATION(3).

DEFAULT
       0

~~~
PROTOCOLS
~~~
       HTTP(S)

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* a silly POST example */
         curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "data=true");

         /* example.com is redirected, so we tell libcurl to send POST on 301, 302 and
            303 HTTP response codes */
         curl_easy_setopt(curl, CURLOPT_POSTREDIR, CURL_REDIR_POST_ALL);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.17.1. This option was known as CURLOPT_POST301 up to 7.19.0 as it only supported the 301 then. CURL_REDIR_POST_303 was added in 7.26.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_FOLLOWLOCATION(3), CURLOPT_POSTFIELDS(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                            CURLOPT_POSTREDIR(3)
CURLOPT_PRE_PROXY(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_PRE_PROXY(3)



NAME
~~~
       CURLOPT_PRE_PROXY - pre-proxy host to use

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PRE_PROXY, char *preproxy);

~~~
DESCRIPTION
~~~
       Set  the  preproxy to use for the upcoming request. The parameter should be a char * to a null-terminated string holding the host name or dotted numerical IP address. A numerical IPv6 address must
       be written within [brackets].

       To specify port number in this string, append :[port] to the end of the host name. The proxy's port number may optionally be specified with the separate option CURLOPT_PROXYPORT(3). If not  specified, libcurl will default to using port 1080 for proxies.

       A pre proxy is a SOCKS proxy that curl connects to before it connects to the HTTP(S) proxy specified in the CURLOPT_PROXY option. The pre proxy can only be a SOCKS proxy.

       The  pre  proxy  string should be prefixed with [scheme]:// to specify which kind of socks is used. Use socks4://, socks4a://, socks5:// or socks5h:// (the last one to enable socks5 and asking the
       proxy to do the resolving, also known as CURLPROXY_SOCKS5_HOSTNAME type) to request the specific SOCKS version to be used. Otherwise SOCKS4 is used as default.

       Setting the pre proxy string to "" (an empty string) will explicitly disable the use of a pre proxy.

       The application does not have to keep the string around after setting this option.

DEFAULT
       Default is NULL, meaning no pre proxy is used.

       When you set a host name to use, do not assume that there's any particular single port number used widely for proxies. Specify it!

~~~
PROTOCOLS
~~~
       All except file://. Note that some protocols don't do very well over proxy.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/file.txt");
         curl_easy_setopt(curl, CURLOPT_PREPROXY, "socks4://socks-proxy:1080");
         curl_easy_setopt(curl, CURLOPT_PROXY, "http://proxy:80");
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if proxies are supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY(3), CURLOPT_HTTPPROXYTUNNEL(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                                            CURLOPT_PRE_PROXY(3)
CURLOPT_PREQUOTE(3)                                                                       curl_easy_setopt options                                                                      CURLOPT_PREQUOTE(3)



NAME
~~~
       CURLOPT_PREQUOTE - commands to run before an FTP transfer

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PREQUOTE,
                                 struct curl_slist *cmds);

~~~
DESCRIPTION
~~~
       Pass  a  pointer to a linked list of FTP commands to pass to the server after the transfer type is set. The linked list should be a fully valid list of struct curl_slist structs properly filled in
       as described for CURLOPT_QUOTE(3). Disable this operation again by setting a NULL to this option.

       While CURLOPT_QUOTE(3) and CURLOPT_POSTQUOTE(3) work for SFTP, this option does not.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       struct curl_slist *cmdlist = NULL;
       cmdlist = curl_slist_append(cmdlist, "SYST");

       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/foo.bin");

         /* pass in the FTP commands to run */
         curl_easy_setopt(curl, CURLOPT_PREQUOTE, cmdlist);

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Along with the protocol support

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_QUOTE(3), CURLOPT_POSTQUOTE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                             CURLOPT_PREQUOTE(3)
CURLOPT_PREREQDATA(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_PREREQDATA(3)



NAME
~~~
       CURLOPT_PREREQDATA - custom pointer passed to the pre-request callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PREREQDATA, void *pointer);

~~~
DESCRIPTION
~~~
       Pass a pointer that will be untouched by libcurl and passed as the first argument in the pre-request callback set with CURLOPT_PREREQFUNCTION(3).

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       static int prereq_callback(void *clientp,
                                  char *conn_primary_ip,
                                  char *conn_local_ip,
                                  int conn_primary_port,
                                  int conn_local_port)
       {
         printf("Connection made to %s:%s0, conn_primary_ip, conn_primary_port);
         return CURL_PREREQFUNC_OK;
       }

       {
         struct data prereq_data;
         curl_easy_setopt(CURL *handle, CURLOPT_PREREQFUNCTION, prereq_callback);
         curl_easy_setopt(CURL *handle, CURLOPT_PREREQDATA, &prereq_data);
       }

~~~
AVAILABILITY
~~~
       Added in 7.80.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_PREREQFUNCTION(3)



libcurl 7.80.0                                                                                   2 Aug 2021                                                                           CURLOPT_PREREQDATA(3)
CURLOPT_PREREQFUNCTION(3)                                                                 curl_easy_setopt options                                                                CURLOPT_PREREQFUNCTION(3)



NAME
~~~
       CURLOPT_PREREQFUNCTION - user callback called when a connection has been established, but before a request has been made.

~~~
NAME
~~~
       #include <curl/curl.h>

       /* These are the return codes for the pre-request callback. */
       #define CURL_PREREQFUNC_OK 0
       #define CURL_PREREQFUNC_ABORT 1 /* fail the entire transfer */

       int prereq_callback(void *clientp,
                           char *conn_primary_ip,
                           char *conn_local_ip,
                           int conn_primary_port,
                           int conn_local_port);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PREREQFUNCTION, prereq_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       This function gets called by libcurl after a connection has been established or a connection has been reused (including any SSL handshaking), but before any request is actually made on the connection. For example, for HTTP, this callback is called once a connection has been established to the server, but before a GET/HEAD/POST/etc request has been sent.

       This function may be called multiple times if redirections are enabled and are being followed (see CURLOPT_FOLLOWLOCATION(3)).

       This function is passed the following information:

       conn_primary_ip
              A nul-terminated pointer to a C string containing the primary IP of the remote server established with this connection. For FTP, this is the IP for the control  connection.  IPv6  addresses
              are represented without surrounding brackets.

       conn_local_ip
              A nul-terminated pointer to a C string containing the originating IP for this connection. IPv6 addresses are represented without surrounding brackets.

       conn_primary_port
              The  primary port number on the remote server established with this connection.  For FTP, this is the port for the control connection. This can be a TCP or a UDP port number dependending on
              the protocol.

       conn_local_port
              The originating port number for this connection. This can be a TCP or a UDP port number dependending on the protocol.

clientp is the pointer you set with CURLOPT_PREREQDATA(3).

The callback function must return CURL_PREREQFUNC_OK on success, or CURL_PREREQFUNC_ABORT to cause the transfer to fail.


DEFAULT
       By default, this is NULL and unused.

~~~
PROTOCOLS
~~~
       ALL

~~~
EXAMPLE
~~~
       static int prereq_callback(void *clientp,
                                  char *conn_primary_ip,
                                  char *conn_local_ip,
                                  int conn_primary_port,
                                  int conn_local_port)
       {
         printf("Connection made to %s:%s0, conn_primary_ip, conn_primary_port);
         return CURL_PREREQFUNC_OK;
       }

       {
         struct data prereq_data;
         curl_easy_setopt(CURL *handle, CURLOPT_PREREQFUNCTION, prereq_callback);
         curl_easy_setopt(CURL *handle, CURLOPT_PREREQDATA, &prereq_data);
       }

~~~
AVAILABILITY
~~~
       Added in 7.80.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_PREREQDATA(3)



libcurl 7.80.0                                                                                   2 Aug 2021                                                                       CURLOPT_PREREQFUNCTION(3)
CURLOPT_PRIVATE(3)                                                                        curl_easy_setopt options                                                                       CURLOPT_PRIVATE(3)



NAME
~~~
       CURLOPT_PRIVATE - store a private pointer

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PRIVATE, void *pointer);

~~~
DESCRIPTION
~~~
       Pass  a  void  *  as  parameter,  pointing  to data that should be associated with this curl handle.  The pointer can subsequently be retrieved using curl_easy_getinfo(3) with the CURLINFO_PRIVATE
       option. libcurl itself never does anything with this data.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       struct private secrets;
       if(curl) {
         struct private *extracted;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* store a pointer to our private struct */
         curl_easy_setopt(curl, CURLOPT_PRIVATE, &secrets);

         curl_easy_perform(curl);

         /* we can extract the private pointer again too */
         curl_easy_getinfo(curl, CURLINFO_PRIVATE, &extracted);
       }

~~~
AVAILABILITY
~~~
       Added in 7.10.3

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_VERBOSE(3), CURLOPT_STDERR(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                              CURLOPT_PRIVATE(3)
CURLOPT_PROGRESSDATA(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_PROGRESSDATA(3)



NAME
~~~
       CURLOPT_PROGRESSDATA - custom pointer passed to the progress callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROGRESSDATA, void *pointer);

~~~
DESCRIPTION
~~~
       Pass a pointer that will be untouched by libcurl and passed as the first argument in the progress callback set with CURLOPT_PROGRESSFUNCTION(3).

DEFAULT
       The default value of this parameter is NULL.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
        struct progress {
          char *private;
          size_t size;
        };

        static size_t progress_callback(void *clientp,
                                        double dltotal,
                                        double dlnow,
                                        double ultotal,
                                        double ulnow)
        {
          struct memory *progress = (struct progress *)userp;

          /* use the values */

          return 0; /* all is good */
        }

        struct progress data;

        /* pass struct to callback  */
        curl_easy_setopt(curl_handle, CURLOPT_PROGRESSDATA, &data);

        curl_easy_setopt(curl_handle, CURLOPT_PROGRESSFUNCTION, progress_callback);

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_PROGRESSFUNCTION(3), CURLOPT_XFERINFOFUNCTION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                         CURLOPT_PROGRESSDATA(3)
CURLOPT_PROGRESSFUNCTION(3)                                                               curl_easy_setopt options                                                              CURLOPT_PROGRESSFUNCTION(3)



NAME
~~~
       CURLOPT_PROGRESSFUNCTION - progress meter callback

~~~
NAME
~~~
       #include <curl/curl.h>

       int progress_callback(void *clientp,
                             double dltotal,
                             double dlnow,
                             double ultotal,
                             double ulnow);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROGRESSFUNCTION, progress_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       We encourage users to use the newer CURLOPT_XFERINFOFUNCTION(3) instead, if you can.

       This  function  gets  called by libcurl instead of its internal equivalent with a frequent interval. While data is being transferred it will be called very frequently, and during slow periods like
       when nothing is being transferred it can slow down to about one call per second.

       clientp is the pointer set with CURLOPT_PROGRESSDATA(3), it is not used by libcurl but is only passed along from the application to the callback.

       The callback gets told how much data libcurl will transfer and has transferred, in number of bytes. dltotal is the total number of bytes libcurl expects to download in this transfer. dlnow is  the
       number of bytes downloaded so far. ultotal is the total number of bytes libcurl expects to upload in this transfer. ulnow is the number of bytes uploaded so far.

       Unknown/unused  argument values passed to the callback will be set to zero (like if you only download data, the upload size will remain 0). Many times the callback will be called one or more times
       first, before it knows the data sizes so a program must be made to handle that.

       If your callback function returns CURL_PROGRESSFUNC_CONTINUE it will cause libcurl to continue executing the default progress function.

       Returning any other non-zero value from this callback will cause libcurl to abort the transfer and return CURLE_ABORTED_BY_CALLBACK.

       If you transfer data with the multi interface, this function will not be called during periods of idleness unless you call the appropriate libcurl function that performs transfers.

       CURLOPT_NOPROGRESS(3) must be set to 0 to make this function actually get called.

DEFAULT
       By default, libcurl has an internal progress meter. That's rarely wanted by users.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
        struct progress {
          char *private;
          size_t size;
        };

        static size_t progress_callback(void *clientp,
                                        double dltotal,
                                        double dlnow,
                                        double ultotal,
                                        double ulnow)
        {
          struct memory *progress = (struct progress *)userp;

          /* use the values */

          return 0; /* all is good */
        }

        struct progress data;

        /* pass struct to callback  */
        curl_easy_setopt(curl_handle, CURLOPT_PROGRESSDATA, &data);

        curl_easy_setopt(curl_handle, CURLOPT_PROGRESSFUNCTION, progress_callback);

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK.

~~~
SEE ALSO
       CURLOPT_VERBOSE(3), CURLOPT_NOPROGRESS(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                     CURLOPT_PROGRESSFUNCTION(3)
CURLOPT_PROTOCOLS(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_PROTOCOLS(3)



NAME
~~~
       CURLOPT_PROTOCOLS - allowed protocols

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROTOCOLS, long bitmask);

~~~
DESCRIPTION
~~~
       Pass  a  long  that  holds  a bitmask of CURLPROTO_* defines. If used, this bitmask limits what protocols libcurl may use in the transfer. This allows you to have a libcurl built to support a wide
       range of protocols but still limit specific transfers to only be allowed to use a subset of them. By default  libcurl  will  accept  all  protocols  it  supports  (CURLPROTO_ALL).  See  also  CURLOPT_REDIR_PROTOCOLS(3).

       These are the available protocol defines:
       CURLPROTO_DICT
       CURLPROTO_FILE
       CURLPROTO_FTP
       CURLPROTO_FTPS
       CURLPROTO_GOPHER
       CURLPROTO_HTTP
       CURLPROTO_HTTPS
       CURLPROTO_IMAP
       CURLPROTO_IMAPS
       CURLPROTO_LDAP
       CURLPROTO_LDAPS
       CURLPROTO_POP3
       CURLPROTO_POP3S
       CURLPROTO_RTMP
       CURLPROTO_RTMPE
       CURLPROTO_RTMPS
       CURLPROTO_RTMPT
       CURLPROTO_RTMPTE
       CURLPROTO_RTMPTS
       CURLPROTO_RTSP
       CURLPROTO_SCP
       CURLPROTO_SFTP
       CURLPROTO_SMB
       CURLPROTO_SMBS
       CURLPROTO_SMTP
       CURLPROTO_SMTPS
       CURLPROTO_TELNET
       CURLPROTO_TFTP

DEFAULT
       All protocols built-in

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         /* pass in the URL from an external source */
         curl_easy_setopt(curl, CURLOPT_URL, argv[1]);

         /* only allow HTTP, TFTP and SFTP */
         curl_easy_setopt(curl, CURLOPT_PROTOCOLS,
                          CURLPROTO_HTTP | CURLPROTO_TFTP | CURLPROTO_SFTP);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.4

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_REDIR_PROTOCOLS(3), CURLOPT_URL(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                            CURLOPT_PROTOCOLS(3)
CURLOPT_PROXY(3)                                                                          curl_easy_setopt options                                                                         CURLOPT_PROXY(3)



NAME
~~~
       CURLOPT_PROXY - proxy to use

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY, char *proxy);

~~~
DESCRIPTION
~~~
       Set  the  proxy to use for the upcoming request. The parameter should be a char * to a null-terminated string holding the host name or dotted numerical IP address. A numerical IPv6 address must be
       written within [brackets].

       To specify port number in this string, append :[port] to the end of the host name. The proxy's port number may optionally be specified with the separate option CURLOPT_PROXYPORT(3). If not  specified, libcurl will default to using port 1080 for proxies.

       The proxy string may be prefixed with [scheme]:// to specify which kind of proxy is used.


              http://
                     HTTP Proxy. Default when no scheme or proxy type is specified.

              https://
                     HTTPS Proxy. (Added in 7.52.0 for OpenSSL, GnuTLS and NSS)

              socks4://
                     SOCKS4 Proxy.

              socks4a://
                     SOCKS4a Proxy. Proxy resolves URL hostname.

              socks5://
                     SOCKS5 Proxy.

              socks5h://
                     SOCKS5 Proxy. Proxy resolves URL hostname.

       Without a scheme prefix, CURLOPT_PROXYTYPE(3) can be used to specify which kind of proxy the string identifies.

       When  you  tell  the  library  to use an HTTP proxy, libcurl will transparently convert operations to HTTP even if you specify an FTP URL etc. This may have an impact on what other features of the
       library you can use, such as CURLOPT_QUOTE(3) and similar FTP specifics that don't work unless you tunnel through the HTTP proxy. Such tunneling is activated with CURLOPT_HTTPPROXYTUNNEL(3).

       Setting the proxy string to "" (an empty string) will explicitly disable the use of a proxy, even if there is an environment variable set for it.

       A proxy host string can also include protocol scheme (http://) and embedded user + password.

       The application does not have to keep the string around after setting this option.

Environment variables
       libcurl respects the proxy environment variables named http_proxy, ftp_proxy, sftp_proxy etc. If set, libcurl will use the specified proxy for that URL scheme. So for a "FTP://" URL, the ftp_proxy
       is considered. all_proxy is used if no protocol specific proxy was set.

       If no_proxy (or NO_PROXY) is set, it is the exact equivalent of setting the CURLOPT_NOPROXY(3) option.

       The CURLOPT_PROXY(3) and CURLOPT_NOPROXY(3) options override environment variables.

DEFAULT
       Default is NULL, meaning no proxy is used.

       When you set a host name to use, do not assume that there's any particular single port number used widely for proxies. Specify it!

~~~
PROTOCOLS
~~~
       All except file://. Note that some protocols don't do very well over proxy.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/file.txt");
         curl_easy_setopt(curl, CURLOPT_PROXY, "http://proxy:80");
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Since 7.14.1 the proxy environment variable names can include the protocol scheme.

       Since 7.21.7 the proxy string supports the socks protocols as "schemes".

       Since 7.50.2, unsupported schemes in proxy strings cause libcurl to return error.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if proxies are supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXYPORT(3), CURLOPT_HTTPPROXYTUNNEL(3), CURLOPT_PROXYTYPE(3)



libcurl 7.37.0                                                                                  17 Jun 2014                                                                                CURLOPT_PROXY(3)
CURLOPT_PROXYAUTH(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_PROXYAUTH(3)



NAME
~~~
       CURLOPT_PROXYAUTH - HTTP proxy authentication methods

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXYAUTH, long bitmask);

~~~
DESCRIPTION
~~~
       Pass  a  long  as  parameter, which is set to a bitmask, to tell libcurl which HTTP authentication method(s) you want it to use for your proxy authentication.  If more than one bit is set, libcurl
       will first query the site to see what authentication methods it supports and then pick the best one you allow it to use. For some methods, this will induce an extra  network  round-trip.  Set  the
       actual name and password with the CURLOPT_PROXYUSERPWD(3) option.

       The bitmask can be constructed by or'ing together the bits fully listed and described in the CURLOPT_HTTPAUTH(3) man page.

DEFAULT
       CURLAUTH_BASIC

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* use this proxy */
         curl_easy_setopt(curl, CURLOPT_PROXY, "http://local.example.com:1080");
         /* allow whatever auth the proxy speaks */
         curl_easy_setopt(curl, CURLOPT_PROXYAUTH, CURLAUTH_ANY);
         /* set the proxy credentials */
         curl_easy_setopt(curl, CURLOPT_PROXYUSERPWD, "james:007");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.10.7

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_NOT_BUILT_IN if the bitmask specified no supported authentication methods.

~~~
SEE ALSO
       CURLOPT_PROXY(3), CURLOPT_PROXYTYPE(3), CURLOPT_PROXYUSERPWD(3), CURLOPT_PROXYPORT(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                            CURLOPT_PROXYAUTH(3)
CURLOPT_PROXY_CAINFO(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_PROXY_CAINFO(3)



NAME
~~~
       CURLOPT_PROXY_CAINFO - path to proxy Certificate Authority (CA) bundle

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_CAINFO, char *path);

~~~
DESCRIPTION
~~~
       This option is for connecting to an HTTPS proxy, not an HTTPS server.

       Pass a char * to a null-terminated string naming a file holding one or more certificates to verify the HTTPS proxy with.

       If CURLOPT_PROXY_SSL_VERIFYPEER(3) is zero and you avoid verifying the server's certificate, CURLOPT_PROXY_CAINFO(3) need not even indicate an accessible file.

       This option is by default set to the system path where libcurl's cacert bundle is assumed to be stored, as established at build time.

       If curl is built against the NSS SSL library, the NSS PEM PKCS#11 module (libnsspem.so) needs to be available for this option to work properly.

       (iOS and macOS only) If curl is built against Secure Transport, then this option is supported for backward compatibility with other SSL engines, but it should not be set. If the option is not set,
       then curl will use the certificates in the system and user Keychain to verify the peer, which is the preferred method of verifying the peer's certificate chain.

       The application does not have to keep the string around after setting this option.

DEFAULT
       Built-in system specific

~~~
PROTOCOLS
~~~
       Used with HTTPS proxy

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* using an HTTPS proxy */
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://localhost:443");
         curl_easy_setopt(curl, CURLOPT_PROXY_CAINFO, "/etc/certs/cabundle.pem");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

       For TLS backends that don't support certificate files, the CURLOPT_PROXY_CAINFO(3) option is ignored. Refer to https://curl.se/docs/ssl-compared.html

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY_CAINFO_BLOB(3),  CURLOPT_PROXY_CAPATH(3),  CURLOPT_PROXY_SSL_VERIFYPEER(3),  CURLOPT_PROXY_SSL_VERIFYHOST(3),  CURLOPT_CAINFO(3),  CURLOPT_CAINFO_BLOB(3),   CURLOPT_CAPATH(3),   CURLOPT_SSL_VERIFYPEER(3), CURLOPT_SSL_VERIFYHOST(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                                         CURLOPT_PROXY_CAINFO(3)
CURLOPT_PROXY_CAINFO_BLOB(3)                                                              curl_easy_setopt options                                                             CURLOPT_PROXY_CAINFO_BLOB(3)



NAME
~~~
       CURLOPT_PROXY_CAINFO_BLOB - proxy Certificate Authority (CA) bundle in PEM format

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_CAINFO_BLOB, struct curl_blob *stblob);

~~~
DESCRIPTION
~~~
       This option is for connecting to an HTTPS proxy, not an HTTPS server.

       Pass  a  pointer to a curl_blob structure, which contains information (pointer and size) about a memory block with binary data of PEM encoded content holding one or more certificates to verify the
       HTTPS proxy with.

       If CURLOPT_PROXY_SSL_VERIFYPEER(3) is zero and you avoid verifying the server's certificate, CURLOPT_PROXY_CAINFO_BLOB(3) is not needed.

       This option overrides CURLOPT_PROXY_CAINFO(3).

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       Used with HTTPS proxy

~~~
EXAMPLE
~~~
       char *strpem; /* strpem must point to a PEM string */
       CURL *curl = curl_easy_init();
       if(curl) {
         struct curl_blob blob;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* using an HTTPS proxy */
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://localhost:443");
         blob.data = strpem;
         blob.len = strlen(strpem);
         blob.flags = CURL_BLOB_COPY;
         curl_easy_setopt(curl, CURLOPT_PROXY_CAINFO_BLOB, &blob);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.77.0.

       This option is supported by the OpenSSL, Secure Transport and Schannel backends.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY_CAINFO(3), CURLOPT_PROXY_CAPATH(3), CURLOPT_PROXY_SSL_VERIFYPEER(3), CURLOPT_PROXY_SSL_VERIFYHOST(3), CURLOPT_CAINFO(3), CURLOPT_CAINFO_BLOB(3), CURLOPT_CAPATH(3),  CURLOPT_SSL_VERIFYPEER(3), CURLOPT_SSL_VERIFYHOST(3),



libcurl 7.77.0                                                                                 31 March 2021                                                                   CURLOPT_PROXY_CAINFO_BLOB(3)
CURLOPT_PROXY_CAPATH(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_PROXY_CAPATH(3)



NAME
~~~
       CURLOPT_PROXY_CAPATH - directory holding HTTPS proxy CA certificates

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_CAPATH, char *capath);

~~~
DESCRIPTION
~~~
       Pass a char * to a null-terminated string naming a directory holding multiple CA certificates to verify the HTTPS proxy with. If libcurl is built against OpenSSL, the certificate directory must be
       prepared using the openssl c_rehash utility. This makes sense only when CURLOPT_PROXY_SSL_VERIFYPEER(3) is enabled (which it is by default).

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       Everything used over an HTTPS proxy

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* using an HTTPS proxy */
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://localhost:443");
         curl_easy_setopt(curl, CURLOPT_PROXY_CAPATH, "/etc/cert-dir");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

       This option is supported by the OpenSSL, GnuTLS, and mbedTLS (since 7.56.0) backends. The NSS backend provides the option only for backward compatibility.

~~~
RETURN VALUE
~~~
       CURLE_OK if supported; or an error such as:

       CURLE_NOT_BUILT_IN - Not supported by the SSL backend

       CURLE_UNKNOWN_OPTION

       CURLE_OUT_OF_MEMORY

~~~
SEE ALSO
       CURLOPT_PROXY_CAINFO(3), CURLOPT_CAINFO(3), CURLOPT_PROXY_SSL_VERIFYHOST(3), CURLOPT_STDERR(3), CURLOPT_DEBUGFUNCTION(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                                         CURLOPT_PROXY_CAPATH(3)
CURLOPT_PROXY_CRLFILE(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_PROXY_CRLFILE(3)



NAME
~~~
       CURLOPT_PROXY_CRLFILE - HTTPS proxy Certificate Revocation List file

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_CRLFILE, char *file);

~~~
DESCRIPTION
~~~
       This option is for connecting to an HTTPS proxy, not an HTTPS server.

       Pass a char * to a null-terminated string naming a file with the concatenation of CRL (in PEM format) to use in the certificate validation that occurs during the SSL exchange.

       When  curl  is built to use NSS or GnuTLS, there is no way to influence the use of CRL passed to help in the verification process. When libcurl is built with OpenSSL support, X509_V_FLAG_CRL_CHECK
       and X509_V_FLAG_CRL_CHECK_ALL are both set, requiring CRL check against all the elements of the certificate chain if a CRL file is passed.

       This option makes sense only when used in combination with the CURLOPT_PROXY_SSL_VERIFYPEER(3) option.

       A specific error code (CURLE_SSL_CRL_BADFILE) is defined with the option. It is returned when the SSL exchange fails because the CRL file cannot be loaded.  A failure in  certificate  verification
       due to a revocation information found in the CRL does not trigger this specific error.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       Used with HTTPS proxy.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://localhost:80");
         curl_easy_setopt(curl, CURLOPT_PROXY_CRLFILE, "/etc/certs/crl.pem");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY_SSL_VERIFYPEER(3), CURLOPT_PROXY_SSL_VERIFYHOST(3), CURLOPT_SSL_VERIFYPEER(3), CURLOPT_SSL_VERIFYHOST(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                                        CURLOPT_PROXY_CRLFILE(3)
CURLOPT_PROXYHEADER(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_PROXYHEADER(3)



NAME
~~~
       CURLOPT_PROXYHEADER - custom HTTP headers to pass to proxy

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXYHEADER,
                                 struct curl_slist *headers);

~~~
DESCRIPTION
~~~
       Pass a pointer to a linked list of HTTP headers to pass in your HTTP request sent to a proxy. The rules for this list is identical to the CURLOPT_HTTPHEADER(3) option's.

       The headers set with this option is only ever used in requests sent to a proxy - when there's also a request sent to a host.

       The  first  line in a request (containing the method, usually a GET or POST) is NOT a header and cannot be replaced using this option. Only the lines following the request-line are headers. Adding
       this method line in this list of headers will only cause your request to send an invalid header.

       Pass a NULL to this to reset back to no custom headers.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();

       struct curl_slist *list;

       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_PROXY, "http://proxy.example.com:80");

         list = curl_slist_append(NULL, "Shoesize: 10");
         list = curl_slist_append(list, "Accept:");

         curl_easy_setopt(curl, CURLOPT_PROXYHEADER, list);

         curl_easy_perform(curl);

         curl_slist_free_all(list); /* free the list again */
       }

~~~
AVAILABILITY
~~~
       Added in 7.37.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_HEADEROPT(3), CURLOPT_HTTPHEADER(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                          CURLOPT_PROXYHEADER(3)
CURLOPT_PROXY_ISSUERCERT(3)                                                               curl_easy_setopt options                                                              CURLOPT_PROXY_ISSUERCERT(3)



NAME
~~~
       CURLOPT_PROXY_ISSUERCERT - proxy issuer SSL certificate filename

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_ISSUERCERT, char *file);

~~~
DESCRIPTION
~~~
       Pass  a  char  * to a null-terminated string naming a file holding a CA certificate in PEM format. If the option is set, an additional check against the peer certificate is performed to verify the
       issuer of the the HTTPS proxy is indeed the one associated with the certificate provided by the option.  This additional check is useful in multi-level PKI where one needs to enforce that the peer
       certificate is from a specific branch of the tree.

       This option makes sense only when used in combination with the CURLOPT_PROXY_SSL_VERIFYPEER(3) option. Otherwise, the result of the check is not considered as failure.

       A  specific  error  code (CURLE_SSL_ISSUER_ERROR) is defined with the option, which is returned if the setup of the SSL/TLS session has failed due to a mismatch with the issuer of peer certificate
       (CURLOPT_PROXY_SSL_VERIFYPEER(3) has to be set too for the check to fail).

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS-based protocols

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* using an HTTPS proxy */
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://localhost:443");
         curl_easy_setopt(curl, CURLOPT_PROXY_ISSUERCERT, "/etc/certs/cacert.pem");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in libcurl 7.71.0. This option is supported by the OpenSSL backends.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY_SSL_VERIFYPEER(3), CURLOPT_PROXY_SSL_VERIFYHOST(3), CURLOPT_SSL_VERIFYPEER(3), CURLOPT_SSL_VERIFYHOST(3),



libcurl 7.71.0                                                                                  24 Jun 2020                                                                     CURLOPT_PROXY_ISSUERCERT(3)
CURLOPT_PROXY_ISSUERCERT_BLOB(3)                                                          curl_easy_setopt options                                                         CURLOPT_PROXY_ISSUERCERT_BLOB(3)



NAME
~~~
       CURLOPT_ISSUERCERT_BLOB - proxy issuer SSL certificate from memory blob

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_ISSUERCERT_BLOB,
                                 struct curl_blob *blob);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to  a curl_blob struct, which contains information (pointer and size) about a memory block with binary data of a CA certificate in PEM format. If the option is set, an additional
       check against the peer certificate is performed to verify the issuer of the the HTTPS proxy is indeed the one associated with the certificate provided by the option. This additional check is  useful in multi-level PKI where one needs to enforce that the peer certificate is from a specific branch of the tree.

       This option should be used in combination with the CURLOPT_PROXY_SSL_VERIFYPEER(3) option. Otherwise, the result of the check is not considered as failure.

       A  specific  error  code (CURLE_SSL_ISSUER_ERROR) is defined with the option, which is returned if the setup of the SSL/TLS session has failed due to a mismatch with the issuer of peer certificate
       (CURLOPT_PROXY_SSL_VERIFYPEER(3) has to be set too for the check to fail).

       If the blob is initialized with the flags member of struct curl_blob set to CURL_BLOB_COPY, the application does not have to keep the buffer around after setting this.

       This option is an alternative to CURLOPT_PROXY_ISSUERCERT(3) which instead expects a file name as input.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS-based protocols

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         struct curl_blob blob;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* using an HTTPS proxy */
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://localhost:443");
         blob.data = certificateData;
         blob.len = filesize;
         blob.flags = CURL_BLOB_COPY;
         curl_easy_setopt(curl, CURLOPT_PROXY_ISSUERCERT_BLOB, &blob);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in libcurl 7.71.0. This option is supported by the OpenSSL backends.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY_SSL_VERIFYPEER(3), CURLOPT_PROXY_SSL_VERIFYHOST(3), CURLOPT_SSL_VERIFYPEER(3), CURLOPT_SSL_VERIFYHOST(3),



libcurl 7.71.0                                                                                  24 Jun 2020                                                                CURLOPT_PROXY_ISSUERCERT_BLOB(3)
CURLOPT_PROXY_KEYPASSWD(3)                                                                curl_easy_setopt options                                                               CURLOPT_PROXY_KEYPASSWD(3)



NAME
~~~
       CURLOPT_PROXY_KEYPASSWD - passphrase for the proxy private key

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_KEYPASSWD, char *pwd);

~~~
DESCRIPTION
~~~
       This option is for connecting to an HTTPS proxy, not an HTTPS server.

       Pass a pointer to a null-terminated string as parameter. It will be used as the password required to use the CURLOPT_PROXY_SSLKEY(3) private key.  You never needed a pass phrase to load a certificate but you need one to load your private key.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       Used with HTTPS proxy

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://proxy:443");
         curl_easy_setopt(curl, CURLOPT_PROXY_KEYPASSWD, "superman");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS enabled, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY_SSLKEY(3), CURLOPT_SSH_PRIVATE_KEYFILE(3), CURLOPT_SSLKEY(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                                      CURLOPT_PROXY_KEYPASSWD(3)
CURLOPT_PROXYPASSWORD(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_PROXYPASSWORD(3)



NAME
~~~
       CURLOPT_PROXYPASSWORD - password to use with proxy authentication

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXYPASSWORD, char *pwd);

~~~
DESCRIPTION
~~~
       Pass a char * as parameter, which should be pointing to the null-terminated password to use for authentication with the proxy.

       The CURLOPT_PROXYPASSWORD(3) option should be used in conjunction with the CURLOPT_PROXYUSERNAME(3) option.

       The application does not have to keep the string around after setting this option.

DEFAULT
       blank

~~~
PROTOCOLS
~~~
       Most

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
         curl_easy_setopt(curl, CURLOPT_PROXY, "http://localhost:8080");
         curl_easy_setopt(curl, CURLOPT_PROXYUSERNAME, "mrsmith");
         curl_easy_setopt(curl, CURLOPT_PROXYPASSWORD, "qwerty");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PASSWORD(3), CURLOPT_PROXYUSERNAME(3), CURLOPT_HTTPAUTH(3), CURLOPT_PROXYAUTH(3)



libcurl 7.37.0                                                                                  19 Jun 2014                                                                        CURLOPT_PROXYPASSWORD(3)
CURLOPT_PROXY_PINNEDPUBLICKEY(3)                                                          curl_easy_setopt options                                                         CURLOPT_PROXY_PINNEDPUBLICKEY(3)



NAME
~~~
       CURLOPT_PROXY_PINNEDPUBLICKEY - pinned public key for https proxy

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_PINNEDPUBLICKEY, char *pinnedpubkey);

~~~
DESCRIPTION
~~~
       Pass  a  pointer to a null-terminated string as parameter. The string can be the file name of your pinned public key. The file format expected is "PEM" or "DER".  The string can also be any number
       of base64 encoded sha256 hashes preceded by "sha256//" and separated by ";"

       When negotiating a TLS or SSL connection, the https proxy sends a certificate indicating its identity. A public key is extracted from this certificate and if it does not exactly match  the  public
       key provided to this option, curl will abort the connection before sending or receiving any data.

       On mismatch, CURLE_SSL_PINNEDPUBKEYNOTMATCH is returned.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://proxy:443");
         curl_easy_setopt(curl, CURLOPT_PROXY_PINNEDPUBLICKEY,
         "sha256//YhKJKSzoTt2b5FP18fvpHo7fJYqQCjAa3HWY3tvRMwE=;sha256//t62CeU2tQiqkexU74Gxa2eg7fRbEgoChTociMee9wno=");

         /* Perform the request */
         curl_easy_perform(curl);
       }

PUBLIC KEY EXTRACTION
       If you do not have the https proxy server's public key file you can extract it from the https proxy server's certificate.
       # retrieve the server's certificate if you don't already have it
       #
       # be sure to examine the certificate to see if it is what you expected
       #
       # Windows-specific:
       # - Use NUL instead of /dev/null.
       # - OpenSSL may wait for input instead of disconnecting. Hit enter.
       # - If you don't have sed, then just copy the certificate into a file:
       #   Lines from -----BEGIN CERTIFICATE----- to -----END CERTIFICATE-----.
       #
       openssl s_client -servername www.example.com -connect www.example.com:443 < /dev/null | sed -n "/-----BEGIN/,/-----END/p" > www.example.com.pem

       # extract public key in pem format from certificate
       openssl x509 -in www.example.com.pem -pubkey -noout > www.example.com.pubkey.pem

       # convert public key from pem to der
       openssl asn1parse -noout -inform pem -in www.example.com.pubkey.pem -out www.example.com.pubkey.der

       # sha256 hash and base64 encode der to string for use
       openssl dgst -sha256 -binary www.example.com.pubkey.der | openssl base64
       The public key in PEM format contains a header, base64 data and a footer:
       -----BEGIN PUBLIC KEY-----
       [BASE 64 DATA]
       -----END PUBLIC KEY-----

~~~
AVAILABILITY
~~~
       PEM/DER support:

         7.52.0: GSKit, GnuTLS, NSS, OpenSSL, mbedtls, wolfSSL

       sha256 support:

         7.52.0: GnuTLS, NSS, OpenSSL, mbedtls, wolfSSL

       Other SSL backends not supported.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS enabled, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY_SSL_VERIFYPEER(3), CURLOPT_PROXY_SSL_VERIFYHOST(3), CURLOPT_PROXY_CAINFO(3), CURLOPT_PROXY_CAPATH(3),



libcurl 7.52.0                                                                                  24 Nov 2016                                                                CURLOPT_PROXY_PINNEDPUBLICKEY(3)
CURLOPT_PROXYPORT(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_PROXYPORT(3)



NAME
~~~
       CURLOPT_PROXYPORT - port number the proxy listens on

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXYPORT, long port);

~~~
DESCRIPTION
~~~
       Pass a long with this option to set the proxy port to connect to unless it is specified in the proxy string CURLOPT_PROXY(3) or uses 443 for https proxies and 1080 for all others as default.

       While this accepts a 'long', the port number is 16 bit so it can't be larger than 65535.

DEFAULT
       0, not specified which makes it use the default port

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
         curl_easy_setopt(curl, CURLOPT_PROXY, "localhost");
         curl_easy_setopt(curl, CURLOPT_PROXYPORT, 8080L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_PROXY(3), CURLOPT_PROXYTYPE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                            CURLOPT_PROXYPORT(3)
CURLOPT_PROXY_SERVICE_NAME(3)                                                             curl_easy_setopt options                                                            CURLOPT_PROXY_SERVICE_NAME(3)



NAME
~~~
       CURLOPT_PROXY_SERVICE_NAME - proxy authentication service name

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_SERVICE_NAME, char *name);

~~~
DESCRIPTION
~~~
       Pass a char * as parameter to a string holding the name of the service. The default service name is "HTTP" for HTTP based proxies and "rcmd" for SOCKS5. This option allows you to change it.

       The application does not have to keep the string around after setting this option.

DEFAULT
       See above

~~~
PROTOCOLS
~~~
       All network protocols

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY_SERVICE_NAME, "custom");
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.43.0 for HTTP proxies, 7.49.0 for SOCKS5 proxies.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY(3), CURLOPT_PROXYTYPE(3),



libcurl 7.43.0                                                                                  17 Jun 2015                                                                   CURLOPT_PROXY_SERVICE_NAME(3)
CURLOPT_PROXY_SSLCERT(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_PROXY_SSLCERT(3)



NAME
~~~
       CURLOPT_PROXY_SSLCERT - HTTPS proxy client certificate

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_SSLCERT, char *cert);

~~~
DESCRIPTION
~~~
       This option is for connecting to an HTTPS proxy, not an HTTPS server.

       Pass  a  pointer  to a null-terminated string as parameter. The string should be the file name of your client certificate used to connect to the HTTPS proxy.  The default format is "P12" on Secure
       Transport and "PEM" on other engines, and can be changed with CURLOPT_PROXY_SSLCERTTYPE(3).

       With NSS or Secure Transport, this can also be the nickname of the certificate you wish to authenticate with as it is named in the security database. If you want to use a  file  from  the  current
       directory, please precede it with "./" prefix, in order to avoid confusion with a nickname.

       When using a client certificate, you most likely also need to provide a private key with CURLOPT_PROXY_SSLKEY(3).

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       Used with HTTPS proxy

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://proxy");
         curl_easy_setopt(curl, CURLOPT_PROXY_SSLCERT, "client.pem");
         curl_easy_setopt(curl, CURLOPT_PROXY_SSLKEY, "key.pem");
         curl_easy_setopt(curl, CURLOPT_PROXY_KEYPASSWD, "s3cret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS enabled, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY_SSLCERTTYPE(3), CURLOPT_PROXY_SSLKEY(3), CURLOPT_SSLCERT(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                                        CURLOPT_PROXY_SSLCERT(3)
CURLOPT_PROXY_SSLCERT_BLOB(3)                                                             curl_easy_setopt options                                                            CURLOPT_PROXY_SSLCERT_BLOB(3)



NAME
~~~
       CURLOPT_PROXY_SSLCERT_BLOB - SSL proxy client certificate from memory blob

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_SSLCERT_BLOB, struct curl_blob *blob);

~~~
DESCRIPTION
~~~
       Pass  a pointer to a curl_blob structure, which contains information (pointer and size) about a memory block with binary data of the certificate used to connect to the HTTPS proxy. The format must
       be "P12" on Secure Transport or Schannel. The format must be "P12" or "PEM" on OpenSSL.  The string "P12" or "PEM" must be specified with CURLOPT_PROXY_SSLCERTTYPE(3).

       If the blob is initialized with the flags member of struct curl_blob set to CURL_BLOB_COPY, the application does not have to keep the buffer around after setting this.

       This option is an alternative to CURLOPT_PROXY_SSLCERT(3) which instead expects a file name as input.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         struct curl_blob blob;
         blob.data = certificateData;
         blob.len = filesize;
         blob.flags = CURL_BLOB_COPY;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://proxy");
         curl_easy_setopt(curl, CURLOPT_PROXY_SSLKEY, "key.pem");
         curl_easy_setopt(curl, CURLOPT_PROXY_KEYPASSWD, "s3cret");
         curl_easy_setopt(curl, CURLOPT_PROXY_SSLCERT_BLOB, &blob);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in libcurl 7.71.0. This option is supported by the OpenSSL, Secure Transport and Schannel backends.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS enabled, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY_SSLCERTTYPE(3), CURLOPT_PROXY_SSLKEY(3), CURLOPT_PROXY_SSLCERT(3),



libcurl 7.71.0                                                                                  24 Jun 2020                                                                   CURLOPT_PROXY_SSLCERT_BLOB(3)
CURLOPT_PROXY_SSLCERTTYPE(3)                                                              curl_easy_setopt options                                                             CURLOPT_PROXY_SSLCERTTYPE(3)



NAME
~~~
       CURLOPT_PROXY_SSLCERTTYPE - type of the proxy client SSL certificate

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_SSLCERTTYPE, char *type);

~~~
DESCRIPTION
~~~
       Pass a pointer to a null-terminated string as parameter. The string should be the format of your client certificate used when connecting to an HTTPS proxy.

       Supported  formats  are  "PEM"  and  "DER",  except with Secure Transport. OpenSSL (versions 0.9.3 and later) and Secure Transport (on iOS 5 or later, or OS X 10.7 or later) also support "P12" for
       PKCS#12-encoded files.

       The application does not have to keep the string around after setting this option.

DEFAULT
       "PEM"

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://proxy");
         curl_easy_setopt(curl, CURLOPT_PROXY_SSLCERT, "client.pem");
         curl_easy_setopt(curl, CURLOPT_PROXY_SSLCERTTYPE, "PEM");
         curl_easy_setopt(curl, CURLOPT_PROXY_SSLKEY, "key.pem");
         curl_easy_setopt(curl, CURLOPT_PROXY_KEYPASSWD, "s3cret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

       If built TLS enabled.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY_SSLCERT(3), CURLOPT_PROXY_SSLKEY(3), CURLOPT_SSLCERTTYPE(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                                    CURLOPT_PROXY_SSLCERTTYPE(3)
CURLOPT_PROXY_SSL_CIPHER_LIST(3)                                                          curl_easy_setopt options                                                         CURLOPT_PROXY_SSL_CIPHER_LIST(3)



NAME
~~~
       CURLOPT_PROXY_SSL_CIPHER_LIST - ciphers to use for HTTPS proxy

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_SSL_CIPHER_LIST, char *list);

~~~
DESCRIPTION
~~~
       Pass  a  char  *,  pointing to a null-terminated string holding the list of ciphers to use for the connection to the HTTPS proxy. The list must be syntactically correct, it consists of one or more
       cipher strings separated by colons. Commas or spaces are also acceptable separators but colons are normally used, !, - and + can be used as operators.

       For OpenSSL and GnuTLS valid examples of cipher lists include 'RC4-SHA', 麓SHA1+DES麓, 'TLSv1' and 'DEFAULT'. The default list is normally set when you compile OpenSSL.

       You'll find more details about cipher lists on this URL:

        https://www.openssl.org/docs/apps/ciphers.html

       For NSS, valid examples of cipher lists include 'rsa_rc4_128_md5', 麓rsa_aes_128_sha麓, etc. With NSS you don't add/remove ciphers. If one uses this option then all known ciphers  are  disabled  and
       only those passed in are enabled.

       You'll find more details about the NSS cipher lists on this URL:

        http://git.fedorahosted.org/cgit/mod_nss.git/plain/docs/mod_nss.html#Directives

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL, use internal default

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://localhost");
         curl_easy_setopt(curl, CURLOPT_PROXY_SSL_CIPHER_LIST, "TLSv1");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

       If built TLS enabled.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY_TLS13_CIPHERS(3), CURLOPT_PROXY_SSLVERSION(3), CURLOPT_SSL_CIPHER_LIST(3), CURLOPT_TLS13_CIPHERS(3), CURLOPT_SSLVERSION(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                                CURLOPT_PROXY_SSL_CIPHER_LIST(3)
CURLOPT_PROXY_SSLKEY(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_PROXY_SSLKEY(3)



NAME
~~~
       CURLOPT_PROXY_SSLKEY - private keyfile for HTTPS proxy client cert

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_SSLKEY, char *keyfile);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to  a  null-terminated  string as parameter. The string should be the file name of your private key used for connecting to the HTTPS proxy. The default format is "PEM" and can be
       changed with CURLOPT_PROXY_SSLKEYTYPE(3).

       (iOS and Mac OS X only) This option is ignored if curl was built against Secure Transport. Secure Transport expects the private key to be already present in the keychain or PKCS#12 file containing
       the certificate.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://proxy");
         curl_easy_setopt(curl, CURLOPT_PROXY_SSLCERT, "client.pem");
         curl_easy_setopt(curl, CURLOPT_PROXY_SSLKEY, "key.pem");
         curl_easy_setopt(curl, CURLOPT_PROXY_KEYPASSWD, "s3cret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

       If built TLS enabled.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY_SSLKEYTYPE(3), CURLOPT_PROXY_SSLCERT(3), CURLOPT_SSLKEYTYPE(3), CURLOPT_SSLCERT(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                                         CURLOPT_PROXY_SSLKEY(3)
CURLOPT_PROXY_SSLKEY_BLOB(3)                                                              curl_easy_setopt options                                                             CURLOPT_PROXY_SSLKEY_BLOB(3)



NAME
~~~
       CURLOPT_PROXY_SSLKEY_BLOB - private key for proxy cert from memory blob

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_SSLKEY_BLOB,
                                 struct curl_blob *blob);

~~~
DESCRIPTION
~~~
       Pass  a pointer to a curl_blob structure that contains information (pointer and size) about the private key for connecting to the HTTPS proxy. Compatible with OpenSSL. The format (like "PEM") must
       be specified with CURLOPT_PROXY_SSLKEYTYPE(3).

       If the blob is initialized with the flags member of struct curl_blob set to CURL_BLOB_COPY, the application does not have to keep the buffer around after setting this.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         struct curl_blob blob;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://proxy");
         blob.data = certificateData;
         blob.len = filesize;
         blob.flags = CURL_BLOB_COPY;
         curl_easy_setopt(curl, CURLOPT_PROXY_SSLCERT_BLOB, &blob);
         curl_easy_setopt(curl, CURLOPT_PROXY_SSLCERTTYPE, "PEM");

         blob.data = privateKeyData;
         blob.len = privateKeySize;
         curl_easy_setopt(curl, CURLOPT_PROXY_SSLKEY_BLOB, &blob);
         curl_easy_setopt(curl, CURLOPT_PROXY_KEYPASSWD, "s3cret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in libcurl 7.71.0. This option is supported by the OpenSSL backends.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS enabled, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSLKEYTYPE(3), CURLOPT_SSLKEY(3),



libcurl 7.71.0                                                                                  24 Jun 2020                                                                    CURLOPT_PROXY_SSLKEY_BLOB(3)
CURLOPT_PROXY_SSLKEYTYPE(3)                                                               curl_easy_setopt options                                                              CURLOPT_PROXY_SSLKEYTYPE(3)



NAME
~~~
       CURLOPT_PROXY_SSLKEYTYPE - type of the proxy private key file

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_SSLKEYTYPE, char *type);

~~~
DESCRIPTION
~~~
       This option is for connecting to an HTTPS proxy, not an HTTPS server.

       Pass a pointer to a null-terminated string as parameter. The string should be the format of your private key. Supported formats are "PEM", "DER" and "ENG".

       The application does not have to keep the string around after setting this option.

~~~
PROTOCOLS
~~~
       Used with HTTPS proxy

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://proxy");
         curl_easy_setopt(curl, CURLOPT_PROXY_SSLCERT, "client.pem");
         curl_easy_setopt(curl, CURLOPT_PROXY_SSLKEY, "key.pem");
         curl_easy_setopt(curl, CURLOPT_PROXY_SSLKEYTYPE, "PEM");
         curl_easy_setopt(curl, CURLOPT_PROXY_KEYPASSWD, "s3cret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY_SSLKEY(3), CURLOPT_PROXY_SSLCERT(3), CURLOPT_SSLKEYTYPE(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                                     CURLOPT_PROXY_SSLKEYTYPE(3)
CURLOPT_PROXY_SSL_OPTIONS(3)                                                              curl_easy_setopt options                                                             CURLOPT_PROXY_SSL_OPTIONS(3)



NAME
~~~
       CURLOPT_PROXY_SSL_OPTIONS - HTTPS proxy SSL behavior options

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_SSL_OPTIONS, long bitmask);

~~~
DESCRIPTION
~~~
       Pass a long with a bitmask to tell libcurl about specific SSL behaviors. Available bits:

       CURLSSLOPT_ALLOW_BEAST
              Tells  libcurl to not attempt to use any workarounds for a security flaw in the SSL3 and TLS1.0 protocols.  If this option isn't used or this bit is set to 0, the SSL layer libcurl uses may
              use a work-around for this flaw although it might cause interoperability problems with some (older) SSL implementations. WARNING: avoiding this work-around lessens the security, and by set       ting this option to 1 you ask for exactly that.  This option is only supported for Secure Transport, NSS and OpenSSL.

       CURLSSLOPT_NO_REVOKE
              Tells  libcurl  to  disable certificate revocation checks for those SSL backends where such behavior is present. This option is only supported for Schannel (the native Windows SSL library),
              with an exception in the case of Windows' Untrusted Publishers block list which it seems can't be bypassed. (Added in 7.44.0)

       CURLSSLOPT_NO_PARTIALCHAIN
              Tells libcurl to not accept "partial" certificate chains, which it otherwise does by default. This option is only supported for OpenSSL and will fail the  certificate  verification  if  the
              chain ends with an intermediate certificate and not with a root cert. (Added in 7.68.0)

       CURLSSLOPT_REVOKE_BEST_EFFORT
              Tells  libcurl to ignore certificate revocation checks in case of missing or offline distribution points for those SSL backends where such behavior is present. This option is only supported
              for Schannel (the native Windows SSL library). If combined with CURLSSLOPT_NO_REVOKE, the latter takes precedence. (Added in 7.70.0)

       CURLSSLOPT_AUTO_CLIENT_CERT
              Tell libcurl to automatically locate and use a client certificate for authentication, when requested by the server. This option is only  supported  for  Schannel  (the  native  Windows  SSL
              library).  Prior  to  7.77.0  this  was the default behavior in libcurl with Schannel. Since the server can request any certificate that supports client authentication in the OS certificate
              store it could be a privacy violation and unexpected.  (Added in 7.77.0)

DEFAULT
       0

~~~
PROTOCOLS
~~~
       All TLS-based protocols

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://proxy");
         /* weaken TLS only for use with silly proxies */
         curl_easy_setopt(curl, CURLOPT_PROXY_SSL_OPTIONS, CURLSSLOPT_ALLOW_BEAST |
                          CURLSSLOPT_NO_REVOKE);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_PROXY_SSLVERSION(3), CURLOPT_PROXY_SSL_CIPHER_LIST(3), CURLOPT_SSLVERSION(3), CURLOPT_SSL_CIPHER_LIST(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                                    CURLOPT_PROXY_SSL_OPTIONS(3)
CURLOPT_PROXY_SSL_VERIFYHOST(3)                                                           curl_easy_setopt options                                                          CURLOPT_PROXY_SSL_VERIFYHOST(3)



NAME
~~~
       CURLOPT_PROXY_SSL_VERIFYHOST - verify the proxy certificate's name against host

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_SSL_VERIFYHOST, long verify);

~~~
DESCRIPTION
~~~
       Pass a long set to 2L as asking curl to verify in the HTTPS proxy's certificate name fields against the proxy name.

       This option determines whether libcurl verifies that the proxy cert contains the correct name for the name it is known as.

       When CURLOPT_PROXY_SSL_VERIFYHOST(3) is 2, the proxy certificate must indicate that the server is the proxy to which you meant to connect to, or the connection fails.

       Curl considers the proxy the intended one when the Common Name field or a Subject Alternate Name field in the certificate matches the host name in the proxy string which you told curl to use.

       If verify value is set to 1:

       In 7.28.0 and earlier: treated as a debug option of some sorts, not supported anymore due to frequently leading to programmer mistakes.

       From 7.28.1 to 7.65.3: setting it to 1 made curl_easy_setopt() return an error and leaving the flag untouched.

       From 7.66.0: treats 1 and 2 the same.

       When the verify value is 0L, the connection succeeds regardless of the names used in the certificate. Use that ability with caution!

       See  also  CURLOPT_PROXY_SSL_VERIFYPEER(3)  to  verify  the  digital  signature  of  the  proxy  certificate.   If  libcurl  is  built against NSS and CURLOPT_PROXY_SSL_VERIFYPEER(3) is zero, CURLOPT_PROXY_SSL_VERIFYHOST(3) is also set to zero and cannot be overridden.

DEFAULT
       2

~~~
PROTOCOLS
~~~
       All protocols when used over an HTTPS proxy.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Set the default value: strict name check please */
         curl_easy_setopt(curl, CURLOPT_PROXY_SSL_VERIFYHOST, 2L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0.

       If built TLS enabled.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS is supported, and CURLE_UNKNOWN_OPTION if not.

       If 1 is set as argument, CURLE_BAD_FUNCTION_ARGUMENT is returned.

~~~
SEE ALSO
       CURLOPT_PROXY_SSL_VERIFYPEER(3), CURLOPT_PROXY_CAINFO(3), CURLOPT_SSL_VERIFYPEER(3), CURLOPT_CAINFO(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                                 CURLOPT_PROXY_SSL_VERIFYHOST(3)
CURLOPT_PROXY_SSL_VERIFYPEER(3)                                                           curl_easy_setopt options                                                          CURLOPT_PROXY_SSL_VERIFYPEER(3)



NAME
~~~
       CURLOPT_PROXY_SSL_VERIFYPEER - verify the proxy's SSL certificate

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_SSL_VERIFYPEER, long verify);

~~~
DESCRIPTION
~~~
       Pass a long as parameter set to 1L to enable or 0L to disable.

       This option tells curl to verifies the authenticity of the HTTPS proxy's certificate. A value of 1 means curl verifies; 0 (zero) means it doesn't.

       This is the proxy version of CURLOPT_SSL_VERIFYPEER(3) that's used for ordinary HTTPS servers.

       When negotiating a TLS or SSL connection, the server sends a certificate indicating its identity. Curl verifies whether the certificate is authentic, i.e. that you can trust that the server is who
       the certificate says it is.  This trust is based on a chain of digital signatures, rooted in certification authority (CA) certificates you supply.  curl uses a default bundle  of  CA  certificates
       (the path for that is determined at build time) and you can specify alternate certificates with the CURLOPT_PROXY_CAINFO(3) option or the CURLOPT_PROXY_CAPATH(3) option.

       When CURLOPT_PROXY_SSL_VERIFYPEER(3) is enabled, and the verification fails to prove that the certificate is authentic, the connection fails.  When the option is zero, the peer certificate verification succeeds regardless.

       Authenticating the certificate is not enough to be sure about the server. You typically also want to ensure that the server is the server you mean to be talking to.  Use  CURLOPT_PROXY_SSL_VERIFYHOST(3) for that. The check that the host name in the certificate is valid for the host name you're connecting to is done independently of the CURLOPT_PROXY_SSL_VERIFYPEER(3) option.

       WARNING: disabling verification of the certificate allows bad guys to man-in-the-middle the communication without you knowing it. Disabling verification makes the communication insecure. Just having encryption on a transfer is not enough as you cannot be sure that you are communicating with the correct end-point.

DEFAULT
       1

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Set the default value: strict certificate check please */
         curl_easy_setopt(curl, CURLOPT_PROXY_SSL_VERIFYPEER, 1L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

       If built TLS enabled.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_PROXY_SSL_VERIFYHOST(3), CURLOPT_SSL_VERIFYPEER(3), CURLOPT_SSL_VERIFYHOST(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                                 CURLOPT_PROXY_SSL_VERIFYPEER(3)
CURLOPT_PROXY_SSLVERSION(3)                                                               curl_easy_setopt options                                                              CURLOPT_PROXY_SSLVERSION(3)



NAME
~~~
       CURLOPT_PROXY_SSLVERSION - preferred HTTPS proxy TLS version

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_SSLVERSION, long version);

~~~
DESCRIPTION
~~~
       Pass a long as parameter to control which version of SSL/TLS to attempt to use when connecting to an HTTPS proxy.

       Use one of the available defines for this purpose. The available options are:

              CURL_SSLVERSION_DEFAULT
                     The default action. This will attempt to figure out the remote SSL protocol version.

              CURL_SSLVERSION_TLSv1
                     TLSv1.x

              CURL_SSLVERSION_TLSv1_0
                     TLSv1.0

              CURL_SSLVERSION_TLSv1_1
                     TLSv1.1

              CURL_SSLVERSION_TLSv1_2
                     TLSv1.2

              CURL_SSLVERSION_TLSv1_3
                     TLSv1.3
       The  maximum TLS version can be set by using one of the CURL_SSLVERSION_MAX_ macros below. It is also possible to OR one of the CURL_SSLVERSION_ macros with one of the CURL_SSLVERSION_MAX_ macros.
       The MAX macros are not supported for WolfSSL.

              CURL_SSLVERSION_MAX_DEFAULT
                     The flag defines the maximum supported TLS version as TLSv1.2, or the default value from the SSL library.  (Added in 7.54.0)

              CURL_SSLVERSION_MAX_TLSv1_0
                     The flag defines maximum supported TLS version as TLSv1.0.  (Added in 7.54.0)

              CURL_SSLVERSION_MAX_TLSv1_1
                     The flag defines maximum supported TLS version as TLSv1.1.  (Added in 7.54.0)

              CURL_SSLVERSION_MAX_TLSv1_2
                     The flag defines maximum supported TLS version as TLSv1.2.  (Added in 7.54.0)

              CURL_SSLVERSION_MAX_TLSv1_3
                     The flag defines maximum supported TLS version as TLSv1.3.  (Added in 7.54.0)

       In versions of curl prior to 7.54 the CURL_SSLVERSION_TLS options were documented to allow only the specified TLS version, but behavior was inconsistent depending on the TLS library.


DEFAULT
       CURL_SSLVERSION_DEFAULT

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* ask libcurl to use TLS version 1.0 or later */
         curl_easy_setopt(curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_USE_SSL(3), CURLOPT_HTTP_VERSION(3), CURLOPT_IPRESOLVE(3) CURLOPT_SSLVERSION(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                                     CURLOPT_PROXY_SSLVERSION(3)
CURLOPT_PROXY_TLS13_CIPHERS(3)                                                            curl_easy_setopt options                                                           CURLOPT_PROXY_TLS13_CIPHERS(3)



NAME
~~~
       CURLOPT_PROXY_TLS13_CIPHERS - ciphers suites for proxy TLS 1.3

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_TLS13_CIPHERS, char *list);

~~~
DESCRIPTION
~~~
       Pass  a  char  *, pointing to a null-terminated string holding the list of cipher suites to use for the TLS 1.3 connection to a proxy. The list must be syntactically correct, it consists of one or
       more cipher suite strings separated by colons.

       You'll find more details about cipher lists on this URL:

        https://curl.se/docs/ssl-ciphers.html

       This option is currently used only when curl is built to use OpenSSL 1.1.1 or later. If you are using a different SSL backend you  can  try  setting  TLS  1.3  cipher  suites  by  using  the  CURLOPT_PROXY_SSL_CIPHER_LIST option.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL, use internal default

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY_TLS13_CIPHERS,
                          "TLS13-CHACHA20-POLY1305-SHA256");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.61.0.  Available when built with OpenSSL >= 1.1.1.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if supported, CURLE_NOT_BUILT_IN otherwise.

~~~
SEE ALSO
       CURLOPT_PROXY_SSL_CIPHER_LIST(3), CURLOPT_PROXY_SSLVERSION(3), CURLOPT_SSL_CIPHER_LIST(3), CURLOPT_TLS13_CIPHERS(3), CURLOPT_SSLVERSION(3),



libcurl 7.61.0                                                                                  25 May 2018                                                                  CURLOPT_PROXY_TLS13_CIPHERS(3)
CURLOPT_PROXY_TLSAUTH_PASSWORD(3)                                                         curl_easy_setopt options                                                        CURLOPT_PROXY_TLSAUTH_PASSWORD(3)



NAME
~~~
       CURLOPT_PROXY_TLSAUTH_PASSWORD - password to use for proxy TLS authentication

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_TLSAUTH_PASSWORD, char *pwd);

~~~
DESCRIPTION
~~~
       Pass  a  char * as parameter, which should point to the null-terminated password to use for the TLS authentication method specified with the CURLOPT_PROXY_TLSAUTH_TYPE(3) option. Requires that the
       CURLOPT_PROXY_TLSAUTH_USERNAME(3) option also be set.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://proxy");
         curl_easy_setopt(curl, CURLOPT_PROXY_TLSAUTH_TYPE, "SRP");
         curl_easy_setopt(curl, CURLOPT_PROXY_TLSAUTH_USERNAME, "user");
         curl_easy_setopt(curl, CURLOPT_PROXY_TLSAUTH_PASSWORD, "secret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY_TLSAUTH_TYPE(3), CURLOPT_PROXY_TLSAUTH_USERNAME(3), CURLOPT_TLSAUTH_TYPE(3), CURLOPT_TLSAUTH_USERNAME(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                               CURLOPT_PROXY_TLSAUTH_PASSWORD(3)
CURLOPT_PROXY_TLSAUTH_TYPE(3)                                                             curl_easy_setopt options                                                            CURLOPT_PROXY_TLSAUTH_TYPE(3)



NAME
~~~
       CURLOPT_PROXY_TLSAUTH_TYPE - HTTPS proxy TLS authentication methods

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_TLSAUTH_TYPE, char *type);

~~~
DESCRIPTION
~~~
       Pass a pointer to a null-terminated string as parameter. The string should be the method of the TLS authentication used for the HTTPS connection. Supported method is "SRP".


       SRP    TLS-SRP  authentication.  Secure Remote Password authentication for TLS is defined in RFC5054 and provides mutual authentication if both sides have a shared secret. To use TLS-SRP, you must
              also set the CURLOPT_PROXY_TLSAUTH_USERNAME(3) and CURLOPT_PROXY_TLSAUTH_PASSWORD(3) options.

              The application does not have to keep the string around after setting this option.

DEFAULT
       blank

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://proxy");
         curl_easy_setopt(curl, CURLOPT_PROXY_TLSAUTH_TYPE, "SRP");
         curl_easy_setopt(curl, CURLOPT_PROXY_TLSAUTH_USERNAME, "user");
         curl_easy_setopt(curl, CURLOPT_PROXY_TLSAUTH_PASSWORD, "secret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

       You need to build libcurl with GnuTLS or OpenSSL with TLS-SRP support for this to work.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_PROXY_TLSAUTH_USERNAME(3), CURLOPT_PROXY_TLSAUTH_PASSWORD(3), CURLOPT_TLSAUTH_USERNAME(3), CURLOPT_TLSAUTH_PASSWORD(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                                   CURLOPT_PROXY_TLSAUTH_TYPE(3)
CURLOPT_PROXY_TLSAUTH_USERNAME(3)                                                         curl_easy_setopt options                                                        CURLOPT_PROXY_TLSAUTH_USERNAME(3)



NAME
~~~
       CURLOPT_PROXY_TLSAUTH_USERNAME - user name to use for proxy TLS authentication

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_TLSAUTH_USERNAME, char *user);

~~~
DESCRIPTION
~~~
       Pass  a  char  *  as  parameter,  which  should  point to the null-terminated username to use for the HTTPS proxy TLS authentication method specified with the CURLOPT_PROXY_TLSAUTH_TYPE(3) option.
       Requires that the CURLOPT_PROXY_TLSAUTH_PASSWORD(3) option also be set.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "https://proxy");
         curl_easy_setopt(curl, CURLOPT_PROXY_TLSAUTH_TYPE, "SRP");
         curl_easy_setopt(curl, CURLOPT_PROXY_TLSAUTH_USERNAME, "user");
         curl_easy_setopt(curl, CURLOPT_PROXY_TLSAUTH_PASSWORD, "secret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.52.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY_TLSAUTH_TYPE(3), CURLOPT_PROXY_TLSAUTH_PASSWORD(3), CURLOPT_TLSAUTH_TYPE(3), CURLOPT_TLSAUTH_PASSWORD(3),



libcurl 7.52.0                                                                                  16 Nov 2016                                                               CURLOPT_PROXY_TLSAUTH_USERNAME(3)
CURLOPT_PROXY_TRANSFER_MODE(3)                                                            curl_easy_setopt options                                                           CURLOPT_PROXY_TRANSFER_MODE(3)



NAME
~~~
       CURLOPT_PROXY_TRANSFER_MODE - append FTP transfer mode to URL for proxy

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXY_TRANSFER_MODE, long enabled);

~~~
DESCRIPTION
~~~
       Pass a long. If the value is set to 1 (one), it tells libcurl to set the transfer mode (binary or ASCII) for FTP transfers done via an HTTP proxy, by appending ;type=a or ;type=i to the URL. Without this setting, or it being set to 0 (zero, the default), CURLOPT_TRANSFERTEXT(3) has no effect when doing FTP via a proxy. Beware that not all proxies support this feature.

DEFAULT
       0, disabled

~~~
PROTOCOLS
~~~
       FTP over proxy

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/old-server/file.txt");
         curl_easy_setopt(curl, CURLOPT_PROXY, "http://localhost:80");
         curl_easy_setopt(curl, CURLOPT_PROXY_TRANSFER_MODE, 1L);
         curl_easy_setopt(curl, CURLOPT_TRANSFERTEXT, 1L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.18.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if the enabled value is not supported.

~~~
SEE ALSO
       CURLOPT_PROXY(3), CURLOPT_HTTPPROXYTUNNEL(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                  CURLOPT_PROXY_TRANSFER_MODE(3)
CURLOPT_PROXYTYPE(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_PROXYTYPE(3)



NAME
~~~
       CURLOPT_PROXYTYPE - proxy protocol type

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXYTYPE, long type);

~~~
DESCRIPTION
~~~
       Pass one of the values below to set the type of the proxy.


              CURLPROXY_HTTP
                     HTTP Proxy. Default.

              CURLPROXY_HTTPS
                     HTTPS Proxy. (Added in 7.52.0 for OpenSSL, GnuTLS and NSS)

              CURLPROXY_HTTP_1_0
                     HTTP 1.0 Proxy. This is very similar to CURLPROXY_HTTP except it uses HTTP/1.0 for any CONNECT tunnelling. It does not change the HTTP version of the actual HTTP requests, controlled
                     by CURLOPT_HTTP_VERSION(3).

              CURLPROXY_SOCKS4
                     SOCKS4 Proxy.

              CURLPROXY_SOCKS4A
                     SOCKS4a Proxy. Proxy resolves URL hostname.

              CURLPROXY_SOCKS5
                     SOCKS5 Proxy.

              CURLPROXY_SOCKS5_HOSTNAME
                     SOCKS5 Proxy. Proxy resolves URL hostname.

       Often it is more convenient to specify the proxy type with the scheme part of the CURLOPT_PROXY(3) string.

DEFAULT
       CURLPROXY_HTTP

~~~
PROTOCOLS
~~~
       Most

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "local.example.com:1080");
         /* set the proxy type */
         curl_easy_setopt(curl, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_PROXY(3), CURLOPT_PROXYPORT(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                            CURLOPT_PROXYTYPE(3)
CURLOPT_PROXYUSERNAME(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_PROXYUSERNAME(3)



NAME
~~~
       CURLOPT_PROXYUSERNAME - user name to use for proxy authentication

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXYUSERNAME,
                                 char *username);

~~~
DESCRIPTION
~~~
       Pass a char * as parameter, which should be pointing to the null-terminated user name to use for the transfer.

       CURLOPT_PROXYUSERNAME(3) sets the user name to be used in protocol authentication with the proxy.

       To specify the proxy password use the CURLOPT_PROXYPASSWORD(3).

       The application does not have to keep the string around after setting this option.

DEFAULT
       blank

~~~
PROTOCOLS
~~~
       Most

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
         curl_easy_setopt(curl, CURLOPT_PROXY, "http://localhost:8080");
         curl_easy_setopt(curl, CURLOPT_PROXYUSERNAME, "mrsmith");
         curl_easy_setopt(curl, CURLOPT_PROXYPASSWORD, "qwerty");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXYPASSWORD(3), CURLOPT_USERNAME(3), CURLOPT_HTTPAUTH(3), CURLOPT_PROXYAUTH(3)



libcurl 7.37.0                                                                                  19 Jun 2014                                                                        CURLOPT_PROXYUSERNAME(3)
CURLOPT_PROXYUSERPWD(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_PROXYUSERPWD(3)



NAME
~~~
       CURLOPT_PROXYUSERPWD - user name and password to use for proxy authentication

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROXYUSERPWD, char *userpwd);

~~~
DESCRIPTION
~~~
       Pass  a  char  *  as parameter, which should be [user name]:[password] to use for the connection to the HTTP proxy. Both the name and the password will be URL decoded before use, so to include for
       example a colon in the user name you should encode it as %3A. (This is different to how CURLOPT_USERPWD(3) is used - beware.)

       Use CURLOPT_PROXYAUTH(3) to specify the authentication method.

       The application does not have to keep the string around after setting this option.

DEFAULT
       This is NULL by default.

~~~
PROTOCOLS
~~~
       Used with all protocols that can use a proxy

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");
         curl_easy_setopt(curl, CURLOPT_PROXY, "http://localhost:8080");
         curl_easy_setopt(curl, CURLOPT_PROXYUSERPWD, "clark%20kent:superman");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if proxies are supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY(3), CURLOPT_PROXYTYPE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                         CURLOPT_PROXYUSERPWD(3)
CURLOPT_PUT(3)                                                                            curl_easy_setopt options                                                                           CURLOPT_PUT(3)



NAME
~~~
       CURLOPT_PUT - make an HTTP PUT request

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PUT, long put);

~~~
DESCRIPTION
~~~
       A parameter set to 1 tells the library to use HTTP PUT to transfer data. The data should be set with CURLOPT_READDATA(3) and CURLOPT_INFILESIZE(3).

       This option is deprecated since version 7.12.1. Use CURLOPT_UPLOAD(3)!

DEFAULT
       0, disabled

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         /* we want to use our own read function */
         curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);

         /* enable PUT */
         curl_easy_setopt(curl, CURLOPT_PUT, 1L);

         /* specify target */
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/dir/to/newfile");

         /* now specify which pointer to pass to our callback */
         curl_easy_setopt(curl, CURLOPT_READDATA, hd_src);

         /* Set the size of the file to upload */
         curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, (curl_off_t)fsize);

         /* Now run off and do what you've been told! */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Deprecated since 7.12.1. Do not use.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_UPLOAD(3), CURLOPT_HTTPGET(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                                  CURLOPT_PUT(3)
CURLOPT_QUOTE(3)                                                                          curl_easy_setopt options                                                                         CURLOPT_QUOTE(3)



NAME
~~~
       CURLOPT_QUOTE - (S)FTP commands to run before transfer

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_QUOTE, struct curl_slist *cmds);

~~~
DESCRIPTION
~~~
       Pass  a  pointer to a linked list of FTP or SFTP commands to pass to the server prior to your request. This will be done before any other commands are issued (even before the CWD command for FTP).
       The linked list should be a fully valid list of 'struct curl_slist' structs properly filled in with text strings. Use curl_slist_append(3) to append strings (commands) to the list, and  clear  the
       entire list afterwards with curl_slist_free_all(3).

       Disable this operation again by setting a NULL to this option.

       When speaking to an FTP server, prefix the command with an asterisk (*) to make libcurl continue even if the command fails as by default libcurl will stop at first failure.

       The set of valid FTP commands depends on the server (see RFC959 for a list of mandatory commands).

       libcurl does not inspect, parse or "understand" the commands passed to the server using this option. If you change connection state, working directory or similar using quote commands, libcurl will
       not know about it.

       The valid SFTP commands are:

              atime date file
                     The atime command sets the last access time of the file named by the file operand. The <date expression> can be all sorts of date strings, see the curl_getdate(3) man page  for  date
                     expression details. (Added in 7.73.0)

              chgrp group file
                     The chgrp command sets the group ID of the file named by the file operand to the group ID specified by the group operand. The group operand is a decimal integer group ID.

              chmod mode file
                     The chmod command modifies the file mode bits of the specified file. The mode operand is an octal integer mode number.

              chown user file
                     The chown command sets the owner of the file named by the file operand to the user ID specified by the user operand. The user operand is a decimal integer user ID.

              ln source_file target_file
                     The ln and symlink commands create a symbolic link at the target_file location pointing to the source_file location.

              mkdir directory_name
                     The mkdir command creates the directory named by the directory_name operand.

              mtime date file
                     The  mtime command sets the last modification time of the file named by the file operand. The <date expression> can be all sorts of date strings, see the curl_getdate(3) man page for
                     date expression details. (Added in 7.73.0)

              pwd    The pwd command returns the absolute pathname of the current working directory.

              rename source target
                     The rename command renames the file or directory named by the source operand to the destination path named by the target operand.

              rm file
                     The rm command removes the file specified by the file operand.

              rmdir directory
                     The rmdir command removes the directory entry specified by the directory operand, provided it is empty.

              statvfs file
                     The statvfs command returns statistics on the file system in which specified file resides. (Added in 7.49.0)

              symlink source_file target_file
                     See ln.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       SFTP and FTP

~~~
EXAMPLE
~~~
       struct curl_slist *cmdlist = NULL;
       cmdlist = curl_slist_append(cmdlist, "RNFR source-name");
       cmdlist = curl_slist_append(cmdlist, "RNTO new-name");

       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/foo.bin");

         /* pass in the FTP commands to run before the transfer */
         curl_easy_setopt(curl, CURLOPT_QUOTE, cmdlist);

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       SFTP support added in 7.16.3. *-prefix for SFTP added in 7.24.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_POSTQUOTE(3), CURLOPT_PREQUOTE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                                CURLOPT_QUOTE(3)
CURLOPT_RANDOM_FILE(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_RANDOM_FILE(3)



NAME
~~~
       CURLOPT_RANDOM_FILE - file to read random data from

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RANDOM_FILE, char *path);

~~~
DESCRIPTION
~~~
       Pass a char * to a null-terminated file name. The file might be used to read from to seed the random engine for SSL and more.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL, not used

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_RANDOM_FILE, "junk.txt");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK on success or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_EGDSOCKET(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                          CURLOPT_RANDOM_FILE(3)
CURLOPT_RANGE(3)                                                                          curl_easy_setopt options                                                                         CURLOPT_RANGE(3)



NAME
~~~
       CURLOPT_RANGE - byte range to request

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RANGE, char *range);

~~~
DESCRIPTION
~~~
       Pass a char * as parameter, which should contain the specified range you want to retrieve. It should be in the format "X-Y", where either X or Y may be left out and X and Y are byte indexes.

       HTTP  transfers  also  support  several  intervals,  separated with commas as in "X-Y,N-M". Using this kind of multiple intervals will cause the HTTP server to send the response document in pieces
       (using standard MIME separation techniques). Unfortunately, the HTTP standard (RFC 7233 section 3.1) allows servers to ignore range requests so even when you set CURLOPT_RANGE(3)  for  a  request,
       you may end up getting the full response sent back.

       For RTSP, the formatting of a range should follow RFC2326 Section 12.29. For RTSP, byte ranges are not permitted. Instead, ranges should be given in npt, utc, or smpte formats.

       For  HTTP PUT uploads this option should not be used, since it may conflict with other options. If you need to upload arbitrary parts of a file (like for Amazon's web services) support is limited.
       We suggest set resume position using CURLOPT_RESUME_FROM(3), set end (resume+size) position using CURLOPT_INFILESIZE(3) and seek to the resume position before  initiating  the  transfer  for  each
       part. For more information refer to https://curl.se/mail/lib-2019-05/0012.html

       Pass a NULL to this option to disable the use of ranges.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP, FTP, FILE, RTSP and SFTP.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* get the first 200 bytes */
         curl_easy_setopt(curl, CURLOPT_RANGE, "0-199");

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       FILE since 7.18.0, RTSP since 7.20.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK on success or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_RESUME_FROM(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                                CURLOPT_RANGE(3)
CURLOPT_READDATA(3)                                                                       curl_easy_setopt options                                                                      CURLOPT_READDATA(3)



NAME
~~~
       CURLOPT_READDATA - custom pointer passed to the read callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_READDATA, void *pointer);

~~~
DESCRIPTION
~~~
       Data pointer to pass to the file read function. If you use the CURLOPT_READFUNCTION(3) option, this is the pointer you'll get as input in the 4th argument to the callback.

       If you don't specify a read callback but instead rely on the default internal read function, this data must be a valid readable FILE * (cast to 'void *').

       If you're using libcurl as a win32 DLL, you MUST use a CURLOPT_READFUNCTION(3) if you set this option or you will experience crashes.

DEFAULT
       By default, this is a FILE * to stdin.

~~~
PROTOCOLS
~~~
       This is used for all protocols when sending data.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       struct MyData this;
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* pass pointer that gets passed in to the
            CURLOPT_READFUNCTION callback */
         curl_easy_setopt(curl, CURLOPT_READDATA, &this);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       This option was once known by the older name CURLOPT_INFILE, the name CURLOPT_READDATA(3) was introduced in 7.9.7.

~~~
RETURN VALUE
~~~
       This will return CURLE_OK.

~~~
SEE ALSO
       CURLOPT_READFUNCTION(3), CURLOPT_WRITEDATA(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                             CURLOPT_READDATA(3)
CURLOPT_READFUNCTION(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_READFUNCTION(3)



NAME
~~~
       CURLOPT_READFUNCTION - read callback for data uploads

~~~
NAME
~~~
       #include <curl/curl.h>

       size_t read_callback(char *buffer, size_t size, size_t nitems, void *userdata);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_READFUNCTION, read_callback);


~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, as the prototype shows above.

       This  callback  function gets called by libcurl as soon as it needs to read data in order to send it to the peer - like if you ask it to upload or post data to the server. The data area pointed at
       by the pointer buffer should be filled up with at most size multiplied with nitems number of bytes by your function.

       Set the userdata argument with the CURLOPT_READDATA(3) option.

       Your function must return the actual number of bytes that it stored in the data area pointed at by the pointer buffer. Returning 0 will signal end-of-file to the library and cause it to  stop  the
       current transfer.

       If  you stop the current transfer by returning 0 "pre-maturely" (i.e before the server expected it, like when you've said you will upload N bytes and you upload less than N bytes), you may experience that the server "hangs" waiting for the rest of the data that won't come.

       The read callback may return CURL_READFUNC_ABORT to stop the current operation immediately, resulting in a CURLE_ABORTED_BY_CALLBACK error code from the transfer.

       The callback can return CURL_READFUNC_PAUSE to cause reading from this connection to pause. See curl_easy_pause(3) for further details.

       Bugs: when doing TFTP uploads, you must return the exact amount of data that the callback wants, or it will be considered the final packet by the server end and the transfer will end there.

       If you set this callback pointer to NULL, or don't set it at all, the default internal read function will be used. It is doing an fread() on the FILE * userdata set with CURLOPT_READDATA(3).

       You can set the total size of the data you are sending by using CURLOPT_INFILESIZE_LARGE(3) or CURLOPT_POSTFIELDSIZE_LARGE(3), depending on the type of transfer. For some transfer types it may  be
       required and it allows for better error checking.

DEFAULT
       The default internal read callback is fread().

~~~
PROTOCOLS
~~~
       This is used for all protocols when doing uploads.

~~~
EXAMPLE
~~~
       size_t read_callback(char *ptr, size_t size, size_t nmemb, void *userdata)
       {
         FILE *readhere = (FILE *)userdata;
         curl_off_t nread;

         /* copy as much data as possible into the 'ptr' buffer, but no more than
            'size' * 'nmemb' bytes! */
         size_t retcode = fread(ptr, size, nmemb, readhere);

         nread = (curl_off_t)retcode;

         fprintf(stderr, "*** We read %" CURL_FORMAT_CURL_OFF_T
                 " bytes from file\n", nread);
         return retcode;
       }

       void setup(char *uploadthis)
       {
         FILE *file = fopen(uploadthis, "rb");
         CURLcode result;

         /* set callback to use */
         curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);

         /* pass in suitable argument to callback */
         curl_easy_setopt(curl, CURLOPT_READDATA, (void *)file);

         result = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       CURL_READFUNC_PAUSE return code was added in 7.18.0 and CURL_READFUNC_ABORT was added in 7.12.1.

~~~
RETURN VALUE
~~~
       This will return CURLE_OK.

~~~
SEE ALSO
       CURLOPT_READDATA(3), CURLOPT_WRITEFUNCTION(3), CURLOPT_SEEKFUNCTION(3), CURLOPT_UPLOAD(3), CURLOPT_POST(3), CURLOPT_UPLOAD_BUFFERSIZE(3),



libcurl 7.71.0                                                                                  25 Jun 2020                                                                         CURLOPT_READFUNCTION(3)
CURLOPT_REDIR_PROTOCOLS(3)                                                                curl_easy_setopt options                                                               CURLOPT_REDIR_PROTOCOLS(3)



NAME
~~~
       CURLOPT_REDIR_PROTOCOLS - protocols allowed to redirect to

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_REDIR_PROTOCOLS, long bitmask);

~~~
DESCRIPTION
~~~
       Pass  a  long that holds a bitmask of CURLPROTO_* defines. If used, this bitmask limits what protocols libcurl may use in a transfer that it follows to in a redirect when CURLOPT_FOLLOWLOCATION(3)
       is enabled. This allows you to limit specific transfers to only be allowed to use a subset of protocols in redirections.

       Protocols denied by CURLOPT_PROTOCOLS(3) are not overridden by this option.

       By default libcurl will allow HTTP, HTTPS, FTP and FTPS on redirect (7.65.2).  Older versions of libcurl allowed all protocols on redirect except  several  disabled  for  security  reasons:  Since
       7.19.4 FILE and SCP are disabled, and since 7.40.0 SMB and SMBS are also disabled. CURLPROTO_ALL enables all protocols on redirect, including those disabled for security.

       These are the available protocol defines:
       CURLPROTO_DICT
       CURLPROTO_FILE
       CURLPROTO_FTP
       CURLPROTO_FTPS
       CURLPROTO_GOPHER
       CURLPROTO_HTTP
       CURLPROTO_HTTPS
       CURLPROTO_IMAP
       CURLPROTO_IMAPS
       CURLPROTO_LDAP
       CURLPROTO_LDAPS
       CURLPROTO_POP3
       CURLPROTO_POP3S
       CURLPROTO_RTMP
       CURLPROTO_RTMPE
       CURLPROTO_RTMPS
       CURLPROTO_RTMPT
       CURLPROTO_RTMPTE
       CURLPROTO_RTMPTS
       CURLPROTO_RTSP
       CURLPROTO_SCP
       CURLPROTO_SFTP
       CURLPROTO_SMB
       CURLPROTO_SMBS
       CURLPROTO_SMTP
       CURLPROTO_SMTPS
       CURLPROTO_TELNET
       CURLPROTO_TFTP

DEFAULT
       HTTP, HTTPS, FTP and FTPS (Since 7.65.2).

       Older versions defaulted to all protocols except FILE, SCP and since 7.40.0 SMB and SMBS.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         /* pass in the URL from an external source */
         curl_easy_setopt(curl, CURLOPT_URL, argv[1]);

         /* only allow redirects to HTTP and HTTPS URLs */
         curl_easy_setopt(curl, CURLOPT_REDIR_PROTOCOLS,
                          CURLPROTO_HTTP | CURLPROTO_HTTPS);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.4, before then it would follow all protocols.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_PROTOCOLS(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                      CURLOPT_REDIR_PROTOCOLS(3)
CURLOPT_REFERER(3)                                                                        curl_easy_setopt options                                                                       CURLOPT_REFERER(3)



NAME
~~~
       CURLOPT_REFERER - the HTTP referer header

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_REFERER, char *where);

~~~
DESCRIPTION
~~~
       Pass  a pointer to a null-terminated string as parameter. It will be used to set the Referer: header in the http request sent to the remote server. This can be used to fool servers or scripts. You
       can also set any custom header with CURLOPT_HTTPHEADER(3).

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* tell it where we found the link to this place */
         curl_easy_setopt(curl, CURLOPT_REFERER, "https://example.com/aboutme.html");

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       If built with HTTP support

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP support is enabled, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_USERAGENT(3), CURLOPT_HTTPHEADER(3), CURLINFO_REFERER(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                              CURLOPT_REFERER(3)
CURLOPT_REQUEST_TARGET(3)                                                                 curl_easy_setopt options                                                                CURLOPT_REQUEST_TARGET(3)



NAME
~~~
       CURLOPT_REQUEST_TARGET - alternative target for this request

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_REQUEST_TARGET, string);

~~~
DESCRIPTION
~~~
       Pass a char * to string which libcurl uses in the upcoming request instead of the path as extracted from the URL.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/*");
         curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "OPTIONS");

         /* issue an OPTIONS * request (no leading slash) */
         curl_easy_setopt(curl, CURLOPT_REQUEST_TARGET, "*");

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.55.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_CUSTOMREQUEST(3), CURLOPT_HTTPGET(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                       CURLOPT_REQUEST_TARGET(3)
CURLOPT_RESOLVE(3)                                                                        curl_easy_setopt options                                                                       CURLOPT_RESOLVE(3)



NAME
~~~
       CURLOPT_RESOLVE - provide custom host name to IP address resolves

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RESOLVE,
                                 struct curl_slist *hosts);

~~~
DESCRIPTION
~~~
       Pass  a pointer to a linked list of strings with host name resolve information to use for requests with this handle. The linked list should be a fully valid list of struct curl_slist structs properly filled in. Use curl_slist_append(3) to create the list and curl_slist_free_all(3) to clean up an entire list.

       Each single name resolve string should be written using the format [+]HOST:PORT:ADDRESS[,ADDRESS]... where HOST is the name libcurl will try to resolve, PORT is the  port  number  of  the  service
       where  libcurl  wants to connect to the HOST and ADDRESS is one or more numerical IP addresses. If you specify multiple ip addresses they need to be separated by comma. If libcurl is built to support IPv6, each of the ADDRESS entries can of course be either IPv4 or IPv6 style addressing.

       This option effectively pre-populates the DNS cache with entries for the host+port pair so redirects and everything that operations against the HOST+PORT will instead use your provided ADDRESS.

       The optional leading "+" signifies whether the new entry should time-out or not. Entries added with "HOST:..." will never time-out whereas entries added with "+HOST:..." will  time-out  just  like
       ordinary DNS cache entries.

       If the DNS cache already has an entry for the given host+port pair, then this entry will be removed and a new entry will be created. This is because the old entry may have have different addresses
       or a different time-out setting.

       An ADDRESS provided by this option will only be use if not restricted by the setting of CURLOPT_IPRESOLVE(3) to a different IP version.

       Remove names from the DNS cache again, to stop providing these fake resolves, by including a string in the linked list that uses the format "-HOST:PORT". The host name  must  be  prefixed  with  a
       dash, and the host name and port number must exactly match what was already added previously.

       Support for providing the ADDRESS within [brackets] was added in 7.57.0.

       Support for providing multiple IP addresses per entry was added in 7.59.0.

       Support for adding non-permanent entries by using the "+" prefix was added in 7.75.0.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl;
       struct curl_slist *host = NULL;
       host = curl_slist_append(NULL, "example.com:80:127.0.0.1");

       curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_RESOLVE, host);
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         curl_easy_perform(curl);

         /* always cleanup */
         curl_easy_cleanup(curl);
       }

       curl_slist_free_all(host);

~~~
AVAILABILITY
~~~
       Added in 7.21.3. Removal support added in 7.42.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_IPRESOLVE(3), CURLOPT_DNS_CACHE_TIMEOUT(3), CURLOPT_CONNECT_TO(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                              CURLOPT_RESOLVE(3)
CURLOPT_RESOLVER_START_DATA(3)                                                            curl_easy_setopt options                                                           CURLOPT_RESOLVER_START_DATA(3)



NAME
~~~
       CURLOPT_RESOLVER_START_DATA - custom pointer passed to the resolver start callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RESOLVER_START_DATA, void *pointer);

~~~
DESCRIPTION
~~~
       Pass a pointer that will be untouched by libcurl and passed as the third argument in the resolver start callback set with CURLOPT_RESOLVER_START_FUNCTION(3).

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       static int resolver_start_cb(void *resolver_state, void *reserved,
                                    void *userdata)
       {
         (void)reserved;
         printf("Received resolver_state=%p userdata=%p\n",
                resolver_state, userdata);
         return 0;
       }

       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_RESOLVER_START_FUNCTION, resolver_start_cb);
         curl_easy_setopt(curl, CURLOPT_RESOLVER_START_DATA, curl);
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.59.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_RESOLVER_START_FUNCTION(3)



libcurl 7.59.0                                                                                  14 Feb 2018                                                                  CURLOPT_RESOLVER_START_DATA(3)
CURLOPT_RESOLVER_START_FUNCTION(3)                                                        curl_easy_setopt options                                                       CURLOPT_RESOLVER_START_FUNCTION(3)



NAME
~~~
       CURLOPT_RESOLVER_START_FUNCTION - callback called before a new name resolve is started

~~~
NAME
~~~
       #include <curl/curl.h>

       int resolver_start_cb(void *resolver_state, void *reserved, void *userdata);

       CURLcode curl_easy_setopt(CURL *handle,
                                 CURLOPT_RESOLVER_START_FUNCTION,
                                 resolver_start_cb);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       This callback function gets called by libcurl every time before a new resolve request is started.

       resolver_state  points  to  a backend-specific resolver state. Currently only the ares resolver backend has a resolver state. It can be used to set up any desired option on the ares channel before
       it's used, for example setting up socket callback options.

       reserved is reserved.

       userdata is the user pointer set with the CURLOPT_RESOLVER_START_DATA(3) option.

       The callback must return 0 on success. Returning a non-zero value will cause the resolve to fail.

DEFAULT
       NULL (No callback)

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       static int resolver_start_cb(void *resolver_state, void *reserved,
                                    void *userdata)
       {
         (void)reserved;
         printf("Received resolver_state=%p userdata=%p\n",
                resolver_state, userdata);
         return 0;
       }

       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_RESOLVER_START_FUNCTION, resolver_start_cb);
         curl_easy_setopt(curl, CURLOPT_RESOLVER_START_DATA, curl);
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.59.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_RESOLVER_START_DATA(3)



libcurl 7.59.0                                                                                  14 Feb 2018                                                              CURLOPT_RESOLVER_START_FUNCTION(3)
CURLOPT_RESUME_FROM(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_RESUME_FROM(3)



NAME
~~~
       CURLOPT_RESUME_FROM - offset to resume transfer from

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RESUME_FROM, long from);

~~~
DESCRIPTION
~~~
       Pass  a  long as parameter. It contains the offset in number of bytes that you want the transfer to start from. Set this option to 0 to make the transfer start from the beginning (effectively disabling resume). For FTP, set this option to -1 to make the transfer start from the end of the target file (useful to continue an interrupted upload).

       When doing uploads with FTP, the resume position is where in the local/source file libcurl should try to resume the upload from and it will then append the source file to the remote target file.

       If you need to resume a transfer beyond the 2GB limit, use CURLOPT_RESUME_FROM_LARGE(3) instead.

DEFAULT
       0, not used

~~~
PROTOCOLS
~~~
       HTTP, FTP, SFTP, FILE

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com");

         /* resume upload at byte index 200 */
         curl_easy_setopt(curl, CURLOPT_RESUME_FROM, 200L);

         /* ask for upload */
         curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);

         /* set total data amount to expect */
         curl_easy_setopt(curl, CURLOPT_INFILESIZE, size_of_file);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_RESUME_FROM_LARGE(3), CURLOPT_RANGE(3), CURLOPT_INFILESIZE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                          CURLOPT_RESUME_FROM(3)
CURLOPT_RESUME_FROM_LARGE(3)                                                              curl_easy_setopt options                                                             CURLOPT_RESUME_FROM_LARGE(3)



NAME
~~~
       CURLOPT_RESUME_FROM_LARGE - offset to resume transfer from

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RESUME_FROM_LARGE,
                                 curl_off_t from);

~~~
DESCRIPTION
~~~
       Pass  a curl_off_t as parameter. It contains the offset in number of bytes that you want the transfer to start from. Set this option to 0 to make the transfer start from the beginning (effectively
       disabling resume). For FTP, set this option to -1 to make the transfer start from the end of the target file (useful to continue an interrupted upload).

       When doing uploads with FTP, the resume position is where in the local/source file libcurl should try to resume the upload from and it will then append the source file to the remote target file.

DEFAULT
       0, not used

~~~
PROTOCOLS
~~~
       HTTP, FTP, SFTP, FILE

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_off_t resume_position = GET_IT_SOMEHOW;
         curl_off_t file_size = GET_IT_SOMEHOW_AS_WELL;

         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com");

         /* resuming upload at this position, possibly beyond 2GB */
         curl_easy_setopt(curl, CURLOPT_RESUME_FROM_LARGE, resume_position);

         /* ask for upload */
         curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);

         /* set total data amount to expect */
         curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, file_size);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.11.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_RESUME_FROM(3), CURLOPT_RANGE(3), CURLOPT_INFILESIZE_LARGE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                    CURLOPT_RESUME_FROM_LARGE(3)
CURLOPT_RTSP_CLIENT_CSEQ(3)                                                               curl_easy_setopt options                                                              CURLOPT_RTSP_CLIENT_CSEQ(3)



NAME
~~~
       CURLOPT_RTSP_CLIENT_CSEQ - RTSP client CSEQ number

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RTSP_CLIENT_CSEQ, long cseq);

~~~
DESCRIPTION
~~~
       Pass  a  long  to  set the CSEQ number to issue for the next RTSP request. Useful if the application is resuming a previously broken connection. The CSEQ will increment from this new number henceforth.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       RTSP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "rtsp://example.com/");
         curl_easy_setopt(curl, CURLOPT_RTSP_CLIENT_CSEQ, 1234L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.20.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_RTSP_SERVER_CSEQ(3), CURLOPT_RTSP_REQUEST(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                     CURLOPT_RTSP_CLIENT_CSEQ(3)
CURLOPT_RTSP_REQUEST(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_RTSP_REQUEST(3)



NAME
~~~
       CURLOPT_RTSP_REQUEST - RTSP request

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RTSP_REQUEST, long request);

~~~
DESCRIPTION
~~~
       Tell libcurl what kind of RTSP request to make. Pass one of the following RTSP enum values as a long in the request argument. Unless noted otherwise, commands require the Session ID to be initialized.

       CURL_RTSPREQ_OPTIONS
              Used to retrieve the available methods of the server. The application is responsible for parsing and obeying the response. The session ID is not needed for this method.

       CURL_RTSPREQ_DESCRIBE
              Used to get the low level description of a stream. The application should note what formats it understands in the 'Accept:' header. Unless set manually, libcurl will automatically  fill  in
              'Accept: application/sdp'.  Time-condition headers will be added to Describe requests if the CURLOPT_TIMECONDITION(3) option is active. (The session ID is not needed for this method)

       CURL_RTSPREQ_ANNOUNCE
              When sent by a client, this method changes the description of the session. For example, if a client is using the server to record a meeting, the client can use Announce to inform the server
              of all the meta-information about the session.  ANNOUNCE acts like an HTTP PUT or POST just like CURL_RTSPREQ_SET_PARAMETER

       CURL_RTSPREQ_SETUP
              Setup is used to initialize the transport layer for the session. The application must set the desired Transport options for a session by using the CURLOPT_RTSP_TRANSPORT(3) option prior  to
              calling  setup.  If no session ID is currently set with CURLOPT_RTSP_SESSION_ID(3), libcurl will extract and use the session ID in the response to this request. The session ID is not needed
              for this method.

       CURL_RTSPREQ_PLAY
              Send a Play command to the server. Use the CURLOPT_RANGE(3) option to modify the playback time (e.g. 'npt=10-15').

       CURL_RTSPREQ_PAUSE
              Send a Pause command to the server. Use the CURLOPT_RANGE(3) option with a single value to indicate when the stream should be halted. (e.g. npt='25')

       CURL_RTSPREQ_TEARDOWN
              This command terminates an RTSP session. Simply closing a connection does not terminate the RTSP session since it is valid to control an RTSP session over different connections.

       CURL_RTSPREQ_GET_PARAMETER
              Retrieve a parameter from the server. By default, libcurl will automatically include a Content-Type: text/parameters header on all non-empty requests unless a custom one is set. GET_PARAME       TER  acts  just like an HTTP PUT or POST (see CURL_RTSPREQ_SET_PARAMETER).  Applications wishing to send a heartbeat message (e.g. in the presence of a server-specified timeout) should send
              use an empty GET_PARAMETER request.

       CURL_RTSPREQ_SET_PARAMETER
              Set a parameter on the server. By default, libcurl will automatically include a Content-Type: text/parameters header unless a custom one is set. The interaction with SET_PARAMETER  is  much
              like  an HTTP PUT or POST. An application may either use CURLOPT_UPLOAD(3) with CURLOPT_READDATA(3) like a HTTP PUT, or it may use CURLOPT_POSTFIELDS(3) like an HTTP POST. No chunked trans       fers are allowed, so the application must set the CURLOPT_INFILESIZE(3) in the former and CURLOPT_POSTFIELDSIZE(3) in the latter. Also, there is no use of multi-part POSTs within RTSP.

       CURL_RTSPREQ_RECORD
              Used to tell the server to record a session. Use the CURLOPT_RANGE(3) option to modify the record time.

       CURL_RTSPREQ_RECEIVE
              This is a special request because it does not send any data to the server. The application may call this function in order to receive interleaved RTP data. It will return  after  processing
              one read buffer of data in order to give the application a chance to run.

DEFAULT
~~~
PROTOCOLS
~~~
       RTSP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "rtsp://example.com/");
         /* ask for options! */
         curl_easy_setopt(curl, CURLOPT_RTSP_REQUEST, CURL_RTSPREQ_OPTIONS);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.20.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_RTSP_SESSION_ID(3), CURLOPT_RTSP_STREAM_URI(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                         CURLOPT_RTSP_REQUEST(3)
CURLOPT_RTSP_SERVER_CSEQ(3)                                                               curl_easy_setopt options                                                              CURLOPT_RTSP_SERVER_CSEQ(3)



NAME
~~~
       CURLOPT_RTSP_SERVER_CSEQ - RTSP server CSEQ number

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RTSP_SERVER_CSEQ, long cseq);

~~~
DESCRIPTION
~~~
       Pass a long to set the CSEQ number to expect for the next RTSP Server->Client request.  NOTE: this feature (listening for Server requests) is unimplemented.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       RTSP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "rtsp://example.com/");
         curl_easy_setopt(curl, CURLOPT_RTSP_SERVER_CSEQ, 1234L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.20.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_RTSP_CLIENT_CSEQ(3), CURLOPT_RTSP_STREAM_URI(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                     CURLOPT_RTSP_SERVER_CSEQ(3)
CURLOPT_RTSP_SESSION_ID(3)                                                                curl_easy_setopt options                                                               CURLOPT_RTSP_SESSION_ID(3)



NAME
~~~
       CURLOPT_RTSP_SESSION_ID - RTSP session ID

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RTSP_SESSION_ID, char *id);

~~~
DESCRIPTION
~~~
       Pass  a char * as a parameter to set the value of the current RTSP Session ID for the handle. Useful for resuming an in-progress session. Once this value is set to any non-NULL value, libcurl will
       return CURLE_RTSP_SESSION_ERROR if ID received from the server does not match. If unset (or set to NULL), libcurl will automatically set the ID the first time the server sets it in a response.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       RTSP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         char *prev_id; /* saved from before somehow */
         curl_easy_setopt(curl, CURLOPT_URL, "rtsp://example.com/");
         curl_easy_setopt(curl, CURLOPT_RTSP_SESSION_ID, prev_id);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.20.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_RTSP_REQUEST(3), CURLOPT_RTSP_STREAM_URI(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                      CURLOPT_RTSP_SESSION_ID(3)
CURLOPT_RTSP_STREAM_URI(3)                                                                curl_easy_setopt options                                                               CURLOPT_RTSP_STREAM_URI(3)



NAME
~~~
       CURLOPT_RTSP_STREAM_URI - RTSP stream URI

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RTSP_STREAM_URI, char *URI);

~~~
DESCRIPTION
~~~
       Set  the stream URI to operate on by passing a char * . For example, a single session may be controlling rtsp://foo/twister/audio and rtsp://foo/twister/video and the application can switch to the
       appropriate stream using this option. If unset, libcurl will default to operating on generic server options by passing '*' in the place of the RTSP Stream URI. This option is  distinct  from  CURLOPT_URL(3).  When  working with RTSP, the CURLOPT_RTSP_STREAM_URI(3) indicates what URL to send to the server in the request header while the CURLOPT_URL(3) indicates where to make the connection
       to.  (e.g. the CURLOPT_URL(3) for the above examples might be set to rtsp://foo/twister

       The application does not have to keep the string around after setting this option.

DEFAULT
       '*'

~~~
PROTOCOLS
~~~
       RTSP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         char *prev_id; /* saved from before somehow */
         curl_easy_setopt(curl, CURLOPT_URL, "rtsp://example.com/");
         curl_easy_setopt(curl, CURLOPT_RTSP_STREAM_URI,
                          "rtsp://foo.example.com/twister/video");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.20.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_RTSP_REQUEST(3), CURLOPT_RTSP_TRANSPORT(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                      CURLOPT_RTSP_STREAM_URI(3)
CURLOPT_RTSP_TRANSPORT(3)                                                                 curl_easy_setopt options                                                                CURLOPT_RTSP_TRANSPORT(3)



NAME
~~~
       CURLOPT_RTSP_TRANSPORT - RTSP Transport: header

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_RTSP_TRANSPORT,
                                 char *transport);

~~~
DESCRIPTION
~~~
       Pass  a  char * to tell libcurl what to pass for the Transport: header for this RTSP session. This is mainly a convenience method to avoid needing to set a custom Transport: header for every SETUP
       request. The application must set a Transport: header before issuing a SETUP request.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       RTSP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "rtsp://example.com/");
         curl_easy_setopt(curl, CURLOPT_RTSP_REQUEST, CURL_RTSPREQ_SETUP);
         curl_easy_setopt(curl, CURLOPT_RTSP_TRANSPORT,
                          "RTP/AVP;unicast;client_port=4588-4589");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.20.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_RTSP_REQUEST(3), CURLOPT_RTSP_SESSION_ID(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                       CURLOPT_RTSP_TRANSPORT(3)
CURLOPT_SASL_AUTHZID(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_SASL_AUTHZID(3)



NAME
~~~
       CURLOPT_SASL_AUTHZID - authorisation identity (identity to act as)

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SASL_AUTHZID, char *authzid);

~~~
DESCRIPTION
~~~
       Pass  a  char * as parameter, which should be pointing to the null-terminated authorisation identity (authzid) for the transfer. Only applicable to the PLAIN SASL authentication mechanism where it
       is optional.

       When not specified only the authentication identity (authcid) as specified by the username will be sent to the server, along with the password. The server will derive a authzid  from  the  authcid
       when not provided, which it will then uses internally.

       When the authzid is specified, the use of which is server dependent, it can be used to access another user's inbox, that the user has been granted access to, or a shared mailbox for example.

DEFAULT
       blank

~~~
PROTOCOLS
~~~
       IMAP, POP3 and SMTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "imap://example.com/");
         curl_easy_setopt(curl, CURLOPT_USERNAME, "Kurt");
         curl_easy_setopt(curl, CURLOPT_PASSWORD, "xipj3plmq");
         curl_easy_setopt(curl, CURLOPT_SASL_AUTHZID, "Ursel");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.66.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_USERNAME(3), CURLOPT_PASSWORD(3), .BRCURLOPT_USERPWD(3)



libcurl 7.66.0                                                                                  11 Sep 2019                                                                         CURLOPT_SASL_AUTHZID(3)
CURLOPT_SASL_IR(3)                                                                        curl_easy_setopt options                                                                       CURLOPT_SASL_IR(3)



NAME
~~~
       CURLOPT_SASL_IR - send initial response in first packet

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SASL_IR, long enable);

~~~
DESCRIPTION
~~~
       Pass  a  long.  If  the value is 1, curl will send the initial response to the server in the first authentication packet in order to reduce the number of ping pong requests. Only applicable to the
       following supporting SASL authentication mechanisms:

       * Login * Plain * GSSAPI * NTLM * OAuth 2.0

       Note: Whilst IMAP supports this option there is no need to explicitly set it, as libcurl can determine the feature itself when the server supports the SASL-IR CAPABILITY.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       IMAP, POP3 and SMTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "smtp://example.com/");
         curl_easy_setopt(curl, CURLOPT_SASL_IR, 1L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.31.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_MAIL_AUTH(3), CURLOPT_MAIL_FROM(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                              CURLOPT_SASL_IR(3)
CURLOPT_SEEKDATA(3)                                                                       curl_easy_setopt options                                                                      CURLOPT_SEEKDATA(3)



NAME
~~~
       CURLOPT_SEEKDATA - custom pointer passed to the seek callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SEEKDATA, void *pointer);

~~~
DESCRIPTION
~~~
       Data pointer to pass to the seek callback function. If you use the CURLOPT_SEEKFUNCTION(3) option, this is the pointer you'll get as input.

DEFAULT
       If you don't set this, NULL is passed to the callback.

~~~
PROTOCOLS
~~~
       HTTP, FTP, SFTP

~~~
EXAMPLE
~~~
       static int seek_cb(void *userp, curl_off_t offset, int origin)
       {
         struct data *d = (struct data *)userp;
         lseek(d->our_fd, offset, origin);
         return CURL_SEEKFUNC_OK;
       }

       {
         struct data seek_data;
         curl_easy_setopt(CURL *handle, CURLOPT_SEEKFUNCTION, seek_cb);
         curl_easy_setopt(CURL *handle, CURLOPT_SEEKDATA, &seek_data);
       }

~~~
AVAILABILITY
~~~
       Added in 7.18.0

~~~
RETURN VALUE
~~~
~~~
SEE ALSO
       CURLOPT_STDERR(3), CURLOPT_DEBUGFUNCTION(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                             CURLOPT_SEEKDATA(3)
CURLOPT_SEEKFUNCTION(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_SEEKFUNCTION(3)



NAME
~~~
       CURLOPT_SEEKFUNCTION - user callback for seeking in input stream

~~~
NAME
~~~
       #include <curl/curl.h>

       /* These are the return codes for the seek callbacks */
       #define CURL_SEEKFUNC_OK       0
       #define CURL_SEEKFUNC_FAIL     1 /* fail the entire transfer */
       #define CURL_SEEKFUNC_CANTSEEK 2 /* tell libcurl seeking can't be done, so
                                           libcurl might try other means instead */

       int seek_callback(void *userp, curl_off_t offset, int origin);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SEEKFUNCTION, seek_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       This  function  gets  called by libcurl to seek to a certain position in the input stream and can be used to fast forward a file in a resumed upload (instead of reading all uploaded bytes with the
       normal read function/callback). It is also called to rewind a stream when data has already been sent to the server and needs to be sent again. This may happen when doing an HTTP PUT or POST with a
       multi-pass  authentication  method,  or  when  an  existing  HTTP  connection is reused too late and the server closes the connection. The function shall work like fseek(3) or lseek(3) and it gets
       SEEK_SET, SEEK_CUR or SEEK_END as argument for origin, although libcurl currently only passes SEEK_SET.

       userp is the pointer you set with CURLOPT_SEEKDATA(3).

       The callback function must return CURL_SEEKFUNC_OK on success, CURL_SEEKFUNC_FAIL to cause the upload operation to fail or CURL_SEEKFUNC_CANTSEEK to indicate that while the seek failed, libcurl is
       free to work around the problem if possible. The latter can sometimes be done by instead reading from the input or similar.

       If you forward the input arguments directly to fseek(3) or lseek(3), note that the data type for offset is not the same as defined for curl_off_t on many systems!

DEFAULT
       By default, this is NULL and unused.

~~~
PROTOCOLS
~~~
       HTTP, FTP, SFTP

~~~
EXAMPLE
~~~
       static int seek_cb(void *userp, curl_off_t offset, int origin)
       {
         struct data *d = (struct data *)userp;
         lseek(our_fd, offset, origin);
         return CURL_SEEKFUNC_OK;
       }

       {
         struct data seek_data;
         curl_easy_setopt(CURL *handle, CURLOPT_SEEKFUNCTION, seek_cb);
         curl_easy_setopt(CURL *handle, CURLOPT_SEEKDATA, &seek_data);
       }

~~~
AVAILABILITY
~~~
       Added in 7.18.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SEEKDATA(3), CURLOPT_IOCTLFUNCTION(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                         CURLOPT_SEEKFUNCTION(3)
CURLOPT_SERVICE_NAME(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_SERVICE_NAME(3)



NAME
~~~
       CURLOPT_SERVICE_NAME - authentication service name

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SERVICE_NAME, char *name);

~~~
DESCRIPTION
~~~
       Pass  a  char * as parameter to a string holding the name of the service for DIGEST-MD5, SPNEGO and Kerberos 5 authentication mechanisms. The default service names are "ftp", "HTTP", "imap", "pop"
       and "smtp". This option allows you to change them.

       The application does not have to keep the string around after setting this option.

DEFAULT
       See above

~~~
PROTOCOLS
~~~
       HTTP, FTP, IMAP, POP and SMTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         CURLcode ret;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_SERVICE_NAME, "custom");
         ret = curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.43.0 for HTTP, 7.49.0 for FTP, IMAP, POP3 and SMTP.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY(3), CURLOPT_PROXYTYPE(3),



libcurl 7.43.0                                                                                  17 Jun 2015                                                                         CURLOPT_SERVICE_NAME(3)
CURLOPT_SHARE(3)                                                                          curl_easy_setopt options                                                                         CURLOPT_SHARE(3)



NAME
~~~
       CURLOPT_SHARE - share handle to use

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SHARE, CURLSH *share);

~~~
DESCRIPTION
~~~
       Pass  a  share handle as a parameter. The share handle must have been created by a previous call to curl_share_init(3). Setting this option, will make this curl handle use the data from the shared
       handle instead of keeping the data to itself. This enables several curl handles to share data. If the curl handles are used simultaneously in multiple threads, you MUST use the locking methods  in
       the share handle. See curl_share_setopt(3) for details.

       If  you  add  a  share  that  is  set to share cookies, your easy handle will use that cookie cache and get the cookie engine enabled. If you unshare an object that was using cookies (or change to
       another object that doesn't share cookies), the easy handle will get its cookie engine disabled.

       Data that the share object is not set to share will be dealt with the usual way, as if no share was used.

       Set this option to NULL again to stop using that share object.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       CURL *curl2 = curl_easy_init(); /* a second handle */
       if(curl) {
         CURLSH *shobject = curl_share_init();
         curl_share_setopt(shobject, CURLSHOPT_SHARE, CURL_LOCK_DATA_COOKIE);

         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_COOKIEFILE, "");
         curl_easy_setopt(curl, CURLOPT_SHARE, shobject);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);

         /* the second handle shares cookies from the first */
         curl_easy_setopt(curl2, CURLOPT_URL, "https://example.com/second");
         curl_easy_setopt(curl2, CURLOPT_COOKIEFILE, "");
         curl_easy_setopt(curl2, CURLOPT_SHARE, shobject);
         ret = curl_easy_perform(curl2);
         curl_easy_cleanup(curl2);

         curl_share_cleanup(shobject);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_COOKIE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                                CURLOPT_SHARE(3)
CURLOPT_SOCKOPTDATA(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_SOCKOPTDATA(3)



NAME
~~~
       CURLOPT_SOCKOPTDATA - custom pointer to pass to sockopt callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SOCKOPTDATA, void *pointer);

~~~
DESCRIPTION
~~~
       Pass a pointer that will be untouched by libcurl and passed as the first argument in the sockopt callback set with CURLOPT_SOCKOPTFUNCTION(3).

DEFAULT
       The default value of this parameter is NULL.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       static int sockopt_callback(void *clientp, curl_socket_t curlfd,
                                   curlsocktype purpose)
       {
         int val = *(int *)clientp;
         setsockopt(curldfd, SOL_SOCKET, SO_RCVBUF, (const char *)&val, sizeof(val));
         return CURL_SOCKOPT_OK;
       }

       curl = curl_easy_init();
       if(curl) {
         int recvbuffersize = 256 * 1024;

         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");

         /* call this function to set options for the socket */
         curl_easy_setopt(curl, CURLOPT_SOCKOPTFUNCTION, sockopt_callback);
         curl_easy_setopt(curl, CURLOPT_SOCKOPTDATA, &recvbuffersize);

         res = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.16.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SOCKOPTFUNCTION(3), CURLOPT_OPENSOCKETFUNCTION(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                          CURLOPT_SOCKOPTDATA(3)
CURLOPT_SOCKOPTFUNCTION(3)                                                                curl_easy_setopt options                                                               CURLOPT_SOCKOPTFUNCTION(3)



NAME
~~~
       CURLOPT_SOCKOPTFUNCTION - callback for setting socket options

~~~
NAME
~~~
       #include <curl/curl.h>

       typedef enum  {
         CURLSOCKTYPE_IPCXN,  /* socket created for a specific IP connection */
         CURLSOCKTYPE_ACCEPT, /* socket created by accept() call */
         CURLSOCKTYPE_LAST    /* never use */
       } curlsocktype;

       #define CURL_SOCKOPT_OK 0
       #define CURL_SOCKOPT_ERROR 1 /* causes libcurl to abort and return
                                       CURLE_ABORTED_BY_CALLBACK */
       #define CURL_SOCKOPT_ALREADY_CONNECTED 2

       int sockopt_callback(void *clientp,
                            curl_socket_t curlfd,
                            curlsocktype purpose);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SOCKOPTFUNCTION, sockopt_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       When  set,  this callback function gets called by libcurl when the socket has been created, but before the connect call to allow applications to change specific socket options. The callback's purpose argument identifies the exact purpose for this particular socket:

       CURLSOCKTYPE_IPCXN for actively created connections or since 7.28.0 CURLSOCKTYPE_ACCEPT for FTP when the connection was setup with PORT/EPSV (in earlier versions these sockets  weren't  passed  to
       this callback).

       Future versions of libcurl may support more purposes. libcurl passes the newly created socket descriptor to the callback in the curlfd parameter so additional setsockopt() calls can be done at the
       user's discretion.

       The clientp pointer contains whatever user-defined value set using the CURLOPT_SOCKOPTDATA(3) function.

       Return CURL_SOCKOPT_OK from the callback on success. Return CURL_SOCKOPT_ERROR from the callback function to signal an unrecoverable error to the library and it will close the  socket  and  return
       CURLE_COULDNT_CONNECT.   Alternatively,  the  callback function can return CURL_SOCKOPT_ALREADY_CONNECTED, to tell libcurl that the socket is already connected and then libcurl will not attempt to
       connect it. This allows an application to pass in an already connected socket with CURLOPT_OPENSOCKETFUNCTION(3) and then have this function make libcurl not attempt to connect (again).

DEFAULT
       By default, this callback is NULL and unused.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       /* make libcurl use the already established socket 'sockfd' */

       static curl_socket_t opensocket(void *clientp,
                                       curlsocktype purpose,
                                       struct curl_sockaddr *address)
       {
         curl_socket_t sockfd;
         sockfd = *(curl_socket_t *)clientp;
         /* the actual externally set socket is passed in via the OPENSOCKETDATA
            option */
         return sockfd;
       }

       static int sockopt_callback(void *clientp, curl_socket_t curlfd,
                                   curlsocktype purpose)
       {
         /* This return code was added in libcurl 7.21.5 */
         return CURL_SOCKOPT_ALREADY_CONNECTED;
       }

       curl = curl_easy_init();
       if(curl) {
         /* libcurl will internally think that you connect to the host
          * and port that you specify in the URL option. */
         curl_easy_setopt(curl, CURLOPT_URL, "http://99.99.99.99:9999");
         /* call this function to get a socket */
         curl_easy_setopt(curl, CURLOPT_OPENSOCKETFUNCTION, opensocket);
         curl_easy_setopt(curl, CURLOPT_OPENSOCKETDATA, &sockfd);

         /* call this function to set options for the socket */
         curl_easy_setopt(curl, CURLOPT_SOCKOPTFUNCTION, sockopt_callback);

         res = curl_easy_perform(curl);

         curl_easy_cleanup(curl);

~~~
AVAILABILITY
~~~
       Added in 7.16.0. The CURL_SOCKOPT_ALREADY_CONNECTED return code was added in 7.21.5.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SOCKOPTDATA(3), CURLOPT_OPENSOCKETFUNCTION(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                      CURLOPT_SOCKOPTFUNCTION(3)
CURLOPT_SOCKS5_AUTH(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_SOCKS5_AUTH(3)



NAME
~~~
       CURLOPT_SOCKS5_AUTH - methods for SOCKS5 proxy authentication

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SOCKS5_AUTH, long bitmask);

~~~
DESCRIPTION
~~~
       Pass a long as parameter, which is set to a bitmask, to tell libcurl which authentication method(s) are allowed for SOCKS5 proxy authentication.  The only supported flags are CURLAUTH_BASIC, which
       allows username/password authentication, CURLAUTH_GSSAPI, which allows GSS-API authentication, and CURLAUTH_NONE, which allows no authentication.  Set the actual user name and  password  with  the
       CURLOPT_PROXYUSERPWD(3) option.

DEFAULT
       CURLAUTH_BASIC|CURLAUTH_GSSAPI

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* request to use a SOCKS5 proxy */
         curl_easy_setopt(curl, CURLOPT_PROXY, "socks5://user:pass@myproxy.com");

         /* enable username/password authentication only */
         curl_easy_setopt(curl, CURLOPT_SOCKS5_AUTH, CURLAUTH_BASIC);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.55.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_NOT_BUILT_IN if the bitmask contains unsupported flags.

~~~
SEE ALSO
       CURLOPT_PROXY(3), CURLOPT_PROXYTYPE(3)



libcurl 7.55.0                                                                                 27 April 2017                                                                         CURLOPT_SOCKS5_AUTH(3)
CURLOPT_SOCKS5_GSSAPI_NEC(3)                                                              curl_easy_setopt options                                                             CURLOPT_SOCKS5_GSSAPI_NEC(3)



NAME
~~~
       CURLOPT_SOCKS5_GSSAPI_NEC - socks proxy gssapi negotiation protection

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SOCKS5_GSSAPI_NEC, long nec);

~~~
DESCRIPTION
~~~
       Pass a long set to 1 to enable or 0 to disable. As part of the gssapi negotiation a protection mode is negotiated. The RFC1961 says in section 4.3/4.4 it should be protected, but the NEC reference
       implementation does not.  If enabled, this option allows the unprotected exchange of the protection mode negotiation.

DEFAULT
       ?

~~~
PROTOCOLS
~~~
       Most

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "socks5://proxy");
         curl_easy_setopt(curl, CURLOPT_SOCKS5_GSSAPI_NEC, 1L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.4

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SOCKS5_GSSAPI_SERVICE(3), CURLOPT_PROXY(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                    CURLOPT_SOCKS5_GSSAPI_NEC(3)
CURLOPT_SOCKS5_GSSAPI_SERVICE(3)                                                          curl_easy_setopt options                                                         CURLOPT_SOCKS5_GSSAPI_SERVICE(3)



NAME
~~~
       CURLOPT_SOCKS5_GSSAPI_SERVICE - SOCKS5 proxy authentication service name

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SOCKS5_GSSAPI_SERVICE, char *name);

~~~
DESCRIPTION
~~~
       Deprecated since 7.49.0. Use CURLOPT_PROXY_SERVICE_NAME(3) instead.

       Pass a char * as parameter to a string holding the name of the service.  The default service name for a SOCKS5 server is "rcmd". This option allows you to change it.

       The application does not have to keep the string around after setting this option.

DEFAULT
       See above

~~~
PROTOCOLS
~~~
       All network protocols

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_PROXY, "socks5://proxy");
         curl_easy_setopt(curl, CURLOPT_SOCKS5_GSSAPI_SERVICE, "rcmd-special");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.4, deprecated in 7.49.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_PROXY(3), CURLOPT_PROXYTYPE(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                CURLOPT_SOCKS5_GSSAPI_SERVICE(3)
CURLOPT_SSH_AUTH_TYPES(3)                                                                 curl_easy_setopt options                                                                CURLOPT_SSH_AUTH_TYPES(3)



NAME
~~~
       CURLOPT_SSH_AUTH_TYPES - auth types for SFTP and SCP

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSH_AUTH_TYPES, long bitmask);

~~~
DESCRIPTION
~~~
       Pass a long set to a bitmask consisting of one or more of CURLSSH_AUTH_PUBLICKEY, CURLSSH_AUTH_PASSWORD, CURLSSH_AUTH_HOST, CURLSSH_AUTH_KEYBOARD and CURLSSH_AUTH_AGENT.

       Set  CURLSSH_AUTH_ANY  to let libcurl pick a suitable one. Currently CURLSSH_AUTH_HOST has no effect. If CURLSSH_AUTH_AGENT is used, libcurl attempts to connect to ssh-agent or pageant and let the
       agent attempt the authentication.

DEFAULT
       None

~~~
PROTOCOLS
~~~
       SFTP and SCP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "sftp://example.com/file");
         curl_easy_setopt(curl, CURLOPT_SSH_AUTH_TYPES,
                          CURLSSH_AUTH_PUBLICKEY | CURLSSH_AUTH_KEYBOARD);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       CURLSSH_AUTH_HOST was added in 7.16.1, CURLSSH_AUTH_AGENT was added in 7.28.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SSH_HOST_PUBLIC_KEY_MD5(3), CURLOPT_SSH_PUBLIC_KEYFILE(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                       CURLOPT_SSH_AUTH_TYPES(3)
CURLOPT_SSH_COMPRESSION(3)                                                                curl_easy_setopt options                                                               CURLOPT_SSH_COMPRESSION(3)



NAME
~~~
       CURLOPT_SSH_COMPRESSION - enables compression / decompression of SSH traffic

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSH_COMPRESSION, long enable);

~~~
DESCRIPTION
~~~
       Pass a long as parameter set to 1L to enable or 0L to disable.

       Enables built-in SSH compression.  This is a request, not an order; the server may or may not do it.

DEFAULT
       0, disabled

~~~
PROTOCOLS
~~~
       All SSH based protocols: SCP, SFTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "sftp://example.com");

         /* enable built-in compression */
         curl_easy_setopt(curl, CURLOPT_SSH_COMPRESSION, 1L);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.56.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_ACCEPT_ENCODING(3), CURLOPT_TRANSFER_ENCODING(3),



libcurl 7.56.0                                                                                  05 Aug 2017                                                                      CURLOPT_SSH_COMPRESSION(3)
CURLOPT_SSH_HOST_PUBLIC_KEY_MD5(3)                                                        curl_easy_setopt options                                                       CURLOPT_SSH_HOST_PUBLIC_KEY_MD5(3)



NAME
~~~
       CURLOPT_SSH_HOST_PUBLIC_KEY_MD5 - checksum of SSH server public key

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSH_HOST_PUBLIC_KEY_MD5,
                                 char *md5);

~~~
DESCRIPTION
~~~
       Pass  a  char  * pointing to a string containing 32 hexadecimal digits. The string should be the 128 bit MD5 checksum of the remote host's public key, and libcurl will reject the connection to the
       host unless the md5sums match.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       SCP and SFTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "sftp://example.com/file");
         curl_easy_setopt(curl, CURLOPT_SSH_HOST_PUBLIC_KEY_MD5,
                          "afe17cd62a0f3b61f1ab9cb22ba269a7");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.17.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSH_PUBLIC_KEYFILE(3), CURLOPT_SSH_AUTH_TYPES(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                              CURLOPT_SSH_HOST_PUBLIC_KEY_MD5(3)
CURLOPT_SSH_HOST_PUBLIC_KEY_SHA256(3)                                                     curl_easy_setopt options                                                    CURLOPT_SSH_HOST_PUBLIC_KEY_SHA256(3)



NAME
~~~
       CURLOPT_SSH_HOST_PUBLIC_KEY_SHA256 - SHA256 hash of SSH server public key

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSH_HOST_PUBLIC_KEY_SHA256,
                                 char *sha256);

~~~
DESCRIPTION
~~~
       Pass  a  char  * pointing to a string containing a Base64-encoded SHA256 hash of the remote host's public key.  The transfer will fail if the given hash doesn't match the hash the remote host provides.


DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       SCP and SFTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "sftp://example.com/file");
         curl_easy_setopt(curl, CURLOPT_SSH_HOST_PUBLIC_KEY_SHA256,
                          "NDVkMTQxMGQ1ODdmMjQ3MjczYjAyOTY5MmRkMjVmNDQ=");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.80.0 Requires the libssh2 back-end.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSH_PUBLIC_KEYFILE(3), CURLOPT_SSH_AUTH_TYPES(3),



libcurl 7.80.0                                                                                  27 Aug 2021                                                           CURLOPT_SSH_HOST_PUBLIC_KEY_SHA256(3)
CURLOPT_SSH_KEYDATA(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_SSH_KEYDATA(3)



NAME
~~~
       CURLOPT_SSH_KEYDATA - pointer to pass to the SSH key callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSH_KEYDATA, void *pointer);

~~~
DESCRIPTION
~~~
       Pass a void * as parameter. This pointer will be passed along verbatim to the callback set with CURLOPT_SSH_KEYFUNCTION(3).

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       SFTP and SCP

~~~
EXAMPLE
~~~
       static int keycb(CURL *easy,
                        const struct curl_khkey *knownkey,
                        const struct curl_khkey *foundkey,
                        enum curl_khmatch,
                        void *clientp)
       {
         /* 'clientp' points to the callback_data struct */
         /* investigate the situation and return the correct value */
         return CURLKHSTAT_FINE_ADD_TO_FILE;
       }
       {
         curl_easy_setopt(curl, CURLOPT_URL, "sftp://example.com/thisfile.txt");
         curl_easy_setopt(curl, CURLOPT_SSH_KEYFUNCTION, keycb);
         curl_easy_setopt(curl, CURLOPT_SSH_KEYDATA, &callback_data);
         curl_easy_setopt(curl, CURLOPT_SSH_KNOWNHOSTS, "/home/user/known_hosts");

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.6

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SSH_KEYDATA(3), CURLOPT_SSH_KNOWNHOSTS(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                          CURLOPT_SSH_KEYDATA(3)
CURLOPT_SSH_KEYFUNCTION(3)                                                                curl_easy_setopt options                                                               CURLOPT_SSH_KEYFUNCTION(3)



NAME
~~~
       CURLOPT_SSH_KEYFUNCTION - callback for known host matching logic

~~~
NAME
~~~
       #include <curl/curl.h>

       enum curl_khstat {
         CURLKHSTAT_FINE_ADD_TO_FILE,
         CURLKHSTAT_FINE,
         CURLKHSTAT_REJECT, /* reject the connection, return an error */
         CURLKHSTAT_DEFER,  /* do not accept it, but we can't answer right
                               now so this causes a CURLE_DEFER error but
                               otherwise the connection will be left intact
                               etc */
         CURLKHSTAT_FINE_REPLACE
       };

       enum curl_khmatch {
         CURLKHMATCH_OK,       /* match */
         CURLKHMATCH_MISMATCH, /* host found, key mismatch! */
         CURLKHMATCH_MISSING,  /* no matching host/key found */
       };

       struct curl_khkey {
         const char *key; /* points to a null-terminated string encoded with
                             base64 if len is zero, otherwise to the "raw"
                             data */
         size_t len;
         enum curl_khtype keytype;
       };

       int ssh_keycallback(CURL *easy,
                           const struct curl_khkey *knownkey,
                           const struct curl_khkey *foundkey,
                           enum curl_khmatch,
                           void *clientp);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSH_KEYFUNCTION,
                                 ssh_keycallback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       It  gets  called when the known_host matching has been done, to allow the application to act and decide for libcurl how to proceed. The callback will only be called if CURLOPT_SSH_KNOWNHOSTS(3) is
       also set.

       This callback function gets passed the CURL handle, the key from the known_hosts file knownkey, the key from the remote site foundkey, info from libcurl on the matching status and a custom pointer
       (set with CURLOPT_SSH_KEYDATA(3)). It MUST return one of the following return codes to tell libcurl how to act:

       CURLKHSTAT_FINE_REPLACE
              The  new  host+key  is  accepted and libcurl will replace the old host+key into the known_hosts file before continuing with the connection.  This will also add the new host+key combo to the
              known_host pool kept in memory if it wasn't already present there. The adding of data to the file is done by completely replacing the file with a new copy, so the permissions  of  the  file
              must allow this. (Added in 7.73.0)

       CURLKHSTAT_FINE_ADD_TO_FILE
              The  host+key is accepted and libcurl will append it to the known_hosts file before continuing with the connection. This will also add the host+key combo to the known_host pool kept in mem       ory if it wasn't already present there. The adding of data to the file is done by completely replacing the file with a new copy, so the permissions of the file must allow this.

       CURLKHSTAT_FINE
              The host+key is accepted libcurl will continue with the connection. This will also add the host+key combo to the known_host pool kept in memory if it wasn't already present there.

       CURLKHSTAT_REJECT
              The host+key is rejected. libcurl will deny the connection to continue and it will be closed.

       CURLKHSTAT_DEFER
              The host+key is rejected, but the SSH connection is asked to be kept alive.  This feature could be used when the app wants to somehow return back and act on the host+key situation and  then
              retry without needing the overhead of setting it up from scratch again.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       SFTP and SCP

~~~
EXAMPLE
~~~
       static int keycb(CURL *easy,
                        const struct curl_khkey *knownkey,
                        const struct curl_khkey *foundkey,
                        enum curl_khmatch,
                        void *clientp)
       {
         /* 'clientp' points to the callback_data struct */
         /* investigate the situation and return the correct value */
         return CURLKHSTAT_FINE_ADD_TO_FILE;
       }
       {
         curl_easy_setopt(curl, CURLOPT_URL, "sftp://example.com/thisfile.txt");
         curl_easy_setopt(curl, CURLOPT_SSH_KEYFUNCTION, keycb);
         curl_easy_setopt(curl, CURLOPT_SSH_KEYDATA, &callback_data);
         curl_easy_setopt(curl, CURLOPT_SSH_KNOWNHOSTS, "/home/user/known_hosts");

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.6

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SSH_KEYDATA(3), CURLOPT_SSH_KNOWNHOSTS(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                      CURLOPT_SSH_KEYFUNCTION(3)
CURLOPT_SSH_KNOWNHOSTS(3)                                                                 curl_easy_setopt options                                                                CURLOPT_SSH_KNOWNHOSTS(3)



NAME
~~~
       CURLOPT_SSH_KNOWNHOSTS - file name holding the SSH known hosts

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSH_KNOWNHOSTS, char *fname);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to a null-terminated string holding the file name of the known_host file to use.  The known_hosts file should use the OpenSSH file format as supported by libssh2. If this file is
       specified, libcurl will only accept connections with hosts that are known and present in that file, with a matching public key. Use CURLOPT_SSH_KEYFUNCTION(3) to alter the default behavior on host
       and key (mis)matching.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       SFTP and SCP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "sftp://example.com/file");
         curl_easy_setopt(curl, CURLOPT_SSH_KNOWNHOSTS,
                          "/home/clarkkent/.ssh/known_hosts");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.6

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSH_AUTH_TYPES(3), CURLOPT_SSH_HOST_PUBLIC_KEY_MD5(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                       CURLOPT_SSH_KNOWNHOSTS(3)
CURLOPT_SSH_PRIVATE_KEYFILE(3)                                                            curl_easy_setopt options                                                           CURLOPT_SSH_PRIVATE_KEYFILE(3)



NAME
~~~
       CURLOPT_SSH_PRIVATE_KEYFILE - private key file for SSH auth

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSH_PRIVATE_KEYFILE,
                                 char *filename);

~~~
DESCRIPTION
~~~
       Pass a char * pointing to a filename for your private key. If not used, libcurl defaults to $HOME/.ssh/id_dsa if the HOME environment variable is set, and just "id_dsa" in the current directory if
       HOME is not set.

       If the file is password-protected, set the password with CURLOPT_KEYPASSWD(3).

       The application does not have to keep the string around after setting this option.

DEFAULT
       As explained above

~~~
PROTOCOLS
~~~
       SFTP and SCP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "sftp://example.com/file");
         curl_easy_setopt(curl, CURLOPT_SSH_PRIVATE_KEYFILE,
                          "/home/clarkkent/.ssh/id_rsa");
         curl_easy_setopt(curl, CURLOPT_KEYPASSWD, "password");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.16.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSH_PUBLIC_KEYFILE(3), CURLOPT_SSH_AUTH_TYPES(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                  CURLOPT_SSH_PRIVATE_KEYFILE(3)
CURLOPT_SSH_PUBLIC_KEYFILE(3)                                                             curl_easy_setopt options                                                            CURLOPT_SSH_PUBLIC_KEYFILE(3)



NAME
~~~
       CURLOPT_SSH_PUBLIC_KEYFILE - public key file for SSH auth

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSH_PUBLIC_KEYFILE,
                                 char *filename);

~~~
DESCRIPTION
~~~
       Pass  a  char  *  pointing  to  a filename for your public key. If not used, libcurl defaults to $HOME/.ssh/id_dsa.pub if the HOME environment variable is set, and just "id_dsa.pub" in the current
       directory if HOME is not set.

       If NULL (or an empty string) is passed, libcurl will pass no public key to libssh2, which then tries to compute it from the private key.  This is known to work with libssh2 1.4.0+  linked  against
       OpenSSL.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       SFTP and SCP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "sftp://example.com/file");
         curl_easy_setopt(curl, CURLOPT_SSH_PUBLIC_KEYFILE,
                          "/home/clarkkent/.ssh/id_rsa.pub");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       The "" trick was added in 7.26.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSH_PRIVATE_KEYFILE(3), CURLOPT_SSH_AUTH_TYPES(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                   CURLOPT_SSH_PUBLIC_KEYFILE(3)
CURLOPT_SSLCERT(3)                                                                        curl_easy_setopt options                                                                       CURLOPT_SSLCERT(3)



NAME
~~~
       CURLOPT_SSLCERT - SSL client certificate

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSLCERT, char *cert);

~~~
DESCRIPTION
~~~
       Pass  a pointer to a null-terminated string as parameter. The string should be the file name of your client certificate. The default format is "P12" on Secure Transport and "PEM" on other engines,
       and can be changed with CURLOPT_SSLCERTTYPE(3).

       With NSS or Secure Transport, this can also be the nickname of the certificate you wish to authenticate with as it is named in the security database. If you want to use a  file  from  the  current
       directory, please precede it with "./" prefix, in order to avoid confusion with a nickname.

       (Schannel only) Client certificates can be specified by a path expression to a certificate store. (You can import PFX to a store first). You can use "<store location>\<store name>\<thumbprint>" to
       refer to a certificate in the system certificates store, for example, "CurrentUser\MY\934a7ac6f8a5d579285a74fa61e19f23ddfe8d7a". Thumbprint is usually a SHA-1 hex string which you can see in  certificate  details.  Following store locations are supported: CurrentUser, LocalMachine, CurrentService, Services, CurrentUserGroupPolicy, LocalMachineGroupPolicy, LocalMachineEnterprise.  Schannel
       also support P12 certificate file, with the string "P12" specified with CURLOPT_SSLCERTTYPE(3).

       When using a client certificate, you most likely also need to provide a private key with CURLOPT_SSLKEY(3).

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_SSLCERT, "client.pem");
         curl_easy_setopt(curl, CURLOPT_SSLKEY, "key.pem");
         curl_easy_setopt(curl, CURLOPT_KEYPASSWD, "s3cret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       If built TLS enabled.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS enabled, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSLCERTTYPE(3), CURLOPT_SSLKEY(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                              CURLOPT_SSLCERT(3)
CURLOPT_SSLCERT_BLOB(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_SSLCERT_BLOB(3)



NAME
~~~
       CURLOPT_SSLCERT_BLOB - SSL client certificate from memory blob

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSLCERT_BLOB, struct curl_blob *stblob);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to  a  curl_blob structure, which contains (pointer and size) a client certificate. The format must be "P12" on Secure Transport or Schannel. The format must be "P12" or "PEM" on
       OpenSSL. The string "P12" or "PEM" must be specified with CURLOPT_SSLCERTTYPE(3).

       If the blob is initialized with the flags member of struct curl_blob set to CURL_BLOB_COPY, the application does not have to keep the buffer around after setting this.

       This option is an alternative to CURLOPT_SSLCERT(3) which instead expects a file name as input.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         struct curl_blob stblob;
         stblob.data = certificateData;
         stblob.len = filesize;
         stblob.flags = CURL_BLOB_COPY;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_SSLCERT_BLOB, &stblob);
         curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "P12");
         curl_easy_setopt(curl, CURLOPT_KEYPASSWD, "s3cret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in libcurl 7.71.0. This option is supported by the OpenSSL, Secure Transport and Schannel backends.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS enabled, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSLCERTTYPE(3), CURLOPT_SSLKEY(3),



libcurl 7.71.0                                                                                  24 Jun 2020                                                                         CURLOPT_SSLCERT_BLOB(3)
CURLOPT_SSLCERTTYPE(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_SSLCERTTYPE(3)



NAME
~~~
       CURLOPT_SSLCERTTYPE - type of client SSL certificate

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSLCERTTYPE, char *type);

~~~
DESCRIPTION
~~~
       Pass a pointer to a null-terminated string as parameter. The string should be the format of your certificate. Supported formats are "PEM" and "DER", except with Secure Transport. OpenSSL (versions
       0.9.3 and later) and Secure Transport (on iOS 5 or later, or OS X 10.7 or later) also support "P12" for PKCS#12-encoded files.

       The application does not have to keep the string around after setting this option.

DEFAULT
       "PEM"

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_SSLCERT, "client.pem");
         curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "PEM");
         curl_easy_setopt(curl, CURLOPT_SSLKEY, "key.pem");
         curl_easy_setopt(curl, CURLOPT_KEYPASSWD, "s3cret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       If built TLS enabled. Added in 7.9.3

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSLCERT(3), CURLOPT_SSLKEY(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                          CURLOPT_SSLCERTTYPE(3)
CURLOPT_SSL_CIPHER_LIST(3)                                                                curl_easy_setopt options                                                               CURLOPT_SSL_CIPHER_LIST(3)



NAME
~~~
       CURLOPT_SSL_CIPHER_LIST - ciphers to use for TLS

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_CIPHER_LIST, char *list);

~~~
DESCRIPTION
~~~
       Pass  a  char  *,  pointing to a null-terminated string holding the list of ciphers to use for the SSL connection. The list must be syntactically correct, it consists of one or more cipher strings
       separated by colons. Commas or spaces are also acceptable separators but colons are normally used, !, - and + can be used as operators.

       For OpenSSL and GnuTLS valid examples of cipher lists include 'RC4-SHA', 麓SHA1+DES麓, 'TLSv1' and 'DEFAULT'. The default list is normally set when you compile OpenSSL.

       You'll find more details about cipher lists on this URL:

        https://curl.se/docs/ssl-ciphers.html

       For NSS, valid examples of cipher lists include 'rsa_rc4_128_md5', 麓rsa_aes_128_sha麓, etc. With NSS you don't add/remove ciphers. If one uses this option then all known ciphers  are  disabled  and
       only those passed in are enabled.

       For WolfSSL, valid examples of cipher lists include 麓ECDHE-RSA-RC4-SHA麓, 'AES256-SHA:AES256-SHA256', etc.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL, use internal default

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_SSL_CIPHER_LIST, "TLSv1");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       If built TLS enabled.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_TLS13_CIPHERS(3), CURLOPT_SSLVERSION(3), CURLOPT_PROXY_SSL_CIPHER_LIST(3), CURLOPT_PROXY_TLS13_CIPHERS(3), CURLOPT_USE_SSL(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                      CURLOPT_SSL_CIPHER_LIST(3)
CURLOPT_SSL_CTX_DATA(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_SSL_CTX_DATA(3)



NAME
~~~
       CURLOPT_SSL_CTX_DATA - custom pointer passed to ssl_ctx callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_CTX_DATA, void *pointer);

~~~
DESCRIPTION
~~~
       Data pointer to pass to the ssl context callback set by the option CURLOPT_SSL_CTX_FUNCTION(3), this is the pointer you'll get as third parameter.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       /* OpenSSL specific */

       #include <openssl/ssl.h>
       #include <curl/curl.h>
       #include <stdio.h>

       static CURLcode sslctx_function(CURL *curl, void *sslctx, void *parm)
       {
         X509_STORE *store;
         X509 *cert = NULL;
         BIO *bio;
         char *mypem = parm;
         /* get a BIO */
         bio = BIO_new_mem_buf(mypem, -1);
         /* use it to read the PEM formatted certificate from memory into an
          * X509 structure that SSL can use
          */
         PEM_read_bio_X509(bio, &cert, 0, NULL);
         if(cert == NULL)
           printf("PEM_read_bio_X509 failed...\n");

         /* get a pointer to the X509 certificate store (which may be empty) */
         store = SSL_CTX_get_cert_store((SSL_CTX *)sslctx);

         /* add our certificate to this store */
         if(X509_STORE_add_cert(store, cert) == 0)
           printf("error adding certificate\n");

         /* decrease reference counts */
         X509_free(cert);
         BIO_free(bio);

         /* all set to go */
         return CURLE_OK;
       }

       int main(void)
       {
         CURL * ch;
         CURLcode rv;
         char *mypem = /* example CA cert PEM - shortened */
           "-----BEGIN CERTIFICATE-----\n"
           "MIIHPTCCBSWgAwIBAgIBADANBgkqhkiG9w0BAQQFADB5MRAwDgYDVQQKEwdSb290\n"
           "IENBMR4wHAYDVQQLExVodHRwOi8vd3d3LmNhY2VydC5vcmcxIjAgBgNVBAMTGUNB\n"
           "IENlcnQgU2lnbmluZyBBdXRob3JpdHkxITAfBgkqhkiG9w0BCQEWEnN1cHBvcnRA\n"
           "Y2FjZXJ0Lm9yZzAeFw0wMzAzMzAxMjI5NDlaFw0zMzAzMjkxMjI5NDlaMHkxEDAO\n"
           "GCSNe9FINSkYQKyTYOGWhlC0elnYjyELn8+CkcY7v2vcB5G5l1YjqrZslMZIBjzk\n"
           "zk6q5PYvCdxTby78dOs6Y5nCpqyJvKeyRKANihDjbPIky/qbn3BHLt4Ui9SyIAmW\n"
           "omTxJBzcoTWcFbLUvFUufQb1nA5V9FrWk9p2rSVzTMVD\n"
           "-----END CERTIFICATE-----\n";

         curl_global_init(CURL_GLOBAL_ALL);
         ch = curl_easy_init();

         curl_easy_setopt(ch, CURLOPT_SSLCERTTYPE, "PEM");
         curl_easy_setopt(ch, CURLOPT_SSL_VERIFYPEER, 1L);
         curl_easy_setopt(ch, CURLOPT_URL, "https://www.example.com/");

         /* Retrieve page using cacerts' certificate -> will succeed
          * load the certificate by installing a function doing the necessary
          * "modifications" to the SSL CONTEXT just before link init
          */
         curl_easy_setopt(ch, CURLOPT_SSL_CTX_FUNCTION, *sslctx_function);
         curl_easy_setopt(ch, CURLOPT_SSL_CTX_DATA, mypem);
         rv = curl_easy_perform(ch);
         if(!rv)
           printf("*** transfer succeeded ***\n");
         else
           printf("*** transfer failed ***\n");

         curl_easy_cleanup(ch);
         curl_global_cleanup();
         return rv;
       }

~~~
AVAILABILITY
~~~
       Added in 7.11.0 for OpenSSL, in 7.42.0 for wolfSSL and in 7.54.0 for mbedTLS. Other SSL backends are not supported.

~~~
RETURN VALUE
~~~
       CURLE_OK if supported; or an error such as:

       CURLE_NOT_BUILT_IN - Not supported by the SSL backend

       CURLE_UNKNOWN_OPTION

~~~
SEE ALSO
       CURLOPT_SSL_CTX_FUNCTION(3), CURLOPT_SSLVERSION(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                         CURLOPT_SSL_CTX_DATA(3)
CURLOPT_SSL_CTX_FUNCTION(3)                                                               curl_easy_setopt options                                                              CURLOPT_SSL_CTX_FUNCTION(3)



NAME
~~~
       CURLOPT_SSL_CTX_FUNCTION - SSL context callback for OpenSSL, wolfSSL or mbedTLS

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode ssl_ctx_callback(CURL *curl, void *ssl_ctx, void *userptr);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_CTX_FUNCTION,
                                 ssl_ctx_callback);

~~~
DESCRIPTION
~~~
       This option only works for libcurl powered by OpenSSL, wolfSSL or mbedTLS. If libcurl was built against another SSL library this functionality is absent.

       Pass a pointer to your callback function, which should match the prototype shown above.

       This  callback  function  gets called by libcurl just before the initialization of an SSL connection after having processed all other SSL related options to give a last chance to an application to
       modify the behavior of the SSL initialization. The ssl_ctx parameter is actually a pointer to the SSL library's SSL_CTX for OpenSSL or wolfSSL, and a pointer to mbedtls_ssl_config for mbedTLS.  If
       an  error  is  returned  from  the  callback  no  attempt  to establish a connection is made and the perform operation will return the callback's error code. Set the userptr argument with the CURLOPT_SSL_CTX_DATA(3) option.

       This function will get called on all new connections made to a server, during the SSL negotiation. The ssl_ctx will point to a newly initialized object each time, but note the pointer may  be  the
       same as from a prior call.

       To  use  this  properly,  a non-trivial amount of knowledge of your SSL library is necessary. For example, you can use this function to call library-specific callbacks to add additional validation
       code for certificates, and even to change the actual URI of an HTTPS request.

       WARNING: The CURLOPT_SSL_CTX_FUNCTION(3) callback allows the application to reach in and modify SSL details in the connection without libcurl itself knowing anything about it,  which  then  subsequently can lead to libcurl unknowingly reusing SSL connections with different properties. To remedy this you may set CURLOPT_FORBID_REUSE(3) from the callback function.

       WARNING:  If you are using DNS-over-HTTPS (DoH) via CURLOPT_DOH_URL(3) then the CTX callback will also be called for those transfers and the curl handle is set to an internal handle. This behavior
       is subject to change.  We recommend before performing your transfer set CURLOPT_PRIVATE(3) on your curl handle so you can identify it in the CTX callback. If you have a reason to  modify  DoH  SSL
       context please let us know on the curl-library mailing list because we are considering removing this capability.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       /* OpenSSL specific */

       #include <openssl/ssl.h>
       #include <curl/curl.h>
       #include <stdio.h>

       static CURLcode sslctx_function(CURL *curl, void *sslctx, void *parm)
       {
         X509_STORE *store;
         X509 *cert = NULL;
         BIO *bio;
         char *mypem = parm;
         /* get a BIO */
         bio = BIO_new_mem_buf(mypem, -1);
         /* use it to read the PEM formatted certificate from memory into an
          * X509 structure that SSL can use
          */
         PEM_read_bio_X509(bio, &cert, 0, NULL);
         if(cert == NULL)
           printf("PEM_read_bio_X509 failed...\n");

         /* get a pointer to the X509 certificate store (which may be empty) */
         store = SSL_CTX_get_cert_store((SSL_CTX *)sslctx);

         /* add our certificate to this store */
         if(X509_STORE_add_cert(store, cert) == 0)
           printf("error adding certificate\n");

         /* decrease reference counts */
         X509_free(cert);
         BIO_free(bio);

         /* all set to go */
         return CURLE_OK;
       }

       int main(void)
       {
         CURL * ch;
         CURLcode rv;
         char *mypem = /* example CA cert PEM - shortened */
           "-----BEGIN CERTIFICATE-----\n"
           "MIIHPTCCBSWgAwIBAgIBADANBgkqhkiG9w0BAQQFADB5MRAwDgYDVQQKEwdSb290\n"
           "IENBMR4wHAYDVQQLExVodHRwOi8vd3d3LmNhY2VydC5vcmcxIjAgBgNVBAMTGUNB\n"
           "IENlcnQgU2lnbmluZyBBdXRob3JpdHkxITAfBgkqhkiG9w0BCQEWEnN1cHBvcnRA\n"
           "Y2FjZXJ0Lm9yZzAeFw0wMzAzMzAxMjI5NDlaFw0zMzAzMjkxMjI5NDlaMHkxEDAO\n"
           "GCSNe9FINSkYQKyTYOGWhlC0elnYjyELn8+CkcY7v2vcB5G5l1YjqrZslMZIBjzk\n"
           "zk6q5PYvCdxTby78dOs6Y5nCpqyJvKeyRKANihDjbPIky/qbn3BHLt4Ui9SyIAmW\n"
           "omTxJBzcoTWcFbLUvFUufQb1nA5V9FrWk9p2rSVzTMVD\n"
           "-----END CERTIFICATE-----\n";

         curl_global_init(CURL_GLOBAL_ALL);
         ch = curl_easy_init();

         curl_easy_setopt(ch, CURLOPT_SSLCERTTYPE, "PEM");
         curl_easy_setopt(ch, CURLOPT_SSL_VERIFYPEER, 1L);
         curl_easy_setopt(ch, CURLOPT_URL, "https://www.example.com/");

         /* Retrieve page using cacerts' certificate -> will succeed
          * load the certificate by installing a function doing the necessary
          * "modifications" to the SSL CONTEXT just before link init
          */
         curl_easy_setopt(ch, CURLOPT_SSL_CTX_FUNCTION, *sslctx_function);
         curl_easy_setopt(ch, CURLOPT_SSL_CTX_DATA, mypem);
         rv = curl_easy_perform(ch);
         if(!rv)
           printf("*** transfer succeeded ***\n");
         else
           printf("*** transfer failed ***\n");

         curl_easy_cleanup(ch);
         curl_global_cleanup();
         return rv;
       }

~~~
AVAILABILITY
~~~
       Added in 7.11.0 for OpenSSL, in 7.42.0 for wolfSSL and in 7.54.0 for mbedTLS. Other SSL backends are not supported.

~~~
RETURN VALUE
~~~
       CURLE_OK if supported; or an error such as:

       CURLE_NOT_BUILT_IN - Not supported by the SSL backend

       CURLE_UNKNOWN_OPTION

~~~
SEE ALSO
       CURLOPT_SSL_CTX_DATA(3), CURLOPT_SSL_VERIFYPEER(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                     CURLOPT_SSL_CTX_FUNCTION(3)
CURLOPT_SSL_EC_CURVES(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_SSL_EC_CURVES(3)



NAME
~~~
       CURLOPT_SSL_EC_CURVES - key exchange curves

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_EC_CURVES, char *alg_list);

~~~
DESCRIPTION
~~~
       Pass a string as parameter with a colon delimited list of (EC) algorithms. This option defines the client's key exchange algorithms in the SSL handshake (if the SSL backend libcurl is built to use
       supports it).

DEFAULT
       "", embedded in SSL backend

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_SSL_EC_CURVES, "X25519:P-521");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.73.0. Supported by the OpenSSL backend.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SSL_OPTIONS(3), CURLOPT_SSL_CIPHER_LIST(3), CURLOPT_TLS13_CIPHERS(3),



libcurl 7.73.0                                                                                  29 Aug 2020                                                                        CURLOPT_SSL_EC_CURVES(3)
CURLOPT_SSL_ENABLE_ALPN(3)                                                                curl_easy_setopt options                                                               CURLOPT_SSL_ENABLE_ALPN(3)



NAME
~~~
       CURLOPT_SSL_ENABLE_ALPN - Application Layer Protocol Negotiation

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_ENABLE_ALPN, long npn);

~~~
DESCRIPTION
~~~
       Pass a long as parameter, 0 or 1 where 1 is for enable and 0 for disable. This option enables/disables ALPN in the SSL handshake (if the SSL backend libcurl is built to use supports it), which can
       be used to negotiate http2.

DEFAULT
       1, enabled

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_SSL_ENABLE_ALPN, 0L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.36.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SSL_ENABLE_NPN(3), CURLOPT_SSL_OPTIONS(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                      CURLOPT_SSL_ENABLE_ALPN(3)
CURLOPT_SSL_ENABLE_NPN(3)                                                                 curl_easy_setopt options                                                                CURLOPT_SSL_ENABLE_NPN(3)



NAME
~~~
       CURLOPT_SSL_ENABLE_NPN - use NPN

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_ENABLE_NPN, long npn);

~~~
DESCRIPTION
~~~
       Pass  a long as parameter, 0 or 1 where 1 is for enable and 0 for disable. This option enables/disables NPN in the SSL handshake (if the SSL backend libcurl is built to use supports it), which can
       be used to negotiate http2.

DEFAULT
       1, enabled

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_SSL_ENABLE_NPN, 1L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.36.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SSL_ENABLE_ALPN(3), CURLOPT_SSL_OPTIONS(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                       CURLOPT_SSL_ENABLE_NPN(3)
CURLOPT_SSLENGINE(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_SSLENGINE(3)



NAME
~~~
       CURLOPT_SSLENGINE - SSL engine identifier

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSLENGINE, char *id);

~~~
DESCRIPTION
~~~
       Pass a pointer to a null-terminated string as parameter. It will be used as the identifier for the crypto engine you want to use for your private key.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_SSLENGINE, "dynamic");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Only if the SSL backend is OpenSSL built with engine support.

~~~
RETURN VALUE
~~~
       CURLE_OK - Engine found.

       CURLE_SSL_ENGINE_NOTFOUND - Engine not found, or OpenSSL was not built with engine support.

       CURLE_SSL_ENGINE_INITFAILED - Engine found but initialization failed.

       CURLE_NOT_BUILT_IN - Option not built in, OpenSSL is not the SSL backend.

       CURLE_UNKNOWN_OPTION - Option not recognized.

       CURLE_OUT_OF_MEMORY - Insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSLENGINE_DEFAULT(3), CURLOPT_SSLKEY(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                            CURLOPT_SSLENGINE(3)
CURLOPT_SSLENGINE_DEFAULT(3)                                                              curl_easy_setopt options                                                             CURLOPT_SSLENGINE_DEFAULT(3)



NAME
~~~
       CURLOPT_SSLENGINE_DEFAULT - make SSL engine default

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSLENGINE_DEFAULT, long val);

~~~
DESCRIPTION
~~~
       Pass a long set to 1 to make the already specified crypto engine the default for (asymmetric) crypto operations.

       This option has no effect unless set after CURLOPT_SSLENGINE(3).

DEFAULT
       None

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_SSLENGINE, "dynamic");
         curl_easy_setopt(curl, CURLOPT_SSLENGINE_DEFAULT, 1L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Only if the SSL backend is OpenSSL built with engine support.

~~~
RETURN VALUE
~~~
       CURLE_OK - Engine set as default.

       CURLE_SSL_ENGINE_SETFAILED - Engine could not be set as default.

       CURLE_NOT_BUILT_IN - Option not built in, OpenSSL is not the SSL backend.

       CURLE_UNKNOWN_OPTION - Option not recognized.

       CURLE_OUT_OF_MEMORY - Insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSLENGINE(3), CURLOPT_SSLCERT(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                    CURLOPT_SSLENGINE_DEFAULT(3)
CURLOPT_SSL_FALSESTART(3)                                                                 curl_easy_setopt options                                                                CURLOPT_SSL_FALSESTART(3)



NAME
~~~
       CURLOPT_SSL_FALSESTART - TLS false start

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_FALSESTART, long enable);

~~~
DESCRIPTION
~~~
       Pass a long as parameter set to 1L to enable or 0 to disable.

       This  option  determines whether libcurl should use false start during the TLS handshake. False start is a mode where a TLS client will start sending application data before verifying the server's
       Finished message, thus saving a round trip when performing a full handshake.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_SSL_FALSESTART, 1L);
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.42.0. This option is currently only supported by the NSS and Secure Transport (on iOS 7.0 or later, or OS X 10.9 or later) TLS backends.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if false start is supported by the SSL backend, otherwise returns CURLE_NOT_BUILT_IN.

~~~
SEE ALSO
       CURLOPT_TCP_FASTOPEN(3),



libcurl 7.41.0                                                                                  14 Feb 2015                                                                       CURLOPT_SSL_FALSESTART(3)
CURLOPT_SSLKEY(3)                                                                         curl_easy_setopt options                                                                        CURLOPT_SSLKEY(3)



NAME
~~~
       CURLOPT_SSLKEY - private keyfile for TLS and SSL client cert

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSLKEY, char *keyfile);

~~~
DESCRIPTION
~~~
       Pass a pointer to a null-terminated string as parameter. The string should be the file name of your private key. The default format is "PEM" and can be changed with CURLOPT_SSLKEYTYPE(3).

       (iOS and Mac OS X only) This option is ignored if curl was built against Secure Transport. Secure Transport expects the private key to be already present in the keychain or PKCS#12 file containing
       the certificate.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_SSLCERT, "client.pem");
         curl_easy_setopt(curl, CURLOPT_SSLKEY, "key.pem");
         curl_easy_setopt(curl, CURLOPT_KEYPASSWD, "s3cret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       If built TLS enabled.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSLKEYTYPE(3), CURLOPT_SSLCERT(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                               CURLOPT_SSLKEY(3)
CURLOPT_SSLKEY_BLOB(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_SSLKEY_BLOB(3)



NAME
~~~
       CURLOPT_SSLKEY_BLOB - private key for client cert from memory blob

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSLKEY_BLOB,
                                 struct curl_blob *blob);

~~~
DESCRIPTION
~~~
       Pass  a  pointer  to a curl_blob structure, which contains information (pointer and size) for a private key. Compatible with OpenSSL. The format (like "PEM") must be specified with CURLOPT_SSLKEYTYPE(3).

       If the blob is initialized with the flags member of struct curl_blob set to CURL_BLOB_COPY, the application does not have to keep the buffer around after setting this.

       This option is an alternative to CURLOPT_SSLKEY(3) which instead expects a file name as input.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         struct curl_blob blob;
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         blob.data = certificateData;
         blob.len = filesize;
         blob.flags = CURL_BLOB_COPY;
         curl_easy_setopt(curl, CURLOPT_SSLCERT_BLOB, &blob);
         curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "PEM");

         blob.data = privateKeyData;
         blob.len = privateKeySize;
         curl_easy_setopt(curl, CURLOPT_SSLKEY_BLOB, &blob);
         curl_easy_setopt(curl, CURLOPT_KEYPASSWD, "s3cret");
         curl_easy_setopt(curl, CURLOPT_SSLKEYTYPE, "PEM");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in libcurl 7.71.0. This option is supported by the OpenSSL backends.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS enabled, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSLKEYTYPE(3), CURLOPT_SSLKEY(3),



libcurl 7.71.0                                                                                  24 Jun 2020                                                                          CURLOPT_SSLKEY_BLOB(3)
CURLOPT_SSLKEYTYPE(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_SSLKEYTYPE(3)



NAME
~~~
       CURLOPT_SSLKEYTYPE - type of the private key file

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSLKEYTYPE, char *type);

~~~
DESCRIPTION
~~~
       Pass a pointer to a null-terminated string as parameter. The string should be the format of your private key. Supported formats are "PEM", "DER" and "ENG".

       The format "ENG" enables you to load the private key from a crypto engine. In this case CURLOPT_SSLKEY(3) is used as an identifier passed to the engine. You have to set the crypto engine with CURLOPT_SSLENGINE(3).  "DER" format key file currently does not work because of a bug in OpenSSL.

       The application does not have to keep the string around after setting this option.

DEFAULT
       "PEM"

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_SSLCERT, "client.pem");
         curl_easy_setopt(curl, CURLOPT_SSLKEY, "key.pem");
         curl_easy_setopt(curl, CURLOPT_SSLKEYTYPE, "PEM");
         curl_easy_setopt(curl, CURLOPT_KEYPASSWD, "s3cret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       If built TLS enabled.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_SSLKEY(3), CURLOPT_SSLCERT(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                           CURLOPT_SSLKEYTYPE(3)
CURLOPT_SSL_OPTIONS(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_SSL_OPTIONS(3)



NAME
~~~
       CURLOPT_SSL_OPTIONS - SSL behavior options

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_OPTIONS, long bitmask);

~~~
DESCRIPTION
~~~
       Pass a long with a bitmask to tell libcurl about specific SSL behaviors. Available bits:

       CURLSSLOPT_ALLOW_BEAST
              Tells  libcurl to not attempt to use any workarounds for a security flaw in the SSL3 and TLS1.0 protocols.  If this option isn't used or this bit is set to 0, the SSL layer libcurl uses may
              use a work-around for this flaw although it might cause interoperability problems with some (older) SSL implementations. WARNING: avoiding this work-around lessens the security, and by set       ting this option to 1 you ask for exactly that.  This option is only supported for Secure Transport, NSS and OpenSSL.

       CURLSSLOPT_NO_REVOKE
              Tells  libcurl  to  disable certificate revocation checks for those SSL backends where such behavior is present. This option is only supported for Schannel (the native Windows SSL library),
              with an exception in the case of Windows' Untrusted Publishers block list which it seems can't be bypassed. (Added in 7.44.0)

       CURLSSLOPT_NO_PARTIALCHAIN
              Tells libcurl to not accept "partial" certificate chains, which it otherwise does by default. This option is only supported for OpenSSL and will fail the  certificate  verification  if  the
              chain ends with an intermediate certificate and not with a root cert. (Added in 7.68.0)

       CURLSSLOPT_REVOKE_BEST_EFFORT
              Tells  libcurl to ignore certificate revocation checks in case of missing or offline distribution points for those SSL backends where such behavior is present. This option is only supported
              for Schannel (the native Windows SSL library). If combined with CURLSSLOPT_NO_REVOKE, the latter takes precedence. (Added in 7.70.0)

       CURLSSLOPT_NATIVE_CA
              Tell libcurl to use the operating system's native CA store for certificate verification. Works only on Windows when built to use OpenSSL. This option is experimental and behavior is subject
              to change.  (Added in 7.71.0)

       CURLSSLOPT_AUTO_CLIENT_CERT
              Tell  libcurl  to  automatically  locate  and  use  a client certificate for authentication, when requested by the server. This option is only supported for Schannel (the native Windows SSL
              library). Prior to 7.77.0 this was the default behavior in libcurl with Schannel. Since the server can request any certificate that supports client  authentication  in  the  OS  certificate
              store it could be a privacy violation and unexpected.  (Added in 7.77.0)

DEFAULT
       0

~~~
PROTOCOLS
~~~
       All TLS-based protocols

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* weaken TLS only for use with silly servers */
         curl_easy_setopt(curl, CURLOPT_SSL_OPTIONS, CURLSSLOPT_ALLOW_BEAST |
                          CURLSSLOPT_NO_REVOKE);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.25.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SSLVERSION(3), CURLOPT_SSL_CIPHER_LIST(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                          CURLOPT_SSL_OPTIONS(3)
CURLOPT_SSL_SESSIONID_CACHE(3)                                                            curl_easy_setopt options                                                           CURLOPT_SSL_SESSIONID_CACHE(3)



NAME
~~~
       CURLOPT_SSL_SESSIONID_CACHE - use the SSL session-ID cache

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_SESSIONID_CACHE,
                                long enabled);

~~~
DESCRIPTION
~~~
       Pass  a  long set to 0 to disable libcurl's use of SSL session-ID caching. Set this to 1 to enable it. By default all transfers are done using the cache enabled. While nothing ever should get hurt
       by attempting to reuse SSL session-IDs, there seem to be or have been broken SSL implementations in the wild that may require you to disable this in order for you to succeed.

DEFAULT
       1

~~~
PROTOCOLS
~~~
       All TLS-based

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* switch off session-id use! */
         curl_easy_setopt(curl, CURLOPT_SSL_SESSIONID_CACHE, 0L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.16.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_DNS_CACHE_TIMEOUT(3), CURLOPT_SSLVERSION(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                  CURLOPT_SSL_SESSIONID_CACHE(3)
CURLOPT_SSL_VERIFYHOST(3)                                                                 curl_easy_setopt options                                                                CURLOPT_SSL_VERIFYHOST(3)



NAME
~~~
       CURLOPT_SSL_VERIFYHOST - verify the certificate's name against host

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_VERIFYHOST, long verify);

~~~
DESCRIPTION
~~~
       Pass a long as parameter specifying what to verify.

       This option determines whether libcurl verifies that the server cert is for the server it is known as.

       When negotiating TLS and SSL connections, the server sends a certificate indicating its identity.

       When  CURLOPT_SSL_VERIFYHOST(3)  is  2, that certificate must indicate that the server is the server to which you meant to connect, or the connection fails. Simply put, it means it has to have the
       same name in the certificate as is in the URL you operate against.

       Curl considers the server the intended one when the Common Name field or a Subject Alternate Name field in the certificate matches the host name in the URL to which you told Curl to connect.

       If verify value is set to 1:

       In 7.28.0 and earlier: treated as a debug option of some sorts, not supported anymore due to frequently leading to programmer mistakes.

       From 7.28.1 to 7.65.3: setting it to 1 made curl_easy_setopt() return an error and leaving the flag untouched.

       From 7.66.0: treats 1 and 2 the same.

       When the verify value is 0, the connection succeeds regardless of the names in the certificate. Use that ability with caution!

       The default value for this option is 2.

       This option controls checking the server's certificate's claimed identity.  The server could be lying.  To control lying, see CURLOPT_SSL_VERIFYPEER(3).

LIMITATIONS
       Secure Transport: If verify value is 0, then SNI is also disabled. SNI is a TLS extension that sends the hostname to the server. The server may use that information to do such  things  as  sending
       back a specific certificate for the hostname, or forwarding the request to a specific origin server. Some hostnames may be inaccessible if SNI is not sent.

       NSS: If CURLOPT_SSL_VERIFYPEER(3) is zero, CURLOPT_SSL_VERIFYHOST(3) is also set to zero and cannot be overridden.

DEFAULT
       2

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Set the default value: strict name check please */
         curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       If built TLS enabled.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TLS is supported, and CURLE_UNKNOWN_OPTION if not.

       If 1 is set as argument, CURLE_BAD_FUNCTION_ARGUMENT is returned.

~~~
SEE ALSO
       CURLOPT_SSL_VERIFYPEER(3), CURLOPT_CAINFO(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                       CURLOPT_SSL_VERIFYHOST(3)
CURLOPT_SSL_VERIFYPEER(3)                                                                 curl_easy_setopt options                                                                CURLOPT_SSL_VERIFYPEER(3)



NAME
~~~
       CURLOPT_SSL_VERIFYPEER - verify the peer's SSL certificate

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_VERIFYPEER, long verify);

~~~
DESCRIPTION
~~~
       Pass a long as parameter to enable or disable.

       This option determines whether curl verifies the authenticity of the peer's certificate. A value of 1 means curl verifies; 0 (zero) means it doesn't.

       When  negotiating  a  TLS or SSL connection, the server sends a certificate indicating its identity.  Curl verifies whether the certificate is authentic, i.e. that you can trust that the server is
       who the certificate says it is.  This trust is based on a chain of digital signatures, rooted in certification authority (CA) certificates you supply.  curl uses a default bundle  of  CA  certificates (the path for that is determined at build time) and you can specify alternate certificates with the CURLOPT_CAINFO(3) option or the CURLOPT_CAPATH(3) option.

       When  CURLOPT_SSL_VERIFYPEER(3) is enabled, and the verification fails to prove that the certificate is authentic, the connection fails.  When the option is zero, the peer certificate verification
       succeeds regardless.

       Authenticating the certificate is not enough to be sure about the server. You typically also want to ensure that the server is the server you mean to be talking to.  Use  CURLOPT_SSL_VERIFYHOST(3)
       for that. The check that the host name in the certificate is valid for the host name you're connecting to is done independently of the CURLOPT_SSL_VERIFYPEER(3) option.

       WARNING: disabling verification of the certificate allows bad guys to man-in-the-middle the communication without you knowing it. Disabling verification makes the communication insecure. Just having encryption on a transfer is not enough as you cannot be sure that you are communicating with the correct end-point.

       NOTE: even when this option is disabled, depending on the used TLS backend, curl may still load the certificate file specified in CURLOPT_CAINFO(3). curl default  settings  in  some  distributions
       might  use  quite  a large file as a default setting for CURLOPT_CAINFO(3), so loading the file can be quite expensive, especially when dealing with many connections. Thus, in some situations, you
       might want to disable verification fully to save resources by setting CURLOPT_CAINFO(3) to NULL - but please also consider the warning above!

DEFAULT
       By default, curl assumes a value of 1.

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Set the default value: strict certificate check please */
         curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       If built TLS enabled.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SSL_VERIFYHOST(3), CURLOPT_PROXY_SSL_VERIFYPEER(3), CURLOPT_PROXY_SSL_VERIFYHOST(3), CURLOPT_CAINFO(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                       CURLOPT_SSL_VERIFYPEER(3)
CURLOPT_SSL_VERIFYSTATUS(3)                                                               curl_easy_setopt options                                                              CURLOPT_SSL_VERIFYSTATUS(3)



NAME
~~~
       CURLOPT_SSL_VERIFYSTATUS - verify the certificate's status

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSL_VERIFYSTATUS, long verify);

~~~
DESCRIPTION
~~~
       Pass a long as parameter set to 1 to enable or 0 to disable.

       This option determines whether libcurl verifies the status of the server cert using the "Certificate Status Request" TLS extension (aka. OCSP stapling).

       Note that if this option is enabled but the server does not support the TLS extension, the verification will fail.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* ask for OCSP stapling! */
         curl_easy_setopt(curl, CURLOPT_SSL_VERIFYSTATUS, 1L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.41.0. This option is currently only supported by the OpenSSL, GnuTLS and NSS TLS backends.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if OCSP stapling is supported by the SSL backend, otherwise returns CURLE_NOT_BUILT_IN.

~~~
SEE ALSO
       CURLOPT_SSL_VERIFYHOST(3), CURLOPT_SSL_VERIFYPEER(3), CURLOPT_CAINFO(3),



libcurl 7.40.0                                                                                  04 Dec 2014                                                                     CURLOPT_SSL_VERIFYSTATUS(3)
CURLOPT_SSLVERSION(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_SSLVERSION(3)



NAME
~~~
       CURLOPT_SSLVERSION - preferred TLS/SSL version

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SSLVERSION, long version);

~~~
DESCRIPTION
~~~
       Pass a long as parameter to control which version range of SSL/TLS versions to use.

       The  SSL and TLS versions have typically developed from the most insecure version to be more and more secure in this order through history: SSL v2, SSLv3, TLS v1.0, TLS v1.1, TLS v1.2 and the most
       recent TLS v1.3.

       Use one of the available defines for this purpose. The available options are:

              CURL_SSLVERSION_DEFAULT
                     The default acceptable version range. The minimum acceptable version is by default TLS v1.0 since 7.39.0 (unless the TLS library has a stricter rule).

              CURL_SSLVERSION_TLSv1
                     TLS v1.0 or later

              CURL_SSLVERSION_SSLv2
                     SSL v2 - refused

              CURL_SSLVERSION_SSLv3
                     SSL v3 - refused

              CURL_SSLVERSION_TLSv1_0
                     TLS v1.0 or later (Added in 7.34.0)

              CURL_SSLVERSION_TLSv1_1
                     TLS v1.1 or later (Added in 7.34.0)

              CURL_SSLVERSION_TLSv1_2
                     TLS v1.2 or later (Added in 7.34.0)

              CURL_SSLVERSION_TLSv1_3
                     TLS v1.3 or later (Added in 7.52.0)

       The maximum TLS version can be set by using one of the CURL_SSLVERSION_MAX_ macros below. It is also possible to OR one of the CURL_SSLVERSION_ macros with one of the CURL_SSLVERSION_MAX_  macros.
       The MAX macros are not supported for WolfSSL.

              CURL_SSLVERSION_MAX_DEFAULT
                     The  flag  defines the maximum supported TLS version by libcurl, or the default value from the SSL library is used. libcurl will use a sensible default maximum, which was TLS v1.2 up
                     to before 7.61.0 and is TLS v1.3 since then - assuming the TLS library support it. (Added in 7.54.0)

              CURL_SSLVERSION_MAX_TLSv1_0
                     The flag defines maximum supported TLS version as TLS v1.0.  (Added in 7.54.0)

              CURL_SSLVERSION_MAX_TLSv1_1
                     The flag defines maximum supported TLS version as TLS v1.1.  (Added in 7.54.0)

              CURL_SSLVERSION_MAX_TLSv1_2
                     The flag defines maximum supported TLS version as TLS v1.2.  (Added in 7.54.0)

              CURL_SSLVERSION_MAX_TLSv1_3
                     The flag defines maximum supported TLS version as TLS v1.3.  (Added in 7.54.0)

       In versions of curl prior to 7.54 the CURL_SSLVERSION_TLS options were documented to allow only the specified TLS version, but behavior was inconsistent depending on the TLS library.


DEFAULT
       CURL_SSLVERSION_DEFAULT

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* ask libcurl to use TLS version 1.0 or later */
         curl_easy_setopt(curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       SSLv2 and SSLv3 are refused completely since curl 7.77.0

       SSLv2 is disabled by default since 7.18.1. Other SSL versions availability may vary depending on which backend libcurl has been built to use.

       SSLv3 is disabled by default since 7.39.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_USE_SSL(3), CURLOPT_HTTP_VERSION(3), CURLOPT_PROXY_SSLVERSION(3), CURLOPT_IPRESOLVE(3)



libcurl 7.37.0                                                                                  17 Jun 2014                                                                           CURLOPT_SSLVERSION(3)
CURLOPT_STDERR(3)                                                                         curl_easy_setopt options                                                                        CURLOPT_STDERR(3)



NAME
~~~
       CURLOPT_STDERR - redirect stderr to another stream

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_STDERR, FILE *stream);

~~~
DESCRIPTION
~~~
       Pass a FILE * as parameter. Tell libcurl to use this stream instead of stderr when showing the progress meter and displaying CURLOPT_VERBOSE(3) data.

DEFAULT
       stderr

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       FILE *filep = fopen("dump", "wb");
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_STDERR, filep);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_VERBOSE(3), CURLOPT_NOPROGRESS(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                               CURLOPT_STDERR(3)
CURLOPT_STREAM_DEPENDS(3)                                                                 curl_easy_setopt options                                                                CURLOPT_STREAM_DEPENDS(3)



NAME
~~~
       CURLOPT_STREAM_DEPENDS - stream this transfer depends on

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_STREAM_DEPENDS, CURL *dephandle);

~~~
DESCRIPTION
~~~
       Pass  a CURL * pointer in dephandle to identify the stream within the same connection that this stream is depending upon. This option clears the exclusive bit and is mutually exclusive to the CURLOPT_STREAM_DEPENDS_E(3) option.

       The spec says "Including a dependency expresses a preference to allocate resources to the identified stream rather than to the dependent stream."

       This option can be set during transfer.

       dephandle must not be the same as handle, that will cause this function to return an error. It must be another easy handle, and it also needs to be a handle of a transfer that will  be  sent  over
       the same HTTP/2 connection for this option to have an actual effect.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP/2

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       CURL *curl2 = curl_easy_init(); /* a second handle */
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/one");

         /* the second depends on the first */
         curl_easy_setopt(curl2, CURLOPT_URL, "https://example.com/two");
         curl_easy_setopt(curl2, CURLOPT_STREAM_DEPENDS, curl);

         /* then add both to a multi handle and transfer them! */
       }

~~~
AVAILABILITY
~~~
       Added in 7.46.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_STREAM_WEIGHT(3), CURLOPT_STREAM_DEPENDS_E(3),



libcurl 7.46.0                                                                                  13 Sep 2015                                                                       CURLOPT_STREAM_DEPENDS(3)
CURLOPT_STREAM_DEPENDS_E(3)                                                               curl_easy_setopt options                                                              CURLOPT_STREAM_DEPENDS_E(3)



NAME
~~~
       CURLOPT_STREAM_DEPENDS_E - stream this transfer depends on exclusively

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_STREAM_DEPENDS_E, CURL *dephandle);

~~~
DESCRIPTION
~~~
       Pass a CURL * pointer in dephandle to identify the stream within the same connection that this stream is depending upon exclusively. That means it depends on it and sets the Exclusive bit.

       The spec says "Including a dependency expresses a preference to allocate resources to the identified stream rather than to the dependent stream."

       Setting a dependency with the exclusive flag for a reprioritized stream causes all the dependencies of the new parent stream to become dependent on the reprioritized stream.

       This option can be set during transfer.

       dephandle  must  not  be the same as handle, that will cause this function to return an error. It must be another easy handle, and it also needs to be a handle of a transfer that will be sent over
       the same HTTP/2 connection for this option to have an actual effect.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP/2

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       CURL *curl2 = curl_easy_init(); /* a second handle */
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/one");

         /* the second depends on the first */
         curl_easy_setopt(curl2, CURLOPT_URL, "https://example.com/two");
         curl_easy_setopt(curl2, CURLOPT_STREAM_DEPENDS_E, curl);

         /* then add both to a multi handle and transfer them! */
       }

~~~
AVAILABILITY
~~~
       Added in 7.46.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_STREAM_WEIGHT(3), CURLOPT_STREAM_DEPENDS(3),



libcurl 7.46.0                                                                                  13 Sep 2015                                                                     CURLOPT_STREAM_DEPENDS_E(3)
CURLOPT_STREAM_WEIGHT(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_STREAM_WEIGHT(3)



NAME
~~~
       CURLOPT_STREAM_WEIGHT - numerical stream weight

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_STREAM_WEIGHT, long weight);

~~~
DESCRIPTION
~~~
       Set the long weight to a number between 1 and 256.

       When  using  HTTP/2,  this  option  sets the individual weight for this particular stream used by the easy handle. Setting and using weights only makes sense and is only usable when doing multiple
       streams over the same connections, which thus implies that you use CURLMOPT_PIPELINING(3).

       This option can be set during transfer and will then cause the updated weight info get sent to the server the next time an HTTP/2 frame is sent to the server.

       See section 5.3 of RFC 7540 for protocol details: https://httpwg.github.io/specs/rfc7540.html#StreamPriority

       Streams with the same parent should be allocated resources proportionally based on their weight. So if you have two streams going, stream A with weight 16 and stream B with  weight  32,  stream  B
       will get two thirds (32/48) of the available bandwidth (assuming the server can send off the data equally for both streams).

DEFAULT
       If nothing is set, the HTTP/2 protocol itself will use its own default which is 16.

~~~
PROTOCOLS
~~~
       HTTP/2

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       CURL *curl2 = curl_easy_init(); /* a second handle */
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/one");
         curl_easy_setopt(curl, CURLOPT_STREAM_WEIGHT, 10L);

         /* the second has twice the weight */
         curl_easy_setopt(curl2, CURLOPT_URL, "https://example.com/two");
         curl_easy_setopt(curl2, CURLOPT_STREAM_WEIGHT, 20L);

         /* then add both to a multi handle and transfer them! */
       }

~~~
AVAILABILITY
~~~
       Added in 7.46.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_STREAM_DEPENDS(3), CURLOPT_STREAM_DEPENDS_E(3), CURLOPT_PIPEWAIT(3), CURLMOPT_PIPELINING(3),



libcurl 7.46.0                                                                                  13 Sep 2015                                                                        CURLOPT_STREAM_WEIGHT(3)
CURLOPT_SUPPRESS_CONNECT_HEADERS(3)                                                       curl_easy_setopt options                                                      CURLOPT_SUPPRESS_CONNECT_HEADERS(3)



NAME
~~~
       CURLOPT_SUPPRESS_CONNECT_HEADERS - suppress proxy CONNECT response headers from user callbacks

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_SUPPRESS_CONNECT_HEADERS, long onoff);

~~~
DESCRIPTION
~~~
       When  CURLOPT_HTTPPROXYTUNNEL(3)  is  used  and a CONNECT request is made, suppress proxy CONNECT response headers from the user callback functions CURLOPT_HEADERFUNCTION(3) and CURLOPT_WRITEFUNCTION(3).

       Proxy CONNECT response headers can complicate header processing since it's essentially a separate set of headers. You can enable this option to suppress those headers.

       For example let's assume an HTTPS URL is to be retrieved via CONNECT. On success there would normally be two sets of headers, and each header line sent to the  header  function  and/or  the  write
       function. The data given to the callbacks would look like this:

       HTTP/1.1 200 Connection established
       {headers}...

       HTTP/1.1 200 OK
       Content-Type: application/json
       {headers}...

       {body}...

       However by enabling this option the CONNECT response headers are suppressed, so the data given to the callbacks would look like this:

       HTTP/1.1 200 OK
       Content-Type: application/json
       {headers}...

       {body}...


DEFAULT
       0

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         curl_easy_setopt(curl, CURLOPT_HEADER, 1L);
         curl_easy_setopt(curl, CURLOPT_PROXY, "http://foo:3128");
         curl_easy_setopt(curl, CURLOPT_HTTPPROXYTUNNEL, 1L);
         curl_easy_setopt(curl, CURLOPT_SUPPRESS_CONNECT_HEADERS, 1L);

         curl_easy_perform(curl);

         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.54.0

~~~
RETURN VALUE
~~~
       CURLE_OK or an error such as CURLE_UNKNOWN_OPTION.

~~~
SEE ALSO
       CURLOPT_HEADER(3), CURLOPT_PROXY(3), CURLOPT_HTTPPROXYTUNNEL(3),



libcurl 7.54.0                                                                                13 February 2017                                                          CURLOPT_SUPPRESS_CONNECT_HEADERS(3)
CURLOPT_TCP_FASTOPEN(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_TCP_FASTOPEN(3)



NAME
~~~
       CURLOPT_TCP_FASTOPEN - TCP Fast Open

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TCP_FASTOPEN, long enable);

~~~
DESCRIPTION
~~~
       Pass a long as parameter set to 1L to enable or 0 to disable.

       TCP  Fast  Open  (RFC7413)  is a mechanism that allows data to be carried in the SYN and SYN-ACK packets and consumed by the receiving end during the initial connection handshake, saving up to one
       full round-trip time (RTT).

DEFAULT
       0

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_TCP_FASTOPEN, 1L);
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.49.0. This option is currently only supported on Linux and  OS X El Capitan.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if fast open is supported by the operating system, otherwise returns CURLE_NOT_BUILT_IN.

~~~
SEE ALSO
       CURLOPT_SSL_FALSESTART(3),



libcurl 7.49.0                                                                                  16 Feb 2016                                                                         CURLOPT_TCP_FASTOPEN(3)
CURLOPT_TCP_KEEPALIVE(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_TCP_KEEPALIVE(3)



NAME
~~~
       CURLOPT_TCP_KEEPALIVE - TCP keep-alive probing

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TCP_KEEPALIVE, long probe);

~~~
DESCRIPTION
~~~
       Pass a long. If set to 1, TCP keepalive probes will be sent. The delay and frequency of these probes can be controlled by the CURLOPT_TCP_KEEPIDLE(3) and CURLOPT_TCP_KEEPINTVL(3) options, provided
       the operating system supports them. Set to 0 (default behavior) to disable keepalive probes

DEFAULT
       0

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* enable TCP keep-alive for this transfer */
         curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1L);

         /* keep-alive idle time to 120 seconds */
         curl_easy_setopt(curl, CURLOPT_TCP_KEEPIDLE, 120L);

         /* interval time between keep-alive probes: 60 seconds */
         curl_easy_setopt(curl, CURLOPT_TCP_KEEPINTVL, 60L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.25.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_TCP_KEEPIDLE(3), CURLOPT_TCP_KEEPINTVL(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                        CURLOPT_TCP_KEEPALIVE(3)
CURLOPT_TCP_KEEPIDLE(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_TCP_KEEPIDLE(3)



NAME
~~~
       CURLOPT_TCP_KEEPIDLE - TCP keep-alive idle time wait

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TCP_KEEPIDLE, long delay);

~~~
DESCRIPTION
~~~
       Pass a long. Sets the delay, in seconds, that the operating system will wait while the connection is idle before sending keepalive probes. Not all operating systems support this option.

DEFAULT
       60

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* enable TCP keep-alive for this transfer */
         curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1L);

         /* set keep-alive idle time to 120 seconds */
         curl_easy_setopt(curl, CURLOPT_TCP_KEEPIDLE, 120L);

         /* interval time between keep-alive probes: 60 seconds */
         curl_easy_setopt(curl, CURLOPT_TCP_KEEPINTVL, 60L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.25.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_TCP_KEEPALIVE(3), CURLOPT_TCP_KEEPINTVL(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                         CURLOPT_TCP_KEEPIDLE(3)
CURLOPT_TCP_KEEPINTVL(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_TCP_KEEPINTVL(3)



NAME
~~~
       CURLOPT_TCP_KEEPINTVL - TCP keep-alive interval

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TCP_KEEPINTVL, long interval);

~~~
DESCRIPTION
~~~
       Pass a long. Sets the interval, in seconds, that the operating system will wait between sending keepalive probes. Not all operating systems support this option. (Added in 7.25.0)

DEFAULT
       60

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* enable TCP keep-alive for this transfer */
         curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1L);

         /* set keep-alive idle time to 120 seconds */
         curl_easy_setopt(curl, CURLOPT_TCP_KEEPIDLE, 120L);

         /* interval time between keep-alive probes: 60 seconds */
         curl_easy_setopt(curl, CURLOPT_TCP_KEEPINTVL, 60L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_TCP_KEEPALIVE(3), CURLOPT_TCP_KEEPIDLE(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                        CURLOPT_TCP_KEEPINTVL(3)
CURLOPT_TCP_NODELAY(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_TCP_NODELAY(3)



NAME
~~~
       CURLOPT_TCP_NODELAY - the TCP_NODELAY option

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TCP_NODELAY, long nodelay);

~~~
DESCRIPTION
~~~
       Pass  a  long  specifying  whether the TCP_NODELAY option is to be set or cleared (1L = set, 0 = clear). The option is set by default. This will have no effect after the connection has been established.

       Setting this option to 1L will disable TCP's Nagle algorithm on this connection. The purpose of this algorithm is to try to minimize the number of small packets on the network (where "small  packets" means TCP segments less than the Maximum Segment Size (MSS) for the network).

       Maximizing  the  amount  of  data  sent per TCP segment is good because it amortizes the overhead of the send. However, in some cases small segments may need to be sent without delay. This is less
       efficient than sending larger amounts of data at a time, and can contribute to congestion on the network if overdone.

DEFAULT
       1

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         /* leave Nagle enabled */
         curl_easy_setopt(curl, CURLOPT_TCP_NODELAY, 0);
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always. The default was changed to 1 from 0 in 7.50.2.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_SOCKOPTFUNCTION(3), CURLOPT_TCP_KEEPALIVE(3),



libcurl 7.50.0                                                                                  30 Jun 2016                                                                          CURLOPT_TCP_NODELAY(3)
CURLOPT_TELNETOPTIONS(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_TELNETOPTIONS(3)



NAME
~~~
       CURLOPT_TELNETOPTIONS - custom telnet options

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TELNETOPTIONS,
                                 struct curl_slist *cmds);

~~~
DESCRIPTION
~~~
       Provide  a  pointer  to  a  curl_slist with variables to pass to the telnet negotiations. The variables should be in the format <option=value>. libcurl supports the options 'TTYPE', 'XDISPLOC' and
       'NEW_ENV'. See the TELNET standard for details.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       TELNET

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         struct curl_slist *options;
         options = curl_slist_append(NULL, "TTTYPE=vt100");
         options = curl_slist_append(options, "USER=foobar");
         curl_easy_setopt(curl, CURLOPT_URL, "telnet://example.com/");
         curl_easy_setopt(curl, CURLOPT_TELNETOPTIONS, options);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
         curl_slist_free_all(options);
       }

~~~
AVAILABILITY
~~~
       Along with TELNET

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if TELNET is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_HTTPHEADER(3), CURLOPT_QUOTE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                        CURLOPT_TELNETOPTIONS(3)
CURLOPT_TFTP_BLKSIZE(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_TFTP_BLKSIZE(3)



NAME
~~~
       CURLOPT_TFTP_BLKSIZE - TFTP block size

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TFTP_BLKSIZE, long blocksize);

~~~
DESCRIPTION
~~~
       Specify  blocksize  to  use for TFTP data transmission. Valid range as per RFC2348 is 8-65464 bytes. The default of 512 bytes will be used if this option is not specified. The specified block size
       will only be used pending support by the remote server. If the server does not return an option acknowledgement or returns an option acknowledgement with no blksize, the default of 512 bytes  will
       be used.

DEFAULT
       512

~~~
PROTOCOLS
~~~
       TFTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "tftp://example.com/bootimage");
         /* try using larger blocks */
         curl_easy_setopt(curl, CURLOPT_TFTP_BLKSIZE, 2048L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.4

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_MAXFILESIZE(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                         CURLOPT_TFTP_BLKSIZE(3)
CURLOPT_TFTP_NO_OPTIONS(3)                                                                curl_easy_setopt options                                                               CURLOPT_TFTP_NO_OPTIONS(3)



NAME
~~~
       CURLOPT_TFTP_NO_OPTIONS - send no TFTP options requests

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TFTP_NO_OPTIONS, long onoff);

~~~
DESCRIPTION
~~~
       Set onoff to 1L to exclude all TFTP options defined in RFC2347, RFC2348 and RFC2349 from read and write requests (RRQs/WRQs).

       This option improves interop with some legacy servers that do not acknowledge or properly implement TFTP options. When this option is used CURLOPT_TFTP_BLKSIZE(3) is ignored.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       TFTP

~~~
EXAMPLE
~~~
       size_t write_callback(char *ptr, size_t size, size_t nmemb, void *fp)
       {
         return fwrite(ptr, size, nmemb, (FILE *)fp);
       }

       CURL *curl = curl_easy_init();
       if(curl) {
         FILE *fp = fopen("foo.bin", "wb");
         if(fp) {
           curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)fp);
           curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);

           curl_easy_setopt(curl, CURLOPT_URL, "tftp://example.com/foo.bin");

           /* do not send TFTP options requests */
           curl_easy_setopt(curl, CURLOPT_TFTP_NO_OPTIONS, 1L);

           /* Perform the request */
           curl_easy_perform(curl);

           fclose(fp);
         }
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.48.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_TFTP_BLKSIZE(3),



libcurl 7.48.0                                                                                  23 Feb 2016                                                                      CURLOPT_TFTP_NO_OPTIONS(3)
CURLOPT_TIMECONDITION(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_TIMECONDITION(3)



NAME
~~~
       CURLOPT_TIMECONDITION - select condition for a time request

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TIMECONDITION, long cond);

~~~
DESCRIPTION
~~~
       Pass a long as parameter. This defines how the CURLOPT_TIMEVALUE(3) time value is treated. You can set this parameter to CURL_TIMECOND_IFMODSINCE or CURL_TIMECOND_IFUNMODSINCE.

       The  last modification time of a file is not always known and in such instances this feature will have no effect even if the given time condition would not have been met. curl_easy_getinfo(3) with
       the CURLINFO_CONDITION_UNMET option can be used after a transfer to learn if a zero-byte successful "transfer" was due to this condition not matching.

DEFAULT
       CURL_TIMECOND_NONE (0)

~~~
PROTOCOLS
~~~
       HTTP, FTP, RTSP, and FILE

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* January 1, 2020 is 1577833200 */
         curl_easy_setopt(curl, CURLOPT_TIMEVALUE, 1577833200L);

         /* If-Modified-Since the above time stamp */
         curl_easy_setopt(curl, CURLOPT_TIMECONDITION, CURL_TIMECOND_IFMODSINCE);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_TIMEVALUE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                        CURLOPT_TIMECONDITION(3)
CURLOPT_TIMEOUT(3)                                                                        curl_easy_setopt options                                                                       CURLOPT_TIMEOUT(3)



NAME
~~~
       CURLOPT_TIMEOUT - maximum time the transfer is allowed to complete

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TIMEOUT, long timeout);

~~~
DESCRIPTION
~~~
       Pass  a  long  as parameter containing timeout - the maximum time in seconds that you allow the libcurl transfer operation to take. Normally, name lookups can take a considerable time and limiting
       operations risk aborting perfectly normal operations. This option may cause libcurl to use the SIGALRM signal to timeout system calls.

       In unix-like systems, this might cause signals to be used unless CURLOPT_NOSIGNAL(3) is set.

       If both CURLOPT_TIMEOUT(3) and CURLOPT_TIMEOUT_MS(3) are set, the value set last will be used.

       Since this option puts a hard limit on how long time a request is allowed to take, it has limited use in dynamic use cases with varying transfer times. That is especially apparent when  using  the
       multi  interface,  which may queue the transfer, and that time is included. You are advised to explore CURLOPT_LOW_SPEED_LIMIT(3), CURLOPT_LOW_SPEED_TIME(3) or using CURLOPT_PROGRESSFUNCTION(3) to
       implement your own timeout logic.

DEFAULT
       Default timeout is 0 (zero) which means it never times out during transfer.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* complete within 20 seconds */
         curl_easy_setopt(curl, CURLOPT_TIMEOUT, 20L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK. Returns CURLE_BAD_FUNCTION_ARGUMENT if set to a negative value or a value that when converted to milliseconds is too large.

~~~
SEE ALSO
       CURLOPT_TIMEOUT_MS(3), CURLOPT_CONNECTTIMEOUT(3), CURLOPT_LOW_SPEED_LIMIT(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                              CURLOPT_TIMEOUT(3)
CURLOPT_TIMEOUT_MS(3)                                                                     curl_easy_setopt options                                                                    CURLOPT_TIMEOUT_MS(3)



NAME
~~~
       CURLOPT_TIMEOUT_MS - maximum time the transfer is allowed to complete

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TIMEOUT_MS, long timeout);

~~~
DESCRIPTION
~~~
       Pass a long as parameter containing timeout - the maximum time in milliseconds that you allow the libcurl transfer operation to take. Normally, name lookups can take a considerable time and limiting operations to less than a few minutes risk aborting perfectly normal operations. This option may cause libcurl to use the SIGALRM signal to timeout system calls.

       If libcurl is built to use the standard system name resolver, that portion of the transfer will still use full-second resolution for timeouts with a minimum timeout allowed of one second.

       In unix-like systems, this might cause signals to be used unless CURLOPT_NOSIGNAL(3) is set.

       If both CURLOPT_TIMEOUT(3) and CURLOPT_TIMEOUT_MS(3) are set, the value set last will be used.

       Since this puts a hard limit for how long time a request is allowed to take, it has limited use in  dynamic  use  cases  with  varying  transfer  times.  You  are  then  advised  to  explore  CURLOPT_LOW_SPEED_LIMIT(3), CURLOPT_LOW_SPEED_TIME(3) or using CURLOPT_PROGRESSFUNCTION(3) to implement your own timeout logic.

DEFAULT
       Default timeout is 0 (zero) which means it never times out during transfer.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* complete within 20000 milliseconds */
         curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, 20000L);

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_TIMEOUT(3), CURLOPT_CONNECTTIMEOUT(3), CURLOPT_LOW_SPEED_LIMIT(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                           CURLOPT_TIMEOUT_MS(3)
CURLOPT_TIMEVALUE(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_TIMEVALUE(3)



NAME
~~~
       CURLOPT_TIMEVALUE - time value for conditional

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TIMEVALUE, long val);

~~~
DESCRIPTION
~~~
       Pass a long val as parameter. This should be the time counted as seconds since 1 Jan 1970, and the time will be used in a condition as specified with CURLOPT_TIMECONDITION(3).

       On systems with 32 bit 'long' variables, this option cannot set dates beyond the year 2038. Consider CURLOPT_TIMEVALUE_LARGE(3) instead.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       HTTP, FTP, RTSP, and FILE

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* January 1, 2020 is 1577833200 */
         curl_easy_setopt(curl, CURLOPT_TIMEVALUE, 1577833200L);

         /* If-Modified-Since the above time stamp */
         curl_easy_setopt(curl, CURLOPT_TIMECONDITION, CURL_TIMECOND_IFMODSINCE);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_TIMECONDITION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                            CURLOPT_TIMEVALUE(3)
CURLOPT_TIMEVALUE_LARGE(3)                                                                curl_easy_setopt options                                                               CURLOPT_TIMEVALUE_LARGE(3)



NAME
~~~
       CURLOPT_TIMEVALUE_LARGE - time value for conditional

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TIMEVALUE_LARGE, curl_off_t val);

~~~
DESCRIPTION
~~~
       Pass a curl_off_t val as parameter. This should be the time counted as seconds since 1 Jan 1970, and the time will be used in a condition as specified with CURLOPT_TIMECONDITION(3).

       The difference between this option and CURLOPT_TIMEVALUE(3) is the type of the argument. On systems where 'long' is only 32 bit wide, this option has to be used to set dates beyond the year 2038.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       HTTP, FTP, RTSP, and FILE

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* January 1, 2020 is 1577833200 */
         curl_easy_setopt(curl, CURLOPT_TIMEVALUE_LARGE, (curl_off_t)1577833200);

         /* If-Modified-Since the above time stamp */
         curl_easy_setopt(curl, CURLOPT_TIMECONDITION, CURL_TIMECOND_IFMODSINCE);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.59.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_TIMECONDITION(3), CURLOPT_TIMEVALUE_LARGE(3),



libcurl 7.59.0                                                                                  25 Jan 2018                                                                      CURLOPT_TIMEVALUE_LARGE(3)
CURLOPT_TLS13_CIPHERS(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_TLS13_CIPHERS(3)



NAME
~~~
       CURLOPT_TLS13_CIPHERS - ciphers suites to use for TLS 1.3

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TLS13_CIPHERS, char *list);

~~~
DESCRIPTION
~~~
       Pass  a  char *, pointing to a null-terminated string holding the list of cipher suites to use for the TLS 1.3 connection. The list must be syntactically correct, it consists of one or more cipher
       suite strings separated by colons.

       You'll find more details about cipher lists on this URL:

        https://curl.se/docs/ssl-ciphers.html

       This option is currently used only when curl is built to use OpenSSL 1.1.1 or later. If you are using a different SSL backend you  can  try  setting  TLS  1.3  cipher  suites  by  using  the  CURLOPT_SSL_CIPHER_LIST option.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL, use internal default

~~~
PROTOCOLS
~~~
       All TLS based protocols: HTTPS, FTPS, IMAPS, POP3S, SMTPS etc.

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_TLS13_CIPHERS,
                          "TLS13-CHACHA20-POLY1305-SHA256");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.61.0.  Available when built with OpenSSL >= 1.1.1.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if supported, CURLE_NOT_BUILT_IN otherwise.

~~~
SEE ALSO
       CURLOPT_SSL_CIPHER_LIST(3), CURLOPT_SSLVERSION(3), CURLOPT_PROXY_SSL_CIPHER_LIST(3), CURLOPT_PROXY_TLS13_CIPHERS(3), CURLOPT_PROXY_SSLVERSION(3), CURLOPT_USE_SSL(3),



libcurl 7.61.0                                                                                  25 May 2018                                                                        CURLOPT_TLS13_CIPHERS(3)
CURLOPT_TLSAUTH_PASSWORD(3)                                                               curl_easy_setopt options                                                              CURLOPT_TLSAUTH_PASSWORD(3)



NAME
~~~
       CURLOPT_TLSAUTH_PASSWORD - password to use for TLS authentication

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TLSAUTH_PASSWORD, char *pwd);

~~~
DESCRIPTION
~~~
       Pass  a  char  * as parameter, which should point to the null-terminated password to use for the TLS authentication method specified with the CURLOPT_TLSAUTH_TYPE(3) option. Requires that the CURLOPT_TLSAUTH_USERNAME(3) option also be set.

       The application does not have to keep the string around after setting this option.

       This feature relies in TLS SRP which doesn't work with TLS 1.3.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS-based protocols

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_TLSAUTH_TYPE, "SRP");
         curl_easy_setopt(curl, CURLOPT_TLSAUTH_USERNAME, "user");
         curl_easy_setopt(curl, CURLOPT_TLSAUTH_PASSWORD, "secret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.21.4

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_TLSAUTH_TYPE(3), CURLOPT_TLSAUTH_USERNAME(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                     CURLOPT_TLSAUTH_PASSWORD(3)
CURLOPT_TLSAUTH_TYPE(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_TLSAUTH_TYPE(3)



NAME
~~~
       CURLOPT_TLSAUTH_TYPE - TLS authentication methods

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TLSAUTH_TYPE, char *type);

~~~
DESCRIPTION
~~~
       Pass a pointer to a null-terminated string as parameter. The string should be the method of the TLS authentication. Supported method is "SRP".


       SRP    TLS-SRP  authentication.  Secure Remote Password authentication for TLS is defined in RFC5054 and provides mutual authentication if both sides have a shared secret. To use TLS-SRP, you must
              also set the CURLOPT_TLSAUTH_USERNAME(3) and CURLOPT_TLSAUTH_PASSWORD(3) options.

              The application does not have to keep the string around after setting this option.

              TLS SRP doesn't work with TLS 1.3.

DEFAULT
       blank

~~~
PROTOCOLS
~~~
       All TLS-based protocols

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_TLSAUTH_TYPE, "SRP");
         curl_easy_setopt(curl, CURLOPT_TLSAUTH_USERNAME, "user");
         curl_easy_setopt(curl, CURLOPT_TLSAUTH_PASSWORD, "secret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       You need to build libcurl with GnuTLS or OpenSSL with TLS-SRP support for this to work. Added in 7.21.4

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_TLSAUTH_USERNAME(3), CURLOPT_TLSAUTH_PASSWORD(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                         CURLOPT_TLSAUTH_TYPE(3)
CURLOPT_TLSAUTH_USERNAME(3)                                                               curl_easy_setopt options                                                              CURLOPT_TLSAUTH_USERNAME(3)



NAME
~~~
       CURLOPT_TLSAUTH_USERNAME - user name to use for TLS authentication

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TLSAUTH_USERNAME, char *user);

~~~
DESCRIPTION
~~~
       Pass  a  char  * as parameter, which should point to the null-terminated username to use for the TLS authentication method specified with the CURLOPT_TLSAUTH_TYPE(3) option. Requires that the CURLOPT_TLSAUTH_PASSWORD(3) option also be set.

       The application does not have to keep the string around after setting this option.

       This feature relies in TLS SRP which doesn't work with TLS 1.3.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       All TLS-based protocols

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         curl_easy_setopt(curl, CURLOPT_TLSAUTH_TYPE, "SRP");
         curl_easy_setopt(curl, CURLOPT_TLSAUTH_USERNAME, "user");
         curl_easy_setopt(curl, CURLOPT_TLSAUTH_PASSWORD, "secret");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.21.4

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_TLSAUTH_TYPE(3), CURLOPT_TLSAUTH_PASSWORD(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                     CURLOPT_TLSAUTH_USERNAME(3)
CURLOPT_TRAILERDATA(3)                                                                    curl_easy_setopt options                                                                   CURLOPT_TRAILERDATA(3)



NAME
~~~
       CURLOPT_TRAILERDATA - pointer passed to trailing headers callback

~~~
NAME
~~~
       #include <curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TRAILERDATA, void *userdata);

~~~
DESCRIPTION
~~~
       Data pointer to be passed to the HTTP trailer callback function.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       /* Assuming we have a CURL handle in the hndl variable. */

       struct MyData data;

       curl_easy_setopt(hndl, CURLOPT_TRAILERDATA, &data);

       A more complete example can be found in examples/http_trailers.html

~~~
AVAILABILITY
~~~
       This option was added in curl 7.64.0 and is present if HTTP support is enabled

~~~
RETURN VALUE
~~~
       Returns CURLE_OK.

~~~
SEE ALSO
       CURLOPT_TRAILERFUNCTION(3),



libcurl 7.64.0                                                                                  14 Aug 2018                                                                          CURLOPT_TRAILERDATA(3)
CURLOPT_TRAILERFUNCTION(3)                                                                curl_easy_setopt options                                                               CURLOPT_TRAILERFUNCTION(3)



NAME
~~~
       CURLOPT_TRAILERFUNCTION - callback for sending trailing headers

~~~
NAME
~~~
       #include <curl.h>

       int curl_trailer_callback(struct curl_slist ** list, void *userdata);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TRAILERFUNCTION, curl_trailer_callback *func);

~~~
DESCRIPTION
~~~
       Pass a pointer to a callback function.

       This callback function will be called once right before sending the final CR LF in an HTTP chunked transfer to fill a list of trailing headers to be sent before finishing the HTTP transfer.

       You can set the userdata argument with the CURLOPT_TRAILERDATA option.

       The trailing headers included in the linked list must not be CRLF-terminated, because libcurl will add the appropriate line termination characters after each header item.

       If you use curl_slist_append to add trailing headers to the curl_slist then libcurl will duplicate the strings, and will free the curl_slist and the duplicates once the trailers have been sent.

       If one of the trailing headers is not formatted correctly (i.e. HeaderName: headerdata) it will be ignored and an info message will be emitted.

       The return value can either be CURL_TRAILERFUNC_OK or CURL_TRAILERFUNC_ABORT which would respectively instruct libcurl to either continue with sending the trailers or to abort the request.

       If you set this option to NULL, then the transfer proceeds as usual without any interruptions.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       #include <curl/curl.h>

       static int trailer_cb(struct curl_slist **tr, void *data) {
         /* libcurl will free the list */
         tr = curl_slist_append(*tr, "My-super-awesome-trailer: trailer-stuff");
         return CURL_TRAILERFUNC_OK; }

       CURL *curl = curl_easy_init(); if(curl) {
         /* Set the URL of the request */
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/");
         /* Now set it as a put */
         curl_easy_setopt(curl, CURLOPT_PUT, 1L);

         /* Assuming we have a function that will return the data to be pushed
            Let that function be read_cb */
         curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_cb);

         struct curl_slist *headers = NULL;
         headers = curl_slist_append(headers, "Trailer: My-super-awesome-trailer");
         res = curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

         /* Set the trailers filling callback */
         curl_easy_setopt(curl, CURLOPT_TRAILERFUNCTION, trailer_cb);

         /* Perform the request, res will get the return code */
         res = curl_easy_perform(curl);

         curl_easy_cleanup(curl);

         curl_slist_free_all(headers); }

~~~
AVAILABILITY
~~~
       This option was added in curl 7.64.0 and is present if HTTP support is enabled

~~~
RETURN VALUE
~~~
       Returns CURLE_OK.

~~~
SEE ALSO
       CURLOPT_TRAILERDATA(3),



libcurl 7.64.0                                                                                  14 Aug 2018                                                                      CURLOPT_TRAILERFUNCTION(3)
CURLOPT_TRANSFER_ENCODING(3)                                                              curl_easy_setopt options                                                             CURLOPT_TRANSFER_ENCODING(3)



NAME
~~~
       CURLOPT_TRANSFER_ENCODING - ask for HTTP Transfer Encoding

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TRANSFER_ENCODING, long enable);

~~~
DESCRIPTION
~~~
       Pass a long set to 1L to enable or 0 to disable.

       Adds  a  request  for compressed Transfer Encoding in the outgoing HTTP request. If the server supports this and so desires, it can respond with the HTTP response sent using a compressed Transfer-
       Encoding that will be automatically uncompressed by libcurl on reception.

       Transfer-Encoding differs slightly from the Content-Encoding you ask for with CURLOPT_ACCEPT_ENCODING(3) in that a Transfer-Encoding is strictly meant to be for  the  transfer  and  thus  MUST  be
       decoded before the data arrives in the client. Traditionally, Transfer-Encoding has been much less used and supported by both HTTP clients and HTTP servers.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_TRANSFER_ENCODING, 1L);
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.21.6

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_ACCEPT_ENCODING(3), CURLOPT_HTTP_TRANSFER_DECODING(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                    CURLOPT_TRANSFER_ENCODING(3)
CURLOPT_TRANSFERTEXT(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_TRANSFERTEXT(3)



NAME
~~~
       CURLOPT_TRANSFERTEXT - request a text based transfer for FTP

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TRANSFERTEXT, long text);

~~~
DESCRIPTION
~~~
       A  parameter set to 1 tells the library to use ASCII mode for FTP transfers, instead of the default binary transfer. For win32 systems it does not set the stdout to binary mode. This option can be
       usable when transferring text data between systems with different views on certain characters, such as newlines or similar.

       libcurl does not do a complete ASCII conversion when doing ASCII transfers over FTP. This is a known limitation/flaw that nobody has rectified. libcurl simply sets the mode to ASCII and performs a
       standard transfer.

DEFAULT
       0, disabled

~~~
PROTOCOLS
~~~
       FTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/textfile");
         curl_easy_setopt(curl, CURLOPT_TRANSFERTEXT, 1L);
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Along with FTP

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if FTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_CRLF(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                         CURLOPT_TRANSFERTEXT(3)
CURLOPT_UNIX_SOCKET_PATH(3)                                                               curl_easy_setopt options                                                              CURLOPT_UNIX_SOCKET_PATH(3)



NAME
~~~
       CURLOPT_UNIX_SOCKET_PATH - Unix domain socket

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_UNIX_SOCKET_PATH, char *path);

~~~
DESCRIPTION
~~~
       Enables  the  use  of  Unix domain sockets as connection endpoint and sets the path to path. If path is NULL, then Unix domain sockets are disabled. An empty string will result in an error at some
       point, it will not disable use of Unix domain sockets.

       When enabled, curl will connect to the Unix domain socket instead of establishing a TCP connection to a host. Since no TCP connection is created, curl does not need to resolve the DNS hostname  in
       the URL.

       The maximum path length on Cygwin, Linux and Solaris is 107. On other platforms it might be even less.

       Proxy and TCP options such as CURLOPT_TCP_NODELAY(3) are not supported. Proxy options such as CURLOPT_PROXY(3) have no effect either as these are TCP-oriented, and asking a proxy server to connect
       to a certain Unix domain socket is not possible.

       The application does not have to keep the string around after setting this option.

DEFAULT
       Default is NULL, meaning that no Unix domain sockets are used.

~~~
PROTOCOLS
~~~
       All protocols except for FILE and FTP are supported in theory. HTTP, IMAP, POP3 and SMTP should in particular work (including their SSL/TLS variants).

~~~
EXAMPLE
~~~
       Given that you have an HTTP server running listening on /tmp/httpd.sock, you can request an HTTP resource with:

         curl_easy_setopt(curl_handle, CURLOPT_UNIX_SOCKET_PATH, "/tmp/httpd.sock");
         curl_easy_setopt(curl_handle, CURLOPT_URL, "http://localhost/");

       If you are on Linux and somehow have a need for paths larger than 107 bytes, you could use the proc filesystem to bypass the limitation:

         int dirfd = open(long_directory_path_to_socket, O_DIRECTORY | O_RDONLY);
         char path[108];
         snprintf(path, sizeof(path), "/proc/self/fd/%d/httpd.sock", dirfd);
         curl_easy_setopt(curl_handle, CURLOPT_UNIX_SOCKET_PATH, path);
         /* Be sure to keep dirfd valid until you discard the handle */

~~~
AVAILABILITY
~~~
       Since 7.40.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_ABSTRACT_UNIX_SOCKET(3), CURLOPT_OPENSOCKETFUNCTION(3), unix(7),



libcurl 7.40.0                                                                                  09 Oct 2014                                                                     CURLOPT_UNIX_SOCKET_PATH(3)
CURLOPT_UNRESTRICTED_AUTH(3)                                                              curl_easy_setopt options                                                             CURLOPT_UNRESTRICTED_AUTH(3)



NAME
~~~
       CURLOPT_UNRESTRICTED_AUTH - send credentials to other hosts too

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_UNRESTRICTED_AUTH,
                                 long goahead);

~~~
DESCRIPTION
~~~
       Set the long gohead parameter to 1L to make libcurl continue to send authentication (user+password) credentials when following locations, even when hostname changed. This option is meaningful only
       when setting CURLOPT_FOLLOWLOCATION(3).

       By default, libcurl will only send given credentials to the initial host name as given in the original URL, to avoid leaking username + password to other sites.

DEFAULT
       0

~~~
PROTOCOLS
~~~
       HTTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");
         curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
         curl_easy_setopt(curl, CURLOPT_UNRESTRICTED_AUTH, 1L);
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Along with HTTP

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_FOLLOWLOCATION(3), CURLOPT_USERPWD(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                    CURLOPT_UNRESTRICTED_AUTH(3)
CURLOPT_UPKEEP_INTERVAL_MS(3)                                                             curl_easy_setopt options                                                            CURLOPT_UPKEEP_INTERVAL_MS(3)



NAME
~~~
       CURLOPT_UPKEEP_INTERVAL_MS - connection upkeep interval

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_UPKEEP_INTERVAL_MS, long upkeep_interval_ms);

~~~
DESCRIPTION
~~~
       Some  protocols  have "connection upkeep" mechanisms. These mechanisms usually send some traffic on existing connections in order to keep them alive; this can prevent connections from being closed
       due to overzealous firewalls, for example.

       The user needs to explicitly call curl_easy_upkeep(3) in order to perform the upkeep work.

       Currently the only protocol with a connection upkeep mechanism is HTTP/2: when the connection upkeep interval is exceeded and curl_easy_upkeep(3) is called, an HTTP/2 PING frame  is  sent  on  the
       connection.


DEFAULT
       CURL_UPKEEP_INTERVAL_DEFAULT (currently defined as 60000L, which is 60 seconds)

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         /* Make a connection to an HTTP/2 server. */
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* Set the interval to 30000ms / 30s */
         curl_easy_setopt(curl, CURLOPT_UPKEEP_INTERVAL_MS, 30000L);

         curl_easy_perform(curl);

         /* Perform more work here. */

         /* While the connection is being held open, curl_easy_upkeep() can be
            called. If curl_easy_upkeep() is called and the time since the last
            upkeep exceeds the interval, then an HTTP/2 PING is sent. */
         curl_easy_upkeep(curl);

         /* Perform more work here. */

         /* always cleanup */
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.62.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_TCP_KEEPALIVE(3),




libcurl 7.62.0                                                                                  31 Oct 2018                                                                   CURLOPT_UPKEEP_INTERVAL_MS(3)
CURLOPT_UPLOAD(3)                                                                         curl_easy_setopt options                                                                        CURLOPT_UPLOAD(3)



NAME
~~~
       CURLOPT_UPLOAD - data upload

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_UPLOAD, long upload);

~~~
DESCRIPTION
~~~
       The  long parameter upload set to 1 tells the library to prepare for and perform an upload. The CURLOPT_READDATA(3) and CURLOPT_INFILESIZE(3) or CURLOPT_INFILESIZE_LARGE(3) options are also interesting for uploads. If the protocol is HTTP, uploading means using the PUT request unless you tell libcurl otherwise.

       Using PUT with HTTP 1.1 implies the use of a "Expect: 100-continue" header.  You can disable this header with CURLOPT_HTTPHEADER(3) as usual.

       If you use PUT to an HTTP 1.1 server, you can upload data without knowing the size before starting the transfer if you use chunked encoding. You enable this by  adding  a  header  like  "Transfer-
       Encoding: chunked" with CURLOPT_HTTPHEADER(3). With HTTP 1.0 or without chunked transfer, you must specify the size.

DEFAULT
       0, default is download

~~~
PROTOCOLS
~~~
       Most

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         /* we want to use our own read function */
         curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);

         /* enable uploading */
         curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);

         /* specify target */
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/dir/to/newfile");

         /* now specify which pointer to pass to our callback */
         curl_easy_setopt(curl, CURLOPT_READDATA, hd_src);

         /* Set the size of the file to upload */
         curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, (curl_off_t)fsize);

         /* Now run off and do what you've been told! */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_PUT(3), CURLOPT_READFUNCTION(3), CURLOPT_INFILESIZE_LARGE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                               CURLOPT_UPLOAD(3)
CURLOPT_UPLOAD_BUFFERSIZE(3)                                                              curl_easy_setopt options                                                             CURLOPT_UPLOAD_BUFFERSIZE(3)



NAME
~~~
       CURLOPT_UPLOAD_BUFFERSIZE - upload buffer size

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_UPLOAD_BUFFERSIZE, long size);

~~~
DESCRIPTION
~~~
       Pass  a  long  specifying your preferred size (in bytes) for the upload buffer in libcurl. It makes libcurl uses a larger buffer that gets passed to the next layer in the stack to get sent off. In
       some setups and for some protocols, there's a huge performance benefit of having a larger upload buffer.

       This is just treated as a request, not an order. You cannot be guaranteed to actually get the given size.

       The upload buffer size is by default 64 kilobytes. The maximum buffer size allowed to be set is 2 megabytes. The minimum buffer size allowed to be set is 16 kilobytes.

       Since curl 7.61.1 the upload buffer is allocated on-demand - so if the handle isn't used for upload, this buffer will not be allocated at all.

       DO NOT set this option on a handle that is currently used for an active transfer as that may lead to unintended consequences.

DEFAULT
       64 kB

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "sftp://example.com/foo.bin");

         /* ask libcurl to allocate a larger upload buffer */
         curl_easy_setopt(curl, CURLOPT_UPLOAD_BUFFERSIZE, 120000L);

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.62.0.

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_BUFFERSIZE(3), CURLOPT_READFUNCTION(3),



libcurl 7.62.0                                                                                  18 Aug 2018                                                                    CURLOPT_UPLOAD_BUFFERSIZE(3)
CURLOPT_URL(3)                                                                            curl_easy_setopt options                                                                           CURLOPT_URL(3)



NAME
~~~
       CURLOPT_URL - provide the URL to use in the request

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_URL, char *URL);

~~~
DESCRIPTION
~~~
       Pass in a pointer to the URL to work with. The parameter should be a char * to a null-terminated string which must be URL-encoded in the following format:

       scheme://host:port/path

       For a greater explanation of the format please see RFC3986.

       libcurl doesn't validate the syntax or use this variable until the transfer is issued. Even if you set a crazy value here, curl_easy_setopt(3) will still return CURLE_OK.

       If the given URL is missing a scheme name (such as "http://" or "ftp://" etc) then libcurl will make a guess based on the host. If the outermost sub-domain name matches DICT, FTP, IMAP, LDAP, POP3
       or SMTP then that protocol will be used, otherwise HTTP will be used. Since 7.45.0 guessing can be disabled by setting a default protocol, see CURLOPT_DEFAULT_PROTOCOL(3) for details.

       Should the protocol, either that specified by the scheme or deduced by libcurl from the host name, not be supported by libcurl then CURLE_UNSUPPORTED_PROTOCOL will  be  returned  from  either  the
       curl_easy_perform(3)  or  curl_multi_perform(3)  functions  when  you  call them. Use curl_version_info(3) for detailed information of which protocols are supported by the build of libcurl you are
       using.

       CURLOPT_PROTOCOLS(3) can be used to limit what protocols libcurl will use for this transfer, independent of what libcurl has been compiled to support. That may be useful if you accept the URL from
       an external source and want to limit the accessibility.

       The CURLOPT_URL(3) string will be ignored if CURLOPT_CURLU(3) is set.

       CURLOPT_URL(3) or CURLOPT_CURLU(3) must be set before a transfer is started.

       The application does not have to keep the string around after setting this option.

ENCODING
       The string pointed to in the CURLOPT_URL(3) argument is generally expected to be a sequence of characters using an ASCII compatible encoding.

       If  libcurl  is built with IDN support, the server name part of the URL can use an "international name" by using the current encoding (according to locale) or UTF-8 (when winidn is used; or a Windows Unicode build using libidn2).

       If libcurl is built without IDN support, the server name is used exactly as specified when passed to the name resolver functions.

DEFAULT
       There is no default URL. If this option isn't set, no transfer can be performed.

SECURITY CONCERNS
       Applications may at times find it convenient to allow users to specify URLs for various purposes and that string would then end up fed to this option.

       Getting a URL from an external untrusted party will bring reasons for several security concerns:

       If you have an application that runs as or in a server application, getting an unfiltered URL can easily trick your application to access a local resource instead of a remote. Protecting  yourself
       against localhost accesses is very hard when accepting user provided URLs.

       Such  custom URLs can also access other ports than you planned as port numbers are part of the regular URL format. The combination of a local host and a custom port number can allow external users
       to play tricks with your local services.

       Accepting external URLs may also use other protocols than http:// or other common ones. Restrict what accept with CURLOPT_PROTOCOLS(3).

       User provided URLs can also be made to point to sites that redirect further on (possibly to other protocols too). Consider your CURLOPT_FOLLOWLOCATION(3) and CURLOPT_REDIR_PROTOCOLS(3) settings.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       POP3 and SMTP were added in 7.31.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK on success or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

       Note that curl_easy_setopt(3) won't actually parse the given string so given a bad URL, it will not be detected until curl_easy_perform(3) or similar is called.

~~~
SEE ALSO
       CURLOPT_VERBOSE(3), CURLOPT_PROTOCOLS(3), CURLOPT_FORBID_REUSE(3), CURLOPT_FRESH_CONNECT(3), curl_easy_perform(3), CURLINFO_REDIRECT_URL(3), CURLOPT_PATH_AS_IS(3), CURLOPT_CURLU(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                                  CURLOPT_URL(3)
CURLOPT_USERAGENT(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_USERAGENT(3)



NAME
~~~
       CURLOPT_USERAGENT - HTTP user-agent header

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_USERAGENT, char *ua);

~~~
DESCRIPTION
~~~
       Pass  a  pointer to a null-terminated string as parameter. It will be used to set the User-Agent: header in the HTTP request sent to the remote server. This can be used to fool servers or scripts.
       You can also set any custom header with CURLOPT_HTTPHEADER(3).

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL, no User-Agent: header is used by default.

~~~
PROTOCOLS
~~~
       HTTP, HTTPS

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         curl_easy_setopt(curl, CURLOPT_USERAGENT, "Dark Secret Ninja/1.0");

         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       As long as HTTP is supported

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if HTTP is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_REFERER(3), CURLOPT_HTTPHEADER(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                            CURLOPT_USERAGENT(3)
CURLOPT_USERNAME(3)                                                                       curl_easy_setopt options                                                                      CURLOPT_USERNAME(3)



NAME
~~~
       CURLOPT_USERNAME - user name to use in authentication

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_USERNAME,
                                 char *username);

~~~
DESCRIPTION
~~~
       Pass a char * as parameter, which should be pointing to the null-terminated user name to use for the transfer.

       CURLOPT_USERNAME(3) sets the user name to be used in protocol authentication. You should not use this option together with the (older) CURLOPT_USERPWD(3) option.

       When  using  Kerberos V5 authentication with a Windows based server, you should include the domain name in order for the server to successfully obtain a Kerberos Ticket. If you don't then the initial part of the authentication handshake may fail.

       When using NTLM, the user name can be specified simply as the user name without the domain name should the server be part of a single domain and forest.

       To include the domain name use either Down-Level Logon Name or UPN (User Principal Name) formats. For example, ~~~
EXAMPLE
~~~\user and user@example.com respectively.

       Some HTTP servers (on Windows) support inclusion of the domain for Basic authentication as well.

       To specify the password and login options, along with the user name, use the CURLOPT_PASSWORD(3) and CURLOPT_LOGIN_OPTIONS(3) options.

       The application does not have to keep the string around after setting this option.

DEFAULT
       blank

~~~
PROTOCOLS
~~~
       Most

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

         curl_easy_setopt(curl, CURLOPT_USERNAME, "clark");

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.19.1

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_USERPWD(3), CURLOPT_PASSWORD(3), CURLOPT_HTTPAUTH(3), CURLOPT_PROXYAUTH(3)



libcurl 7.37.0                                                                                  19 Jun 2014                                                                             CURLOPT_USERNAME(3)
CURLOPT_USERPWD(3)                                                                        curl_easy_setopt options                                                                       CURLOPT_USERPWD(3)



NAME
~~~
       CURLOPT_USERPWD - user name and password to use in authentication

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_USERPWD, char *userpwd);

~~~
DESCRIPTION
~~~
       Pass a char * as parameter, pointing to a null-terminated login details string for the connection. The format of which is: [user name]:[password].

       When  using  Kerberos V5 authentication with a Windows based server, you should specify the user name part with the domain name in order for the server to successfully obtain a Kerberos Ticket. If
       you don't then the initial part of the authentication handshake may fail.

       When using NTLM, the user name can be specified simply as the user name without the domain name should the server be part of a single domain and forest.

       To specify the domain name use either Down-Level Logon Name or UPN (User Principal Name) formats. For example, ~~~
EXAMPLE
~~~\user and user@example.com respectively.

       Some HTTP servers (on Windows) support inclusion of the domain for Basic authentication as well.

       When using HTTP and CURLOPT_FOLLOWLOCATION(3), libcurl might perform several requests to possibly different hosts. libcurl will only send this user and password information to hosts using the initial  host name (unless CURLOPT_UNRESTRICTED_AUTH(3) is set), so if libcurl follows locations to other hosts it will not send the user and password to those. This is enforced to prevent accidental
       information leakage.

       Use CURLOPT_HTTPAUTH(3) to specify the authentication method for HTTP based connections or CURLOPT_LOGIN_OPTIONS(3) to control IMAP, POP3 and SMTP options.

       The user and password strings are not URL decoded, so there's no way to send in a user name containing a colon using this option. Use CURLOPT_USERNAME(3) for that, or include it in the URL.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       Most

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com/foo.bin");

         curl_easy_setopt(curl, CURLOPT_USERPWD, "clark:kent");

         ret = curl_easy_perform(curl);

         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK on success or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_USERNAME(3), CURLOPT_PASSWORD(3), CURLOPT_PROXYUSERPWD(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                              CURLOPT_USERPWD(3)
CURLOPT_USE_SSL(3)                                                                        curl_easy_setopt options                                                                       CURLOPT_USE_SSL(3)



NAME
~~~
       CURLOPT_USE_SSL - request using SSL / TLS for the transfer

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_USE_SSL, long level);

~~~
DESCRIPTION
~~~
       Pass a long using one of the values from below, to make libcurl use your desired level of SSL for the transfer.

       These are all protocols that start out plain text and get "upgraded" to SSL using the STARTTLS command.

       This is for enabling SSL/TLS when you use FTP, SMTP, POP3, IMAP etc.

       CURLUSESSL_NONE
              Don't attempt to use SSL.

       CURLUSESSL_TRY
              Try using SSL, proceed as normal otherwise.

       CURLUSESSL_CONTROL
              Require SSL for the control connection or fail with CURLE_USE_SSL_FAILED.

       CURLUSESSL_ALL
              Require SSL for all communication or fail with CURLE_USE_SSL_FAILED.

DEFAULT
       CURLUSESSL_NONE

~~~
PROTOCOLS
~~~
       FTP, SMTP, POP3, IMAP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "ftp://example.com/dir/file.ext");

         /* require use of SSL for this, or fail */
         curl_easy_setopt(curl, CURLOPT_USE_SSL, CURLUSESSL_ALL);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.11.0. This option was known as CURLOPT_FTP_SSL up to 7.16.4, and the constants were known as CURLFTPSSL_*

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_SSLVERSION(3), CURLOPT_PROXY_SSLVERSION(3), CURLOPT_SSL_OPTIONS(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                              CURLOPT_USE_SSL(3)
CURLOPT_VERBOSE(3)                                                                        curl_easy_setopt options                                                                       CURLOPT_VERBOSE(3)



NAME
~~~
       CURLOPT_VERBOSE - verbose mode

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_VERBOSE, long onoff);

~~~
DESCRIPTION
~~~
       Set  the onoff parameter to 1 to make the library display a lot of verbose information about its operations on this handle. Very useful for libcurl and/or protocol debugging and understanding. The
       verbose information will be sent to stderr, or the stream set with CURLOPT_STDERR(3).

       You hardly ever want this set in production use, you will almost always want this when you debug/report problems.

       To also get all the protocol data sent and received, consider using the CURLOPT_DEBUGFUNCTION(3).

DEFAULT
       0, meaning disabled.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "https://example.com");

         /* ask libcurl to show us the verbose output */
         curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

         /* Perform the request */
         curl_easy_perform(curl);
       }

~~~
AVAILABILITY
~~~
       Always

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_STDERR(3), CURLOPT_DEBUGFUNCTION(3), CURLOPT_ERRORBUFFER(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                              CURLOPT_VERBOSE(3)
CURLOPT_WILDCARDMATCH(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_WILDCARDMATCH(3)



NAME
~~~
       CURLOPT_WILDCARDMATCH - directory wildcard transfers

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_WILDCARDMATCH, long onoff);

~~~
DESCRIPTION
~~~
       Set onoff to 1 if you want to transfer multiple files according to a file name pattern. The pattern can be specified as part of the CURLOPT_URL(3) option, using an fnmatch-like pattern (Shell Pattern Matching) in the last part of URL (file name).

       By default, libcurl uses its internal wildcard matching implementation. You can provide your own matching function by the CURLOPT_FNMATCH_FUNCTION(3) option.

       A brief introduction of its syntax follows:

              * - ASTERISK
                     ftp://example.com/some/path/*.txt (for all txt's from the root directory). Only two asterisks are allowed within the same pattern string.

              ? - QUESTION MARK
                     Question mark matches any (exactly one) character.

                     ftp://example.com/some/path/photo?.jpeg

              [ - BRACKET EXPRESSION
                     The left bracket opens a bracket expression. The question mark and asterisk have no special meaning in a bracket expression. Each bracket expression ends by  the  right  bracket  and
                     matches exactly one character. Some examples follow:

                     [a-zA-Z0-9] or [f-gF-G] - character interval

                     [abc] - character enumeration

                     [^abc] or [!abc] - negation

                     [[:name:]] class expression. Supported classes are alnum,lower, space, alpha, digit, print, upper, blank, graph, xdigit.

                     [][-!^] - special case - matches only '-', ']', '[', '!' or '^'. These characters have no special purpose.

                     [\[\]\\] - escape syntax. Matches '[', ']' or '\'.

                     Using the rules above, a file name pattern can be constructed:

                     ftp://example.com/some/path/[a-z[:upper:]\\].jpeg

~~~
PROTOCOLS
~~~
       This feature is only supported for FTP download.

~~~
EXAMPLE
~~~
         /* initialization of easy handle */
         handle = curl_easy_init();

         /* turn on wildcard matching */
         curl_easy_setopt(handle, CURLOPT_WILDCARDMATCH, 1L);

         /* callback is called before download of concrete file started */
         curl_easy_setopt(handle, CURLOPT_CHUNK_BGN_FUNCTION, file_is_coming);

         /* callback is called after data from the file have been transferred */
         curl_easy_setopt(handle, CURLOPT_CHUNK_END_FUNCTION, file_is_downloaded);

         /* See more on https://curl.se/libcurl/c/ftp-wildcard.html */

~~~
AVAILABILITY
~~~
       Added in 7.21.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_FNMATCH_FUNCTION(3), CURLOPT_URL(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                        CURLOPT_WILDCARDMATCH(3)
CURLOPT_WRITEDATA(3)                                                                      curl_easy_setopt options                                                                     CURLOPT_WRITEDATA(3)



NAME
~~~
       CURLOPT_WRITEDATA - custom pointer passed to the write callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_WRITEDATA, void *pointer);

~~~
DESCRIPTION
~~~
       A data pointer to pass to the write callback. If you use the CURLOPT_WRITEFUNCTION(3) option, this is the pointer you'll get in that callback's 4th argument. If you don't use a write callback, you
       must make pointer a 'FILE *' (cast to 'void *') as libcurl will pass this to fwrite(3) when writing data.

       The internal CURLOPT_WRITEFUNCTION(3) will write the data to the FILE * given with this option, or to stdout if this option hasn't been set.

       If you're using libcurl as a win32 DLL, you MUST use a CURLOPT_WRITEFUNCTION(3) if you set this option or you will experience crashes.

DEFAULT
       By default, this is a FILE * to stdout.

~~~
PROTOCOLS
~~~
       Used for all protocols.

~~~
EXAMPLE
~~~
       A common technique is to use the write callback to store the incoming data into a dynamically growing allocated buffer, and then this CURLOPT_WRITEDATA(3) is used to point to a struct or the  buffer to store data in. Like in the getinmemory example: https://curl.se/libcurl/c/getinmemory.html

~~~
AVAILABILITY
~~~
       Available in all libcurl versions. This option was formerly known as CURLOPT_FILE, the name CURLOPT_WRITEDATA(3) was introduced in 7.9.7.

~~~
RETURN VALUE
~~~
       This will return CURLE_OK.

~~~
SEE ALSO
       CURLOPT_WRITEFUNCTION(3), CURLOPT_READDATA(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                            CURLOPT_WRITEDATA(3)
CURLOPT_WRITEFUNCTION(3)                                                                  curl_easy_setopt options                                                                 CURLOPT_WRITEFUNCTION(3)



NAME
~~~
       CURLOPT_WRITEFUNCTION - callback for writing received data

~~~
NAME
~~~
       #include <curl/curl.h>

       size_t write_callback(char *ptr, size_t size, size_t nmemb, void *userdata);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_WRITEFUNCTION, write_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       This  callback  function  gets called by libcurl as soon as there is data received that needs to be saved. For most transfers, this callback gets called many times and each invoke delivers another
       chunk of data. ptr points to the delivered data, and the size of that data is nmemb; size is always 1.

       The callback function will be passed as much data as possible in all invokes, but you must not make any assumptions. It may be one byte, it may be thousands. The maximum amount of body  data  that
       will  be  passed  to the write callback is defined in the curl.h header file: CURL_MAX_WRITE_SIZE (the usual default is 16K). If CURLOPT_HEADER(3) is enabled, which makes header data get passed to
       the write callback, you can get up to CURL_MAX_HTTP_HEADER bytes of header data passed into it. This usually means 100K.

       This function may be called with zero bytes data if the transferred file is empty.

       The data passed to this function will not be null-terminated!

       Set the userdata argument with the CURLOPT_WRITEDATA(3) option.

       Your callback should return the number of bytes actually taken care of. If that amount differs from the amount passed to your callback function, it'll signal an error  condition  to  the  library.
       This will cause the transfer to get aborted and the libcurl function used will return CURLE_WRITE_ERROR.

       If your callback function returns CURL_WRITEFUNC_PAUSE it will cause this transfer to become paused.  See curl_easy_pause(3) for further details.

       Set this option to NULL to get the internal default function used instead of your callback. The internal default function will write the data to the FILE * given with CURLOPT_WRITEDATA(3).

       This option doesn't enable HSTS, you need to use CURLOPT_HSTS_CTRL(3) to do that.

DEFAULT
       libcurl will use 'fwrite' as a callback by default.

~~~
PROTOCOLS
~~~
       For all protocols

~~~
EXAMPLE
~~~
        struct memory {
          char *response;
          size_t size;
        };

        static size_t cb(void *data, size_t size, size_t nmemb, void *userp)
        {
          size_t realsize = size * nmemb;
          struct memory *mem = (struct memory *)userp;

          char *ptr = realloc(mem->response, mem->size + realsize + 1);
          if(ptr == NULL)
            return 0;  /* out of memory! */

          mem->response = ptr;
          memcpy(&(mem->response[mem->size]), data, realsize);
          mem->size += realsize;
          mem->response[mem->size] = 0;

          return realsize;
        }

        struct memory chunk = {0};

        /* send all data to this function  */
        curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, cb);

        /* we pass our 'chunk' struct to the callback function */
        curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);

~~~
AVAILABILITY
~~~
       Support for the CURL_WRITEFUNC_PAUSE return code was added in version 7.18.0.

~~~
RETURN VALUE
~~~
       This will return CURLE_OK.

~~~
SEE ALSO
       CURLOPT_WRITEDATA(3), CURLOPT_READFUNCTION(3), CURLOPT_HEADERFUNCTION(3),



libcurl 7.37.0                                                                                  16 Jun 2014                                                                        CURLOPT_WRITEFUNCTION(3)
CURLOPT_XFERINFODATA(3)                                                                   curl_easy_setopt options                                                                  CURLOPT_XFERINFODATA(3)



NAME
~~~
       CURLOPT_XFERINFODATA - custom pointer passed to the progress callback

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_XFERINFODATA, void *pointer);

~~~
DESCRIPTION
~~~
       Pass a pointer that will be untouched by libcurl and passed as the first argument in the progress callback set with CURLOPT_XFERINFOFUNCTION(3).

       This is an alias for CURLOPT_PROGRESSDATA(3).

DEFAULT
       The default value of this parameter is NULL.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
        struct progress {
          char *private;
          size_t size;
        };

        static size_t progress_callback(void *clientp,
                                        curl_off_t dltotal,
                                        curl_off_t dlnow,
                                        curl_off_t ultotal,
                                        curl_off_t ulnow)
        {
          struct memory *progress = (struct progress *)userp;

          /* use the values */

          return 0; /* all is good */
        }

        struct progress data;

        /* pass struct to callback  */
        curl_easy_setopt(curl_handle, CURLOPT_XFERINFODATA, &data);

        curl_easy_setopt(curl_handle, CURLOPT_XFERINFOFUNCTION, progress_callback);

~~~
AVAILABILITY
~~~
       Added in 7.32.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK

~~~
SEE ALSO
       CURLOPT_XFERINFOFUNCTION(3), CURLOPT_VERBOSE(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                         CURLOPT_XFERINFODATA(3)
CURLOPT_XFERINFOFUNCTION(3)                                                               curl_easy_setopt options                                                              CURLOPT_XFERINFOFUNCTION(3)



NAME
~~~
       CURLOPT_XFERINFOFUNCTION - progress meter callback

~~~
NAME
~~~
       #include <curl/curl.h>

       int progress_callback(void *clientp,
                             curl_off_t dltotal,
                             curl_off_t dlnow,
                             curl_off_t ultotal,
                             curl_off_t ulnow);

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_XFERINFOFUNCTION, progress_callback);

~~~
DESCRIPTION
~~~
       Pass a pointer to your callback function, which should match the prototype shown above.

       This  function  gets  called by libcurl instead of its internal equivalent with a frequent interval. While data is being transferred it will be called very frequently, and during slow periods like
       when nothing is being transferred it can slow down to about one call per second.

       clientp is the pointer set with CURLOPT_XFERINFODATA(3), it is not used by libcurl but is only passed along from the application to the callback.

       The callback gets told how much data libcurl will transfer and has transferred, in number of bytes. dltotal is the total number of bytes libcurl expects to download in this transfer. dlnow is  the
       number of bytes downloaded so far. ultotal is the total number of bytes libcurl expects to upload in this transfer. ulnow is the number of bytes uploaded so far.

       Unknown/unused  argument values passed to the callback will be set to zero (like if you only download data, the upload size will remain 0). Many times the callback will be called one or more times
       first, before it knows the data sizes so a program must be made to handle that.

       If your callback function returns CURL_PROGRESSFUNC_CONTINUE it will cause libcurl to continue executing the default progress function.

       Returning any other non-zero value from this callback will cause libcurl to abort the transfer and return CURLE_ABORTED_BY_CALLBACK.

       If you transfer data with the multi interface, this function will not be called during periods of idleness unless you call the appropriate libcurl function that performs transfers.

       CURLOPT_NOPROGRESS(3) must be set to 0 to make this function actually get called.

DEFAULT
       By default, libcurl has an internal progress meter. That's rarely wanted by users.

~~~
PROTOCOLS
~~~
       All

~~~
EXAMPLE
~~~
        struct progress {
          char *private;
          size_t size;
        };

        static size_t progress_callback(void *clientp,
                                        curl_off_t dltotal,
                                        curl_off_t dlnow,
                                        curl_off_t ultotal,
                                        curl_off_t ulnow)
        {
          struct memory *progress = (struct progress *)userp;

          /* use the values */

          return 0; /* all is good */
        }

        struct progress data;

        /* pass struct to callback  */
        curl_easy_setopt(curl_handle, CURLOPT_XFERINFODATA, &data);

        curl_easy_setopt(curl_handle, CURLOPT_XFERINFOFUNCTION, progress_callback);

~~~
AVAILABILITY
~~~
       Added in 7.32.0. This callback replaces CURLOPT_PROGRESSFUNCTION(3)

~~~
RETURN VALUE
~~~
       Returns CURLE_OK.

~~~
SEE ALSO
       CURLOPT_XFERINFODATA(3), CURLOPT_NOPROGRESS(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                     CURLOPT_XFERINFOFUNCTION(3)
CURLOPT_XOAUTH2_BEARER(3)                                                                 curl_easy_setopt options                                                                CURLOPT_XOAUTH2_BEARER(3)



NAME
~~~
       CURLOPT_XOAUTH2_BEARER - OAuth 2.0 access token

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_XOAUTH2_BEARER, char *token);

~~~
DESCRIPTION
~~~
       Pass  a  char * as parameter, which should point to the null-terminated OAuth 2.0 Bearer Access Token for use with HTTP, IMAP, POP3 and SMTP servers that support the OAuth 2.0 Authorization Framework.

       Note: For IMAP, POP3 and SMTP, the user name used to generate the Bearer Token should be supplied via the CURLOPT_USERNAME(3) option.

       The application does not have to keep the string around after setting this option.

DEFAULT
       NULL

~~~
PROTOCOLS
~~~
       IMAP, POP3 and SMTP

~~~
EXAMPLE
~~~
       CURL *curl = curl_easy_init();
       if(curl) {
         curl_easy_setopt(curl, CURLOPT_URL, "pop3://example.com/");
         curl_easy_setopt(curl, CURLOPT_XOAUTH2_BEARER, "1ab9cb22ba269a7");
         ret = curl_easy_perform(curl);
         curl_easy_cleanup(curl);
       }

~~~
AVAILABILITY
~~~
       Added in 7.33.0

~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, CURLE_UNKNOWN_OPTION if not, or CURLE_OUT_OF_MEMORY if there was insufficient heap space.

~~~
SEE ALSO
       CURLOPT_MAIL_AUTH(3), CURLOPT_USERNAME(3),



libcurl 7.37.0                                                                                  19 Jun 2014                                                                       CURLOPT_XOAUTH2_BEARER(3)
CURLOPT_TEMPLATE(3)                                                                       curl_easy_setopt options                                                                      CURLOPT_TEMPLATE(3)



NAME
~~~
       CURLOPT_TEMPLATE - [short desc]

~~~
NAME
~~~
       #include <curl/curl.h>

       CURLcode curl_easy_setopt(CURL *handle, CURLOPT_TEMPLATE, [argument]);

~~~
DESCRIPTION
~~~
DEFAULT
~~~
PROTOCOLS
~~~
~~~
EXAMPLE
~~~
~~~
AVAILABILITY
~~~
~~~
RETURN VALUE
~~~
       Returns CURLE_OK if the option is supported, and CURLE_UNKNOWN_OPTION if not.

~~~
SEE ALSO
       CURLOPT_STDERR(3), CURLOPT_DEBUGFUNCTION(3),



libcurl 7.37.0                                                                                  17 Jun 2014                                                                             CURLOPT_TEMPLATE(3)