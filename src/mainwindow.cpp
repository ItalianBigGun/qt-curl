#include "include/mainwindow.h"
#include "ui_mainwindow.h"
#include "include/qeasycurl.h"
#include <curl/curl.h>
#include <QAxWidget>
#include <QDebug>
static MainWindow *m_instance = nullptr;
static
int my_trace(CURL *handle, curl_infotype type,
             char *data, size_t size,
             void *userp)
{
    void **p = NULL;
    p = (void **)userp;
    *p = malloc(size*sizeof(char));
    static int trace_count = 0;

  const char *text;
  (void)handle; /* prevent compiler warning */
  (void)userp;

  switch (type) {
  case CURLINFO_TEXT:
    text = "=> INFO text";
  default: /* in case a new one is introduced to shock us */
    break;

  case CURLINFO_HEADER_OUT:
    text = "=> Send header";
    break;
  case CURLINFO_DATA_OUT:
    text = "=> Send data";
    break;
  case CURLINFO_SSL_DATA_OUT:
    text = "=> Send SSL data";
    break;
  case CURLINFO_HEADER_IN:
    text = "<= Recv header";
    break;
  case CURLINFO_DATA_IN:
    text = "<= Recv data";
    break;
  case CURLINFO_SSL_DATA_IN:
    text = "<= Recv SSL data";
    break;
  }
  qDebug() << "/*====================================";
  qDebug() << "trace count:" << trace_count ++;
  qDebug()<<"trace data type:" << text;
  qDebug() << "trace data size:" << size;
  if (data)
    qDebug() << "info text:" << data;
  qDebug() << "====================================*/";
  if (m_instance && (type==CURLINFO_DATA_IN))
      m_instance->resultText.append(QString(data).toUtf8());
  return 0;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    method_str = "GET";//ui->methdComBox->currentText();
    trace_data = NULL;
    m_instance = this;
}

MainWindow::~MainWindow()
{
    delete ui;
}

//  Ues the getinfo example here.
void MainWindow::on_requestBtn_clicked()
{
//    CURL *curl;
//    CURLcode res;
      resultText.clear();
//    curl = curl_easy_init();
//    if(curl) {
//        // add a line code to set the request method.
//        curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, my_trace);
//        curl_easy_setopt(curl, CURLOPT_DEBUGDATA, (void*)&trace_data);
//        /* the DEBUGFUNCTION has no effect until we enable VERBOSE */
//        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
//        /* example.com is redirected, so we tell libcurl to follow redirection */
//        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
//        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, method_str.toStdString().c_str());
//        curl_easy_setopt(curl, CURLOPT_URL, ui->urlEdit->text().toStdString().c_str());
//        res = curl_easy_perform(curl);

//        if(CURLE_OK == res) {
//            char *ct;
//            /* ask for the content-type */
//            res = curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &ct);
//            if((CURLE_OK == res) && ct)
//                ;//ui->resultTextBrowser->setText(QString(ct));
//        }

//        /* always cleanup */
//        curl_easy_cleanup(curl);

        QEasyCurl m_curl;
        m_curl.setDebugData((void**)&trace_data);
        m_curl.setDebugCallBackFunc(my_trace);
        m_curl.setMethod(method_str);
        m_curl.setUrl(ui->urlEdit->text());
        m_curl.sendRequest();

        QAxWidget *flash = new QAxWidget(0, ui->resultShowTab);      //QAxWidget使用的是ActiveX插件
        flash->setControl(QString::fromUtf8("{8856F961-340A-11D0-A96B-00C04FD705A2}"));//注册组件ID
        flash->setProperty("DisplayAlerts", true);//不显示警告信息
        flash->setProperty("DisplayScrollBars", true);//不显示滚动条
        flash->dynamicCall("Navigate(const QString&)", ui->urlEdit->text());
        flash->resize(ui->resultShowTab->size());
        flash->showFullScreen();
        ui->resultTextBrowser->setPlainText(resultText.toUtf8());

        //  release the memory of the buffer.
        if (trace_data)
            free(trace_data);
        trace_data = 0;
}

void MainWindow::on_methdComBox_currentIndexChanged(const QString &arg1)
{
    method_str = arg1;
    if (arg1 == "GET")
        cur_method = 1;
    else {
        cur_method = 2;
    }
}
