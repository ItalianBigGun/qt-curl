#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    Ui::MainWindow *ui; //  for test
    QString resultText;
private slots:
    void on_requestBtn_clicked();

    void on_methdComBox_currentIndexChanged(const QString &arg1);

private:

    int cur_method;
    QString method_str;
    char *trace_data;
    int trace_data_size;
};

#endif // MAINWINDOW_H
