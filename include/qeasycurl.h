#ifndef QEASYCURL_H
#define QEASYCURL_H

#include <QObject>
#include <curl/curl.h>
typedef size_t (*curl_wirtecbkfunc_t)(char *data, size_t n, size_t l, void *userp);
typedef int (*curl_debugcbkfunc_t)(CURL *handle, curl_infotype type,
             char *data, size_t size,
             void *userp);
class QEasyCurl : public QObject
{
public:
    QEasyCurl();
    virtual ~QEasyCurl();

    /*
    *   @desc   
    *       Set the write data pointer which will used in the call back function.
    *   @para   data the write data pointer
    *   @ret    NULL
    *   @warn   
    *       You'd better to impelement that how to use the data pointer in
    *       the call back function. After call back function compeleted, and 
    *       you release it.
    *   @note   
    *       See the setWriteCallBackFunc.
    */
    void setWriteData(void **pdata);

    /*
    *   @desc   
    *       Set the debug data pointer which will used in the call back function.
    *   @para   data the debug data pointer
    *   @ret    NULL
    *   @warn   
    *       You'd better to impelement that how to use the data pointer in
    *       the call back function. After call back function compeleted, and 
    *       you release it.
    *   @note   
    *       See the setDebugCallBackFunc.
    */
    void setDebugData(void **pdata);
    
    /*
    *   @desc   
    *       Set the write call back function of the result pre-process.
    *   @para   func the write call back function pointer
    *   @ret    NULL
    *   @warn   
    *       You'd better to call this function before calling the
    *       sendRequest function.
    *   @note   
    *       See the sendRequest;
    */
    void setWriteCallBackFunc(curl_wirtecbkfunc_t func);
    /*
    *   @desc   
    *       Set the debug call back function of the result pre-process.
    *   @para   func the debug call back function pointer
    *   @ret    NULL
    *   @warn   
    *       You'd better to call this function before calling the
    *       sendRequest function.
    *   @note   
    *       See the sendRequest;
    */
    void setDebugCallBackFunc(curl_debugcbkfunc_t func);
   
    /*
    *   @desc   
    *       Set the current user data only one.
    *   @para   para the pointer of user data
    *   @ret    NULL
    *   @warn   
    *       If the request method is POST, you can call this 
    *       function some time and reset the parameter.
    *   @note   
    *       NULL.
    */
    void setParameter(void *para);

    /*
    *   @desc   
    *       Set the method of the http request.
    *   @para   method the string is "POST" or "GET"
    *   @ret    NULL
    *   @warn   
    *       NULL.
    *   @note   
    *       NULL.
    */
    void setMethod(const QString& method);

    /*
    *   @desc   
    *       Process the request.
    *   @para   NULL
    *   @ret    
    *       0 succeed, 1 failed
    *   @warn   
    *       We must call setParameter, setCallBackFunc and setUrl
    *       before calling this function.
    *   @note   
    *       See setParameter, setCallBackFunc and setUrl.
    */
    int sendRequest();

    /*
    *   @desc   
    *       Set the url of the request.
    *   @para   url the url string
    *   @ret    NULL
    *   @warn   
    *       NULL.
    *   @note   
    *       NULL.
    */
    void setUrl(const QString& url);

    /*
    *   @desc   
    *       NULL
    *   @para   slist the slist string.
    *   @ret    NULL
    *   @warn   
    *       NULL.
    *   @note   
    *       NULL.
    */
    void addsList(const QString& slist);
signals:
   //void sendWriteData(void *data);
public slots:
   ;
    //void sendWriteData(void *data);
private:
    void **m_pWrite_data; // for callback function write data.
    void **m_pDebug_data;
    void *m_pUser_para;  //  trainfer to request.
    QString m_user_para;
    curl_wirtecbkfunc_t m_write_callback_func;
    curl_debugcbkfunc_t m_debug_callback_func;
    struct curl_slist m_slist;
    QString m_url;
    CURL *m_pCurl;
    QString m_method;
};

#endif // QEASYCURL_H
